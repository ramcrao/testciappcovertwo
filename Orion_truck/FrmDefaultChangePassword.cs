﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Orion_truck.Model;
using Orion_truck.Entity;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Resources;

namespace Orion_truck
{
    public partial class FrmDefaultChangePassword : Form
    {
        public User_Model _usermodel;
        User_Entity _userentity = new User_Entity();
        MessageValidation_Model _alertmodel = new MessageValidation_Model();
        MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        public string[] SGID;
        //public bool flag;
        public FrmDefaultChangePassword()
        {
            InitializeComponent();
            _usermodel = new User_Model();
        }

        private void butlogin_Click(object sender, EventArgs e)
        {
            try
            {
                ChangePassword();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public void ChangePassword()
        {
            try
            {
                DataTable dtMINL = _usermodel.getMasterData(Globals.GlobalSite, "PMINL");
                DataTable dtMAXL = _usermodel.getMasterData(Globals.GlobalSite, "PMAXL");
                Regex regPassword = new Regex(@"^((?=.*\d)(?=.*[a-z])((?=.*\W)|(?=.*_)))", RegexOptions.IgnorePatternWhitespace);
                if (txtoldpassword.Text == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A538", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Enter Old Password", "Information Message");
                    txtoldpassword.Focus();
                    return;
                }
                if (txtnewpassword.Text == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A539", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Enter New Password", "Information Message");
                    txtnewpassword.Focus();
                    return;
                }
                else if (txtnewpassword.Text.Length < Convert.ToInt32(dtMINL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A540", Globals.GlobalLanguage);
                    MessageBox.Show(string.Format(_alertentity.alertmessage, dtMINL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtnewpassword.Focus();
                    return;
                }
                else if (txtnewpassword.Text.Length > Convert.ToInt32(dtMAXL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A550", Globals.GlobalLanguage);
                    MessageBox.Show(string.Format(_alertentity.alertmessage, dtMAXL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtnewpassword.Focus();
                    return;
                }
                else if (regPassword.IsMatch(txtnewpassword.Text) == false)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A541", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtnewpassword.Focus();
                    return;
                }
                if (txtconfirmpassword.Text == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A542", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtconfirmpassword.Focus();
                    return;
                }
                else if (txtconfirmpassword.Text.Length < Convert.ToInt32(dtMINL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A543", Globals.GlobalLanguage);
                    MessageBox.Show(string.Format(_alertentity.alertmessage, dtMINL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtconfirmpassword.Focus();
                    return;
                }
                else if (txtconfirmpassword.Text.Length > Convert.ToInt32(dtMAXL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A551", Globals.GlobalLanguage);
                    MessageBox.Show(string.Format(_alertentity.alertmessage, dtMAXL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtconfirmpassword.Focus();
                    return;
                }
                else if (regPassword.IsMatch(txtconfirmpassword.Text) == false)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A544", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtconfirmpassword.Focus();
                    return;
                }

                //SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');
                _userentity = _usermodel.getValidateLoginUser(Globals.GlobalUsername, Encryptdata(txtoldpassword.Text.ToString().Trim()));

                if (_userentity.username != "" && _userentity.username != null)
                {
                    if (txtnewpassword.Text == txtconfirmpassword.Text)
                    {
                        DataTable dt2 = _usermodel.getPasswordHistory(Globals.GlobalUsername, Encryptdata(txtnewpassword.Text.ToString().Trim()));
                        if (dt2.Rows.Count > 0)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A545", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtnewpassword.Focus();
                            return;
                        }
                        _userentity.flag = _usermodel.getUpdateNewPwd(Globals.GlobalUsername, Encryptdata(txtnewpassword.Text.ToString().Trim()), Globals.GlobalUsername);

                        if (_userentity.flag == true)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A546", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtoldpassword.Text = "";
                            txtnewpassword.Text = "";
                            txtconfirmpassword.Text = "";
                            this.Hide();
                        }
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A547", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtnewpassword.Focus();
                        return;
                    }
                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A548", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtoldpassword.Focus();
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                //ChangeLanguage("en-US");
                DataTable dtMINL = _usermodel.getMasterData(Globals.GlobalSite, "PMINL");
                DataTable dtMAXL = _usermodel.getMasterData(Globals.GlobalSite, "PMAXL");
                label4.Text = string.Format("1.Length minimum      {0} and maximum       {1} - characters.", dtMINL.Rows[0]["NEWVALUE"].ToString(), dtMAXL.Rows[0]["NEWVALUE"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ChangeLanguage(string lang)
        {
            CultureInfo cul = new CultureInfo(lang);
            ResourceManager rm = new ResourceManager("SGGI_Connect.App_Global.frmDefaultChangePassword", this.GetType().Assembly);
            this.Text = rm.GetString("label1", cul);
            foreach (System.Windows.Forms.Control c in this.Controls)
            {
                c.Text = rm.GetString(c.Name, cul);
                foreach (System.Windows.Forms.Control c1 in c.Controls)
                {
                    c1.Text = rm.GetString(c1.Name, cul);
                    foreach (System.Windows.Forms.Control c2 in c1.Controls)
                    {
                        c2.Text = rm.GetString(c2.Name, cul);
                    }
                }
            }



        }

        private void txtoldpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangePassword();
            }
        }

        private void txtnewpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangePassword();
            }
        }

        private void txtconfirmpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangePassword();
            }
        }

        private void butcancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void txtnewpassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == ' ')
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A549", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Spaces are not allowed in password.", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtconfirmpassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == ' ')
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A549", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Spaces are not allowed in password.", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtoldpassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == ' ')
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A549", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Spaces are not allowed in password.", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}

