﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Orion_truck.Model;
using Orion_truck.Entity;

namespace Orion_truck
{
    public partial class FrmGetCountryCode : Form
    {
        DataTable dt_code;
        public  string countrycode;
        public int imgcode;
        MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        MessageValidation_Model _alertmodel = new MessageValidation_Model();
        public FrmGetCountryCode(DataTable dt)
        {
            InitializeComponent();
            dt_code = dt;
        }

        private void FrmGetCountryCode_Load(object sender, EventArgs e)
        {
            if (dt_code.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_code.Rows)
                {
                    grid_countrycode.Rows.Add(imageList_country.Images[Convert.ToInt32(dr["SLNO"].ToString())], dr["COUNTRY"].ToString(),dr["CODE"].ToString(), dr["SLNO"].ToString());
                }
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            countrycode = string.Empty;
            
            if (grid_countrycode.Rows.Count > 0)
            {
                if(grid_countrycode.SelectedCells.Count != 0)
                {
                    int rowIndex = (int)grid_countrycode.CurrentCell.RowIndex;
                    if(rowIndex != -1)
                    {
                        countrycode = grid_countrycode.Rows[rowIndex].Cells[2].Value.ToString();
                        imgcode = Convert.ToInt32(grid_countrycode.Rows[rowIndex].Cells[3].Value.ToString());
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
            }
            else
            {
                _alertentity = _alertmodel.Alert_Information("LG", "A2300", Globals.GlobalLanguage);
                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
           
           

        }

        private void txt_countryfilter_TextChanged(object sender, EventArgs e)
        {
            DataView dv = new DataView(dt_code);
            string filter = string.Empty;

            if(!string.IsNullOrEmpty(txt_countryfilter.Text.Trim()))
            {
                filter = filter + " CODE LIKE '%" + txt_countryfilter.Text + "%'" + " OR COUNTRY LIKE '%" + txt_countryfilter.Text + "%'";

                dv.RowFilter = filter;
                dv.Sort = "";
                dv.RowStateFilter = DataViewRowState.Unchanged;
                grid_countrycode.Rows.Clear();

                if (dv.ToTable().Rows.Count > 0)
                {
                    foreach (DataRow dr in dv.ToTable().Rows)
                    {
                        grid_countrycode.Rows.Add(imageList_country.Images[Convert.ToInt32(dr["SLNO"].ToString())], dr["COUNTRY"].ToString(), dr["CODE"].ToString(), dr["SLNO"].ToString());
                    }
                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2300", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

        }
    }
}
