﻿namespace Orion_truck
{
    partial class MDIParent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblusername = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolsitedesc = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolversion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tooldate = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.imglogout = new System.Windows.Forms.PictureBox();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imglogout)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblusername,
            this.toolsitedesc,
            this.toolversion,
            this.tooldate});
            this.statusStrip.Location = new System.Drawing.Point(0, 676);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // lblusername
            // 
            this.lblusername.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(48, 17);
            this.lblusername.Text = "Status";
            // 
            // toolsitedesc
            // 
            this.toolsitedesc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.toolsitedesc.Margin = new System.Windows.Forms.Padding(90, 3, 0, 2);
            this.toolsitedesc.Name = "toolsitedesc";
            this.toolsitedesc.Size = new System.Drawing.Size(147, 17);
            this.toolsitedesc.Text = "toolStripStatusLabel1";
            // 
            // toolversion
            // 
            this.toolversion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.toolversion.Margin = new System.Windows.Forms.Padding(90, 3, 0, 2);
            this.toolversion.Name = "toolversion";
            this.toolversion.Size = new System.Drawing.Size(147, 17);
            this.toolversion.Text = "toolStripStatusLabel1";
            // 
            // tooldate
            // 
            this.tooldate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.tooldate.Margin = new System.Windows.Forms.Padding(90, 3, 0, 2);
            this.tooldate.Name = "tooldate";
            this.tooldate.Size = new System.Drawing.Size(147, 17);
            this.tooldate.Text = "toolStripStatusLabel1";
            // 
            // imglogout
            // 
            this.imglogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imglogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imglogout.Image = global::Orion_truck.Properties.Resources.logout1;
            this.imglogout.Location = new System.Drawing.Point(975, 3);
            this.imglogout.Name = "imglogout";
            this.imglogout.Size = new System.Drawing.Size(28, 19);
            this.imglogout.TabIndex = 20;
            this.imglogout.TabStop = false;
            this.toolTip.SetToolTip(this.imglogout, "Logout");
            this.imglogout.Click += new System.EventHandler(this.imglogout_Click);
            // 
            // MDIParent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 698);
            this.Controls.Add(this.imglogout);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIParent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Orion - Truck Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDIParent_FormClosing);
            this.Load += new System.EventHandler(this.MDIParent_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imglogout)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.PictureBox imglogout;
        private System.Windows.Forms.ToolStripStatusLabel lblusername;
        private System.Windows.Forms.ToolStripStatusLabel toolsitedesc;
        private System.Windows.Forms.ToolStripStatusLabel tooldate;
        private System.Windows.Forms.ToolStripStatusLabel toolversion;
    }
}



