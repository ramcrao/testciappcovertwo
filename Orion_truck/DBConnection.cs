﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Orion_truck.Entity;
using System.Windows.Forms;
using System.IO;

namespace Orion_truck
{
    public class DBConnection
    {
        private SqlDataAdapter myAdapter;
        private SqlConnection conn;
        User_Entity _userentity = new User_Entity();
        public string connstr;
        //private int status;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public DBConnection()
        {
            myAdapter = new SqlDataAdapter();
            if (Globals.GlobalConnection == "")
            {
                conn = new SqlConnection(Decryptdata(ConfigurationManager.ConnectionStrings["SGGIDBConnection"].ConnectionString));

                #region clickonece source
                //string text = string.Empty;
                //string path1 = AssemblyDirectory + "\\Orion\\" + Globals.GlobalServer + ".txt";
                ////string path1 = AssemblyDirectory + "\\Orion\\" + "Orion France" + ".txt";
                //if (File.Exists(path1))
                //{
                //    using (StreamReader sr = new StreamReader(path1))
                //    {
                //        do
                //        {
                //            text = sr.ReadToEnd();
                //        } while (sr.Peek() != -1);
                //    }
                //}

                //conn = new SqlConnection(text);
                #endregion

            }
            else
            {
                //connection
                conn = new SqlConnection(Globals.GlobalConnection);
                //SqlParameter[] sqlParameters = new SqlParameter[0];
                //dsstopDB = executeSelectQueryWithSP("SP_PARAM_STOPDB", sqlParameters);
                //if (dsstopDB.Tables.Count > 0)
                //{
                //    if (dsstopDB.Tables[0].Rows[0].ItemArray[0].ToString()=="11")
                //    {
                //        MessageBox.Show("This database will be shutdown later. Please logout your Orion application.");
                //        Application.ExitThread();
                //    }
                //}
            }
            //string conname = ConfigurationSettings.AppSettings["SGGIDBConnection"].ToString();
            //conn = new SqlConnection(conname);

        }

        static public string AssemblyDirectory
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            }
        }

        private string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        public SqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.Open();

                if (Globals.Globallog == "0" || Globals.Globallog == "2")
                {
                    SqlCommand myCommand = new SqlCommand();
                    DataSet ds = new DataSet();
                    myCommand.Connection = conn;
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "SP_GETUSERIDLESTA";
                    myCommand.Parameters.AddWithValue("@USERID", Globals.GlobalUsername);
                    SqlDataAdapter myAdapter1 = new SqlDataAdapter();
                    myAdapter1.SelectCommand = myCommand;
                    myAdapter1.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[3].Rows.Count == 0)
                        {
                            MessageBox.Show(ds.Tables[5].Rows[0]["MSG"].ToString());
                            Globals.Setlog("1");
                            Application.Exit();
                        }
                        if (Globals.Globallog == "2")
                        {
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                int param = 0;
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    param = Convert.ToInt32(ds.Tables[2].Rows[0][0].ToString());
                                }
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    int y = Convert.ToInt32(ds.Tables[0].Rows[0]["D_DIFF"].ToString());
                                    if (y < param)
                                    {
                                        SqlCommand myCommand1 = new SqlCommand();
                                        myCommand1.Connection = conn;
                                        myCommand1.CommandType = CommandType.StoredProcedure;
                                        myCommand1.CommandText = "SP_LOGINMODUPT";
                                        myCommand1.Parameters.AddWithValue("@USERID", Globals.GlobalUsername);
                                        myAdapter.InsertCommand = myCommand1;
                                        _userentity.status = myCommand1.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        MessageBox.Show(ds.Tables[4].Rows[0]["MSG"].ToString());
                                        Globals.Setlog("1");
                                        Application.Exit();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return conn;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQueryWithSPNew(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            myCommand.CommandTimeout = 0;
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        public DataSet executeSelectQueryNew(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;
        }

        public DataSet executeSelectQueryWithSP(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            try
            {
                myCommand.CommandTimeout = 0;
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;
        }
        /// <summary>
        /// For report . Returning adapter 
        /// </summary>
        /// <param name="_query"></param>
        /// <param name="sqlParameter"></param>
        /// <returns></returns>
        public SqlDataAdapter executeSelectQueryWithSPreturnsSqlDataAdapter(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                return myAdapter;
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            //return ds;
        }
        /// <method>
        /// Method to Return Data Table -- Added by Shankar.M January 05,2012 04:13:00 PM
        /// </method>
        public DataTable executeSelectQuerySPDataTable(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                myCommand.CommandTimeout = 0;
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myAdapter.InsertCommand = myCommand;
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt;
        }

        /// <method>
        /// Method to Return Data Reader -- Added by Shankar.M June 05,2013 10:09:00 AM
        /// </method>
        public SqlDataReader executeSelectQueryDtReader(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            SqlDataReader reader = null;
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                //myCommand.Connection.Close();
            }
            return reader;
        }

        /// <method>
        /// Method to Return Data Table -- Added by Shankar.M January 05,2012 04:13:00 PM
        /// </method>
        public DataTable executeSelectQueryOnlySPDataTable(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");

                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt;
        }


        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                //myCommand.ExecuteNonQuery();
                _userentity.status = myCommand.ExecuteNonQuery();
                if (_userentity.status > 0)
                {
                    _userentity.flag = true;
                }
                else
                {
                    _userentity.flag = false;
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");

                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return _userentity.flag;
        }

        /// <method>
        /// Insert Query WITH STORED PROCEDURE
        /// </method>
        public bool executeInsertQueryWithSP(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                _userentity.status = myCommand.ExecuteNonQuery();
                if (_userentity.status > 0)
                {
                    _userentity.flag = true;
                }
                else
                {
                    _userentity.flag = false;
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _storedprocedure + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _storedprocedure + " \nException: \n" + e.StackTrace.ToString());
                //return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return _userentity.flag;

        }

        /// <method>
        /// Insert Query WITH STORED PROCEDURE RETURNING DATASET
        /// </method>
        public DataSet executeInsertQueryDSWithSP(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //if (_userentity.status > 0)
                //{
                //    _userentity.flag = true;
                //}
                //else
                //{
                //    _userentity.flag = false;
                //}
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _storedprocedure + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");

                //MessageBox.Show(e.Message);
                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _storedprocedure + " \nException: \n" + e.StackTrace.ToString());
                //return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;

        }


        public DataTable executeSelectQueryBayandRack(String _query)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                //  myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeUpdateQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return true;
        }
        public bool executeUpdateQueryTest(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return true;
        }
        public string Backlog_fich_generation(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            string retn = string.Empty;
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                myCommand.ExecuteNonQuery();

                retn = Convert.ToString(myCommand.Parameters["@Return"].Value);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());

            }
            finally
            {
                myCommand.Connection.Close();
            }
            return retn;
        }

        #region SP without Param Code
        public DataTable DBDataTableExecuteParamSP(string strSPName, params object[] parameters)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016 
            //PARAM WISE EXECUTE PROCEDURE PROCESS
            DataTable dt_returnvalue = new DataTable();
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand = new SqlCommand(strSPName, conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(myCommand);
                for (int i = 0; i < parameters.Length; i++)
                {
                    myCommand.Parameters[i + 1].Value = parameters[i];
                }
                SqlDataAdapter da = new SqlDataAdapter(myCommand);
                da.Fill(dt_returnvalue);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strSPName + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt_returnvalue;
        }

        public bool DBInsertExecuteParamSP(string strSPName, params object[] parameters)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016
            //INSERT AND UPDATE PROCESS
            bool result = false;
            SqlCommand myCommand = new SqlCommand();
            myCommand.Connection = openConnection();
            SqlTransaction transaction = conn.BeginTransaction();
            try
            {

                myCommand = new SqlCommand(strSPName, conn, transaction);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(myCommand);
                for (int i = 0; i < parameters.Length; i++)
                {
                    myCommand.Parameters[i + 1].Value = parameters[i];
                }
                myCommand.ExecuteNonQuery();
                transaction.Commit();
                result = true;
            }
            catch (SqlException e)
            {
                transaction.Rollback();
                result = false;
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strSPName + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }

            return result;
        }

        public DataSet DBDataSetExecuteParamSP(string strSPName, params object[] parameters)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016
            //DATASET USE GET MULTI TABLES IN WITH PARAMS
            SqlCommand myCommand = new SqlCommand();
            DataSet dt_returnvalue = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand = new SqlCommand(strSPName, conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(myCommand);
                for (int i = 0; i < parameters.Length; i++)
                {
                    myCommand.Parameters[i + 1].Value = parameters[i];
                }

                SqlDataAdapter da = new SqlDataAdapter(myCommand);
                da.Fill(dt_returnvalue);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strSPName + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt_returnvalue;
        }

        public DataSet DBDataSetExecuteSP(string strSPName)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016
            //DATASET USE GET MULTI TABLES IN WITHOUT PARAMS
            SqlCommand myCommand = new SqlCommand();
            DataSet dt_sp = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand = new SqlCommand(strSPName, conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter dacmd = new SqlDataAdapter(myCommand);
                dacmd.Fill(dt_sp);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strSPName + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt_sp;
        }

        public DataTable DBExecuteSqlQuery(string strquery)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016
            //DIRECTLY EXECUTE QUERY GIVEN BY USER
            SqlCommand myCommand = new SqlCommand();
            DataTable dt_SqlQuery = new DataTable();
            try
            {
                myCommand.Connection = openConnection();
                SqlDataAdapter dacmd = new SqlDataAdapter(strquery, conn);
                dacmd.Fill(dt_SqlQuery);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strquery + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt_SqlQuery;
        }

        public DataTable DBDataTableExecuteSP(string strSPName)
        {
            //CREATED BY  :  S SATHISH KUMAR (S1289523), CREATE DATE :  25/04/2016
            //EXECUTE DATATABLE STORED PROCEDURE 
            SqlCommand myCommand = new SqlCommand();
            DataTable dt_sp = new DataTable();
            try
            {
                myCommand.Connection = openConnection();
                myCommand = new SqlCommand(strSPName, conn);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter dacmd = new SqlDataAdapter(myCommand);
                dacmd.Fill(dt_sp);
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message + "\r\n" + "Procedure : " + strSPName + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt_sp;
        }
        #endregion
    }

}

