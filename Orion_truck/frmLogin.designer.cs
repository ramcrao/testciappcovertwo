﻿namespace Orion_truck
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblauthentication = new System.Windows.Forms.Label();
            this.butcancel = new System.Windows.Forms.Button();
            this.butlogin = new System.Windows.Forms.Button();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.cmbcategory = new System.Windows.Forms.ComboBox();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.lblcategory = new System.Windows.Forms.Label();
            this.lblpassword = new System.Windows.Forms.Label();
            this.lbllanguage = new System.Windows.Forms.Label();
            this.lblusername = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbbadge = new System.Windows.Forms.RadioButton();
            this.rdbsgid = new System.Windows.Forms.RadioButton();
            this.rdblgauthentication = new System.Windows.Forms.RadioButton();
            this.rdbbiometric = new System.Windows.Forms.RadioButton();
            this.cmblanguage = new TyroDeveloper.ColorPicker();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.colorPicker1 = new TyroDeveloper.ColorPicker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.colorPicker1);
            this.groupBox1.Controls.Add(this.lblauthentication);
            this.groupBox1.Controls.Add(this.butcancel);
            this.groupBox1.Controls.Add(this.butlogin);
            this.groupBox1.Controls.Add(this.txtpassword);
            this.groupBox1.Controls.Add(this.cmbcategory);
            this.groupBox1.Controls.Add(this.txtusername);
            this.groupBox1.Controls.Add(this.lblcategory);
            this.groupBox1.Controls.Add(this.lblpassword);
            this.groupBox1.Controls.Add(this.lbllanguage);
            this.groupBox1.Controls.Add(this.lblusername);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(227, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 242);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // lblauthentication
            // 
            this.lblauthentication.AutoSize = true;
            this.lblauthentication.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblauthentication.Location = new System.Drawing.Point(1, 68);
            this.lblauthentication.Name = "lblauthentication";
            this.lblauthentication.Size = new System.Drawing.Size(88, 13);
            this.lblauthentication.TabIndex = 36;
            this.lblauthentication.Text = "Authentication";
            // 
            // butcancel
            // 
            this.butcancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.butcancel.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.butcancel.Location = new System.Drawing.Point(102, 199);
            this.butcancel.Name = "butcancel";
            this.butcancel.Size = new System.Drawing.Size(100, 34);
            this.butcancel.TabIndex = 9;
            this.butcancel.Text = "&Cancel";
            this.butcancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butcancel.UseVisualStyleBackColor = false;
            this.butcancel.Click += new System.EventHandler(this.butcancel_Click);
            // 
            // butlogin
            // 
            this.butlogin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.butlogin.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.butlogin.Location = new System.Drawing.Point(203, 199);
            this.butlogin.Name = "butlogin";
            this.butlogin.Size = new System.Drawing.Size(100, 34);
            this.butlogin.TabIndex = 8;
            this.butlogin.Text = "&Login";
            this.butlogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.butlogin.UseVisualStyleBackColor = false;
            this.butlogin.Click += new System.EventHandler(this.butlogin_Click);
            // 
            // txtpassword
            // 
            this.txtpassword.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.txtpassword.Location = new System.Drawing.Point(102, 172);
            this.txtpassword.MaxLength = 25;
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(200, 21);
            this.txtpassword.TabIndex = 7;
            this.txtpassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyDown);
            this.txtpassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpassword_KeyPress);
            this.txtpassword.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyUp);
            // 
            // cmbcategory
            // 
            this.cmbcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcategory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbcategory.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.cmbcategory.FormattingEnabled = true;
            this.cmbcategory.Location = new System.Drawing.Point(102, 110);
            this.cmbcategory.Name = "cmbcategory";
            this.cmbcategory.Size = new System.Drawing.Size(200, 21);
            this.cmbcategory.TabIndex = 5;
            this.cmbcategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbcategory_KeyDown);
            // 
            // txtusername
            // 
            this.txtusername.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.txtusername.Location = new System.Drawing.Point(102, 143);
            this.txtusername.MaxLength = 15;
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(200, 21);
            this.txtusername.TabIndex = 6;
            this.txtusername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtusername_KeyDown);
            this.txtusername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtusername_KeyPress_1);
            this.txtusername.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtusername_KeyUp);
            // 
            // lblcategory
            // 
            this.lblcategory.AutoSize = true;
            this.lblcategory.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblcategory.Location = new System.Drawing.Point(1, 114);
            this.lblcategory.Name = "lblcategory";
            this.lblcategory.Size = new System.Drawing.Size(45, 13);
            this.lblcategory.TabIndex = 30;
            this.lblcategory.Text = "Facility";
            // 
            // lblpassword
            // 
            this.lblpassword.AutoSize = true;
            this.lblpassword.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblpassword.Location = new System.Drawing.Point(1, 178);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(61, 13);
            this.lblpassword.TabIndex = 29;
            this.lblpassword.Text = "Password";
            // 
            // lbllanguage
            // 
            this.lbllanguage.AutoSize = true;
            this.lbllanguage.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lbllanguage.Location = new System.Drawing.Point(1, 19);
            this.lbllanguage.Name = "lbllanguage";
            this.lbllanguage.Size = new System.Drawing.Size(62, 13);
            this.lbllanguage.TabIndex = 31;
            this.lbllanguage.Text = "Language";
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblusername.Location = new System.Drawing.Point(1, 147);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(70, 13);
            this.lblusername.TabIndex = 28;
            this.lblusername.Text = "User Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbbadge);
            this.groupBox2.Controls.Add(this.rdbsgid);
            this.groupBox2.Controls.Add(this.rdblgauthentication);
            this.groupBox2.Controls.Add(this.rdbbiometric);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.groupBox2.Location = new System.Drawing.Point(102, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(197, 62);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            // 
            // rdbbadge
            // 
            this.rdbbadge.AutoSize = true;
            this.rdbbadge.Enabled = false;
            this.rdbbadge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdbbadge.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.rdbbadge.ForeColor = System.Drawing.Color.Gray;
            this.rdbbadge.Location = new System.Drawing.Point(117, 37);
            this.rdbbadge.Name = "rdbbadge";
            this.rdbbadge.Size = new System.Drawing.Size(67, 18);
            this.rdbbadge.TabIndex = 38;
            this.rdbbadge.Text = "Badge";
            this.rdbbadge.UseVisualStyleBackColor = true;
            // 
            // rdbsgid
            // 
            this.rdbsgid.AutoSize = true;
            this.rdbsgid.Checked = true;
            this.rdbsgid.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdbsgid.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.rdbsgid.Location = new System.Drawing.Point(16, 15);
            this.rdbsgid.Name = "rdbsgid";
            this.rdbsgid.Size = new System.Drawing.Size(81, 18);
            this.rdbsgid.TabIndex = 1;
            this.rdbsgid.TabStop = true;
            this.rdbsgid.Text = "Windows";
            this.rdbsgid.UseVisualStyleBackColor = true;
            this.rdbsgid.CheckedChanged += new System.EventHandler(this.rdbsgid_CheckedChanged);
            // 
            // rdblgauthentication
            // 
            this.rdblgauthentication.AutoSize = true;
            this.rdblgauthentication.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdblgauthentication.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.rdblgauthentication.Location = new System.Drawing.Point(117, 16);
            this.rdblgauthentication.Name = "rdblgauthentication";
            this.rdblgauthentication.Size = new System.Drawing.Size(62, 18);
            this.rdblgauthentication.TabIndex = 2;
            this.rdblgauthentication.Text = "Orion";
            this.rdblgauthentication.UseVisualStyleBackColor = true;
            this.rdblgauthentication.CheckedChanged += new System.EventHandler(this.rdblgauthentication_CheckedChanged);
            // 
            // rdbbiometric
            // 
            this.rdbbiometric.AutoSize = true;
            this.rdbbiometric.Enabled = false;
            this.rdbbiometric.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdbbiometric.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.rdbbiometric.ForeColor = System.Drawing.Color.Gray;
            this.rdbbiometric.Location = new System.Drawing.Point(16, 37);
            this.rdbbiometric.Name = "rdbbiometric";
            this.rdbbiometric.Size = new System.Drawing.Size(85, 18);
            this.rdbbiometric.TabIndex = 37;
            this.rdbbiometric.Text = "Biometric";
            this.rdbbiometric.UseVisualStyleBackColor = true;
            // 
            // cmblanguage
            // 
            this.cmblanguage.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmblanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmblanguage.FormattingEnabled = true;
            this.cmblanguage.Location = new System.Drawing.Point(102, 14);
            this.cmblanguage.Name = "cmblanguage";
            this.cmblanguage.SelectedItem = null;
            this.cmblanguage.SelectedValue = null;
            this.cmblanguage.Size = new System.Drawing.Size(200, 21);
            this.cmblanguage.TabIndex = 37;
            this.cmblanguage.SelectedIndexChanged += new System.EventHandler(this.cmblanguage_SelectedIndexChanged_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "UK_flag.png");
            this.imageList1.Images.SetKeyName(1, "France_flag.png");
            this.imageList1.Images.SetKeyName(2, "Korean_flag.png");
            this.imageList1.Images.SetKeyName(3, "Polish.png");
            this.imageList1.Images.SetKeyName(4, "Romanion.png");
            this.imageList1.Images.SetKeyName(5, "german.png");
            this.imageList1.Images.SetKeyName(6, "Spanish1.png");
            this.imageList1.Images.SetKeyName(7, "italy.png");
            this.imageList1.Images.SetKeyName(8, "Portugal_flag.png");
            this.imageList1.Images.SetKeyName(9, "");
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Location = new System.Drawing.Point(365, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(164, 59);
            this.pictureBox2.TabIndex = 36;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(0, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 307);
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // colorPicker1
            // 
            this.colorPicker1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorPicker1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorPicker1.FormattingEnabled = true;
            this.colorPicker1.Location = new System.Drawing.Point(99, 16);
            this.colorPicker1.Name = "colorPicker1";
            this.colorPicker1.SelectedItem = null;
            this.colorPicker1.SelectedValue = null;
            this.colorPicker1.Size = new System.Drawing.Size(200, 21);
            this.colorPicker1.TabIndex = 40;
            // 
            // frmLogin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(537, 306);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(553, 345);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(553, 345);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.Shown += new System.EventHandler(this.frmLogin_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdblgauthentication;
        private System.Windows.Forms.RadioButton rdbsgid;
        private System.Windows.Forms.Button butcancel;
        private System.Windows.Forms.Button butlogin;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.ComboBox cmbcategory;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label lblcategory;
        private System.Windows.Forms.Label lblpassword;
        private System.Windows.Forms.Label lbllanguage;
        private System.Windows.Forms.Label lblusername;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblauthentication;
        private System.Windows.Forms.RadioButton rdbbiometric;
        private System.Windows.Forms.RadioButton rdbbadge;
        private System.Windows.Forms.GroupBox groupBox2;
        private TyroDeveloper.ColorPicker cmblanguage;
        private System.Windows.Forms.ImageList imageList1;
        private TyroDeveloper.ColorPicker colorPicker1;
    }
}