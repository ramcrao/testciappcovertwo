﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orion_truck
{
    public partial class FrmTruckInterface_new : Form
    {
        public FrmTruckInterface_new()
        {
            InitializeComponent();
        }
        int formMaximized = 0;
        double rW = 0;
        double rH = 0;
        int fH = 0;
        int fW = 0;


        private void FrmTruckInterface_new_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            //this.Height = resolution.Size.Height;
            //this.Width = resolution.Size.Width;
            // imgload("MAX");
            imgload("MAX");
        }

        private void imgload(string type)
        {


            DataTable dt = new DataTable();
            dt.Columns.Add("RNO");
            dt.Columns.Add("TEXT");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("RNO");
            dt1.Columns.Add("TEXT");
            DataRow dr;

            //int row;
            //row = 0;
            //if (type == "NOR")
            //{
            //    row = 4;

            //}
            //else
            //{

            //    row = 4;
            //}



            //foreach (System.Windows.Forms.Control item in panel13.Controls)
            //{
            //    if (item.Name != "panel14" && item.Name != "label10")
            //    {
            //        panel13.Controls.Remove(item);

            //    }
            //}

            int ctr_cnt = panel13.Controls.Count;

            for (int y = ctr_cnt - 1; y >= 0; y--)
            {
                if (panel13.Controls[y].Name != "panel14" && panel13.Controls[y].Name != "label10")
                {
                    panel13.Controls.RemoveAt(y);

                }
            }




            string[] lnagname = new string[16];

            lnagname[0] = "Français";
            lnagname[1] = "Germany";
            lnagname[2] = "Italiano";
            lnagname[3] = "Polski";
            lnagname[4] = "Portugues";
            lnagname[5] = "Romana";
            lnagname[6] = "Espanol";
            lnagname[7] = "English";
            lnagname[8] = "Australia";
            lnagname[9] = "Canada";
            lnagname[10] = "Czech";
            lnagname[11] = "Ελλάδα";
            lnagname[12] = "Estonia";
            lnagname[13] = "대한민국";
            lnagname[14] = "Malaysia";
            lnagname[15] = "Россия";

            for (int a = 0; a < 13; a++)
            {
                dr = dt.NewRow();
                dr["RNO"] = a.ToString();
                dr["TEXT"] = lnagname[a].ToString();
                dt.Rows.Add(dr);

                if (a < 4)
                {
                    dr = dt1.NewRow();
                    dr["RNO"] = a.ToString();
                    dr["TEXT"] = lnagname[a].ToString();
                    dt1.Rows.Add(dr);

                }
            }

            int x = 0;
            int xRef1 = 50;
            int yRef1 = 0;
            int y2ref = 0;
            int cnt = 0;
            int _distanceval = 200;

            if (panel10.Controls.Count <= 2)
            {

                foreach (DataRow dr1 in dt1.Rows)
                {


                    PictureBox pb = new PictureBox();
                    int XPos = panel11.Location.X;
                    int YPos = panel11.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label9.Location.X;
                    int lyPos = label9.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    // lbl.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl.Font = label9.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    panel10.Controls.Add(pb);
                    panel10.Controls.Add(lbl);
                    xRef1 += _distanceval;

                }
            }

            xRef1 = 50;
            yRef1 = 0;
            y2ref = 0;
            cnt = 0;


            foreach (DataRow dr1 in dt.Rows)
            {

                if (cnt < 4)
                {
                    PictureBox pb = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    //  pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    //  lbl.Font = new Font("Open Sans", 10.0f, FontStyle.SemiBold);
                    lbl.Font = label10.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    panel13.Controls.Add(pb);
                    panel13.Controls.Add(lbl);
                    xRef1 += _distanceval;
                }
                else if (cnt < 8)
                {

                    PictureBox pb1 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb1.Name = dr1["TEXT"].ToString();
                    pb1.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb1.Image = imageList1.Images[x];
                    pb1.AutoSize = true;
                    pb1.Cursor = Cursors.Hand;
                    pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    //pb1.SizeMode = PictureBoxSizeMode.Normal;
                    pb1.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb1.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_1 = new Label();
                    lbl_1.Text = dr1["TEXT"].ToString();
                    lbl_1.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_1.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_1.Font = label10.Font;
                    lbl_1.Size = new Size(22, 4);
                    lbl_1.AutoSize = true;
                    lbl_1.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    panel13.Controls.Add(pb1);
                    panel13.Controls.Add(lbl_1);
                    xRef1 += _distanceval;
                }
                else if (cnt < 12)
                {

                    PictureBox pb2 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb2.Name = dr1["TEXT"].ToString();
                    pb2.Size = new  Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb2.Image = imageList1.Images[x];
                    pb2.AutoSize = true;
                    pb2.Cursor = Cursors.Hand;
                    pb2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    //pb2.SizeMode = PictureBoxSizeMode.Normal;
                    pb2.Location = new Point(XPos + xRef1, YPos + y2ref);
                    pb2.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_2 = new Label();
                    lbl_2.Text = dr1["TEXT"].ToString();
                    lbl_2.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_2.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_2.Font = label10.Font;
                    lbl_2.Size = new Size(22, 4);
                    lbl_2.AutoSize = true;
                    lbl_2.Location = new Point(lXPos + xRef1, lyPos + y2ref);
                    panel13.Controls.Add(pb2);
                    panel13.Controls.Add(lbl_2);
                    xRef1 += _distanceval;
                }
                else if (cnt < 16)
                {
                    PictureBox pb3 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb3.Name = dr1["TEXT"].ToString();
                    pb3.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb3.Image = imageList1.Images[x];
                    pb3.AutoSize = true;
                    pb3.Cursor = Cursors.Hand;
                    pb3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    pb3.SizeMode = PictureBoxSizeMode.Normal;
                    pb3.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb3.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_3 = new Label();
                    lbl_3.Text = dr1["TEXT"].ToString();
                    lbl_3.Name = "lb" + dr1["TEXT"].ToString();
                    // lbl_3.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_3.Font = label10.Font;
                    lbl_3.Size = new Size(22, 4);
                    lbl_3.AutoSize = true;
                    lbl_3.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    panel13.Controls.Add(pb3);
                    panel13.Controls.Add(lbl_3);
                    xRef1 += _distanceval;
                }




                cnt = cnt + 1;
                if (cnt == 4)
                {

                    yRef1 =120;
                    xRef1 = 50;
                }

                if (cnt == 8)
                {

                    y2ref =  240;
                    xRef1 = 50;
                }
                if (cnt == 12)
                {

                    yRef1 =360;
                    xRef1 =50;
                }




                //rW = panel10.Width;
                //rH = panel10.Height;

                //fW = panel10.Width;
                //fH = panel10.Height;
                //foreach (System.Windows.Forms.Control c in panel10.Controls)
                //{
                //    c.Tag = c.Name + "/" + c.Left + "/" + c.Top + "/" + c.Width + "/" + c.Height + "/" + (int)c.Font.Size;
                //   // dtSize.Rows.Add(c.Name, c.Left, c.Top, c.Width + 8, c.Height);
                //    // c.Anchor = (AnchorStyles.Right |  AnchorStyles.Left );  
                //}

                //  this.Resize += MyForm_Resize;
                //   this.WindowState = FormWindowState.Maximized;
                // Rectangle resolution = Screen.PrimaryScreen.Bounds;
                // this.Height = resolution.Size.Height;
                //  this.Width = resolution.Size.Width;
            }
        }

        private void FrmTruckInterface_new_Resize(object sender, EventArgs e)
        {
            imgload("MAX");

            //if (this.Height < 950)
            //{
            //    imgload("NOR");
            //}
            //else
            //{
            //    imgload("MAX");
            //}
        }
    }
}
