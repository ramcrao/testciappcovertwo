﻿namespace Orion_truck
{
    partial class FrmOrionAutoUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrionAutoUpdater));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblNewerVersion = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSkipVersion = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(8, 118);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(572, 273);
            this.webBrowser1.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Release Updates :";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::Orion_truck.Properties.Resources.OrionUpdater;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(485, 396);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(88, 28);
            this.btnUpdate.TabIndex = 41;
            this.btnUpdate.Text = "   Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // lblNewerVersion
            // 
            this.lblNewerVersion.AutoSize = true;
            this.lblNewerVersion.Location = new System.Drawing.Point(119, 404);
            this.lblNewerVersion.Name = "lblNewerVersion";
            this.lblNewerVersion.Size = new System.Drawing.Size(69, 15);
            this.lblNewerVersion.TabIndex = 40;
            this.lblNewerVersion.Text = "NewVersion";
            this.lblNewerVersion.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(5, 71);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(64, 15);
            this.lblMessage.TabIndex = 39;
            this.lblMessage.Text = "lblMessage";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(5, 42);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(70, 13);
            this.lblHeader.TabIndex = 38;
            this.lblHeader.Text = "lblHeader";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::Orion_truck.Properties.Resources.Login_OrionText;
            this.pictureBox2.Location = new System.Drawing.Point(334, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(250, 66);
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            // 
            // btnSkipVersion
            // 
            this.btnSkipVersion.Image = ((System.Drawing.Image)(resources.GetObject("btnSkipVersion.Image")));
            this.btnSkipVersion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSkipVersion.Location = new System.Drawing.Point(4, 396);
            this.btnSkipVersion.Name = "btnSkipVersion";
            this.btnSkipVersion.Size = new System.Drawing.Size(88, 28);
            this.btnSkipVersion.TabIndex = 42;
            this.btnSkipVersion.Text = "   Skip";
            this.btnSkipVersion.UseVisualStyleBackColor = true;
            // 
            // FrmOrionAutoUpdater
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(589, 427);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSkipVersion);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblNewerVersion);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblHeader);
            this.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(605, 466);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(605, 466);
            this.Name = "FrmOrionAutoUpdater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmOrionAutoUpdater";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSkipVersion;
        private System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Label lblNewerVersion;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblHeader;
    }
}