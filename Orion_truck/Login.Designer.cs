﻿namespace Orion_truck
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.imglst_lang = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.butlogin = new System.Windows.Forms.Button();
            this.lbl_pass = new System.Windows.Forms.Label();
            this.lbl_uid = new System.Windows.Forms.Label();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.lbl_plant = new System.Windows.Forms.Label();
            this.lbl_lang = new System.Windows.Forms.Label();
            this.pb_ori = new System.Windows.Forms.PictureBox();
            this.cmbcategory = new System.Windows.Forms.ComboBox();
            this.lbl_auth = new System.Windows.Forms.Label();
            this.pb_win = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmblanguage = new TyroDeveloper.ColorPicker();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ori)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_win)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // imglst_lang
            // 
            this.imglst_lang.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglst_lang.ImageStream")));
            this.imglst_lang.TransparentColor = System.Drawing.Color.Transparent;
            this.imglst_lang.Images.SetKeyName(0, "English_S.png");
            this.imglst_lang.Images.SetKeyName(1, "France_S.png");
            this.imglst_lang.Images.SetKeyName(2, "Korean_S.png");
            this.imglst_lang.Images.SetKeyName(3, "Polski_S.jpg");
            this.imglst_lang.Images.SetKeyName(4, "Romana_S.png");
            this.imglst_lang.Images.SetKeyName(5, "Deutsch_S.png");
            this.imglst_lang.Images.SetKeyName(6, "Espanol_S.png");
            this.imglst_lang.Images.SetKeyName(7, "Italiano_S.png");
            this.imglst_lang.Images.SetKeyName(8, "Portuges_S.jpg");
            this.imglst_lang.Images.SetKeyName(9, "Mexico_flag.jpg");
            this.imglst_lang.Images.SetKeyName(10, "Mexico_flag.jpg");
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Controls.Add(this.pictureBox1);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox2);
            this.tableLayoutPanel2.Controls.Add(this.panel1);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1009, 688);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Orion_truck.Properties.Resources.orion_consolLoadingPage_1048;
            this.pictureBox1.Location = new System.Drawing.Point(732, 360);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 325);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox2.Image = global::Orion_truck.Properties.Resources.orion_logo;
            this.pictureBox2.Location = new System.Drawing.Point(3, 535);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(435, 148);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImage = global::Orion_truck.Properties.Resources.login_white;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.btn_cancel);
            this.panel1.Controls.Add(this.txtpassword);
            this.panel1.Controls.Add(this.butlogin);
            this.panel1.Controls.Add(this.lbl_pass);
            this.panel1.Controls.Add(this.lbl_uid);
            this.panel1.Controls.Add(this.txtusername);
            this.panel1.Controls.Add(this.lbl_plant);
            this.panel1.Controls.Add(this.lbl_lang);
            this.panel1.Controls.Add(this.pb_ori);
            this.panel1.Controls.Add(this.cmbcategory);
            this.panel1.Controls.Add(this.lbl_auth);
            this.panel1.Controls.Add(this.pb_win);
            this.panel1.Controls.Add(this.cmblanguage);
            this.panel1.Location = new System.Drawing.Point(423, 50);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(510, 302);
            this.panel1.TabIndex = 4;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_cancel.BackgroundImage = global::Orion_truck.Properties.Resources.close1;
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.Location = new System.Drawing.Point(361, 248);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(101, 33);
            this.btn_cancel.TabIndex = 6;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // txtpassword
            // 
            this.txtpassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpassword.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpassword.Location = new System.Drawing.Point(162, 208);
            this.txtpassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(300, 21);
            this.txtpassword.TabIndex = 2;
            this.txtpassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyDown);
            this.txtpassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpassword_KeyPress);
            this.txtpassword.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyUp);
            // 
            // butlogin
            // 
            this.butlogin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.butlogin.BackgroundImage = global::Orion_truck.Properties.Resources._1356532475_Login1;
            this.butlogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.butlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butlogin.Location = new System.Drawing.Point(241, 248);
            this.butlogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.butlogin.Name = "butlogin";
            this.butlogin.Size = new System.Drawing.Size(103, 33);
            this.butlogin.TabIndex = 5;
            this.butlogin.Text = "Login";
            this.butlogin.UseVisualStyleBackColor = false;
            this.butlogin.Click += new System.EventHandler(this.butlogin_Click);
            // 
            // lbl_pass
            // 
            this.lbl_pass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_pass.AutoSize = true;
            this.lbl_pass.BackColor = System.Drawing.Color.White;
            this.lbl_pass.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pass.Location = new System.Drawing.Point(33, 211);
            this.lbl_pass.Name = "lbl_pass";
            this.lbl_pass.Size = new System.Drawing.Size(58, 18);
            this.lbl_pass.TabIndex = 4;
            this.lbl_pass.Text = "Password";
            this.lbl_pass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_uid
            // 
            this.lbl_uid.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_uid.AutoSize = true;
            this.lbl_uid.BackColor = System.Drawing.Color.White;
            this.lbl_uid.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_uid.Location = new System.Drawing.Point(33, 162);
            this.lbl_uid.Name = "lbl_uid";
            this.lbl_uid.Size = new System.Drawing.Size(47, 18);
            this.lbl_uid.TabIndex = 3;
            this.lbl_uid.Text = "User ID";
            this.lbl_uid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtusername
            // 
            this.txtusername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtusername.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtusername.Location = new System.Drawing.Point(162, 162);
            this.txtusername.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(300, 21);
            this.txtusername.TabIndex = 1;
            this.txtusername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtusername_KeyDown);
            this.txtusername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtusername_KeyPress);
            this.txtusername.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtusername_KeyUp);
            // 
            // lbl_plant
            // 
            this.lbl_plant.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_plant.AutoSize = true;
            this.lbl_plant.BackColor = System.Drawing.Color.White;
            this.lbl_plant.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_plant.Location = new System.Drawing.Point(33, 113);
            this.lbl_plant.Name = "lbl_plant";
            this.lbl_plant.Size = new System.Drawing.Size(36, 18);
            this.lbl_plant.TabIndex = 2;
            this.lbl_plant.Text = "Plant";
            this.lbl_plant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_lang
            // 
            this.lbl_lang.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_lang.AutoSize = true;
            this.lbl_lang.BackColor = System.Drawing.Color.White;
            this.lbl_lang.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lang.Location = new System.Drawing.Point(33, 63);
            this.lbl_lang.Name = "lbl_lang";
            this.lbl_lang.Size = new System.Drawing.Size(59, 18);
            this.lbl_lang.TabIndex = 1;
            this.lbl_lang.Text = "Language";
            this.lbl_lang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_ori
            // 
            this.pb_ori.Location = new System.Drawing.Point(318, 6);
            this.pb_ori.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_ori.Name = "pb_ori";
            this.pb_ori.Size = new System.Drawing.Size(145, 37);
            this.pb_ori.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ori.TabIndex = 5;
            this.pb_ori.TabStop = false;
            this.pb_ori.Click += new System.EventHandler(this.pb_ori_Click);
            // 
            // cmbcategory
            // 
            this.cmbcategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcategory.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcategory.FormattingEnabled = true;
            this.cmbcategory.Location = new System.Drawing.Point(162, 113);
            this.cmbcategory.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbcategory.Name = "cmbcategory";
            this.cmbcategory.Size = new System.Drawing.Size(300, 26);
            this.cmbcategory.TabIndex = 7;
            this.cmbcategory.SelectedIndexChanged += new System.EventHandler(this.cmbcategory_SelectedIndexChanged);
            this.cmbcategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbcategory_KeyDown);
            // 
            // lbl_auth
            // 
            this.lbl_auth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_auth.AutoSize = true;
            this.lbl_auth.BackColor = System.Drawing.Color.White;
            this.lbl_auth.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_auth.Location = new System.Drawing.Point(33, 12);
            this.lbl_auth.Name = "lbl_auth";
            this.lbl_auth.Size = new System.Drawing.Size(91, 18);
            this.lbl_auth.TabIndex = 0;
            this.lbl_auth.Text = "Authentication";
            this.lbl_auth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_win
            // 
            this.pb_win.Location = new System.Drawing.Point(164, 6);
            this.pb_win.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_win.Name = "pb_win";
            this.pb_win.Size = new System.Drawing.Size(145, 37);
            this.pb_win.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_win.TabIndex = 4;
            this.pb_win.TabStop = false;
            this.pb_win.Click += new System.EventHandler(this.pb_win_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Orion_truck.Properties.Resources.SGGILogo_new_1;
            this.pictureBox3.Location = new System.Drawing.Point(3, 50);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(221, 95);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmblanguage
            // 
            this.cmblanguage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmblanguage.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmblanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmblanguage.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmblanguage.FormattingEnabled = true;
            this.cmblanguage.Location = new System.Drawing.Point(162, 63);
            this.cmblanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmblanguage.Name = "cmblanguage";
            this.cmblanguage.SelectedItem = null;
            this.cmblanguage.SelectedValue = null;
            this.cmblanguage.Size = new System.Drawing.Size(300, 22);
            this.cmblanguage.TabIndex = 11;
            this.cmblanguage.SelectedIndexChanged += new System.EventHandler(this.cmblanguage_SelectedIndexChanged);
            this.cmblanguage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmblanguage_KeyDown);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1006, 687);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1022, 726);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1022, 726);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ori)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_win)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imglst_lang;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Button butlogin;
        private System.Windows.Forms.Label lbl_pass;
        private System.Windows.Forms.Label lbl_uid;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label lbl_plant;
        private System.Windows.Forms.Label lbl_lang;
        private System.Windows.Forms.PictureBox pb_ori;
        private System.Windows.Forms.ComboBox cmbcategory;
        private System.Windows.Forms.Label lbl_auth;
        private System.Windows.Forms.PictureBox pb_win;
        private TyroDeveloper.ColorPicker cmblanguage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel tableLayoutPanel2;
        private System.Windows.Forms.Timer timer1;
    }
}