﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Orion_truck.Model;
using Orion_truck.Entity;
using System.Globalization;
using System.Resources;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Deployment.Application;
using System.Diagnostics;
using System.Collections.Specialized;

namespace Orion_truck
{
    public partial class Login : Form
    {
        //Login_Model _model = new Login_Model();
        //User_Entity _userentity = new User_Entity();
        //MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        //MessageValidation_Model _alertmodel = new MessageValidation_Model();
        //string[] site;
        //string Authentication = string.Empty;
        //InPlaceHostingManager iphm = null;
        //public string param1 = string.Empty;
        //public string param2 = string.Empty;
        //public string launchUri = string.Empty;
        //public User_Model _usermodel;   
        //public string[] SGID;
        //DataTable dt = new DataTable();
        //public string idoclinedata, fileLoc, connection, langlocprocess, encryptconn, decryptconn, filename, currlang;
        ////public bool flag;
        //public FileStream filein, fileout;        
        //public bool isValid;
        Login_Model _model;
        User_Entity _userentity = new User_Entity();
        MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        MessageValidation_Model _alertmodel;
        string[] site;
        string Authentication = string.Empty;
        InPlaceHostingManager iphm = null;
        public string param1 = string.Empty;
        public string param2 = string.Empty;
        public string launchUri = string.Empty;
        public User_Model _usermodel;
        public string[] SGID;
        DataTable dt = new DataTable();
        public string idoclinedata, fileLoc, connection, langlocprocess, encryptconn, decryptconn, filename, currlang;
        //public bool flag;
        public FileStream filein, fileout;
        public bool isValid;
        public DataTable dt1 = new DataTable();


        public Login()
        {
            //#region clickonece source
            //GetArgsToShow();
            //InstallUpdateSyncWithInfo();
            //#endregion

            InitializeComponent();
            _model = new Login_Model();
            _alertmodel = new MessageValidation_Model();
            _usermodel = new User_Model();
        }

        [System.Runtime.InteropServices.DllImport("advapi32.dll")]
        public static extern bool LogonUser(string userName, string domainName, string password, int LogonType, int LogonProvider, ref IntPtr phToken);
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {

                Properties.Settings.Default.UsrProcess = "";
                string x = Environment.MachineName.ToString();

                string str = Properties.Settings.Default.UsrProcess;
                string[] plant = str.Split('-');
                pb_win.Image = Properties.Resources.win_e;
                pb_ori.Image = Properties.Resources.ori_d;
                Authentication = "WIN";
                DataSet ds_load = new DataSet();

                if (string.IsNullOrEmpty(plant[0].ToString()))
                {
                    ds_load = _model.getload_det("", Environment.MachineName.ToString());
                }
                else
                {
                    ds_load = _model.getload_det(plant[0].ToString(), Environment.MachineName.ToString());
                }


                dt1 = ds_load.Tables[0];

                for (int i = 0; i <= dt1.Rows.Count - 1; i++)
                {
                    cmblanguage.Items.Add(new TyroDeveloper.ColorPicker.ColorInfo(dt1.Rows[i].ItemArray[0].ToString(), imglst_lang.Images[Convert.ToInt32(dt1.Rows[i].ItemArray[2])], dt1.Rows[i].ItemArray[1].ToString()));
                }
                cmblanguage.SelectedText = dt1.Rows[0].ItemArray[0].ToString();
                cmblanguage.SelectedText = Properties.Settings.Default.UsrLang;

                //DataView dv2 = new DataView(dt1, "C_LANG='" + cmblanguage.SelectedItem.Value + "' ", "C_LANG ASC", DataViewRowState.CurrentRows);

                DataRow[] dr2 = dt1.Select("C_LANG = '" + cmblanguage.SelectedItem.Value + "'");
                if (dr2.Length > 0)
                {
                    
                    ChangeLanguage(dr2[0].ItemArray[3].ToString());
                }
               

                cmbcategory.SelectedIndexChanged -=  cmbcategory_SelectedIndexChanged;
                DataTable dt2 = new DataTable();
                dt2 = ds_load.Tables[1];
                cmbcategory.DisplayMember = "C_SITEL";
                cmbcategory.ValueMember = "C_SITE";
                cmbcategory.DataSource = dt2;
                cmbcategory.SelectedIndex = -1;
                cmbcategory.SelectedIndexChanged += cmbcategory_SelectedIndexChanged;
              
                cmbcategory.Text = Properties.Settings.Default.UsrProcess;

                if (ds_load.Tables[2].Rows.Count > 0)
                {
                    Globals.SetGlobalConnection(Decryptdata(ds_load.Tables[2].Rows[0][0].ToString()));
                    _usermodel = new User_Model();

                    DataTable dt = new DataTable();
                    dt = _usermodel.getuserid(Environment.MachineName.ToString(), "GET", "");
                    if (dt.Rows.Count > 0)
                    {
                        this.Hide();
                        txtusername.Text = dt.Rows[0][0].ToString();
                        txtpassword.Text = Decryptdata(dt.Rows[0][1].ToString());
                        pb_win.Image = Properties.Resources.win_d;
                        pb_ori.Image = Properties.Resources.ori_e;
                        Authentication = "ORI";
                        // LoginValidation_v1();
                        timer1.Start();
                       
                        butlogin.Focus();
                    }
                    else
                    {
                        txtusername.Text = string.Empty;
                        txtpassword.Text = string.Empty;
                        txtusername.Focus();
                    }
                    //txtusername.Text = ds_load.Tables[2].Rows[0][0].ToString();
                    //txtpassword.Text = Decryptdata( ds_load.Tables[2].Rows[0][1].ToString());
                    //pb_win.Image = Properties.Resources.win_d;
                    //pb_ori.Image = Properties.Resources.ori_e;
                    //Authentication = "ORI";
                    //butlogin.Focus();
                }
                else
                {
                    txtusername.Focus();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }


        //public void ChangeLanguage(string lang)
        //{
        //    Globals.SetGlobalLangString(lang);
        //    CultureInfo cul = new CultureInfo(lang);
        //    ResourceManager rm = new ResourceManager("Orion_truck.App_Global.Login", this.GetType().Assembly);
        //    this.Text = rm.GetString("Logintext", cul);
        //    foreach (System.Windows.Forms.Control c in this.Controls)
        //    {

        //        foreach (System.Windows.Forms.Control c1 in c.Controls)
        //        {
        //            if(c1.Name == "tableLayoutPanel2")
        //            {
        //                foreach (System.Windows.Forms.Control c2 in c1.Controls)
        //                {
        //                    if(c2.Name == "panel1")
        //                    {
        //                        foreach (System.Windows.Forms.Control c3 in c2.Controls)
        //                        {
        //                            if (c3.GetType() != typeof(TextBox) && c3.GetType() != typeof(ComboBox)  && c3.Name != "cmblanguage")
        //                            {
        //                                c3.Text = rm.GetString(c3.Name, cul);
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        public void ChangeLanguage(string lang)
        {
            try
            { 
            Globals.SetGlobalLangString(lang);
            CultureInfo cul = new CultureInfo(lang);
            ResourceManager rm = new ResourceManager("Orion_truck.App_Global.Login", this.GetType().Assembly);
            this.Text = rm.GetString("Logintext", cul);
            foreach (System.Windows.Forms.Control c in this.Controls)
            {

                foreach (System.Windows.Forms.Control c2 in c.Controls)
                {

                    if (c2.Name == "panel1")
                    {
                        foreach (System.Windows.Forms.Control c3 in c2.Controls)
                        {
                            if (c3.GetType() != typeof(TextBox) && c3.GetType() != typeof(ComboBox) && c3.Name != "cmblanguage")
                            {
                                c3.Text = rm.GetString(c3.Name, cul);
                            }

                        }
                    }

                }
            }

    }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void butlogin_Click(object sender, EventArgs e)
        {

            try
            {
                LoginValidation_v1();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void LoginValidation()
        {
            try
            {
                if (Authentication=="ORI")
                {
                    Globals.SetGlobalConnection("");

                  
                    
                    if (cmblanguage.SelectedItem.Text == "--Language--")
                    {
                        MessageBox.Show("Select Language", "Information Message");                        
                        cmblanguage.Focus();
                        return;
                    }

                    _userentity = _model.getValidateLanguage(cmblanguage.SelectedItem.Value.ToString().Trim());
                    if (_userentity.language == "" || _userentity.language == null)
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A095", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);                        
                        cmblanguage.Focus();
                        return;
                    }

                    if (cmbcategory.Text == "--Process Type--")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A098", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);                       
                        cmbcategory.Focus();
                        return;
                    }

                    if (cmbcategory.Text.ToString() != null && cmbcategory.Text.ToString() != "")
                    {
                        site = cmbcategory.Text.Split('-');
                    }

                    
                    if (txtusername.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A100", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);                       
                        txtusername.Focus();
                        return;
                    }
                    if (txtpassword.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A101", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);                        
                        txtpassword.Focus();
                        return;
                    }

                    if (cmbcategory.Text.ToString() != null && cmbcategory.Text.ToString() != "")
                    {
                        site = cmbcategory.Text.Split('-');
                    }
                   
                    _userentity = _model.getCurrentConnection(site[1].ToString().Trim(), site[2].ToString().Trim());
                    if (_userentity.connection != "" && _userentity.connection != null)
                    {
                        Globals.SetGlobalConnection(Decryptdata(_userentity.connection));
                        Globals.SetGlobalConnectionEncrpt(_userentity.connection);

                        MDIParent frm = new MDIParent();
                        this.Hide();
                        frm.Show();
                        
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A099", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        
                        txtusername.Focus();
                        return;
                    }

                }
               
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Globals.Setlog("");
            }
        }

        private string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private void txtusername_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (txtusername.Text != "" && txtpassword.Text != "")
                {
                    butlogin.Enabled = true;
                }
                else
                {
                    butlogin.Enabled = false;
                }

                if (e.KeyCode == Keys.Enter && (txtusername.Text != "" && txtpassword.Text != ""))
                {
                    LoginValidation_v1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (txtusername.Text != "" && txtpassword.Text != "")
                {
                    butlogin.Enabled = true;
                }
                else
                {
                    butlogin.Enabled = false;
                }

                if (e.KeyCode == Keys.Enter && (txtusername.Text != "" && txtpassword.Text != ""))
                {
                    LoginValidation_v1();
                }
                else if (e.KeyCode == Keys.Enter && (txtusername.Text != ""))
                {
                    txtpassword.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmblanguage_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    LoginValidation_v1();
                }
                else
                {
                    butlogin.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbcategory_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    LoginValidation_v1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtusername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {
                butlogin.Enabled = true;
            }
            else
            {
                butlogin.Enabled = false;
            }
        }

        private void txtpassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {
                butlogin.Enabled = true;
            }
            else
            {
                butlogin.Enabled = false;
            }
        }

        private void txtusername_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {
                butlogin.Enabled = true;
            }
            else
            {
                butlogin.Enabled = false;
            }
        }

        private void txtpassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {
                butlogin.Enabled = true;
            }
            else
            {
                butlogin.Enabled = false;
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void LoginValidation_v1()
        {
            try
            {
                if (Authentication == "ORI")
                {
                    Globals.SetGlobalConnection("");
                    _usermodel = new User_Model();

                    if (cmblanguage.SelectedItem.Text == "--Language--")
                    {
                        MessageBox.Show("Select Language", "Information Message");                        
                        cmblanguage.Focus();
                        return;
                    }

                    _userentity = _usermodel.getValidateLanguage(cmblanguage.SelectedItem.Value.ToString().Trim());
                    if (_userentity.language == "" || _userentity.language == null)
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A095", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);                        
                        cmblanguage.Focus();
                        return;
                    }

                    

                    if (cmbcategory.Text == "--Process Type--")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A098", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Select Process Type", "Information Message");
                        cmbcategory.Focus();
                        return;
                    }

                    if (cmbcategory.Text.ToString() != null && cmbcategory.Text.ToString() != "")
                    {
                        site = cmbcategory.Text.Split('-');
                    }

                   

                    if (txtusername.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A100", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Enter Username", "Information Message");
                        txtusername.Focus();
                        return;
                    }
                    if (txtpassword.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A101", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Enter Password", "Information Message");
                        txtpassword.Focus();
                        return;
                    }

                    if (cmbcategory.Text.ToString() != null && cmbcategory.Text.ToString() != "")
                    {
                        site = cmbcategory.Text.Split('-');
                    }

                    
                    _userentity = _usermodel.getCurrentConnection(site[1].ToString().Trim(), site[2].ToString().Trim());
                    if (_userentity.connection != "" && _userentity.connection != null)
                    {
                        Globals.SetGlobalConnection(Decryptdata(_userentity.connection));
                        Globals.SetGlobalConnectionEncrpt(_userentity.connection);
                        _usermodel = new User_Model();
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A099", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Invalid User", "Warning Message");
                        txtusername.Focus();
                        return;
                    }

                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                        _userentity = _usermodel.getValidateLoginUserOnly(txtusername.Text, Encryptdata(txtpassword.Text.ToString().Trim()));
                    }

                    if (_userentity.username != null)
                    {

                        _userentity.flag = _usermodel.getUpdateWrongPwd(txtusername.Text, Encryptdata(txtpassword.Text.ToString().Trim()));

                        if (_userentity.flag == true)
                        {
                            _userentity = _usermodel.getValidateLoginUserOnly(txtusername.Text);
                            if (_userentity.wrngpassattmpt < _userentity.wrngpasslimit)
                            {
                                Globals.SetGlobalConnection("");
                                _alertentity = _alertmodel.Alert_Information("LG", "A102", cmblanguage.SelectedItem.Value.ToString());
                                MessageBox.Show(string.Format(_alertentity.alertmessage, _userentity.wrngpasslimit - _userentity.wrngpassattmpt), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                txtpassword.Focus();
                                //MessageBox.Show("Wrong Password. You have (" + (_userentity.wrngpasslimit - _userentity.wrngpassattmpt) + ") more chance is left for login, if it is wrong your account will be locked.", "Warning Message");
                                return;
                            }

                            if (_userentity.wrngpassattmpt == _userentity.wrngpasslimit || _userentity.wrngpassattmpt > _userentity.wrngpasslimit)
                            {
                                _userentity.flag = _usermodel.getAccountLock(txtusername.Text);
                                if (_userentity.flag == true)
                                {
                                    Globals.SetGlobalConnection("");
                                    _alertentity = _alertmodel.Alert_Information("LG", "A103", cmblanguage.SelectedItem.Value.ToString());
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    txtusername.Focus();
                                    //MessageBox.Show("Wrong Password. Your account has been locked, contact Administrator", "Warning Message");
                                    return;
                                }
                            }
                        }
                        else
                        {
                            Globals.SetGlobalConnection("");
                            _alertentity = _alertmodel.Alert_Information("LG", "A104", cmblanguage.SelectedItem.Value.ToString());
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //MessageBox.Show("Your account has been locked, contact Administrator", "Warning Message");
                            txtusername.Focus();
                            return;
                        }
                    }

                 

                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                       
                        _userentity = _usermodel.getValidateLoginUser(txtusername.Text, Encryptdata(txtpassword.Text.ToString().Trim()));
                       
                    }

                    if (_userentity.username != null && _userentity.loginstatus == 1)
                    {
                        SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');

                        currlang = _usermodel.getCurrLnag(cmblanguage.SelectedItem.Value.ToString());

                        Globals.SetGlobalString(_userentity.username.ToUpper(), cmblanguage.SelectedItem.Value.ToString(), SGID[1].ToUpper(), _userentity.cparam, _userentity.site, _userentity.form, _userentity.application, _userentity.group, currlang);

                        DataTable dtLock = _usermodel.getMultiUserLoginLock();
                        if (dtLock.Rows.Count > 0)
                        {
                            if (dtLock.Rows[0][0].ToString() == "Y")
                            {
                             
                                _userentity = _usermodel.getValidateMultiUserLogin(txtusername.Text);

                                if (_userentity.sgid != null)
                                {
                                    Globals.SetGlobalConnection("");
                                    _alertentity = _alertmodel.Alert_Information("LG", "A105", cmblanguage.SelectedItem.Value.ToString());
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                 
                                    return;
                                }
                                else
                                {
                                    Globals.Setlog("0");
                                    //function here
                                    fun_loadform();
                                }
                            }
                            else
                            {
                                Globals.Setlog("0");
                                //function here
                                fun_loadform();
                            }
                        }
                        

                    }
                    else if (_userentity.loginstatus == 0)
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A106", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Your account has been locked, contact Administrator.", "Warning Message");
                        txtusername.Focus();
                    }
                    else if (_userentity.loginstatus == 1)
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A107", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Invalid User or Password.", "Warning Message");
                        txtusername.Focus();
                    }
                    else
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A108", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("User is not available.", "Warning Message");
                        txtusername.Focus();
                    }
                }
                else
                {
                    //sgid validation here

                    Globals.SetGlobalConnection("");
                    _usermodel = new User_Model();

                    if (cmblanguage.SelectedItem.Text == "--Language--")
                    {
                        MessageBox.Show("Select Language", "Information Message");
                        cmblanguage.Focus();
                        return;
                    }

                    

                    _userentity = _usermodel.getValidateLanguage(cmblanguage.SelectedItem.Value.ToString().Trim());
                    if (_userentity.language == "" || _userentity.language == null)
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A095", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Invalid Language", "Warning Message");
                        cmblanguage.Focus();
                        return;
                    }

                    

                    if (cmbcategory.Text == "--Process Type--")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A098", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Select Process Type", "Information Message");
                        cmbcategory.Focus();
                        return;
                    }

                    if (cmbcategory.Text.ToString() != null && cmbcategory.Text.ToString() != "")
                    {
                        site = cmbcategory.Text.Split('-');
                    }

                   

                    if (txtusername.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A100", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Enter Username", "Information Message");
                        txtusername.Focus();
                        return;
                    }
                    if (txtpassword.Text == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A101", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Enter Password", "Information Message");
                        txtpassword.Focus();
                        return;
                    }

                    //_userentity = _usermodel.getCurrentConnection(cmblocation.SelectedValue.ToString().Trim(), cmbcategory.Text.ToString().Trim());
                    _userentity = _usermodel.getCurrentConnection(site[1].ToString().Trim(), site[2].ToString().Trim());
                    if (_userentity.connection != "" && _userentity.connection != null)
                    {
                        Globals.SetGlobalConnection(Decryptdata(_userentity.connection));
                        Globals.SetGlobalConnectionEncrpt(_userentity.connection);
                        _usermodel = new User_Model();
                    }
                   



                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                        _userentity = _usermodel.getValidateLoginUserOnlyNew(txtusername.Text);
                        //isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), "zl");
                        isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), _userentity.domainname);
                        if (_userentity.username != null)
                        {
                            //isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), "zl");
                            isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), _userentity.domainname);
                        }
                        else
                        {
                            isValid = true;
                        }
                    }

                    //added on 25-03-2015 for emp master check
                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                        _userentity = _usermodel.getValidateEmpMaster(txtusername.Text);
                        if (_userentity.username != null && _userentity.username != "")
                        {
                            _userentity = _usermodel.getValidateLoginUser(txtusername.Text, Encryptdata(txtpassword.Text.ToString().Trim()));
                        }
                        else
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A613", cmblanguage.SelectedItem.Value.ToString());
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //MessageBox.Show("You don't have permikssion's to access this application because your access was closed by administrator.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    if (isValid == false)
                    {
                        _userentity.flag = _usermodel.getUpdateWrongPwd(txtusername.Text, Encryptdata(txtpassword.Text.ToString().Trim()));

                        if (_userentity.flag == true)
                        {
                            _userentity = _usermodel.getValidateLoginUserOnly(txtusername.Text);
                            if (_userentity.wrngpassattmpt < _userentity.wrngpasslimit)
                            {
                                Globals.SetGlobalConnection("");
                                _alertentity = _alertmodel.Alert_Information("LG", "A102", cmblanguage.SelectedItem.Value.ToString());
                                MessageBox.Show(string.Format(_alertentity.alertmessage, _userentity.wrngpasslimit - _userentity.wrngpassattmpt), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //MessageBox.Show("Wrong Password. You have (" + (_userentity.wrngpasslimit - _userentity.wrngpassattmpt) + ") more chance is left for login, if it is wrong your account will be locked.", "Warning Message");
                                txtpassword.Focus();
                                return;
                            }

                            if (_userentity.wrngpassattmpt == _userentity.wrngpasslimit || _userentity.wrngpassattmpt > _userentity.wrngpasslimit)
                            {
                                _userentity.flag = _usermodel.getAccountLock(txtusername.Text);
                                if (_userentity.flag == true)
                                {
                                    Globals.SetGlobalConnection("");
                                    _alertentity = _alertmodel.Alert_Information("LG", "A103", cmblanguage.SelectedItem.Value.ToString());
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //MessageBox.Show("Wrong Password. Your account has been locked, contact Administrator", "Warning Message");
                                    txtusername.Focus();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            Globals.SetGlobalConnection("");
                            //_alertentity = _alertmodel.Alert_Information("LG", "A104", cmblanguage.SelectedItem.Value.ToString());
                            _alertentity = _alertmodel.Alert_Information("LG", "A614", cmblanguage.SelectedItem.Value.ToString());
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //MessageBox.Show("Your account has been locked, contact Administrator", "Warning Message");
                            txtpassword.Focus();
                            return;
                        }
                    }




                    if (txtusername.Text != "" && txtpassword.Text != "")
                    {
                        SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');

                        _userentity = _usermodel.getValidateLoginUserSGIDNEW(txtusername.Text);

                        if (_userentity.username != null)
                        {

                            //isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), "zl");
                            isValid = IsValidateCredentials(txtusername.Text.ToString(), txtpassword.Text.ToString(), _userentity.domainname);
                            currlang = _usermodel.getCurrLnag(cmblanguage.SelectedItem.Value.ToString());

                            Globals.SetGlobalString(txtusername.Text.ToUpper(), cmblanguage.SelectedItem.Value.ToString(), SGID[1].ToUpper(), _userentity.cparam, _userentity.site, _userentity.form, _userentity.application, _userentity.group, currlang);
                        }
                        else
                        {
                            _userentity = _usermodel.getValidateLoginUserSGIDNEW(txtusername.Text);
                        }
                    }

                    if (isValid == true && _userentity.loginstatus == 1)
                    {
                        DataTable dtLock = _usermodel.getMultiUserLoginLock();
                        if (dtLock.Rows.Count > 0)
                        {
                            if (dtLock.Rows[0][0].ToString() == "Y")
                            {
                                //check the multiple user login here
                                //_userentity = _usermodel.getValidateMultiUserLogin(txtusername.Text, Globals.GlobalSGID.ToString());
                                _userentity = _usermodel.getValidateMultiUserLogin(txtusername.Text);

                                if (_userentity.sgid != null)
                                {
                                    Globals.SetGlobalConnection("");
                                    _alertentity = _alertmodel.Alert_Information("LG", "A105", cmblanguage.SelectedItem.Value.ToString());
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //MessageBox.Show("You are already login with other system.", "Warning Message");
                                    return;
                                }
                            }
                            Globals.Setlog("0");
                            DataTable dt = _usermodel.getMasterData(Globals.GlobalSite, "DP");
                            DataTable dt1 = _usermodel.getMasterData(Globals.GlobalSite, "PTL");
                            DataTable dt2 = _usermodel.getPasswordChangeTime(txtusername.Text.Trim(), Encryptdata(txtpassword.Text.ToString().Trim()));
                            
                            string NewerVersion = string.Empty;
                            string ReleaseNotesPath = string.Empty;
                            bool UpdaterCheck = VersionCheck(out NewerVersion, out ReleaseNotesPath);
                            UpdaterCheck = false;
                            if (UpdaterCheck)
                            {
                                this.Hide();
                                FrmOrionAutoUpdater frm = new FrmOrionAutoUpdater();
                                frm.lblNewerVersion.Text = NewerVersion;
                                frm.webBrowser1.Url = new Uri(ReleaseNotesPath);
                                DialogResult result = frm.ShowDialog();

                                if (result == DialogResult.No)
                                {
                                    //insert login user to table
                                    _userentity.flag = _usermodel.getInsertLoginUsers(txtusername.Text.ToUpper(), Globals.GlobalSGID.ToString().ToUpper(), Environment.MachineName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                                    if (_userentity.flag == true)
                                    {
                                        Properties.Settings.Default.UsrLang = cmblanguage.SelectedItem.Text;
                                        //Properties.Settings.Default.UsrPlant = cmblocation.Text;
                                        Properties.Settings.Default.UsrProcess = cmbcategory.Text;
                                        Properties.Settings.Default.Save();
                                    }

                                    if (dt.Rows.Count > 0)
                                    {
                                        if (txtpassword.Text.ToUpper() == dt.Rows[0]["NEWVALUE"].ToString().ToUpper())
                                        {
                                            this.Show();
                                            Globals.SetGlobalConnection("");
                                            _alertentity = _alertmodel.Alert_Information("LG", "A015", "E");
                                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                                            DialogResult result2 = pw.ShowDialog();
                                            if (result2 == DialogResult.No)
                                            {
                                                _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                                                return;
                                            }
                                            else
                                            {
                                                Globals.Setlog("");
                                            }
                                        }
                                    }

                                    dt1 = _usermodel.getMasterData(Globals.GlobalSite, "PTL");
                                    dt2 = _usermodel.getPasswordChangeTime(txtusername.Text.Trim(), Encryptdata(txtpassword.Text.ToString().Trim()));
                                    if (dt1.Rows.Count > 0 && dt2.Rows.Count > 0)
                                    {
                                        if (Convert.ToInt32(dt1.Rows[0]["NEWVALUE"].ToString()) < Convert.ToInt32(dt2.Rows[0]["TDAYS"].ToString()))
                                        {
                                            this.Show();
                                            _alertentity = _alertmodel.Alert_Information("LG", "A016", "E");
                                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                                            DialogResult result1 = pw.ShowDialog();
                                            if (result1 == DialogResult.No)
                                            {
                                                _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                                                return;
                                            }
                                            else
                                            {
                                                Globals.Setlog("");
                                            }
                                        }
                                    }
                                    //this.Hide();

                                    //MDIParent frm1 = new MDIParent();
                                    //frm1.Show();
                                    DataTable dt_a = new DataTable();
                                    dt_a = _usermodel.val_UsersAccess(Globals.GlobalUsername);

                                    if (dt_a.Rows.Count > 0)
                                    {
                                        this.Hide();

                                        MDIParent frm1 = new MDIParent();
                                        frm1.Show();
                                    }
                                    else
                                    {
                                        _alertentity = _alertmodel.Alert_Information("LG", "A2162", "E");
                                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }


                                }
                            }
                            else
                            {
                                Globals.Setlog("0");
                                //insert login user to table
                                _userentity.flag = _usermodel.getInsertLoginUsers(txtusername.Text.ToUpper(), Globals.GlobalSGID.ToString().ToUpper(), Environment.MachineName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                                if (_userentity.flag == true)
                                {
                                   

                                    Properties.Settings.Default.UsrLang = cmblanguage.SelectedItem.Text;
                                   
                                    Properties.Settings.Default.UsrProcess = cmbcategory.Text;
                                    Properties.Settings.Default.Save();
                                }

                                if (dt.Rows.Count > 0)
                                {
                                    if (txtpassword.Text.ToUpper() == dt.Rows[0]["NEWVALUE"].ToString().ToUpper())
                                    {
                                        _alertentity = _alertmodel.Alert_Information("LG", "A015", "E");
                                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                                        DialogResult result2 = pw.ShowDialog();
                                        if (result2 == DialogResult.No)
                                        {
                                            _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                                            return;
                                        }
                                        else
                                        {
                                            Globals.Setlog("");
                                        }
                                    }
                                }

                                dt1 = _usermodel.getMasterData(Globals.GlobalSite, "PTL");
                                dt2 = _usermodel.getPasswordChangeTime(txtusername.Text.Trim(), Encryptdata(txtpassword.Text.ToString().Trim()));
                                if (dt1.Rows.Count > 0 && dt2.Rows.Count > 0)
                                {
                                    if (Convert.ToInt32(dt1.Rows[0]["NEWVALUE"].ToString()) < Convert.ToInt32(dt2.Rows[0]["TDAYS"].ToString()))
                                    {
                                        _alertentity = _alertmodel.Alert_Information("LG", "A016", "E");
                                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                                        DialogResult result1 = pw.ShowDialog();
                                        if (result1 == DialogResult.No)
                                        {
                                            _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                                            return;
                                        }
                                        else
                                        {
                                            Globals.Setlog("");
                                        }
                                    }
                                }

                                //this.Hide();
                                //MDIParent frm2 = new MDIParent();
                                //frm2.Show();
                                DataTable dt_a = new DataTable();
                                dt_a = _usermodel.val_UsersAccess(Globals.GlobalUsername);

                                if (dt_a.Rows.Count > 0)
                                {
                                   
                                    this.Hide();

                                    MDIParent frm1 = new MDIParent();
                                    frm1.Show();
                                }
                                else
                                {
                                    _alertentity = _alertmodel.Alert_Information("LG", "A2162", "E");
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }
                    else if (_userentity.loginstatus == 0)
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A106", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Your account has been locked, contact Administrator.", "Warning Message");
                        txtusername.Focus();
                    }
                    else if (_userentity.loginstatus == 1)
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A107", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("Invalid User or Password.", "Warning Message");
                        txtusername.Focus();
                    }
                    else
                    {
                        Globals.SetGlobalConnection("");
                        _alertentity = _alertmodel.Alert_Information("LG", "A108", cmblanguage.SelectedItem.Value.ToString());
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("User is not available.", "Warning Message");
                        txtusername.Focus();
                    }
                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Globals.Setlog("");
            }
        }

        private string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        private void pb_win_Click(object sender, EventArgs e)
        {
            Authentication = "WIN";

            pb_win.Image = Properties.Resources.win_e;
            pb_ori.Image = Properties.Resources.ori_d;
        }

        private void pb_ori_Click(object sender, EventArgs e)
        {
            Authentication = "ORI";
            pb_ori.Image = Properties.Resources.ori_e;
            pb_win.Image = Properties.Resources.win_d;
        }

        private void cmblanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChangeLanguage("en-US");
            //DataView dv2 = new DataView(dt1, "C_LANG='" + cmblanguage.SelectedItem.Value + "' ", "C_LANG ASC", DataViewRowState.CurrentRows);

            //if (dv2.Table.Rows.Count > 0)
            //{
            //    ChangeLanguage(dv2.Table.Rows[0]["C_LNGCODE"].ToString());
            //}
            try
            {
                DataView dv2 = new DataView(dt1, "C_LANG='" + cmblanguage.SelectedItem.Value + "' ", "C_LANG ASC", DataViewRowState.CurrentRows);

                DataTable dtg1 = new DataTable();
                dtg1 = dv2.ToTable();


                if (dtg1.Rows.Count > 0)
                {
                    ChangeLanguage(dtg1.Rows[0]["C_LNGCODE"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void fun_loadform()
        {
            DataTable dt = _usermodel.getMasterData(Globals.GlobalSite, "DP");
            DataTable dt1 = _usermodel.getMasterData(Globals.GlobalSite, "PTL");
            DataTable dt2 = _usermodel.getPasswordChangeTime(txtusername.Text.Trim(), Encryptdata(txtpassword.Text.ToString().Trim()));
           
            //insert login user to table
            _userentity.flag = _usermodel.getInsertLoginUsers(txtusername.Text.ToUpper(), Globals.GlobalSGID.ToString().ToUpper(), Environment.MachineName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

            if (_userentity.flag == true)
            {
                Properties.Settings.Default.UsrLang = cmblanguage.SelectedItem.Text;
                //Properties.Settings.Default.UsrPlant = cmblocation.Text;
                Properties.Settings.Default.UsrProcess = cmbcategory.Text;
                Properties.Settings.Default.Save();



            }

            if (dt.Rows.Count > 0)
            {
                if (txtpassword.Text.ToUpper() == dt.Rows[0]["NEWVALUE"].ToString().ToUpper())
                {
                    this.Show();
                    _alertentity = _alertmodel.Alert_Information("LG", "A015", "E");
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                    DialogResult result2 = pw.ShowDialog();
                    if (result2 == DialogResult.No)
                    {
                        _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                        return;
                    }
                    else
                    {
                        Globals.Setlog("");
                    }
                }
            }

            dt1 = _usermodel.getMasterData(Globals.GlobalSite, "PTL");
            dt2 = _usermodel.getPasswordChangeTime(txtusername.Text.Trim(), Encryptdata(txtpassword.Text.ToString().Trim()));
            if (dt1.Rows.Count > 0 && dt2.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt1.Rows[0]["NEWVALUE"].ToString()) < Convert.ToInt32(dt2.Rows[0]["TDAYS"].ToString()))
                {
                    this.Show();
                    _alertentity = _alertmodel.Alert_Information("LG", "A016", "E");
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FrmDefaultChangePassword pw = new FrmDefaultChangePassword();
                    DialogResult result1 = pw.ShowDialog();
                    if (result1 == DialogResult.No)
                    {
                        _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
                        return;
                    }
                    else
                    {
                        Globals.Setlog("");
                    }
                }
            }

            DataTable dt_a = new DataTable();
            dt_a = _usermodel.val_UsersAccess(Globals.GlobalUsername);

            if (dt_a.Rows.Count > 0)
            {
                this.Hide();

                MDIParent frm1 = new MDIParent();
                frm1.Show();
            }
            else
            {
                _alertentity = _alertmodel.Alert_Information("LG", "A2162", "E");
                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //this.Hide();
            ////  MDIParent1 mdifrm = new MDIParent1();
            //// mdifrm.Show();
            //MDIParent frm = new MDIParent();
            //frm.Show();
        }

        public bool IsValidateCredentials(string userName, string password, string domain)
        {
            IntPtr tokenHandler = IntPtr.Zero;
            bool isValid = LogonUser(userName, domain, password, 2, 0, ref tokenHandler);
            return isValid;
        }

        private bool VersionCheck(out string NewerVersion, out string ReleaseNotesPath)
        {
            string newVersion = string.Empty;
            string releasenotes = string.Empty;
            SqlConnection vConn = null;
            SqlCommand vCmd = null;
            bool UpdaterAvailable = false;

            try
            {

                //this.Hide();
                DataTable dt = new DataTable();
                string VconnString = Decryptdata(ConfigurationManager.ConnectionStrings["SGGIDBConnection"].ConnectionString);
                //string VconnString = ConfigurationManager.ConnectionStrings["SGGIDBConnection"].ConnectionString;

                vConn = new SqlConnection(VconnString);
                vCmd = new SqlCommand(@"SELECT A.C_SITE,A.VER_NO,A.REL_NOTE_PATH FROM XT_VERMSTR A,
                                        XT_VERSIONCHK B WHERE A.C_SITE=B.C_SITE AND A.VER_NO=B.NEW_VERSION 
                                        AND  A.C_SITE = @PLANT_CODE");
                vCmd.Parameters.AddWithValue("@PLANT_CODE", Globals.GlobalSite);
                vCmd.Connection = vConn;
                vCmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(vCmd);
                da.Fill(dt);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        newVersion = dt.Rows[0].ItemArray[1].ToString();
                        releasenotes = dt.Rows[0].ItemArray[2].ToString();
                        string oldVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                        if (!string.IsNullOrEmpty(newVersion))
                        {
                            if (newVersion != oldVersion)
                            {
                                UpdaterAvailable = true;
                            }
                            else
                            {
                                UpdaterAvailable = false;
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (vCmd.Connection != null)
                    vConn.Close();
            }

            NewerVersion = newVersion;
            ReleaseNotesPath = releasenotes;
            return UpdaterAvailable;
        }

        private void InstallUpdateSyncWithInfo()
        {
            try
            {
                UpdateCheckInfo info = null;

                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

                    try
                    {
                        launchUri = ad.UpdateLocation.AbsoluteUri;
                        info = ad.CheckForDetailedUpdate(false);
                    }
                    catch (DeploymentDownloadException dde)
                    {
                        MessageBox.Show("The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: " + dde.Message);
                        return;
                    }
                    catch (InvalidDeploymentException ide)
                    {
                        MessageBox.Show("Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: " + ide.Message);
                        return;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        MessageBox.Show("This application cannot be updated. It is likely not a ClickOnce application. Error: " + ioe.Message);
                        return;
                    }

                    if (info.UpdateAvailable)
                    {
                        frmInstaller install = new frmInstaller(param1, param2, info.AvailableVersion.ToString());
                        install.ShowDialog();
                    }
                }
            }
            catch (Exception)
            {
                InstallApplication(launchUri);
                //System.Environment.Exit(1);
                //MessageBox.Show(Ex.Message +launchUri);
            }
        }

        private void GetArgsToShow()
        {
            try
            {
                //   Get the ActivationArguments from the SetupInformation property of the domain.
                string[] activationData =
                  AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;

                if (activationData != null && activationData.Length > 0)
                {
                    //querystring starts with ?; file association starts with "file:"
                    if (activationData.Length == 1 && activationData[0].Substring(0, 1) == "?")
                    {
                        ProcessQueryString(activationData);
                    }
                    else if (activationData.Length == 1 && activationData[0].Length >= 5 && activationData[0].Substring(0, 5).ToLower() == @"file:")
                    {
                        ProcessFileAssociation(activationData);
                    }
                    else
                    {
                        ProcessCSVParameters(activationData);
                    }
                }
                else
                {
                    if (activationData == null)
                    {
                        MessageBox.Show("No arguments passed in.");
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Number of args = {0}", activationData.Length));
                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        /// Convert a query string into Name/Value pairs and process it.
        private void ProcessQueryString(string[] activationData)
        {
            NameValueCollection nvc =
              System.Web.HttpUtility.ParseQueryString(activationData[0]);

            //Get all the keys in the collection, then pull the values for each of them.
            //I'm only passing each key once, with one value.
            string[] theKeys = nvc.AllKeys;
            int i = 0;
            foreach (string theKey in theKeys)
            {
                string[] theValue = nvc.GetValues(theKey);
                //lstArgs.Items.Add(string.Format("Key = {0}, Value = {1}", theKey, theValue[0]));
                if (i == 0)
                {
                    param1 = theValue[0];
                }
                else
                {
                    param2 = theValue[0];
                }
                i++;
            }
            Globals.SetGlobalServer(param2);
        }

        private void cmbcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                string[] plant = cmbcategory.Text.Split('-');
                DataSet ds_load = new DataSet();
                ds_load = _model.getload_det(plant[0].ToString(), Environment.MachineName.ToString());

                if (ds_load.Tables[2].Rows.Count > 0)
                {
                    Globals.SetGlobalConnection(Decryptdata(ds_load.Tables[2].Rows[0][0].ToString()));
                    _usermodel = new User_Model();

                    DataTable dt = new DataTable();
                    dt = _usermodel.getuserid(Environment.MachineName.ToString(), "GET", "");
                    if (dt.Rows.Count > 0)
                    {
                        txtusername.Text = dt.Rows[0][0].ToString();
                        txtpassword.Text = Decryptdata(dt.Rows[0][1].ToString());
                        pb_win.Image = Properties.Resources.win_d;
                        pb_ori.Image = Properties.Resources.ori_e;
                        Authentication = "ORI";
                        butlogin.Focus();
                    }
                    else
                    {
                        txtusername.Text = string.Empty;
                        txtpassword.Text = string.Empty;
                        txtusername.Focus();
                    }
                }
                else
                {
                    txtusername.Text = string.Empty;
                    txtpassword.Text = string.Empty;
                    txtusername.Focus();
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            timer1.Stop();
            LoginValidation_v1();

        }

        /// Process a comma-delimited string of values. Not: can't have spaces or double-quotes in the string,
        /// it will only read the first argument.
        private void ProcessCSVParameters(string[] activationData)
        {
            //I have to say here that I've only ever seen 1 entry passed in activationData,
            //  but I'm checking for multiples just in case. 
            //This takes each entry and splits it by comma and separates them into separate entries.

            char[] myComma = { ',' };

            foreach (string arg in activationData)
            {
                string[] myList = activationData[0].Split(myComma);
                //param1 = myList[0].ToString();
                //param2 = myList[1].ToString();
                int i = 0;
                foreach (string item in myList)
                {
                    if (i == 0)
                    {
                        param1 = item;
                    }
                    else
                    {
                        param2 = item;
                    }
                    i++;
                }
                Globals.SetGlobalServer(param2);
            }
        }

        /// Process what you would get if you set up a file association, 
        /// and the user double-clicked on one of the associated file.
        private void ProcessFileAssociation(string[] activationData)
        {
            //This is what you get when you set up a file association and the user double-clicks 
            //  on an associated file. 
            Uri uri = new Uri(activationData[0]);
            MessageBox.Show(uri.LocalPath.ToString());
        }

        private void UpdateApplication()
        {
            try
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {

                    ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

                    ad.CheckForUpdateCompleted += new CheckForUpdateCompletedEventHandler(ad_CheckForUpdateCompleted);

                    launchUri = ad.UpdateLocation.AbsoluteUri;

                    ad.CheckForUpdateAsync();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    MessageBox.Show("ERROR: Could not retrieve new version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
                    return;
                }
                else if (e.Cancelled == true)
                {
                    MessageBox.Show("The update was cancelled.");
                }

                // Ask the user if they would like to update the application now.
                if (e.UpdateAvailable)
                {
                    MessageBox.Show("A mandatory update is available for your application. We will install the update now, after which we will save all of your in-progress data and restart your application.");
                    frmInstaller install = new frmInstaller(param1, param2, e.AvailableVersion.ToString());
                    install.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void InstallApplication(string deployManifestUriStr)
        {
            try
            {
                Uri deploymentUri = new Uri(deployManifestUriStr);
                iphm = new InPlaceHostingManager(deploymentUri, false);
            }
            catch (UriFormatException uriEx)
            {
                MessageBox.Show("Cannot install the application: " +
                    "The deployment manifest URL supplied is not a valid URL. " +
                    "Error: " + uriEx.Message);
                this.Close();
                return;
            }
            catch (PlatformNotSupportedException platformEx)
            {
                MessageBox.Show("Cannot install the application: " +
                    "This program requires Windows XP or higher. " +
                    "Error: " + platformEx.Message);
                this.Close();
                return;
            }
            catch (ArgumentException argumentEx)
            {
                MessageBox.Show("Cannot install the application: " +
                    "The deployment manifest URL supplied is not a valid URL. " +
                    "Error: " + argumentEx.Message);
                this.Close();
                return;
            }

            iphm.GetManifestCompleted += new EventHandler<GetManifestCompletedEventArgs>(iphm_GetManifestCompleted);
            iphm.GetManifestAsync();
        }

        private void iphm_GetManifestCompleted(object sender, GetManifestCompletedEventArgs e)
        {
            // Check for an error.
            if (e.Error != null)
            {
                // Cancel download and install.
                MessageBox.Show("Could not download manifest. Error: " + e.Error.Message);
                this.Close();
                return;
            }

            // bool isFullTrust = CheckForFullTrust(e.ApplicationManifest);

            // Verify this application can be installed.
            try
            {
                // the true parameter allows InPlaceHostingManager
                // to grant the permissions requested in the applicaiton manifest.
                iphm.AssertApplicationRequirements(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while verifying the application. " +
                    "Error: " + ex.Message);
                this.Close();
                return;
            }

            // Download the deployment manifest. 
            iphm.DownloadProgressChanged += new EventHandler<DownloadProgressChangedEventArgs>(iphm_DownloadProgressChanged);
            iphm.DownloadApplicationCompleted += new EventHandler<DownloadApplicationCompletedEventArgs>(iphm_DownloadApplicationCompleted);

            try
            {
                // Usually this shouldn't throw an exception unless AssertApplicationRequirements() failed, 
                // or you did not call that method before calling this one.
                iphm.DownloadApplicationAsync();
            }
            catch (Exception downloadEx)
            {
                MessageBox.Show("Cannot initiate download of application. Error: " +
                    downloadEx.Message);
                this.Close();
                return;
            }
            string shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\SAINT-GOBAIN\\", param1, ".appref-ms");
            Process.Start(shortcutName, param1 + ',' + param2);
            System.Environment.Exit(1);
        }

        private void iphm_DownloadApplicationCompleted(object sender, DownloadApplicationCompletedEventArgs e)
        {
            // Check for an error.
            if (e.Error != null)
            {
                // Cancel download and install.
                MessageBox.Show("Could not download and install application. Error: " + e.Error.Message);
                this.Close();
                return;
            }
            string shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\SAINT-GOBAIN\\", param1, ".appref-ms");
            Process.Start(shortcutName, param1 + ',' + param2);
        }

        private void iphm_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }

    }
}
