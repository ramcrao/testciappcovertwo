﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Deployment.Application;
using System.IO;
using System.Diagnostics;

namespace Orion_truck
{
    public partial class frmInstaller : Form
    {
        long sizeOfUpdate = 0;
        public string param1 = string.Empty;
        public string param2 = string.Empty;
        public string version = string.Empty;
        public frmInstaller(string _param1, string _param2, string _version)
        {
            param1 = _param1;
            param2 = _param2;
            version = _version;
            InitializeComponent();
        }

        private void frmInstaller_Load(object sender, EventArgs e)
        {
            lblAppName.Text = param1;
            lbl_Version.Text = version;
            progressBar1.Visible = false;
            labelPerc.Visible = false;
            btnCancel.Visible = false;
        }

        private void UpdateApplication()
        {
            try
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {

                    ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

                    ad.CheckForUpdateCompleted += new CheckForUpdateCompletedEventHandler(ad_CheckForUpdateCompleted);
                    ad.CheckForUpdateProgressChanged += new DeploymentProgressChangedEventHandler(ad_CheckForUpdateProgressChanged);

                    ad.CheckForUpdateAsync();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {
            //labelPerc.Text = String.Format("Downloading: {0}. {1:D}K of {2:D}K downloaded.", GetProgressString(e.State), e.BytesCompleted / 1024, e.BytesTotal / 1024);
        }

        string GetProgressString(DeploymentProgressState state)
        {
            if (state == DeploymentProgressState.DownloadingApplicationFiles)
            {
                return "application files";
            }
            else if (state == DeploymentProgressState.DownloadingApplicationInformation)
            {
                return "application manifest";
            }
            else
            {
                return "deployment manifest";
            }
        }

        void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    MessageBox.Show("ERROR: Could not retrieve new version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
                    return;
                }
                else if (e.Cancelled == true)
                {
                    MessageBox.Show("The update was cancelled.");
                }

                // Ask the user if they would like to update the application now.
                if (e.UpdateAvailable)
                {
                    sizeOfUpdate = e.UpdateSizeBytes;

                    if (!e.IsUpdateRequired)
                    {
                        BeginUpdate();
                        label2.Text = "Updating....";
                    }
                    else
                    {
                        MessageBox.Show("A mandatory update is available for your application. We will install the update now, after which we will save all of your in-progress data and restart your application.");
                        BeginUpdate();
                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void BeginUpdate()
        {
            try
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

                ad.UpdateCompleted += new AsyncCompletedEventHandler(ad_UpdateCompleted);

                // Indicate progress in the application's status bar.
                ad.UpdateProgressChanged += new DeploymentProgressChangedEventHandler(ad_UpdateProgressChanged);
                ad.UpdateAsync();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        void ad_UpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {
            labelPerc.Text = String.Format("{0:D}KB out of {1:D}KB downloaded - {2:D}% complete", e.BytesCompleted / 1024, e.BytesTotal / 1024, e.ProgressPercentage);
            progressBar1.Value = e.ProgressPercentage;
        }

        void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The update of the application's latest version was cancelled.");
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show("ERROR: Could not install the latest version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
                return;
            }
            label2.Text = "Updation Completed.";
            DialogResult dr = MessageBox.Show("The application has been updated. Restart? (If you do not restart now, the new version will not take effect until after you quit and launch the application again.)", "Restart Application", MessageBoxButtons.OKCancel);
            if (DialogResult.OK == dr)
            {
                string shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\SAINT-GOBAIN\\", param1, ".appref-ms");
                Process.Start(shortcutName, param1 + ',' + param2);
                //Process.Start("rundll32.exe", String.Format("dfshim.dll,ShOpenVerbApplication {0}?Arg1={1}&Arg2={2}", launchUri, param1 + ',' + param2));
                System.Environment.Exit(1);
            }
            else
            {
                this.Close();
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            labelPerc.Visible = true;
            BtnUpdate.Visible = false;
            btnSkip.Visible = false;
            btnCancel.Visible = true;
            UpdateApplication();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
