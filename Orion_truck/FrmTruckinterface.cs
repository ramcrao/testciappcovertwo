﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Orion_truck.Properties;


namespace Orion_truck
{
    public partial class FrmTruckinterface : Form
    {
        public FrmTruckinterface()
        {
            InitializeComponent();
        }

        int formMaximized = 0;
        double rW = 0;
        double rH = 0;
        int fH = 0;
        int fW = 0;

        string sltd_truck,sltd_in_product;

        private void FrmTruckinterface_Load(object sender, EventArgs e)
        {        
            this.WindowState = FormWindowState.Maximized;
            Rectangle resolution = Screen.PrimaryScreen.Bounds;

            clearform();
            langload();
        }

        private void clearform()
        {
            lbl_titlt_truck.Visible = false;
            lbl_titlt_truckin.Visible = false;
            lbl_title_deli.Visible = false;
            lbl_title_out.Visible = false;
            lbl_title_info.Visible = false;
            lbl_title_returnorder.Visible = false;
            lbl_title_lang.Visible = false;

            pan_slt_inloader.Visible = false;
            pan_sltd_container.Visible = false;
            pan_sltd_euro.Visible = false;
            pan_sltd_other.Visible = false;

            lbl_slt_lang_nor.Visible = false;
            lbl_slt_lang_max.Visible = false;
            lbl_slt_truck.Visible = false;
            lbl_slt_truckin.Visible = false;
            lbl_slt_truckout.Visible = false;

            pan_lang.BackgroundImage=Properties.Resources.process_dis_1;
            pan_truck.BackgroundImage = Properties.Resources.process_dis_1;
            pan_del.BackgroundImage = Properties.Resources.process_dis_1;
            pan_order.BackgroundImage = Properties.Resources.process_dis_1;
            pan_info.BackgroundImage = Properties.Resources.process_dis_1;
            pan_comp.BackgroundImage = Properties.Resources.process_dis_1;

            sltd_truck = string.Empty;

        }

        private void langload()
        {
            lbl_title_lang.Visible = true;

            DataSet ds_lang = new DataSet();
            imgload("MAX");
            imgload_nor("max");

        }

        private void LangFlag_Click(object sender, EventArgs e)
        {
            PictureBox flg_img = sender as PictureBox;
            pan_lang.BackgroundImage = Properties.Resources.process_enable_1;

            lbl_slt_lang_nor.Text = lb_invsble_sltd_nor.Text + "  " + flg_img.Name;
            lbl_slt_lang_max.Text = lb_invsble_sltd_max.Text + flg_img.Name;          

            truckslt();
        }

        private void truckslt()
        {
            lbl_title_lang.Visible = false;
            lbl_titlt_truck.Visible = true;

            tabControl1.SelectedIndex = 2;
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            sltd_truck = "INLOADER";
            lbl_slt_truck.Text = string.Empty;
            lbl_slt_truck.Text = lbl_invsble_slttruck.Text + lbl_trktype_Inloader.Text;

            pan_slt_inloader.Visible = true;
            pan_sltd_container.Visible = false;
            pan_sltd_euro.Visible = false;
            pan_sltd_other.Visible = false;

        }

        private void pb_trktype_Container_Click(object sender, EventArgs e)
        {
            sltd_truck = "CONTAINER";
            lbl_slt_truck.Text = string.Empty;
            lbl_slt_truck.Text = lbl_invsble_slttruck.Text + lbl_trktype_Container.Text;

            pan_slt_inloader.Visible = false;
            pan_sltd_container.Visible = true;
            pan_sltd_euro.Visible = false;
            pan_sltd_other.Visible = false;
        }

        private void pb_trktype_euro_Click(object sender, EventArgs e)
        {
            sltd_truck = "EURO";
            lbl_slt_truck.Text = string.Empty;
            lbl_slt_truck.Text = lbl_invsble_slttruck.Text + lbl_trktype_euro.Text;

            pan_slt_inloader.Visible = false;
            pan_sltd_container.Visible = false;
            pan_sltd_euro.Visible = true;
            pan_sltd_other.Visible = false;

        }

        private void pb_trktype_other_Click(object sender, EventArgs e)
        {
            sltd_truck = "OTHER";
            lbl_slt_truck.Text = string.Empty;
            lbl_slt_truck.Text = lbl_invsble_slttruck.Text + lbl_trktype_other.Text;

            pan_slt_inloader.Visible = false;
            pan_sltd_container.Visible = false;
            pan_sltd_euro.Visible = false;
            pan_sltd_other.Visible = true;
        }


        //pb_trktype_next_Click_1
        private void pictureBox13_Click_1(object sender, EventArgs e)
        {
            pan_truck.BackgroundImage = Properties.Resources.process_enable_1;
            lbl_titlt_truck.Visible = false;
            lbl_titlt_truckin.Visible = true;

            tabControl1.SelectedIndex = 3;
            truckin();
        }

        private void truckin()
        {

            pan_trkin1.Visible = false;
            pan_trkin2.Visible = false;
            pan_trkin3.Visible = false;
            pan_trkin4.Visible = false;
            pan_trkin5.Visible = false;

            pb_trkin1.Image = null;
            pb_trkin2.Image = null;
            pb_trkin3.Image = null;
            pb_trkin4.Image = null;
            pb_trkin5.Image = null;

            lbl_trkin1.Text = string.Empty;
            lbl_trkin2.Text = string.Empty;
            lbl_trkin3.Text = string.Empty;
            lbl_trkin4.Text = string.Empty;
            lbl_trkin5.Text = string.Empty;
            if (sltd_truck == "INLOADER")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
                pb_trkin4.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty;                
                pb_trkin3.Image = Properties.Resources.glass_2132;
                pb_trkin4.Image = Properties.Resources.empty_still;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "GLASS";
                pb_trkin4.Tag = "STILLAGE";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_glass.Text;
                lbl_trkin4.Text = lbl_still.Text;
            }
            if (sltd_truck == "CONTAINER")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
                pb_trkin4.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty;
                pb_trkin3.Image = Properties.Resources.glass_2132;
                pb_trkin4.Image = Properties.Resources.empty_still;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "GLASS";
                pb_trkin4.Tag = "STILLAGE";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_glass.Text;
                lbl_trkin4.Text = lbl_still.Text;


            }
            if (sltd_truck == "EURO")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
                pb_trkin4.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty;
                pb_trkin3.Image = Properties.Resources.glass_2132;
                pb_trkin4.Image = Properties.Resources.empty_still;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "GLASS";
                pb_trkin4.Tag = "STILLAGE";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_glass.Text;
                lbl_trkin4.Text = lbl_still.Text;
            }

            if (sltd_truck == "OTHER")
            {
                pb_trkin1.Tag = string.Empty;
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
                pb_trkin4.Tag = string.Empty;
                pb_trkin5.Tag = string.Empty;

                pb_trkin1.Image = Properties.Resources.empty;
                pb_trkin2.Image = Properties.Resources.glass_2132;
                pb_trkin3.Image = Properties.Resources.empty_still;
                pb_trkin4.Image = Properties.Resources.Cullet;
                pb_trkin5.Image = Properties.Resources.other_new_jpg;

                pb_trkin1.Tag = "EMPTY";
                pb_trkin2.Tag = "GLASS";
                pb_trkin3.Tag = "STILLAGE";
                pb_trkin4.Tag = "CULLET";
                pb_trkin5.Tag = "OTHER";

                lbl_trkin1.Text = lbl_empty.Text;
                lbl_trkin2.Text = lbl_glass.Text;
                lbl_trkin3.Text = lbl_still.Text;
                lbl_trkin4.Text = lbl_cullet.Text;
                lbl_trkin5.Text = lbl_other.Text;
            }
        }

        private void pb_trkin1_Click(object sender, EventArgs e)
        {
            if(pb_trkin1.Image != null)
            {
                sltd_in_product = string.Empty;
                sltd_in_product = pb_trkin1.Image.Tag.ToString();
                pan_trkin1.Visible = true;
                pan_trkin2.Visible = false;
                pan_trkin3.Visible = false;
                pan_trkin4.Visible = false;
                pan_trkin5.Visible = false;

            }

        }

        private void pb_trkin2_Click(object sender, EventArgs e)
        {
            if (pb_trkin2.Image != null)
            {
                sltd_in_product = string.Empty;
                sltd_in_product = pb_trkin2.Image.Tag.ToString();
                pan_trkin1.Visible = false;
                pan_trkin2.Visible = true;
                pan_trkin3.Visible = false;
                pan_trkin4.Visible = false;
                pan_trkin5.Visible = false;

            }
        }
      

        private void pb_trkin3_Click(object sender, EventArgs e)
        {
            if (pb_trkin3.Image != null)
            {
                sltd_in_product = string.Empty;
                sltd_in_product = pb_trkin3.Image.Tag.ToString();
                pan_trkin1.Visible = false;
                pan_trkin2.Visible = false;
                pan_trkin3.Visible = true;
                pan_trkin4.Visible = false;
                pan_trkin5.Visible = false;

            }

        }

        private void pb_trkin4_Click(object sender, EventArgs e)
        {
            if (pb_trkin4.Image != null)
            {
                sltd_in_product = string.Empty;
                sltd_in_product = pb_trkin4.Image.Tag.ToString();
                pan_trkin1.Visible = false;
                pan_trkin2.Visible = false;
                pan_trkin3.Visible = false;
                pan_trkin4.Visible = true;
                pan_trkin5.Visible = false;

            }
        }

        private void pb_trkin5_Click(object sender, EventArgs e)
        {
            if (pb_trkin5.Image != null)
            {
                sltd_in_product = string.Empty;
                sltd_in_product = pb_trkin5.Image.Tag.ToString();
                pan_trkin1.Visible = false;
                pan_trkin2.Visible = false;
                pan_trkin3.Visible = false;
                pan_trkin4.Visible = false;
                pan_trkin5.Visible = true;

            }
        }

        private void pb_trkin_next_Click_1(object sender, EventArgs e)
        {
            lbl_title_deli.Visible = true;
            lbl_titlt_truckin.Visible = false;
            tabControl1.SelectedIndex = 4;
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }




        private void imgload(string type)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RNO");
            dt.Columns.Add("TEXT");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("RNO");
            dt1.Columns.Add("TEXT");
            DataRow dr;

            string[] lnagname = new string[16];
            lnagname[0] = "English";
            lnagname[1] = "Français";
            lnagname[2] = "Germany";
            lnagname[3] = "Italiano";
            lnagname[4] = "Polski";
            lnagname[5] = "Portugues";
            lnagname[6] = "Romana";
            lnagname[7] = "Espanol";           
            lnagname[8] = "Australia";
            lnagname[9] = "Canada";
            lnagname[10] = "Czech";
            lnagname[11] = "Ελλάδα";
            lnagname[12] = "Estonia";
            lnagname[13] = "대한민국";
            lnagname[14] = "Malaysia";
            lnagname[15] = "Россия";

            for (int a = 0; a < 16; a++)
            {
                dr = dt.NewRow();
                dr["RNO"] = a.ToString();
                dr["TEXT"] = lnagname[a].ToString();
                dt.Rows.Add(dr);

                if (a < 4)
                {
                    dr = dt1.NewRow();
                    dr["RNO"] = a.ToString();
                    dr["TEXT"] = lnagname[a].ToString();
                    dt1.Rows.Add(dr);

                }
            }

            int x = 0;
            int xRef1 = 50;
            int yRef1 = 0;
            int y2ref = 0;
            int cnt = 0;
            int _distanceval = 170;

         

                foreach (DataRow dr1 in dt1.Rows)
                {


                    PictureBox pb = new PictureBox();
                    int XPos = panel11.Location.X;
                    int YPos = panel11.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label9.Location.X;
                    int lyPos = label9.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    // lbl.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl.Font = label9.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);

           
                panel10.Controls.Add(pb);
                    panel10.Controls.Add(lbl);
            
                    xRef1 += _distanceval;

                }
           
            xRef1 =50;
            yRef1 = 0;
            y2ref = 0;
            cnt = 0;

            
            foreach (DataRow dr1 in dt.Rows)
            {

                if (cnt < 4)
                {
                    PictureBox pb = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                  //  pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    //  lbl.Font = new Font("Open Sans", 10.0f, FontStyle.SemiBold);
                    lbl.Font = label10.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);

                 

                    panel13.Controls.Add(pb);
                    panel13.Controls.Add(lbl);
                    xRef1 += _distanceval;
                }
               else if (cnt < 8)
                {
                    
                    PictureBox pb1 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb1.Name = dr1["TEXT"].ToString();
                    pb1.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb1.Image = imageList1.Images[x];
                    pb1.AutoSize = true;
                    pb1.Cursor = Cursors.Hand;
                    pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    //pb1.SizeMode = PictureBoxSizeMode.Normal;
                    pb1.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb1.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_1 = new Label();
                    lbl_1.Text = dr1["TEXT"].ToString();
                    lbl_1.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_1.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_1.Font = label10.Font;
                    lbl_1.Size = new Size(22, 4);
                    lbl_1.AutoSize = true;
                    lbl_1.Location = new Point(lXPos + xRef1, lyPos + yRef1);

                    

                    panel13.Controls.Add(pb1);
                    panel13.Controls.Add(lbl_1);
                    xRef1 += _distanceval;
                }
              else  if (cnt < 12)
                {
                   
                    PictureBox pb2 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb2.Name = dr1["TEXT"].ToString();
                    pb2.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb2.Image = imageList1.Images[x];
                    pb2.AutoSize = true;
                    pb2.Cursor = Cursors.Hand;
                    pb2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    //pb2.SizeMode = PictureBoxSizeMode.Normal;
                    pb2.Location = new Point(XPos + xRef1, YPos + y2ref);
                    pb2.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_2 = new Label();
                    lbl_2.Text = dr1["TEXT"].ToString();
                    lbl_2.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_2.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_2.Font = label10.Font;
                    lbl_2.Size = new Size(22, 4);
                    lbl_2.AutoSize = true;
                    lbl_2.Location = new Point(lXPos + xRef1, lyPos + y2ref);

                  


                    panel13.Controls.Add(pb2);
                    panel13.Controls.Add(lbl_2);
                    xRef1 += _distanceval;
                }
              else  if (cnt < 16)
                {
                    PictureBox pb3 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb3.Name = dr1["TEXT"].ToString();
                    pb3.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb3.Image = imageList1.Images[x];
                    pb3.AutoSize = true;
                    pb3.Cursor = Cursors.Hand;
                    pb3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom)));
                    pb3.SizeMode = PictureBoxSizeMode.Normal;
                    pb3.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb3.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_3 = new Label();
                    lbl_3.Text = dr1["TEXT"].ToString();
                    lbl_3.Name = "lb" + dr1["TEXT"].ToString();
                    // lbl_3.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_3.Font = label10.Font;
                    lbl_3.Size = new Size(22, 4);
                    lbl_3.AutoSize = true;
                    lbl_3.Location = new Point(lXPos + xRef1, lyPos + yRef1);

                 
                 
                    panel13.Controls.Add(pb3);
                    panel13.Controls.Add(lbl_3);
                    xRef1 += _distanceval;
                }




                cnt = cnt + 1;
                if (cnt == 4)
                {

                    yRef1 = 0 + 120;
                    xRef1 = 50;
                }

                if (cnt == 8)
                {

                    y2ref = 0 + 240;
                    xRef1 = 50;
                }
                if (cnt == 12)
                {

                    yRef1 = 0 + 360;
                    xRef1 = 50;
                }

              


                //rW = panel10.Width;
                //rH = panel10.Height;

                //fW = panel10.Width;
                //fH = panel10.Height;
                //foreach (System.Windows.Forms.Control c in panel10.Controls)
                //{
                //    c.Tag = c.Name + "/" + c.Left + "/" + c.Top + "/" + c.Width + "/" + c.Height + "/" + (int)c.Font.Size;
                //   // dtSize.Rows.Add(c.Name, c.Left, c.Top, c.Width + 8, c.Height);
                //    // c.Anchor = (AnchorStyles.Right |  AnchorStyles.Left );  
                //}

                //  this.Resize += MyForm_Resize;
                //   this.WindowState = FormWindowState.Maximized;
                // Rectangle resolution = Screen.PrimaryScreen.Bounds;
                // this.Height = resolution.Size.Height;
                //  this.Width = resolution.Size.Width;
            }
        }

        private void imgload_nor(string type)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RNO");
            dt.Columns.Add("TEXT");
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("RNO");
            dt1.Columns.Add("TEXT");
            DataRow dr;
            string[] lnagname = new string[16];

            lnagname[0] = "Français";
            lnagname[1] = "Germany";
            lnagname[2] = "Italiano";
            lnagname[3] = "Polski";
            lnagname[4] = "Portugues";
            lnagname[5] = "Romana";
            lnagname[6] = "Espanol";
            lnagname[7] = "English";
            lnagname[8] = "Australia";
            lnagname[9] = "Canada";
            lnagname[10] = "Czech";
            lnagname[11] = "Ελλάδα";
            lnagname[12] = "Estonia";
            lnagname[13] = "대한민국";
            lnagname[14] = "Malaysia";
            lnagname[15] = "Россия";

            for (int a = 0; a < 16; a++)
            {
                dr = dt.NewRow();
                dr["RNO"] = a.ToString();
                dr["TEXT"] = lnagname[a].ToString();
                dt.Rows.Add(dr);
                if (a < 4)
                {
                    dr = dt1.NewRow();
                    dr["RNO"] = a.ToString();
                    dr["TEXT"] = lnagname[a].ToString();
                    dt1.Rows.Add(dr);

                }


            }

            int x = 0;
            int xRef1 = 50;
            int yRef1 = 0;
            int y2ref = 0;
            int cnt = 0;
            int _distanceval = 180;


         


                foreach (DataRow dr1 in dt1.Rows)
                {


                    PictureBox pb = new PictureBox();
                    int XPos = panel16.Location.X;
                    int YPos = panel16.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(140, 90);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList2.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )));
                    pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label11.Location.X;
                    int lyPos = label11.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    // lbl.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl.Font = label11.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                pb.Click += new EventHandler(LangFlag_Click);

             
                panel15.Controls.Add(pb);
                    panel15.Controls.Add(lbl);
                    xRef1 += _distanceval;

                }
          



            xRef1 = 50;
            yRef1 = 0;
            y2ref = 0;
            cnt = 0;


            foreach (DataRow dr1 in dt.Rows)
            {

                if (cnt < 6)
                {
                    PictureBox pb = new PictureBox();
                    int XPos = panel19.Location.X;
                    int YPos = panel19.Location.Y;
                    pb.Name = dr1["TEXT"].ToString();
                    pb.Size = new Size(140, 90);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb.Image = imageList2.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )));
                    //  pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label12.Location.X;
                    int lyPos = label12.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["TEXT"].ToString();
                    lbl.Name = "lb" + dr1["TEXT"].ToString();
                    //  lbl.Font = new Font("Open Sans", 10.0f, FontStyle.SemiBold);
                    lbl.Font = label12.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    pb.Click += new EventHandler(LangFlag_Click);

                    
                    panel18.Controls.Add(pb);
                    panel18.Controls.Add(lbl);
                    xRef1 += _distanceval;
                }
                else if (cnt < 12)
                {

                    PictureBox pb1 = new PictureBox();
                    int XPos = panel19.Location.X;
                    int YPos = panel19.Location.Y;
                    pb1.Name = dr1["TEXT"].ToString();
                    pb1.Size = new Size(140, 90);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb1.Image = imageList2.Images[x];
                    pb1.AutoSize = true;
                    pb1.Cursor = Cursors.Hand;
                    pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    //pb1.SizeMode = PictureBoxSizeMode.Normal;
                    pb1.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb1.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label12.Location.X;
                    int lyPos = label12.Location.Y;
                    Label lbl_1 = new Label();
                    lbl_1.Text = dr1["TEXT"].ToString();
                    lbl_1.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_1.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_1.Font = label12.Font;
                    lbl_1.Size = new Size(22, 4);
                    lbl_1.AutoSize = true;
                    lbl_1.Location = new Point(lXPos + xRef1, lyPos + yRef1);
                    pb1.Click += new EventHandler(LangFlag_Click);

                   
                    panel18.Controls.Add(pb1);
                    panel18.Controls.Add(lbl_1);
                    xRef1 += _distanceval;
                }
                else if (cnt < 16)
                {

                    PictureBox pb2 = new PictureBox();
                    int XPos = panel19.Location.X;
                    int YPos = panel19.Location.Y;
                    pb2.Name = dr1["TEXT"].ToString();
                    pb2.Size = new Size(140, 90);
                    x = Convert.ToInt32(dr1[0].ToString());
                    pb2.Image = imageList2.Images[x];
                    pb2.AutoSize = true;
                    pb2.Cursor = Cursors.Hand;
                    pb2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    //pb2.SizeMode = PictureBoxSizeMode.Normal;
                    pb2.Location = new Point(XPos + xRef1, YPos + y2ref);
                    pb2.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label12.Location.X;
                    int lyPos = label12.Location.Y;
                    Label lbl_2 = new Label();
                    lbl_2.Text = dr1["TEXT"].ToString();
                    lbl_2.Name = "lb" + dr1["TEXT"].ToString();
                    //lbl_2.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_2.Font = label12.Font;
                    lbl_2.Size = new Size(22, 4);
                    lbl_2.AutoSize = true;
                    lbl_2.Location = new Point(lXPos + xRef1, lyPos + y2ref);
                    pb2.Click += new EventHandler(LangFlag_Click);

                
                    panel18.Controls.Add(pb2);
                    panel18.Controls.Add(lbl_2);
                    xRef1 += _distanceval;
                }
        



                cnt = cnt + 1;
                if (cnt == 6)
                {

                    yRef1 = 0 + 120;
                    xRef1 = 50;
                }

                if (cnt == 12)
                {

                    y2ref = 0 + 240;
                    xRef1 = 50;
                }
               
            }
        }

        private void FrmTruckinterface_Resize(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex < 2)
            {
                if (this.Height < 950)
                {
                    tabControl1.SelectedIndex = 0;
                }
                else
                {

                    tabControl1.SelectedIndex = 1;
                }
            }

        }




        private void pictureBox12_Click(object sender, EventArgs e)
        {
            if (this.Height < 950)
            {
                tabControl1.SelectedIndex = 0;
            }
            else
            {

                tabControl1.SelectedIndex = 1;
            }
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {

        }

       
       

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pb_trkout_next_Click(object sender, EventArgs e)
        {
           lbl_title_out.Visible = false;
           lbl_title_returnorder.Visible = true;
            tabControl1.SelectedIndex = 6;
        }

        private void pb_sale_next_Click(object sender, EventArgs e)
        {
           lbl_title_returnorder.Visible = false;
            lbl_title_info.Visible = true;
            tabControl1.SelectedIndex = 7;
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            
            lbl_title_info.Visible = false;
           // lbl_ti.Visible = true;
            tabControl1.SelectedIndex = 8;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            lbl_title_deli.Visible = false;
            lbl_title_out.Visible = true;
            tabControl1.SelectedIndex = 5;
        }

     

      

      
    }
}
