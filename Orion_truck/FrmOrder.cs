﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orion_truck
{
    public partial class FrmOrder : Form
    {
        public FrmOrder()
        {
            InitializeComponent();
        }

        private void pb_r1_Click(object sender, EventArgs e)
        {
            this.Hide();  
            FrmTruck frm = new FrmTruck();
            frm.Show();
        }
    }
}
