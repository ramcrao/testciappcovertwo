﻿namespace Orion_truck
{
    partial class FrmTruckinterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTruckinterface));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_head = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.pan_lang = new System.Windows.Forms.Panel();
            this.lbl_pro_sltlang = new System.Windows.Forms.Label();
            this.pan_truck = new System.Windows.Forms.Panel();
            this.lbl_pro_sltTruck = new System.Windows.Forms.Label();
            this.pan_del = new System.Windows.Forms.Panel();
            this.lbl_pro_deli = new System.Windows.Forms.Label();
            this.pan_order = new System.Windows.Forms.Panel();
            this.lbl_pro_order = new System.Windows.Forms.Label();
            this.pan_info = new System.Windows.Forms.Panel();
            this.lbl_pro_info = new System.Windows.Forms.Label();
            this.pan_comp = new System.Windows.Forms.Panel();
            this.lbl_pro_complete = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lbl_title_info = new System.Windows.Forms.Label();
            this.lbl_title_returnorder = new System.Windows.Forms.Label();
            this.lbl_title_out = new System.Windows.Forms.Label();
            this.lbl_title_deli = new System.Windows.Forms.Label();
            this.lbl_title_lang = new System.Windows.Forms.Label();
            this.lbl_titlt_truck = new System.Windows.Forms.Label();
            this.lbl_titlt_truckin = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_lang = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lbl_lang_otrlnag_nor = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lb_invsble_sltd_nor = new System.Windows.Forms.Label();
            this.lbl_lang_rsnsel_nor = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lbl_slt_lang_nor = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_slt_lang_max = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.lbl_lang_otherlang_max = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.lb_invsble_sltd_max = new System.Windows.Forms.Label();
            this.lb_lang_rsnslt_max = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.pb_trktype_inloader = new System.Windows.Forms.PictureBox();
            this.pb_trktype_Container = new System.Windows.Forms.PictureBox();
            this.pb_trktype_euro = new System.Windows.Forms.PictureBox();
            this.pb_trktype_other = new System.Windows.Forms.PictureBox();
            this.lbl_trktype_Inloader = new System.Windows.Forms.Label();
            this.lbl_trktype_euro = new System.Windows.Forms.Label();
            this.lbl_trktype_Container = new System.Windows.Forms.Label();
            this.lbl_trktype_other = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.lbl_slt_truck = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.pb_trktype_next = new System.Windows.Forms.PictureBox();
            this.pb_tryktype_back = new System.Windows.Forms.PictureBox();
            this.lbl_invsble_slttruck = new System.Windows.Forms.Label();
            this.pan_slt_inloader = new System.Windows.Forms.Panel();
            this.pan_sltd_container = new System.Windows.Forms.Panel();
            this.pan_sltd_euro = new System.Windows.Forms.Panel();
            this.pan_sltd_other = new System.Windows.Forms.Panel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_slt_truckin = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.pb_trkin2 = new System.Windows.Forms.PictureBox();
            this.pb_trkin1 = new System.Windows.Forms.PictureBox();
            this.pb_trkin3 = new System.Windows.Forms.PictureBox();
            this.pb_trkin4 = new System.Windows.Forms.PictureBox();
            this.pb_trkin5 = new System.Windows.Forms.PictureBox();
            this.pan_trkin1 = new System.Windows.Forms.Panel();
            this.pan_trkin2 = new System.Windows.Forms.Panel();
            this.pan_trkin3 = new System.Windows.Forms.Panel();
            this.pan_trkin4 = new System.Windows.Forms.Panel();
            this.pan_trkin5 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkin1 = new System.Windows.Forms.Label();
            this.lbl_trkin2 = new System.Windows.Forms.Label();
            this.lbl_trkin3 = new System.Windows.Forms.Label();
            this.lbl_trkin4 = new System.Windows.Forms.Label();
            this.lbl_trkin5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lbl_other = new System.Windows.Forms.Label();
            this.lbl_cullet = new System.Windows.Forms.Label();
            this.lbl_still = new System.Windows.Forms.Label();
            this.lbl_glass = new System.Windows.Forms.Label();
            this.lbl_empty = new System.Windows.Forms.Label();
            this.lbl_invsble_trkinslt = new System.Windows.Forms.Label();
            this.pb_trkin_next = new System.Windows.Forms.PictureBox();
            this.pb_trkin_back = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_pono = new System.Windows.Forms.Label();
            this.lbl_expo = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_sg_still = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_del_stil = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_nonsg_still = new System.Windows.Forms.Label();
            this.lbl_exstill = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkout1 = new System.Windows.Forms.Label();
            this.lbl_trkout2 = new System.Windows.Forms.Label();
            this.lbl_trkout3 = new System.Windows.Forms.Label();
            this.lbl_trkout4 = new System.Windows.Forms.Label();
            this.lbl_trkout5 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbl_slt_truckout = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.pb_trkout1 = new System.Windows.Forms.PictureBox();
            this.pb_trkout2 = new System.Windows.Forms.PictureBox();
            this.pb_trkout3 = new System.Windows.Forms.PictureBox();
            this.pb_trkout4 = new System.Windows.Forms.PictureBox();
            this.pb_trkout5 = new System.Windows.Forms.PictureBox();
            this.pan_trkout1 = new System.Windows.Forms.Panel();
            this.pan_trkout2 = new System.Windows.Forms.Panel();
            this.pan_trkout3 = new System.Windows.Forms.Panel();
            this.pan_trkout4 = new System.Windows.Forms.Panel();
            this.pan_trkout5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.pb_trkout_next = new System.Windows.Forms.PictureBox();
            this.pb_trkout_back = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_saleno = new System.Windows.Forms.Label();
            this.lbl_ex_so = new System.Windows.Forms.Label();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.pb_sale_next = new System.Windows.Forms.PictureBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo_truckno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_extruckno = new System.Windows.Forms.Label();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo_trailrno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_extrailrno = new System.Windows.Forms.Label();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo_name = new System.Windows.Forms.Label();
            this.lbl_trkinfo_exname = new System.Windows.Forms.Label();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo_mobno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_exmobno = new System.Windows.Forms.Label();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox11 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo1_trkno = new System.Windows.Forms.Label();
            this.lbl_trkinfo1_extrkno = new System.Windows.Forms.Label();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo1_tralrno = new System.Windows.Forms.Label();
            this.lbl_trkinfo1_extralrno = new System.Windows.Forms.Label();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo1_name = new System.Windows.Forms.Label();
            this.lbl_trkinfo1_exname = new System.Windows.Forms.Label();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo1_mobno = new System.Windows.Forms.Label();
            this.lbl_trkinfo1_exmobno = new System.Windows.Forms.Label();
            this.richTextBox12 = new System.Windows.Forms.RichTextBox();
            this.richTextBox13 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox14 = new System.Windows.Forms.RichTextBox();
            this.richTextBox15 = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel49 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_trkinfo1_conno = new System.Windows.Forms.Label();
            this.lbl_trkinfo1_exconno = new System.Windows.Forms.Label();
            this.richTextBox16 = new System.Windows.Forms.RichTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_comp_thanks = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_com_order = new System.Windows.Forms.Label();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_com_confirm = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.pan_lang.SuspendLayout();
            this.pan_truck.SuspendLayout();
            this.pan_del.SuspendLayout();
            this.pan_order.SuspendLayout();
            this.pan_info.SuspendLayout();
            this.pan_comp.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tab_lang.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_inloader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_Container)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_euro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_other)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_tryktype_back)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin5)).BeginInit();
            this.panel21.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin_next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin_back)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tableLayoutPanel24.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout5)).BeginInit();
            this.tableLayoutPanel25.SuspendLayout();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout_next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout_back)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sale_next)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.panel20.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage9.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.panel25.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.tableLayoutPanel49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.panel32.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.panel33.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.tableLayoutPanel34.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1006, 687);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 42);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lbl_head, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(996, 38);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lbl_head
            // 
            this.lbl_head.AutoSize = true;
            this.lbl_head.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_head.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_head.Location = new System.Drawing.Point(10, 0);
            this.lbl_head.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_head.Name = "lbl_head";
            this.lbl_head.Size = new System.Drawing.Size(983, 28);
            this.lbl_head.TabIndex = 0;
            this.lbl_head.Text = "Truck Interface";
            this.lbl_head.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1000, 62);
            this.panel2.TabIndex = 3;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1000, 62);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel4.Controls.Add(this.pan_lang, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.pan_truck, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.pan_del, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.pan_order, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.pan_info, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.pan_comp, 5, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(994, 56);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // pan_lang
            // 
            this.pan_lang.BackgroundImage = global::Orion_truck.Properties.Resources.process_enable_1;
            this.pan_lang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_lang.Controls.Add(this.lbl_pro_sltlang);
            this.pan_lang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_lang.Location = new System.Drawing.Point(3, 3);
            this.pan_lang.Name = "pan_lang";
            this.pan_lang.Size = new System.Drawing.Size(153, 50);
            this.pan_lang.TabIndex = 0;
            // 
            // lbl_pro_sltlang
            // 
            this.lbl_pro_sltlang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_sltlang.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_sltlang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_sltlang.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_sltlang.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_sltlang.Name = "lbl_pro_sltlang";
            this.lbl_pro_sltlang.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_sltlang.TabIndex = 0;
            this.lbl_pro_sltlang.Text = "Choose your Language";
            this.lbl_pro_sltlang.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_truck
            // 
            this.pan_truck.BackgroundImage = global::Orion_truck.Properties.Resources.process_dis_1;
            this.pan_truck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_truck.Controls.Add(this.lbl_pro_sltTruck);
            this.pan_truck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_truck.Location = new System.Drawing.Point(162, 3);
            this.pan_truck.Name = "pan_truck";
            this.pan_truck.Size = new System.Drawing.Size(153, 50);
            this.pan_truck.TabIndex = 1;
            // 
            // lbl_pro_sltTruck
            // 
            this.lbl_pro_sltTruck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_sltTruck.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_sltTruck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_sltTruck.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_sltTruck.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_sltTruck.Name = "lbl_pro_sltTruck";
            this.lbl_pro_sltTruck.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_sltTruck.TabIndex = 0;
            this.lbl_pro_sltTruck.Text = " Select your  Truck Type";
            this.lbl_pro_sltTruck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_del
            // 
            this.pan_del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_del.BackgroundImage")));
            this.pan_del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_del.Controls.Add(this.lbl_pro_deli);
            this.pan_del.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_del.Location = new System.Drawing.Point(321, 3);
            this.pan_del.Name = "pan_del";
            this.pan_del.Size = new System.Drawing.Size(162, 50);
            this.pan_del.TabIndex = 2;
            // 
            // lbl_pro_deli
            // 
            this.lbl_pro_deli.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_deli.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_deli.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_deli.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_deli.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_deli.Name = "lbl_pro_deli";
            this.lbl_pro_deli.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_deli.TabIndex = 0;
            this.lbl_pro_deli.Text = "Your Delivery";
            this.lbl_pro_deli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_order
            // 
            this.pan_order.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_order.BackgroundImage")));
            this.pan_order.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_order.Controls.Add(this.lbl_pro_order);
            this.pan_order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_order.Location = new System.Drawing.Point(489, 3);
            this.pan_order.Name = "pan_order";
            this.pan_order.Size = new System.Drawing.Size(162, 50);
            this.pan_order.TabIndex = 3;
            // 
            // lbl_pro_order
            // 
            this.lbl_pro_order.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_order.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_order.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_order.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_order.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_order.Name = "lbl_pro_order";
            this.lbl_pro_order.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_order.TabIndex = 0;
            this.lbl_pro_order.Text = "Your Order";
            this.lbl_pro_order.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_info
            // 
            this.pan_info.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_info.BackgroundImage")));
            this.pan_info.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_info.Controls.Add(this.lbl_pro_info);
            this.pan_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_info.Location = new System.Drawing.Point(657, 3);
            this.pan_info.Name = "pan_info";
            this.pan_info.Size = new System.Drawing.Size(162, 50);
            this.pan_info.TabIndex = 4;
            // 
            // lbl_pro_info
            // 
            this.lbl_pro_info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_info.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_info.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_info.Location = new System.Drawing.Point(29, 7);
            this.lbl_pro_info.Name = "lbl_pro_info";
            this.lbl_pro_info.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_info.TabIndex = 0;
            this.lbl_pro_info.Text = "Truck  Information";
            this.lbl_pro_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_comp
            // 
            this.pan_comp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_comp.BackgroundImage")));
            this.pan_comp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_comp.Controls.Add(this.lbl_pro_complete);
            this.pan_comp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_comp.Location = new System.Drawing.Point(825, 3);
            this.pan_comp.Name = "pan_comp";
            this.pan_comp.Size = new System.Drawing.Size(166, 50);
            this.pan_comp.TabIndex = 5;
            // 
            // lbl_pro_complete
            // 
            this.lbl_pro_complete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_complete.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_complete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_complete.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_complete.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_complete.Name = "lbl_pro_complete";
            this.lbl_pro_complete.Size = new System.Drawing.Size(113, 35);
            this.lbl_pro_complete.TabIndex = 0;
            this.lbl_pro_complete.Text = "Registration Completed";
            this.lbl_pro_complete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lbl_title_info);
            this.panel6.Controls.Add(this.lbl_title_returnorder);
            this.panel6.Controls.Add(this.lbl_title_out);
            this.panel6.Controls.Add(this.lbl_title_deli);
            this.panel6.Controls.Add(this.lbl_title_lang);
            this.panel6.Controls.Add(this.lbl_titlt_truck);
            this.panel6.Controls.Add(this.lbl_titlt_truckin);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 119);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1000, 35);
            this.panel6.TabIndex = 5;
            // 
            // lbl_title_info
            // 
            this.lbl_title_info.AutoSize = true;
            this.lbl_title_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_title_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_info.Location = new System.Drawing.Point(0, 0);
            this.lbl_title_info.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_title_info.Name = "lbl_title_info";
            this.lbl_title_info.Size = new System.Drawing.Size(281, 24);
            this.lbl_title_info.TabIndex = 7;
            this.lbl_title_info.Text = "Enter your Truck Information.";
            this.lbl_title_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_returnorder
            // 
            this.lbl_title_returnorder.AutoSize = true;
            this.lbl_title_returnorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_returnorder.Location = new System.Drawing.Point(5, 2);
            this.lbl_title_returnorder.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_title_returnorder.Name = "lbl_title_returnorder";
            this.lbl_title_returnorder.Size = new System.Drawing.Size(197, 24);
            this.lbl_title_returnorder.TabIndex = 6;
            this.lbl_title_returnorder.Text = "Enter the Order no. ";
            this.lbl_title_returnorder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_out
            // 
            this.lbl_title_out.AutoSize = true;
            this.lbl_title_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_title_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_out.Location = new System.Drawing.Point(0, 0);
            this.lbl_title_out.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_title_out.Name = "lbl_title_out";
            this.lbl_title_out.Size = new System.Drawing.Size(415, 24);
            this.lbl_title_out.TabIndex = 5;
            this.lbl_title_out.Text = "After Unloading will you load a new Order ?";
            this.lbl_title_out.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_deli
            // 
            this.lbl_title_deli.AutoSize = true;
            this.lbl_title_deli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_title_deli.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_deli.Location = new System.Drawing.Point(0, 0);
            this.lbl_title_deli.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_title_deli.Name = "lbl_title_deli";
            this.lbl_title_deli.Size = new System.Drawing.Size(566, 24);
            this.lbl_title_deli.TabIndex = 2;
            this.lbl_title_deli.Text = "Enter your incoming delivery number (Purchase Order no.) ";
            this.lbl_title_deli.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_lang
            // 
            this.lbl_title_lang.AutoSize = true;
            this.lbl_title_lang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_title_lang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_lang.Location = new System.Drawing.Point(0, 0);
            this.lbl_title_lang.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_title_lang.Name = "lbl_title_lang";
            this.lbl_title_lang.Size = new System.Drawing.Size(217, 24);
            this.lbl_title_lang.TabIndex = 1;
            this.lbl_title_lang.Text = "Select Your Language";
            this.lbl_title_lang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_titlt_truck
            // 
            this.lbl_titlt_truck.AutoSize = true;
            this.lbl_titlt_truck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_titlt_truck.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truck.Location = new System.Drawing.Point(0, 0);
            this.lbl_titlt_truck.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_titlt_truck.Name = "lbl_titlt_truck";
            this.lbl_titlt_truck.Size = new System.Drawing.Size(193, 24);
            this.lbl_titlt_truck.TabIndex = 4;
            this.lbl_titlt_truck.Text = "What kind of truck ?";
            this.lbl_titlt_truck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_titlt_truckin
            // 
            this.lbl_titlt_truckin.AutoSize = true;
            this.lbl_titlt_truckin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_titlt_truckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckin.Location = new System.Drawing.Point(0, 0);
            this.lbl_titlt_truckin.Margin = new System.Windows.Forms.Padding(10, 0, 3, 10);
            this.lbl_titlt_truckin.Name = "lbl_titlt_truckin";
            this.lbl_titlt_truckin.Size = new System.Drawing.Size(437, 24);
            this.lbl_titlt_truckin.TabIndex = 3;
            this.lbl_titlt_truckin.Text = "Do you arrive empty or with goods to deliver? ";
            this.lbl_titlt_truckin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tab_lang);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(30, 30);
            this.tabControl1.Location = new System.Drawing.Point(3, 160);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1000, 524);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tab_lang
            // 
            this.tab_lang.Controls.Add(this.tableLayoutPanel5);
            this.tab_lang.Location = new System.Drawing.Point(4, 34);
            this.tab_lang.Name = "tab_lang";
            this.tab_lang.Padding = new System.Windows.Forms.Padding(3);
            this.tab_lang.Size = new System.Drawing.Size(992, 486);
            this.tab_lang.TabIndex = 0;
            this.tab_lang.Text = "Language";
            this.tab_lang.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.panel13, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.panel12, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lbl_slt_lang_nor, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // panel13
            // 
            this.panel13.AutoScroll = true;
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.Controls.Add(this.lbl_lang_otrlnag_nor);
            this.panel13.Controls.Add(this.label10);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(3, 166);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(980, 311);
            this.panel13.TabIndex = 2;
            // 
            // lbl_lang_otrlnag_nor
            // 
            this.lbl_lang_otrlnag_nor.AutoSize = true;
            this.lbl_lang_otrlnag_nor.Location = new System.Drawing.Point(6, 6);
            this.lbl_lang_otrlnag_nor.Name = "lbl_lang_otrlnag_nor";
            this.lbl_lang_otrlnag_nor.Size = new System.Drawing.Size(84, 13);
            this.lbl_lang_otrlnag_nor.TabIndex = 4;
            this.lbl_lang_otrlnag_nor.Text = "Other Language";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 16);
            this.label10.TabIndex = 2;
            this.label10.Tag = "";
            this.label10.Text = "label10";
            this.label10.Visible = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Silver;
            this.panel14.Location = new System.Drawing.Point(3, 25);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 98);
            this.panel14.TabIndex = 0;
            this.panel14.Visible = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel10);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 27);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(980, 133);
            this.panel12.TabIndex = 1;
            // 
            // panel10
            // 
            this.panel10.AutoScroll = true;
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.lb_invsble_sltd_nor);
            this.panel10.Controls.Add(this.lbl_lang_rsnsel_nor);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(980, 133);
            this.panel10.TabIndex = 1;
            // 
            // lb_invsble_sltd_nor
            // 
            this.lb_invsble_sltd_nor.AutoSize = true;
            this.lb_invsble_sltd_nor.Location = new System.Drawing.Point(193, 7);
            this.lb_invsble_sltd_nor.Name = "lb_invsble_sltd_nor";
            this.lb_invsble_sltd_nor.Size = new System.Drawing.Size(100, 13);
            this.lb_invsble_sltd_nor.TabIndex = 6;
            this.lb_invsble_sltd_nor.Text = "Selected Language";
            this.lb_invsble_sltd_nor.Visible = false;
            // 
            // lbl_lang_rsnsel_nor
            // 
            this.lbl_lang_rsnsel_nor.AutoSize = true;
            this.lbl_lang_rsnsel_nor.Location = new System.Drawing.Point(6, 7);
            this.lbl_lang_rsnsel_nor.Name = "lbl_lang_rsnsel_nor";
            this.lbl_lang_rsnsel_nor.Size = new System.Drawing.Size(89, 13);
            this.lbl_lang_rsnsel_nor.TabIndex = 5;
            this.lbl_lang_rsnsel_nor.Text = "Recent Selection";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 1;
            this.label9.Tag = "";
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(3, 28);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(11, 104);
            this.panel11.TabIndex = 0;
            // 
            // lbl_slt_lang_nor
            // 
            this.lbl_slt_lang_nor.AutoSize = true;
            this.lbl_slt_lang_nor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_slt_lang_nor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_lang_nor.Location = new System.Drawing.Point(3, 0);
            this.lbl_slt_lang_nor.Name = "lbl_slt_lang_nor";
            this.lbl_slt_lang_nor.Size = new System.Drawing.Size(980, 24);
            this.lbl_slt_lang_nor.TabIndex = 3;
            this.lbl_slt_lang_nor.Text = "Selected Language  Français";
            this.lbl_slt_lang_nor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(992, 486);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Lang_max";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.lbl_slt_lang_max, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel18, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.panel17, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lbl_slt_lang_max
            // 
            this.lbl_slt_lang_max.AutoSize = true;
            this.lbl_slt_lang_max.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_slt_lang_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_lang_max.Location = new System.Drawing.Point(3, 0);
            this.lbl_slt_lang_max.Name = "lbl_slt_lang_max";
            this.lbl_slt_lang_max.Size = new System.Drawing.Size(980, 24);
            this.lbl_slt_lang_max.TabIndex = 5;
            this.lbl_slt_lang_max.Text = "Selected Language  Français";
            this.lbl_slt_lang_max.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel18
            // 
            this.panel18.AutoScroll = true;
            this.panel18.BackColor = System.Drawing.Color.White;
            this.panel18.Controls.Add(this.lbl_lang_otherlang_max);
            this.panel18.Controls.Add(this.label12);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(3, 180);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(980, 297);
            this.panel18.TabIndex = 2;
            // 
            // lbl_lang_otherlang_max
            // 
            this.lbl_lang_otherlang_max.AutoSize = true;
            this.lbl_lang_otherlang_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lang_otherlang_max.Location = new System.Drawing.Point(13, 4);
            this.lbl_lang_otherlang_max.Name = "lbl_lang_otherlang_max";
            this.lbl_lang_otherlang_max.Size = new System.Drawing.Size(104, 16);
            this.lbl_lang_otherlang_max.TabIndex = 3;
            this.lbl_lang_otherlang_max.Text = "Other Language";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 16);
            this.label12.TabIndex = 2;
            this.label12.Tag = "";
            this.label12.Text = "label12";
            this.label12.Visible = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.White;
            this.panel19.Location = new System.Drawing.Point(3, 29);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(10, 98);
            this.panel19.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.panel15);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(3, 27);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(980, 147);
            this.panel17.TabIndex = 1;
            // 
            // panel15
            // 
            this.panel15.AutoScroll = true;
            this.panel15.BackColor = System.Drawing.Color.White;
            this.panel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel15.Controls.Add(this.lb_invsble_sltd_max);
            this.panel15.Controls.Add(this.lb_lang_rsnslt_max);
            this.panel15.Controls.Add(this.label11);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(980, 147);
            this.panel15.TabIndex = 1;
            // 
            // lb_invsble_sltd_max
            // 
            this.lb_invsble_sltd_max.AutoSize = true;
            this.lb_invsble_sltd_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_invsble_sltd_max.Location = new System.Drawing.Point(152, 5);
            this.lb_invsble_sltd_max.Name = "lb_invsble_sltd_max";
            this.lb_invsble_sltd_max.Size = new System.Drawing.Size(122, 16);
            this.lb_invsble_sltd_max.TabIndex = 5;
            this.lb_invsble_sltd_max.Text = "Selected language";
            this.lb_invsble_sltd_max.Visible = false;
            // 
            // lb_lang_rsnslt_max
            // 
            this.lb_lang_rsnslt_max.AutoSize = true;
            this.lb_lang_rsnslt_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_lang_rsnslt_max.Location = new System.Drawing.Point(13, 5);
            this.lb_lang_rsnslt_max.Name = "lb_lang_rsnslt_max";
            this.lb_lang_rsnslt_max.Size = new System.Drawing.Size(110, 16);
            this.lb_lang_rsnslt_max.TabIndex = 4;
            this.lb_lang_rsnslt_max.Text = "Recent Selection";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 16);
            this.label11.TabIndex = 1;
            this.label11.Tag = "";
            this.label11.Text = "label11";
            this.label11.Visible = false;
            // 
            // panel16
            // 
            this.panel16.Location = new System.Drawing.Point(3, 29);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(11, 104);
            this.panel16.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.tableLayoutPanel8);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(992, 486);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "truck";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 7;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tableLayoutPanel8.Controls.Add(this.pb_trktype_inloader, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.pb_trktype_Container, 5, 1);
            this.tableLayoutPanel8.Controls.Add(this.pb_trktype_euro, 2, 4);
            this.tableLayoutPanel8.Controls.Add(this.pb_trktype_other, 5, 4);
            this.tableLayoutPanel8.Controls.Add(this.lbl_trktype_Inloader, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.lbl_trktype_euro, 2, 5);
            this.tableLayoutPanel8.Controls.Add(this.lbl_trktype_Container, 5, 2);
            this.tableLayoutPanel8.Controls.Add(this.lbl_trktype_other, 5, 5);
            this.tableLayoutPanel8.Controls.Add(this.panel22, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel23, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.pan_slt_inloader, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.pan_sltd_container, 4, 1);
            this.tableLayoutPanel8.Controls.Add(this.pan_sltd_euro, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.pan_sltd_other, 4, 4);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 7;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // pb_trktype_inloader
            // 
            this.pb_trktype_inloader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_inloader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trktype_inloader.Image = global::Orion_truck.Properties.Resources._3;
            this.pb_trktype_inloader.Location = new System.Drawing.Point(150, 51);
            this.pb_trktype_inloader.Name = "pb_trktype_inloader";
            this.pb_trktype_inloader.Size = new System.Drawing.Size(319, 147);
            this.pb_trktype_inloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_inloader.TabIndex = 0;
            this.pb_trktype_inloader.TabStop = false;
            this.pb_trktype_inloader.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pb_trktype_Container
            // 
            this.pb_trktype_Container.BackColor = System.Drawing.Color.White;
            this.pb_trktype_Container.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trktype_Container.Image = global::Orion_truck.Properties.Resources._1;
            this.pb_trktype_Container.Location = new System.Drawing.Point(523, 51);
            this.pb_trktype_Container.Name = "pb_trktype_Container";
            this.pb_trktype_Container.Size = new System.Drawing.Size(319, 147);
            this.pb_trktype_Container.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_Container.TabIndex = 1;
            this.pb_trktype_Container.TabStop = false;
            this.pb_trktype_Container.Click += new System.EventHandler(this.pb_trktype_Container_Click);
            // 
            // pb_trktype_euro
            // 
            this.pb_trktype_euro.BackColor = System.Drawing.Color.White;
            this.pb_trktype_euro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_euro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trktype_euro.Image = global::Orion_truck.Properties.Resources._2;
            this.pb_trktype_euro.Location = new System.Drawing.Point(150, 246);
            this.pb_trktype_euro.Name = "pb_trktype_euro";
            this.pb_trktype_euro.Size = new System.Drawing.Size(319, 147);
            this.pb_trktype_euro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_euro.TabIndex = 2;
            this.pb_trktype_euro.TabStop = false;
            this.pb_trktype_euro.Click += new System.EventHandler(this.pb_trktype_euro_Click);
            // 
            // pb_trktype_other
            // 
            this.pb_trktype_other.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_other.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trktype_other.Image = global::Orion_truck.Properties.Resources._4;
            this.pb_trktype_other.Location = new System.Drawing.Point(523, 246);
            this.pb_trktype_other.Name = "pb_trktype_other";
            this.pb_trktype_other.Size = new System.Drawing.Size(319, 147);
            this.pb_trktype_other.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_other.TabIndex = 3;
            this.pb_trktype_other.TabStop = false;
            this.pb_trktype_other.Click += new System.EventHandler(this.pb_trktype_other_Click);
            // 
            // lbl_trktype_Inloader
            // 
            this.lbl_trktype_Inloader.AutoSize = true;
            this.lbl_trktype_Inloader.BackColor = System.Drawing.Color.White;
            this.lbl_trktype_Inloader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trktype_Inloader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_Inloader.Location = new System.Drawing.Point(150, 201);
            this.lbl_trktype_Inloader.Name = "lbl_trktype_Inloader";
            this.lbl_trktype_Inloader.Size = new System.Drawing.Size(319, 33);
            this.lbl_trktype_Inloader.TabIndex = 4;
            this.lbl_trktype_Inloader.Text = "Inloader";
            this.lbl_trktype_Inloader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_euro
            // 
            this.lbl_trktype_euro.AutoSize = true;
            this.lbl_trktype_euro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trktype_euro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_euro.Location = new System.Drawing.Point(150, 396);
            this.lbl_trktype_euro.Name = "lbl_trktype_euro";
            this.lbl_trktype_euro.Size = new System.Drawing.Size(319, 33);
            this.lbl_trktype_euro.TabIndex = 5;
            this.lbl_trktype_euro.Text = "Euro Linear";
            this.lbl_trktype_euro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_Container
            // 
            this.lbl_trktype_Container.AutoSize = true;
            this.lbl_trktype_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trktype_Container.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_Container.Location = new System.Drawing.Point(523, 201);
            this.lbl_trktype_Container.Name = "lbl_trktype_Container";
            this.lbl_trktype_Container.Size = new System.Drawing.Size(319, 33);
            this.lbl_trktype_Container.TabIndex = 6;
            this.lbl_trktype_Container.Text = "Container";
            this.lbl_trktype_Container.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_other
            // 
            this.lbl_trktype_other.AutoSize = true;
            this.lbl_trktype_other.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trktype_other.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_other.Location = new System.Drawing.Point(523, 396);
            this.lbl_trktype_other.Name = "lbl_trktype_other";
            this.lbl_trktype_other.Size = new System.Drawing.Size(319, 33);
            this.lbl_trktype_other.TabIndex = 7;
            this.lbl_trktype_other.Text = "Others";
            this.lbl_trktype_other.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel22
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.panel22, 7);
            this.panel22.Controls.Add(this.lbl_slt_truck);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(980, 42);
            this.panel22.TabIndex = 10;
            // 
            // lbl_slt_truck
            // 
            this.lbl_slt_truck.BackColor = System.Drawing.Color.White;
            this.lbl_slt_truck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_slt_truck.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_truck.Location = new System.Drawing.Point(0, 0);
            this.lbl_slt_truck.Name = "lbl_slt_truck";
            this.lbl_slt_truck.Size = new System.Drawing.Size(980, 42);
            this.lbl_slt_truck.TabIndex = 0;
            this.lbl_slt_truck.Text = "Selected truk  Inloader";
            this.lbl_slt_truck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel23
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.panel23, 7);
            this.panel23.Controls.Add(this.pb_trktype_next);
            this.panel23.Controls.Add(this.pb_tryktype_back);
            this.panel23.Controls.Add(this.lbl_invsble_slttruck);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(3, 432);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(980, 45);
            this.panel23.TabIndex = 11;
            // 
            // pb_trktype_next
            // 
            this.pb_trktype_next.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_trktype_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_next.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pb_trktype_next.Location = new System.Drawing.Point(848, 3);
            this.pb_trktype_next.Name = "pb_trktype_next";
            this.pb_trktype_next.Size = new System.Drawing.Size(114, 35);
            this.pb_trktype_next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trktype_next.TabIndex = 13;
            this.pb_trktype_next.TabStop = false;
            this.pb_trktype_next.Click += new System.EventHandler(this.pictureBox13_Click_1);
            // 
            // pb_tryktype_back
            // 
            this.pb_tryktype_back.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pb_tryktype_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_tryktype_back.Image = global::Orion_truck.Properties.Resources.pre;
            this.pb_tryktype_back.Location = new System.Drawing.Point(6, 3);
            this.pb_tryktype_back.Name = "pb_tryktype_back";
            this.pb_tryktype_back.Size = new System.Drawing.Size(135, 36);
            this.pb_tryktype_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_tryktype_back.TabIndex = 12;
            this.pb_tryktype_back.TabStop = false;
            // 
            // lbl_invsble_slttruck
            // 
            this.lbl_invsble_slttruck.AutoSize = true;
            this.lbl_invsble_slttruck.Location = new System.Drawing.Point(159, 15);
            this.lbl_invsble_slttruck.Name = "lbl_invsble_slttruck";
            this.lbl_invsble_slttruck.Size = new System.Drawing.Size(76, 13);
            this.lbl_invsble_slttruck.TabIndex = 11;
            this.lbl_invsble_slttruck.Text = "Selected truck";
            this.lbl_invsble_slttruck.Visible = false;
            // 
            // pan_slt_inloader
            // 
            this.pan_slt_inloader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_slt_inloader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_slt_inloader.Location = new System.Drawing.Point(141, 51);
            this.pan_slt_inloader.Name = "pan_slt_inloader";
            this.pan_slt_inloader.Size = new System.Drawing.Size(3, 147);
            this.pan_slt_inloader.TabIndex = 12;
            // 
            // pan_sltd_container
            // 
            this.pan_sltd_container.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_sltd_container.Location = new System.Drawing.Point(514, 51);
            this.pan_sltd_container.Name = "pan_sltd_container";
            this.pan_sltd_container.Size = new System.Drawing.Size(3, 147);
            this.pan_sltd_container.TabIndex = 13;
            // 
            // pan_sltd_euro
            // 
            this.pan_sltd_euro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_euro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_sltd_euro.Location = new System.Drawing.Point(141, 246);
            this.pan_sltd_euro.Name = "pan_sltd_euro";
            this.pan_sltd_euro.Size = new System.Drawing.Size(3, 147);
            this.pan_sltd_euro.TabIndex = 14;
            // 
            // pan_sltd_other
            // 
            this.pan_sltd_other.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_other.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_sltd_other.Location = new System.Drawing.Point(514, 246);
            this.pan_sltd_other.Name = "pan_sltd_other";
            this.pan_sltd_other.Size = new System.Drawing.Size(3, 147);
            this.pan_sltd_other.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel9);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(992, 486);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "TruckIn";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel9.ColumnCount = 5;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21F));
            this.tableLayoutPanel9.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel11, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.panel5, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.panel21, 4, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.panel24, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // panel4
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.panel4, 5);
            this.panel4.Controls.Add(this.lbl_slt_truckin);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(980, 42);
            this.panel4.TabIndex = 0;
            // 
            // lbl_slt_truckin
            // 
            this.lbl_slt_truckin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_slt_truckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_truckin.Location = new System.Drawing.Point(0, 0);
            this.lbl_slt_truckin.Name = "lbl_slt_truckin";
            this.lbl_slt_truckin.Size = new System.Drawing.Size(980, 42);
            this.lbl_slt_truckin.TabIndex = 0;
            this.lbl_slt_truckin.Text = "You Arrived With Glass Goods";
            this.lbl_slt_truckin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel11.Controls.Add(this.pictureBox6, 1, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(476, 51);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(141, 378);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Image = global::Orion_truck.Properties.Resources.plus;
            this.pictureBox6.Location = new System.Drawing.Point(10, 135);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(120, 107);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.tableLayoutPanel12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(632, 51);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(141, 378);
            this.panel5.TabIndex = 2;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93F));
            this.tableLayoutPanel12.Controls.Add(this.pb_trkin2, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.pb_trkin1, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.pb_trkin3, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.pb_trkin4, 1, 3);
            this.tableLayoutPanel12.Controls.Add(this.pb_trkin5, 1, 4);
            this.tableLayoutPanel12.Controls.Add(this.pan_trkin1, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.pan_trkin2, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.pan_trkin3, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.pan_trkin4, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.pan_trkin5, 0, 4);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 5;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(141, 378);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // pb_trkin2
            // 
            this.pb_trkin2.BackColor = System.Drawing.Color.Transparent;
            this.pb_trkin2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pb_trkin2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkin2.Image = global::Orion_truck.Properties.Resources.glass_2132;
            this.pb_trkin2.Location = new System.Drawing.Point(12, 78);
            this.pb_trkin2.Name = "pb_trkin2";
            this.pb_trkin2.Size = new System.Drawing.Size(126, 69);
            this.pb_trkin2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin2.TabIndex = 10;
            this.pb_trkin2.TabStop = false;
            this.pb_trkin2.Click += new System.EventHandler(this.pb_trkin2_Click);
            // 
            // pb_trkin1
            // 
            this.pb_trkin1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pb_trkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkin1.Image = global::Orion_truck.Properties.Resources.empty;
            this.pb_trkin1.Location = new System.Drawing.Point(12, 3);
            this.pb_trkin1.Name = "pb_trkin1";
            this.pb_trkin1.Size = new System.Drawing.Size(126, 69);
            this.pb_trkin1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin1.TabIndex = 0;
            this.pb_trkin1.TabStop = false;
            this.pb_trkin1.Click += new System.EventHandler(this.pb_trkin1_Click);
            // 
            // pb_trkin3
            // 
            this.pb_trkin3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pb_trkin3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkin3.Image = global::Orion_truck.Properties.Resources.empty_still;
            this.pb_trkin3.Location = new System.Drawing.Point(12, 153);
            this.pb_trkin3.Name = "pb_trkin3";
            this.pb_trkin3.Size = new System.Drawing.Size(126, 69);
            this.pb_trkin3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin3.TabIndex = 2;
            this.pb_trkin3.TabStop = false;
            this.pb_trkin3.Click += new System.EventHandler(this.pb_trkin3_Click);
            // 
            // pb_trkin4
            // 
            this.pb_trkin4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pb_trkin4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkin4.Image = global::Orion_truck.Properties.Resources.Cullet;
            this.pb_trkin4.Location = new System.Drawing.Point(12, 228);
            this.pb_trkin4.Name = "pb_trkin4";
            this.pb_trkin4.Size = new System.Drawing.Size(126, 69);
            this.pb_trkin4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin4.TabIndex = 3;
            this.pb_trkin4.TabStop = false;
            this.pb_trkin4.Click += new System.EventHandler(this.pb_trkin4_Click);
            // 
            // pb_trkin5
            // 
            this.pb_trkin5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pb_trkin5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkin5.Image = global::Orion_truck.Properties.Resources.other_new_jpg;
            this.pb_trkin5.Location = new System.Drawing.Point(12, 303);
            this.pb_trkin5.Name = "pb_trkin5";
            this.pb_trkin5.Size = new System.Drawing.Size(126, 72);
            this.pb_trkin5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin5.TabIndex = 4;
            this.pb_trkin5.TabStop = false;
            this.pb_trkin5.Click += new System.EventHandler(this.pb_trkin5_Click);
            // 
            // pan_trkin1
            // 
            this.pan_trkin1.BackColor = System.Drawing.Color.Green;
            this.pan_trkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkin1.Location = new System.Drawing.Point(3, 3);
            this.pan_trkin1.Name = "pan_trkin1";
            this.pan_trkin1.Size = new System.Drawing.Size(3, 69);
            this.pan_trkin1.TabIndex = 5;
            this.pan_trkin1.Visible = false;
            // 
            // pan_trkin2
            // 
            this.pan_trkin2.BackColor = System.Drawing.Color.Green;
            this.pan_trkin2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkin2.Location = new System.Drawing.Point(3, 78);
            this.pan_trkin2.Name = "pan_trkin2";
            this.pan_trkin2.Size = new System.Drawing.Size(3, 69);
            this.pan_trkin2.TabIndex = 6;
            // 
            // pan_trkin3
            // 
            this.pan_trkin3.BackColor = System.Drawing.Color.Green;
            this.pan_trkin3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkin3.Location = new System.Drawing.Point(3, 153);
            this.pan_trkin3.Name = "pan_trkin3";
            this.pan_trkin3.Size = new System.Drawing.Size(3, 69);
            this.pan_trkin3.TabIndex = 7;
            this.pan_trkin3.Visible = false;
            // 
            // pan_trkin4
            // 
            this.pan_trkin4.BackColor = System.Drawing.Color.Green;
            this.pan_trkin4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkin4.Location = new System.Drawing.Point(3, 228);
            this.pan_trkin4.Name = "pan_trkin4";
            this.pan_trkin4.Size = new System.Drawing.Size(3, 69);
            this.pan_trkin4.TabIndex = 8;
            this.pan_trkin4.Visible = false;
            // 
            // pan_trkin5
            // 
            this.pan_trkin5.BackColor = System.Drawing.Color.Green;
            this.pan_trkin5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkin5.Location = new System.Drawing.Point(3, 303);
            this.pan_trkin5.Name = "pan_trkin5";
            this.pan_trkin5.Size = new System.Drawing.Size(3, 72);
            this.pan_trkin5.TabIndex = 9;
            this.pan_trkin5.Visible = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.tableLayoutPanel13);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(779, 51);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(204, 378);
            this.panel21.TabIndex = 3;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Controls.Add(this.lbl_trkin1, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.lbl_trkin2, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.lbl_trkin3, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.lbl_trkin4, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.lbl_trkin5, 0, 4);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(204, 378);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // lbl_trkin1
            // 
            this.lbl_trkin1.AutoSize = true;
            this.lbl_trkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkin1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin1.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkin1.Name = "lbl_trkin1";
            this.lbl_trkin1.Size = new System.Drawing.Size(198, 75);
            this.lbl_trkin1.TabIndex = 0;
            this.lbl_trkin1.Text = "Empty ";
            this.lbl_trkin1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin2
            // 
            this.lbl_trkin2.AutoSize = true;
            this.lbl_trkin2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkin2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin2.Location = new System.Drawing.Point(3, 75);
            this.lbl_trkin2.Name = "lbl_trkin2";
            this.lbl_trkin2.Size = new System.Drawing.Size(198, 75);
            this.lbl_trkin2.TabIndex = 1;
            this.lbl_trkin2.Text = "Glass";
            this.lbl_trkin2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin3
            // 
            this.lbl_trkin3.AutoSize = true;
            this.lbl_trkin3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkin3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin3.Location = new System.Drawing.Point(3, 150);
            this.lbl_trkin3.Name = "lbl_trkin3";
            this.lbl_trkin3.Size = new System.Drawing.Size(198, 75);
            this.lbl_trkin3.TabIndex = 2;
            this.lbl_trkin3.Text = "Stillage";
            this.lbl_trkin3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin4
            // 
            this.lbl_trkin4.AutoSize = true;
            this.lbl_trkin4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkin4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin4.Location = new System.Drawing.Point(3, 225);
            this.lbl_trkin4.Name = "lbl_trkin4";
            this.lbl_trkin4.Size = new System.Drawing.Size(198, 75);
            this.lbl_trkin4.TabIndex = 3;
            this.lbl_trkin4.Text = "Cullet";
            this.lbl_trkin4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin5
            // 
            this.lbl_trkin5.AutoSize = true;
            this.lbl_trkin5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkin5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin5.Location = new System.Drawing.Point(3, 300);
            this.lbl_trkin5.Name = "lbl_trkin5";
            this.lbl_trkin5.Size = new System.Drawing.Size(198, 78);
            this.lbl_trkin5.TabIndex = 4;
            this.lbl_trkin5.Text = "Others";
            this.lbl_trkin5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 51);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(467, 378);
            this.tableLayoutPanel10.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::Orion_truck.Properties.Resources.white_1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.pictureBox11);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 40);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(461, 296);
            this.panel3.TabIndex = 0;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox11.Image = global::Orion_truck.Properties.Resources.truckin;
            this.pictureBox11.Location = new System.Drawing.Point(0, 0);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(461, 296);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 1;
            this.pictureBox11.TabStop = false;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel9.SetColumnSpan(this.panel24, 5);
            this.panel24.Controls.Add(this.lbl_other);
            this.panel24.Controls.Add(this.lbl_cullet);
            this.panel24.Controls.Add(this.lbl_still);
            this.panel24.Controls.Add(this.lbl_glass);
            this.panel24.Controls.Add(this.lbl_empty);
            this.panel24.Controls.Add(this.lbl_invsble_trkinslt);
            this.panel24.Controls.Add(this.pb_trkin_next);
            this.panel24.Controls.Add(this.pb_trkin_back);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(3, 435);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(980, 42);
            this.panel24.TabIndex = 7;
            // 
            // lbl_other
            // 
            this.lbl_other.AutoSize = true;
            this.lbl_other.Location = new System.Drawing.Point(620, 18);
            this.lbl_other.Name = "lbl_other";
            this.lbl_other.Size = new System.Drawing.Size(38, 13);
            this.lbl_other.TabIndex = 7;
            this.lbl_other.Text = "Others";
            this.lbl_other.Visible = false;
            // 
            // lbl_cullet
            // 
            this.lbl_cullet.AutoSize = true;
            this.lbl_cullet.Location = new System.Drawing.Point(558, 18);
            this.lbl_cullet.Name = "lbl_cullet";
            this.lbl_cullet.Size = new System.Drawing.Size(36, 13);
            this.lbl_cullet.TabIndex = 6;
            this.lbl_cullet.Text = "Cutllet";
            this.lbl_cullet.Visible = false;
            // 
            // lbl_still
            // 
            this.lbl_still.AutoSize = true;
            this.lbl_still.Location = new System.Drawing.Point(482, 18);
            this.lbl_still.Name = "lbl_still";
            this.lbl_still.Size = new System.Drawing.Size(41, 13);
            this.lbl_still.TabIndex = 5;
            this.lbl_still.Text = "Stillage";
            this.lbl_still.Visible = false;
            // 
            // lbl_glass
            // 
            this.lbl_glass.AutoSize = true;
            this.lbl_glass.Location = new System.Drawing.Point(440, 18);
            this.lbl_glass.Name = "lbl_glass";
            this.lbl_glass.Size = new System.Drawing.Size(33, 13);
            this.lbl_glass.TabIndex = 4;
            this.lbl_glass.Text = "Glass";
            this.lbl_glass.Visible = false;
            // 
            // lbl_empty
            // 
            this.lbl_empty.AutoSize = true;
            this.lbl_empty.Location = new System.Drawing.Point(384, 18);
            this.lbl_empty.Name = "lbl_empty";
            this.lbl_empty.Size = new System.Drawing.Size(36, 13);
            this.lbl_empty.TabIndex = 3;
            this.lbl_empty.Text = "Empty";
            this.lbl_empty.Visible = false;
            // 
            // lbl_invsble_trkinslt
            // 
            this.lbl_invsble_trkinslt.AutoSize = true;
            this.lbl_invsble_trkinslt.Location = new System.Drawing.Point(292, 18);
            this.lbl_invsble_trkinslt.Name = "lbl_invsble_trkinslt";
            this.lbl_invsble_trkinslt.Size = new System.Drawing.Size(35, 13);
            this.lbl_invsble_trkinslt.TabIndex = 2;
            this.lbl_invsble_trkinslt.Text = "label2";
            this.lbl_invsble_trkinslt.Visible = false;
            // 
            // pb_trkin_next
            // 
            this.pb_trkin_next.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_trkin_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin_next.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pb_trkin_next.Location = new System.Drawing.Point(831, 3);
            this.pb_trkin_next.Name = "pb_trkin_next";
            this.pb_trkin_next.Size = new System.Drawing.Size(114, 35);
            this.pb_trkin_next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin_next.TabIndex = 1;
            this.pb_trkin_next.TabStop = false;
            this.pb_trkin_next.Click += new System.EventHandler(this.pb_trkin_next_Click_1);
            // 
            // pb_trkin_back
            // 
            this.pb_trkin_back.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pb_trkin_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin_back.Image = global::Orion_truck.Properties.Resources.pre;
            this.pb_trkin_back.Location = new System.Drawing.Point(7, 2);
            this.pb_trkin_back.Name = "pb_trkin_back";
            this.pb_trkin_back.Size = new System.Drawing.Size(135, 36);
            this.pb_trkin_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkin_back.TabIndex = 0;
            this.pb_trkin_back.TabStop = false;
            this.pb_trkin_back.Click += new System.EventHandler(this.pictureBox14_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel7);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(992, 486);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Purchase Order";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel7.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.groupBox2, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.pictureBox10, 2, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.tableLayoutPanel14);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(57, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(651, 426);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Truck Selection";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel16, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel19, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.pictureBox7, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.pictureBox8, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.pictureBox9, 1, 2);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(645, 407);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel15.Controls.Add(this.lbl_pono, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.lbl_expo, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.richTextBox1, 1, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(445, 129);
            this.tableLayoutPanel15.TabIndex = 0;
            // 
            // lbl_pono
            // 
            this.lbl_pono.AutoSize = true;
            this.lbl_pono.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_pono.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pono.Location = new System.Drawing.Point(3, 0);
            this.lbl_pono.Name = "lbl_pono";
            this.lbl_pono.Size = new System.Drawing.Size(127, 64);
            this.lbl_pono.TabIndex = 0;
            this.lbl_pono.Text = "Puchase Order";
            this.lbl_pono.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_expo
            // 
            this.lbl_expo.AutoSize = true;
            this.lbl_expo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_expo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expo.Location = new System.Drawing.Point(3, 64);
            this.lbl_expo.Name = "lbl_expo";
            this.lbl_expo.Size = new System.Drawing.Size(127, 65);
            this.lbl_expo.TabIndex = 1;
            this.lbl_expo.Text = "(82545868)";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(136, 25);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(306, 36);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel18, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 138);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(445, 129);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel17.Controls.Add(this.lbl_sg_still, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.checkBox1, 1, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(439, 58);
            this.tableLayoutPanel17.TabIndex = 0;
            // 
            // lbl_sg_still
            // 
            this.lbl_sg_still.AutoSize = true;
            this.lbl_sg_still.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_sg_still.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sg_still.Location = new System.Drawing.Point(3, 0);
            this.lbl_sg_still.Name = "lbl_sg_still";
            this.lbl_sg_still.Size = new System.Drawing.Size(345, 58);
            this.lbl_sg_still.TabIndex = 1;
            this.lbl_sg_still.Text = "My stillage is a Saint-Gobain stillage";
            this.lbl_sg_still.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox1.Location = new System.Drawing.Point(354, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 52);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel18.Controls.Add(this.lbl_del_stil, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.richTextBox2, 1, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 67);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(439, 59);
            this.tableLayoutPanel18.TabIndex = 1;
            // 
            // lbl_del_stil
            // 
            this.lbl_del_stil.AutoSize = true;
            this.lbl_del_stil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_del_stil.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_del_stil.Location = new System.Drawing.Point(3, 0);
            this.lbl_del_stil.Name = "lbl_del_stil";
            this.lbl_del_stil.Size = new System.Drawing.Size(125, 59);
            this.lbl_del_stil.TabIndex = 5;
            this.lbl_del_stil.Text = "Stillage No";
            this.lbl_del_stil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(134, 20);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(302, 36);
            this.richTextBox2.TabIndex = 4;
            this.richTextBox2.Text = "";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel19.Controls.Add(this.lbl_nonsg_still, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.lbl_exstill, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.checkBox2, 1, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 273);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(445, 131);
            this.tableLayoutPanel19.TabIndex = 2;
            // 
            // lbl_nonsg_still
            // 
            this.lbl_nonsg_still.AutoSize = true;
            this.lbl_nonsg_still.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_nonsg_still.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nonsg_still.Location = new System.Drawing.Point(3, 65);
            this.lbl_nonsg_still.Name = "lbl_nonsg_still";
            this.lbl_nonsg_still.Size = new System.Drawing.Size(350, 66);
            this.lbl_nonsg_still.TabIndex = 3;
            this.lbl_nonsg_still.Text = "My stillage is not a saint gobain stillage";
            this.lbl_nonsg_still.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_exstill
            // 
            this.lbl_exstill.AutoSize = true;
            this.lbl_exstill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_exstill.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_exstill.Location = new System.Drawing.Point(3, 0);
            this.lbl_exstill.Name = "lbl_exstill";
            this.lbl_exstill.Size = new System.Drawing.Size(350, 65);
            this.lbl_exstill.TabIndex = 2;
            this.lbl_exstill.Text = "(82545868)";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox2.Location = new System.Drawing.Point(359, 68);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(83, 60);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Image = global::Orion_truck.Properties.Resources.po;
            this.pictureBox7.Location = new System.Drawing.Point(454, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(123, 129);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 3;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Image = global::Orion_truck.Properties.Resources.sasa;
            this.pictureBox8.Location = new System.Drawing.Point(454, 138);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(123, 129);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 4;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Image = global::Orion_truck.Properties.Resources.still;
            this.pictureBox9.Location = new System.Drawing.Point(454, 273);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(123, 131);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 5;
            this.pictureBox9.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.tableLayoutPanel20);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(714, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(213, 426);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Order List";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel20.Controls.Add(this.panel8, 0, 2);
            this.tableLayoutPanel20.Controls.Add(this.richTextBox3, 0, 1);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 4;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(207, 407);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 63);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(201, 319);
            this.panel8.TabIndex = 0;
            // 
            // richTextBox3
            // 
            this.richTextBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(3, 23);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(201, 34);
            this.richTextBox3.TabIndex = 4;
            this.richTextBox3.Text = "";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox10.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pictureBox10.Location = new System.Drawing.Point(821, 435);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(106, 42);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 2;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Click += new System.EventHandler(this.pictureBox10_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel21);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(992, 486);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "Truckout";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel21.ColumnCount = 9;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel22, 5, 1);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel24, 3, 1);
            this.tableLayoutPanel21.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.panel9, 1, 1);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel25, 7, 1);
            this.tableLayoutPanel21.Controls.Add(this.panel31, 0, 2);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 3;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel21.TabIndex = 2;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 3;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel22.Controls.Add(this.pictureBox5, 1, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(464, 51);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 3;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(171, 378);
            this.tableLayoutPanel22.TabIndex = 9;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Image = global::Orion_truck.Properties.Resources.plus;
            this.pictureBox5.Location = new System.Drawing.Point(11, 135);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(147, 107);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 1;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel24.Controls.Add(this.lbl_trkout1, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.lbl_trkout2, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.lbl_trkout3, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.lbl_trkout4, 0, 3);
            this.tableLayoutPanel24.Controls.Add(this.lbl_trkout5, 0, 4);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(258, 51);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 5;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(171, 378);
            this.tableLayoutPanel24.TabIndex = 8;
            // 
            // lbl_trkout1
            // 
            this.lbl_trkout1.AutoSize = true;
            this.lbl_trkout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkout1.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout1.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkout1.Name = "lbl_trkout1";
            this.lbl_trkout1.Size = new System.Drawing.Size(165, 75);
            this.lbl_trkout1.TabIndex = 0;
            this.lbl_trkout1.Text = "Empty ";
            this.lbl_trkout1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkout2
            // 
            this.lbl_trkout2.AutoSize = true;
            this.lbl_trkout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkout2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout2.Location = new System.Drawing.Point(3, 75);
            this.lbl_trkout2.Name = "lbl_trkout2";
            this.lbl_trkout2.Size = new System.Drawing.Size(165, 75);
            this.lbl_trkout2.TabIndex = 1;
            this.lbl_trkout2.Text = "Glass";
            this.lbl_trkout2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkout3
            // 
            this.lbl_trkout3.AutoSize = true;
            this.lbl_trkout3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkout3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout3.Location = new System.Drawing.Point(3, 150);
            this.lbl_trkout3.Name = "lbl_trkout3";
            this.lbl_trkout3.Size = new System.Drawing.Size(165, 75);
            this.lbl_trkout3.TabIndex = 2;
            this.lbl_trkout3.Text = "Stillage";
            this.lbl_trkout3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkout4
            // 
            this.lbl_trkout4.AutoSize = true;
            this.lbl_trkout4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkout4.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout4.Location = new System.Drawing.Point(3, 225);
            this.lbl_trkout4.Name = "lbl_trkout4";
            this.lbl_trkout4.Size = new System.Drawing.Size(165, 75);
            this.lbl_trkout4.TabIndex = 3;
            this.lbl_trkout4.Text = "Cullet";
            this.lbl_trkout4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkout5
            // 
            this.lbl_trkout5.AutoSize = true;
            this.lbl_trkout5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkout5.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout5.Location = new System.Drawing.Point(3, 300);
            this.lbl_trkout5.Name = "lbl_trkout5";
            this.lbl_trkout5.Size = new System.Drawing.Size(165, 78);
            this.lbl_trkout5.TabIndex = 4;
            this.lbl_trkout5.Text = "Others";
            this.lbl_trkout5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.panel7, 9);
            this.panel7.Controls.Add(this.lbl_slt_truckout);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(980, 42);
            this.panel7.TabIndex = 0;
            // 
            // lbl_slt_truckout
            // 
            this.lbl_slt_truckout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_slt_truckout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_truckout.Location = new System.Drawing.Point(0, 0);
            this.lbl_slt_truckout.Name = "lbl_slt_truckout";
            this.lbl_slt_truckout.Size = new System.Drawing.Size(980, 42);
            this.lbl_slt_truckout.TabIndex = 0;
            this.lbl_slt_truckout.Text = "After Unloading will you load a new Order ?";
            this.lbl_slt_truckout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.tableLayoutPanel23);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(72, 51);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(171, 378);
            this.panel9.TabIndex = 2;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95F));
            this.tableLayoutPanel23.Controls.Add(this.pb_trkout1, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.pb_trkout2, 1, 1);
            this.tableLayoutPanel23.Controls.Add(this.pb_trkout3, 1, 2);
            this.tableLayoutPanel23.Controls.Add(this.pb_trkout4, 1, 3);
            this.tableLayoutPanel23.Controls.Add(this.pb_trkout5, 1, 4);
            this.tableLayoutPanel23.Controls.Add(this.pan_trkout1, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.pan_trkout2, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.pan_trkout3, 0, 2);
            this.tableLayoutPanel23.Controls.Add(this.pan_trkout4, 0, 3);
            this.tableLayoutPanel23.Controls.Add(this.pan_trkout5, 0, 4);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 5;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(171, 378);
            this.tableLayoutPanel23.TabIndex = 0;
            // 
            // pb_trkout1
            // 
            this.pb_trkout1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkout1.Image = global::Orion_truck.Properties.Resources.empty;
            this.pb_trkout1.Location = new System.Drawing.Point(11, 3);
            this.pb_trkout1.Name = "pb_trkout1";
            this.pb_trkout1.Size = new System.Drawing.Size(157, 69);
            this.pb_trkout1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout1.TabIndex = 0;
            this.pb_trkout1.TabStop = false;
            // 
            // pb_trkout2
            // 
            this.pb_trkout2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkout2.Image = global::Orion_truck.Properties.Resources.glass_2132;
            this.pb_trkout2.Location = new System.Drawing.Point(11, 78);
            this.pb_trkout2.Name = "pb_trkout2";
            this.pb_trkout2.Size = new System.Drawing.Size(157, 69);
            this.pb_trkout2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout2.TabIndex = 1;
            this.pb_trkout2.TabStop = false;
            // 
            // pb_trkout3
            // 
            this.pb_trkout3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkout3.Image = global::Orion_truck.Properties.Resources.empty_still;
            this.pb_trkout3.Location = new System.Drawing.Point(11, 153);
            this.pb_trkout3.Name = "pb_trkout3";
            this.pb_trkout3.Size = new System.Drawing.Size(157, 69);
            this.pb_trkout3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout3.TabIndex = 2;
            this.pb_trkout3.TabStop = false;
            // 
            // pb_trkout4
            // 
            this.pb_trkout4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkout4.Image = global::Orion_truck.Properties.Resources.Cullet;
            this.pb_trkout4.Location = new System.Drawing.Point(11, 228);
            this.pb_trkout4.Name = "pb_trkout4";
            this.pb_trkout4.Size = new System.Drawing.Size(157, 69);
            this.pb_trkout4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout4.TabIndex = 3;
            this.pb_trkout4.TabStop = false;
            // 
            // pb_trkout5
            // 
            this.pb_trkout5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_trkout5.Image = global::Orion_truck.Properties.Resources.other_new_jpg;
            this.pb_trkout5.Location = new System.Drawing.Point(11, 303);
            this.pb_trkout5.Name = "pb_trkout5";
            this.pb_trkout5.Size = new System.Drawing.Size(157, 72);
            this.pb_trkout5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout5.TabIndex = 4;
            this.pb_trkout5.TabStop = false;
            // 
            // pan_trkout1
            // 
            this.pan_trkout1.BackColor = System.Drawing.Color.Green;
            this.pan_trkout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkout1.Location = new System.Drawing.Point(3, 3);
            this.pan_trkout1.Name = "pan_trkout1";
            this.pan_trkout1.Size = new System.Drawing.Size(2, 69);
            this.pan_trkout1.TabIndex = 5;
            this.pan_trkout1.Visible = false;
            // 
            // pan_trkout2
            // 
            this.pan_trkout2.BackColor = System.Drawing.Color.Green;
            this.pan_trkout2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkout2.Location = new System.Drawing.Point(3, 78);
            this.pan_trkout2.Name = "pan_trkout2";
            this.pan_trkout2.Size = new System.Drawing.Size(2, 69);
            this.pan_trkout2.TabIndex = 6;
            // 
            // pan_trkout3
            // 
            this.pan_trkout3.BackColor = System.Drawing.Color.Green;
            this.pan_trkout3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkout3.Location = new System.Drawing.Point(3, 153);
            this.pan_trkout3.Name = "pan_trkout3";
            this.pan_trkout3.Size = new System.Drawing.Size(2, 69);
            this.pan_trkout3.TabIndex = 7;
            this.pan_trkout3.Visible = false;
            // 
            // pan_trkout4
            // 
            this.pan_trkout4.BackColor = System.Drawing.Color.Green;
            this.pan_trkout4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkout4.Location = new System.Drawing.Point(3, 228);
            this.pan_trkout4.Name = "pan_trkout4";
            this.pan_trkout4.Size = new System.Drawing.Size(2, 69);
            this.pan_trkout4.TabIndex = 8;
            this.pan_trkout4.Visible = false;
            // 
            // pan_trkout5
            // 
            this.pan_trkout5.BackColor = System.Drawing.Color.Green;
            this.pan_trkout5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_trkout5.Location = new System.Drawing.Point(3, 303);
            this.pan_trkout5.Name = "pan_trkout5";
            this.pan_trkout5.Size = new System.Drawing.Size(2, 72);
            this.pan_trkout5.TabIndex = 9;
            this.pan_trkout5.Visible = false;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 1;
            this.tableLayoutPanel21.SetColumnSpan(this.tableLayoutPanel25, 2);
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Controls.Add(this.panel30, 0, 1);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(670, 51);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 3;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(313, 378);
            this.tableLayoutPanel25.TabIndex = 6;
            // 
            // panel30
            // 
            this.panel30.BackgroundImage = global::Orion_truck.Properties.Resources.white_1;
            this.panel30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel30.Controls.Add(this.pictureBox21);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(3, 40);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(307, 296);
            this.panel30.TabIndex = 0;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox21.Image = global::Orion_truck.Properties.Resources._out;
            this.pictureBox21.Location = new System.Drawing.Point(0, 0);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(307, 296);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 1;
            this.pictureBox21.TabStop = false;
            // 
            // panel31
            // 
            this.tableLayoutPanel21.SetColumnSpan(this.panel31, 9);
            this.panel31.Controls.Add(this.pb_trkout_next);
            this.panel31.Controls.Add(this.pb_trkout_back);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(3, 435);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(980, 42);
            this.panel31.TabIndex = 7;
            // 
            // pb_trkout_next
            // 
            this.pb_trkout_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_trkout_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout_next.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pb_trkout_next.Location = new System.Drawing.Point(818, 0);
            this.pb_trkout_next.Name = "pb_trkout_next";
            this.pb_trkout_next.Size = new System.Drawing.Size(114, 44);
            this.pb_trkout_next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout_next.TabIndex = 1;
            this.pb_trkout_next.TabStop = false;
            this.pb_trkout_next.Click += new System.EventHandler(this.pb_trkout_next_Click);
            // 
            // pb_trkout_back
            // 
            this.pb_trkout_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkout_back.Image = global::Orion_truck.Properties.Resources.pre;
            this.pb_trkout_back.Location = new System.Drawing.Point(24, 0);
            this.pb_trkout_back.Name = "pb_trkout_back";
            this.pb_trkout_back.Size = new System.Drawing.Size(135, 40);
            this.pb_trkout_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_trkout_back.TabIndex = 0;
            this.pb_trkout_back.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel26);
            this.tabPage6.Location = new System.Drawing.Point(4, 34);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(992, 486);
            this.tabPage6.TabIndex = 6;
            this.tabPage6.Text = "Saleordr";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 4;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel26.Controls.Add(this.groupBox3, 1, 0);
            this.tableLayoutPanel26.Controls.Add(this.groupBox4, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.pb_sale_next, 2, 1);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 2;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel26.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel27);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(57, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(651, 426);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Truck Selection";
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 3;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel27.Controls.Add(this.tableLayoutPanel28, 0, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 2;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33555F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66445F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(645, 407);
            this.tableLayoutPanel27.TabIndex = 0;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel28.Controls.Add(this.lbl_saleno, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.lbl_ex_so, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.richTextBox4, 1, 0);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 2;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(445, 129);
            this.tableLayoutPanel28.TabIndex = 0;
            // 
            // lbl_saleno
            // 
            this.lbl_saleno.AutoSize = true;
            this.lbl_saleno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_saleno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_saleno.Location = new System.Drawing.Point(3, 0);
            this.lbl_saleno.Name = "lbl_saleno";
            this.lbl_saleno.Size = new System.Drawing.Size(127, 64);
            this.lbl_saleno.TabIndex = 0;
            this.lbl_saleno.Text = "Sale Order";
            this.lbl_saleno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_ex_so
            // 
            this.lbl_ex_so.AutoSize = true;
            this.lbl_ex_so.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_ex_so.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ex_so.Location = new System.Drawing.Point(3, 64);
            this.lbl_ex_so.Name = "lbl_ex_so";
            this.lbl_ex_so.Size = new System.Drawing.Size(127, 65);
            this.lbl_ex_so.TabIndex = 1;
            this.lbl_ex_so.Text = "(82545868)";
            // 
            // richTextBox4
            // 
            this.richTextBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox4.Location = new System.Drawing.Point(136, 25);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(306, 36);
            this.richTextBox4.TabIndex = 3;
            this.richTextBox4.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel33);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(714, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(213, 426);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Order List";
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 1;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel33.Controls.Add(this.panel29, 0, 2);
            this.tableLayoutPanel33.Controls.Add(this.richTextBox6, 0, 1);
            this.tableLayoutPanel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 4;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(207, 407);
            this.tableLayoutPanel33.TabIndex = 0;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.White;
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(3, 63);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(201, 319);
            this.panel29.TabIndex = 0;
            // 
            // richTextBox6
            // 
            this.richTextBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox6.Location = new System.Drawing.Point(3, 30);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.Size = new System.Drawing.Size(201, 27);
            this.richTextBox6.TabIndex = 4;
            this.richTextBox6.Text = "";
            // 
            // pb_sale_next
            // 
            this.pb_sale_next.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_sale_next.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pb_sale_next.Location = new System.Drawing.Point(821, 435);
            this.pb_sale_next.Name = "pb_sale_next";
            this.pb_sale_next.Size = new System.Drawing.Size(106, 42);
            this.pb_sale_next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_sale_next.TabIndex = 2;
            this.pb_sale_next.TabStop = false;
            this.pb_sale_next.Click += new System.EventHandler(this.pb_sale_next_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.tableLayoutPanel35);
            this.tabPage8.Location = new System.Drawing.Point(4, 34);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(992, 486);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "truck_info";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 3;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel35.Controls.Add(this.panel20, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.pictureBox1, 2, 1);
            this.tableLayoutPanel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel35.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 2;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel35.TabIndex = 7;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.tableLayoutPanel36);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(101, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(733, 426);
            this.panel20.TabIndex = 0;
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel36.ColumnCount = 4;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel36.Controls.Add(this.richTextBox5, 2, 0);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel37, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel38, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel39, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel40, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.richTextBox7, 2, 1);
            this.tableLayoutPanel36.Controls.Add(this.richTextBox8, 2, 2);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel41, 2, 3);
            this.tableLayoutPanel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel36.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 4;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(733, 426);
            this.tableLayoutPanel36.TabIndex = 0;
            // 
            // richTextBox5
            // 
            this.richTextBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox5.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(207, 35);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(463, 35);
            this.richTextBox5.TabIndex = 0;
            this.richTextBox5.Text = "";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 1;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.lbl_trkinfo_truckno, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.lbl_trkinfo_extruckno, 0, 1);
            this.tableLayoutPanel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel37.Location = new System.Drawing.Point(39, 3);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 2;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(162, 100);
            this.tableLayoutPanel37.TabIndex = 1;
            // 
            // lbl_trkinfo_truckno
            // 
            this.lbl_trkinfo_truckno.AutoSize = true;
            this.lbl_trkinfo_truckno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_truckno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_truckno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo_truckno.Name = "lbl_trkinfo_truckno";
            this.lbl_trkinfo_truckno.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_truckno.TabIndex = 0;
            this.lbl_trkinfo_truckno.Text = "Truck No.";
            this.lbl_trkinfo_truckno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_extruckno
            // 
            this.lbl_trkinfo_extruckno.AutoSize = true;
            this.lbl_trkinfo_extruckno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_extruckno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_extruckno.Location = new System.Drawing.Point(3, 50);
            this.lbl_trkinfo_extruckno.Name = "lbl_trkinfo_extruckno";
            this.lbl_trkinfo_extruckno.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_extruckno.TabIndex = 1;
            this.lbl_trkinfo_extruckno.Text = "(TN 01 X 1456)";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 1;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.lbl_trkinfo_trailrno, 0, 0);
            this.tableLayoutPanel38.Controls.Add(this.lbl_trkinfo_extrailrno, 0, 1);
            this.tableLayoutPanel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel38.Location = new System.Drawing.Point(39, 109);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 2;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(162, 100);
            this.tableLayoutPanel38.TabIndex = 2;
            // 
            // lbl_trkinfo_trailrno
            // 
            this.lbl_trkinfo_trailrno.AutoSize = true;
            this.lbl_trkinfo_trailrno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_trailrno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_trailrno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo_trailrno.Name = "lbl_trkinfo_trailrno";
            this.lbl_trkinfo_trailrno.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_trailrno.TabIndex = 0;
            this.lbl_trkinfo_trailrno.Text = "Trailer No.";
            this.lbl_trkinfo_trailrno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_extrailrno
            // 
            this.lbl_trkinfo_extrailrno.AutoSize = true;
            this.lbl_trkinfo_extrailrno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_extrailrno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_extrailrno.Location = new System.Drawing.Point(3, 50);
            this.lbl_trkinfo_extrailrno.Name = "lbl_trkinfo_extrailrno";
            this.lbl_trkinfo_extrailrno.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_extrailrno.TabIndex = 1;
            this.lbl_trkinfo_extrailrno.Text = "(2564896)";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 1;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.lbl_trkinfo_name, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.lbl_trkinfo_exname, 0, 1);
            this.tableLayoutPanel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel39.Location = new System.Drawing.Point(39, 215);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(162, 100);
            this.tableLayoutPanel39.TabIndex = 3;
            // 
            // lbl_trkinfo_name
            // 
            this.lbl_trkinfo_name.AutoSize = true;
            this.lbl_trkinfo_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_name.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_name.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo_name.Name = "lbl_trkinfo_name";
            this.lbl_trkinfo_name.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_name.TabIndex = 0;
            this.lbl_trkinfo_name.Text = "Driver Name";
            this.lbl_trkinfo_name.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_exname
            // 
            this.lbl_trkinfo_exname.AutoSize = true;
            this.lbl_trkinfo_exname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_exname.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_exname.Location = new System.Drawing.Point(3, 50);
            this.lbl_trkinfo_exname.Name = "lbl_trkinfo_exname";
            this.lbl_trkinfo_exname.Size = new System.Drawing.Size(156, 50);
            this.lbl_trkinfo_exname.TabIndex = 1;
            this.lbl_trkinfo_exname.Text = "(Raja)";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 1;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.lbl_trkinfo_mobno, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.lbl_trkinfo_exmobno, 0, 1);
            this.tableLayoutPanel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel40.Location = new System.Drawing.Point(39, 321);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 2;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(162, 102);
            this.tableLayoutPanel40.TabIndex = 4;
            // 
            // lbl_trkinfo_mobno
            // 
            this.lbl_trkinfo_mobno.AutoSize = true;
            this.lbl_trkinfo_mobno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_mobno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_mobno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo_mobno.Name = "lbl_trkinfo_mobno";
            this.lbl_trkinfo_mobno.Size = new System.Drawing.Size(156, 51);
            this.lbl_trkinfo_mobno.TabIndex = 0;
            this.lbl_trkinfo_mobno.Text = "Mobile No.";
            this.lbl_trkinfo_mobno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_exmobno
            // 
            this.lbl_trkinfo_exmobno.AutoSize = true;
            this.lbl_trkinfo_exmobno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo_exmobno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_exmobno.Location = new System.Drawing.Point(3, 51);
            this.lbl_trkinfo_exmobno.Name = "lbl_trkinfo_exmobno";
            this.lbl_trkinfo_exmobno.Size = new System.Drawing.Size(156, 51);
            this.lbl_trkinfo_exmobno.TabIndex = 1;
            this.lbl_trkinfo_exmobno.Text = "(91-9638527412)";
            // 
            // richTextBox7
            // 
            this.richTextBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox7.Location = new System.Drawing.Point(207, 141);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.Size = new System.Drawing.Size(463, 35);
            this.richTextBox7.TabIndex = 7;
            this.richTextBox7.Text = "";
            // 
            // richTextBox8
            // 
            this.richTextBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox8.Location = new System.Drawing.Point(207, 247);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.Size = new System.Drawing.Size(463, 35);
            this.richTextBox8.TabIndex = 8;
            this.richTextBox8.Text = "";
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 2;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel41.Controls.Add(this.richTextBox9, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.richTextBox10, 1, 0);
            this.tableLayoutPanel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel41.Location = new System.Drawing.Point(207, 321);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 1;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(463, 102);
            this.tableLayoutPanel41.TabIndex = 9;
            // 
            // richTextBox9
            // 
            this.richTextBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox9.Location = new System.Drawing.Point(3, 33);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(63, 35);
            this.richTextBox9.TabIndex = 0;
            this.richTextBox9.Text = "";
            // 
            // richTextBox10
            // 
            this.richTextBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox10.Location = new System.Drawing.Point(72, 33);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.Size = new System.Drawing.Size(388, 35);
            this.richTextBox10.TabIndex = 1;
            this.richTextBox10.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pictureBox1.Location = new System.Drawing.Point(877, 435);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 42);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tableLayoutPanel42);
            this.tabPage9.Location = new System.Drawing.Point(4, 34);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(992, 486);
            this.tabPage9.TabIndex = 9;
            this.tabPage9.Text = "truck_info1";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 3;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel42.Controls.Add(this.panel25, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.pictureBox2, 2, 1);
            this.tableLayoutPanel42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel42.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 2;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel42.TabIndex = 8;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.tableLayoutPanel43);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(101, 3);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(733, 426);
            this.panel25.TabIndex = 0;
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel43.ColumnCount = 4;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel43.Controls.Add(this.richTextBox11, 2, 0);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel44, 1, 0);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel45, 1, 1);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel46, 1, 3);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel47, 1, 4);
            this.tableLayoutPanel43.Controls.Add(this.richTextBox12, 2, 1);
            this.tableLayoutPanel43.Controls.Add(this.richTextBox13, 2, 3);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel48, 2, 4);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel49, 1, 2);
            this.tableLayoutPanel43.Controls.Add(this.richTextBox16, 2, 2);
            this.tableLayoutPanel43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel43.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 5;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(733, 426);
            this.tableLayoutPanel43.TabIndex = 0;
            // 
            // richTextBox11
            // 
            this.richTextBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox11.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox11.Location = new System.Drawing.Point(207, 25);
            this.richTextBox11.Name = "richTextBox11";
            this.richTextBox11.Size = new System.Drawing.Size(463, 35);
            this.richTextBox11.TabIndex = 0;
            this.richTextBox11.Text = "";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 1;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.lbl_trkinfo1_trkno, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.lbl_trkinfo1_extrkno, 0, 1);
            this.tableLayoutPanel44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel44.Location = new System.Drawing.Point(39, 3);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 2;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(162, 79);
            this.tableLayoutPanel44.TabIndex = 1;
            // 
            // lbl_trkinfo1_trkno
            // 
            this.lbl_trkinfo1_trkno.AutoSize = true;
            this.lbl_trkinfo1_trkno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_trkno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_trkno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo1_trkno.Name = "lbl_trkinfo1_trkno";
            this.lbl_trkinfo1_trkno.Size = new System.Drawing.Size(156, 39);
            this.lbl_trkinfo1_trkno.TabIndex = 0;
            this.lbl_trkinfo1_trkno.Text = "Truck No.";
            this.lbl_trkinfo1_trkno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo1_extrkno
            // 
            this.lbl_trkinfo1_extrkno.AutoSize = true;
            this.lbl_trkinfo1_extrkno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_extrkno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_extrkno.Location = new System.Drawing.Point(3, 39);
            this.lbl_trkinfo1_extrkno.Name = "lbl_trkinfo1_extrkno";
            this.lbl_trkinfo1_extrkno.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_extrkno.TabIndex = 1;
            this.lbl_trkinfo1_extrkno.Text = "(TN 01 X 1456)";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 1;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.Controls.Add(this.lbl_trkinfo1_tralrno, 0, 0);
            this.tableLayoutPanel45.Controls.Add(this.lbl_trkinfo1_extralrno, 0, 1);
            this.tableLayoutPanel45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel45.Location = new System.Drawing.Point(39, 88);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 2;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(162, 79);
            this.tableLayoutPanel45.TabIndex = 2;
            // 
            // lbl_trkinfo1_tralrno
            // 
            this.lbl_trkinfo1_tralrno.AutoSize = true;
            this.lbl_trkinfo1_tralrno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_tralrno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_tralrno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo1_tralrno.Name = "lbl_trkinfo1_tralrno";
            this.lbl_trkinfo1_tralrno.Size = new System.Drawing.Size(156, 39);
            this.lbl_trkinfo1_tralrno.TabIndex = 0;
            this.lbl_trkinfo1_tralrno.Text = "Trailer No.";
            this.lbl_trkinfo1_tralrno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo1_extralrno
            // 
            this.lbl_trkinfo1_extralrno.AutoSize = true;
            this.lbl_trkinfo1_extralrno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_extralrno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_extralrno.Location = new System.Drawing.Point(3, 39);
            this.lbl_trkinfo1_extralrno.Name = "lbl_trkinfo1_extralrno";
            this.lbl_trkinfo1_extralrno.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_extralrno.TabIndex = 1;
            this.lbl_trkinfo1_extralrno.Text = "(2564896)";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 1;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.Controls.Add(this.lbl_trkinfo1_name, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.lbl_trkinfo1_exname, 0, 1);
            this.tableLayoutPanel46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel46.Location = new System.Drawing.Point(39, 258);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 2;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(162, 79);
            this.tableLayoutPanel46.TabIndex = 3;
            // 
            // lbl_trkinfo1_name
            // 
            this.lbl_trkinfo1_name.AutoSize = true;
            this.lbl_trkinfo1_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_name.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_name.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo1_name.Name = "lbl_trkinfo1_name";
            this.lbl_trkinfo1_name.Size = new System.Drawing.Size(156, 39);
            this.lbl_trkinfo1_name.TabIndex = 0;
            this.lbl_trkinfo1_name.Text = "Driver Name";
            this.lbl_trkinfo1_name.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo1_exname
            // 
            this.lbl_trkinfo1_exname.AutoSize = true;
            this.lbl_trkinfo1_exname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_exname.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_exname.Location = new System.Drawing.Point(3, 39);
            this.lbl_trkinfo1_exname.Name = "lbl_trkinfo1_exname";
            this.lbl_trkinfo1_exname.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_exname.TabIndex = 1;
            this.lbl_trkinfo1_exname.Text = "(Raja)";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 1;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.Controls.Add(this.lbl_trkinfo1_mobno, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.lbl_trkinfo1_exmobno, 0, 1);
            this.tableLayoutPanel47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel47.Location = new System.Drawing.Point(39, 343);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 2;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(162, 80);
            this.tableLayoutPanel47.TabIndex = 4;
            // 
            // lbl_trkinfo1_mobno
            // 
            this.lbl_trkinfo1_mobno.AutoSize = true;
            this.lbl_trkinfo1_mobno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_mobno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_mobno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo1_mobno.Name = "lbl_trkinfo1_mobno";
            this.lbl_trkinfo1_mobno.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_mobno.TabIndex = 0;
            this.lbl_trkinfo1_mobno.Text = "Mobile No.";
            this.lbl_trkinfo1_mobno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo1_exmobno
            // 
            this.lbl_trkinfo1_exmobno.AutoSize = true;
            this.lbl_trkinfo1_exmobno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_exmobno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_exmobno.Location = new System.Drawing.Point(3, 40);
            this.lbl_trkinfo1_exmobno.Name = "lbl_trkinfo1_exmobno";
            this.lbl_trkinfo1_exmobno.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_exmobno.TabIndex = 1;
            this.lbl_trkinfo1_exmobno.Text = "(91-9638527412)";
            // 
            // richTextBox12
            // 
            this.richTextBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox12.Location = new System.Drawing.Point(207, 110);
            this.richTextBox12.Name = "richTextBox12";
            this.richTextBox12.Size = new System.Drawing.Size(463, 35);
            this.richTextBox12.TabIndex = 7;
            this.richTextBox12.Text = "";
            // 
            // richTextBox13
            // 
            this.richTextBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox13.Location = new System.Drawing.Point(207, 280);
            this.richTextBox13.Name = "richTextBox13";
            this.richTextBox13.Size = new System.Drawing.Size(463, 35);
            this.richTextBox13.TabIndex = 8;
            this.richTextBox13.Text = "";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 2;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel48.Controls.Add(this.richTextBox14, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.richTextBox15, 1, 0);
            this.tableLayoutPanel48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel48.Location = new System.Drawing.Point(207, 343);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 1;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(463, 80);
            this.tableLayoutPanel48.TabIndex = 9;
            // 
            // richTextBox14
            // 
            this.richTextBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox14.Location = new System.Drawing.Point(3, 22);
            this.richTextBox14.Name = "richTextBox14";
            this.richTextBox14.Size = new System.Drawing.Size(63, 35);
            this.richTextBox14.TabIndex = 0;
            this.richTextBox14.Text = "";
            // 
            // richTextBox15
            // 
            this.richTextBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox15.Location = new System.Drawing.Point(72, 22);
            this.richTextBox15.Name = "richTextBox15";
            this.richTextBox15.Size = new System.Drawing.Size(388, 35);
            this.richTextBox15.TabIndex = 1;
            this.richTextBox15.Text = "";
            // 
            // tableLayoutPanel49
            // 
            this.tableLayoutPanel49.ColumnCount = 1;
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel49.Controls.Add(this.lbl_trkinfo1_conno, 0, 0);
            this.tableLayoutPanel49.Controls.Add(this.lbl_trkinfo1_exconno, 0, 1);
            this.tableLayoutPanel49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel49.Location = new System.Drawing.Point(39, 173);
            this.tableLayoutPanel49.Name = "tableLayoutPanel49";
            this.tableLayoutPanel49.RowCount = 2;
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel49.Size = new System.Drawing.Size(162, 79);
            this.tableLayoutPanel49.TabIndex = 10;
            // 
            // lbl_trkinfo1_conno
            // 
            this.lbl_trkinfo1_conno.AutoSize = true;
            this.lbl_trkinfo1_conno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_conno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_conno.Location = new System.Drawing.Point(3, 0);
            this.lbl_trkinfo1_conno.Name = "lbl_trkinfo1_conno";
            this.lbl_trkinfo1_conno.Size = new System.Drawing.Size(156, 39);
            this.lbl_trkinfo1_conno.TabIndex = 0;
            this.lbl_trkinfo1_conno.Text = "Container No.";
            this.lbl_trkinfo1_conno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo1_exconno
            // 
            this.lbl_trkinfo1_exconno.AutoSize = true;
            this.lbl_trkinfo1_exconno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_trkinfo1_exconno.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo1_exconno.Location = new System.Drawing.Point(3, 39);
            this.lbl_trkinfo1_exconno.Name = "lbl_trkinfo1_exconno";
            this.lbl_trkinfo1_exconno.Size = new System.Drawing.Size(156, 40);
            this.lbl_trkinfo1_exconno.TabIndex = 1;
            this.lbl_trkinfo1_exconno.Text = "(256889946)";
            // 
            // richTextBox16
            // 
            this.richTextBox16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox16.Location = new System.Drawing.Point(207, 195);
            this.richTextBox16.Name = "richTextBox16";
            this.richTextBox16.Size = new System.Drawing.Size(463, 35);
            this.richTextBox16.TabIndex = 11;
            this.richTextBox16.Text = "";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::Orion_truck.Properties.Resources.next_new;
            this.pictureBox2.Location = new System.Drawing.Point(877, 435);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.tableLayoutPanel29);
            this.tabPage7.Location = new System.Drawing.Point(4, 34);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(992, 486);
            this.tabPage7.TabIndex = 7;
            this.tabPage7.Text = "complete";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 3;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel29.Controls.Add(this.lbl_comp_thanks, 1, 1);
            this.tableLayoutPanel29.Controls.Add(this.panel32, 1, 0);
            this.tableLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 2;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(986, 480);
            this.tableLayoutPanel29.TabIndex = 5;
            // 
            // lbl_comp_thanks
            // 
            this.lbl_comp_thanks.AutoSize = true;
            this.lbl_comp_thanks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_comp_thanks.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_comp_thanks.Location = new System.Drawing.Point(52, 408);
            this.lbl_comp_thanks.Name = "lbl_comp_thanks";
            this.lbl_comp_thanks.Size = new System.Drawing.Size(832, 72);
            this.lbl_comp_thanks.TabIndex = 1;
            this.lbl_comp_thanks.Text = "Thank you for registering";
            this.lbl_comp_thanks.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.White;
            this.panel32.Controls.Add(this.tableLayoutPanel30);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(52, 3);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(832, 402);
            this.panel32.TabIndex = 0;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 1;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Controls.Add(this.panel33, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.tableLayoutPanel32, 0, 1);
            this.tableLayoutPanel30.Controls.Add(this.tableLayoutPanel34, 0, 2);
            this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel30.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 3;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(832, 402);
            this.tableLayoutPanel30.TabIndex = 0;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.tableLayoutPanel31);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(3, 3);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(826, 114);
            this.panel33.TabIndex = 0;
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 3;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel31.Controls.Add(this.lbl_com_order, 1, 0);
            this.tableLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel31.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 1;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(826, 114);
            this.tableLayoutPanel31.TabIndex = 0;
            // 
            // lbl_com_order
            // 
            this.lbl_com_order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_com_order.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_com_order.Location = new System.Drawing.Point(85, 0);
            this.lbl_com_order.Name = "lbl_com_order";
            this.lbl_com_order.Size = new System.Drawing.Size(654, 114);
            this.lbl_com_order.TabIndex = 0;
            this.lbl_com_order.Text = "Welcome  Raja. You are now registerd for Order 2568963";
            this.lbl_com_order.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel32.Controls.Add(this.pictureBox24, 1, 0);
            this.tableLayoutPanel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 123);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 154F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(826, 154);
            this.tableLayoutPanel32.TabIndex = 1;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox24.Image = global::Orion_truck.Properties.Resources.complete;
            this.pictureBox24.Location = new System.Drawing.Point(250, 3);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(324, 148);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox24.TabIndex = 0;
            this.pictureBox24.TabStop = false;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 3;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel34.Controls.Add(this.lbl_com_confirm, 1, 0);
            this.tableLayoutPanel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 283);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(826, 116);
            this.tableLayoutPanel34.TabIndex = 2;
            // 
            // lbl_com_confirm
            // 
            this.lbl_com_confirm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_com_confirm.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_com_confirm.Location = new System.Drawing.Point(85, 0);
            this.lbl_com_confirm.Name = "lbl_com_confirm";
            this.lbl_com_confirm.Size = new System.Drawing.Size(654, 116);
            this.lbl_com_confirm.TabIndex = 0;
            this.lbl_com_confirm.Text = "You will soon receive a confirmation SMS. You will then receive a second SMS once" +
    " your order is ready with the  gate number.";
            this.lbl_com_confirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "UK.png");
            this.imageList1.Images.SetKeyName(1, "fr.png");
            this.imageList1.Images.SetKeyName(2, "ger.png");
            this.imageList1.Images.SetKeyName(3, "italy.png");
            this.imageList1.Images.SetKeyName(4, "pol.png");
            this.imageList1.Images.SetKeyName(5, "por.png");
            this.imageList1.Images.SetKeyName(6, "Rom.png");
            this.imageList1.Images.SetKeyName(7, "spa.png");
            this.imageList1.Images.SetKeyName(8, "Aus.png");
            this.imageList1.Images.SetKeyName(9, "Canada.png");
            this.imageList1.Images.SetKeyName(10, "Czech.png");
            this.imageList1.Images.SetKeyName(11, "Egypt.png");
            this.imageList1.Images.SetKeyName(12, "Estonia.png");
            this.imageList1.Images.SetKeyName(13, "greece.png");
            this.imageList1.Images.SetKeyName(14, "Korea.png");
            this.imageList1.Images.SetKeyName(15, "Malays.png");
            this.imageList1.Images.SetKeyName(16, "Russia.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "UK.png");
            this.imageList2.Images.SetKeyName(1, "fr.png");
            this.imageList2.Images.SetKeyName(2, "ger.png");
            this.imageList2.Images.SetKeyName(3, "ita.png");
            this.imageList2.Images.SetKeyName(4, "pol.png");
            this.imageList2.Images.SetKeyName(5, "por.png");
            this.imageList2.Images.SetKeyName(6, "Rom.png");
            this.imageList2.Images.SetKeyName(7, "spa.png");
            this.imageList2.Images.SetKeyName(8, "aus_140x90.png");
            this.imageList2.Images.SetKeyName(9, "canada_140x90.png");
            this.imageList2.Images.SetKeyName(10, "czech_140x90.png");
            this.imageList2.Images.SetKeyName(11, "Egypt_140x90.png");
            this.imageList2.Images.SetKeyName(12, "Estonia_140x90.png");
            this.imageList2.Images.SetKeyName(13, "greece_140x90.png");
            this.imageList2.Images.SetKeyName(14, "koria_140x90.png");
            this.imageList2.Images.SetKeyName(15, "malasia_140x90.png");
            this.imageList2.Images.SetKeyName(16, "russia_140x90.png");
            // 
            // FrmTruckinterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 687);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Corbel", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmTruckinterface";
            this.Text = "FrmTruckinterface";
            this.Load += new System.EventHandler(this.FrmTruckinterface_Load);
            this.Resize += new System.EventHandler(this.FrmTruckinterface_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.pan_lang.ResumeLayout(false);
            this.pan_truck.ResumeLayout(false);
            this.pan_del.ResumeLayout(false);
            this.pan_order.ResumeLayout(false);
            this.pan_info.ResumeLayout(false);
            this.pan_comp.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tab_lang.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_inloader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_Container)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_euro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_other)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_tryktype_back)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin5)).EndInit();
            this.panel21.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin_next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin_back)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout5)).EndInit();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.panel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout_next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkout_back)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_sale_next)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.tableLayoutPanel37.PerformLayout();
            this.tableLayoutPanel38.ResumeLayout(false);
            this.tableLayoutPanel38.PerformLayout();
            this.tableLayoutPanel39.ResumeLayout(false);
            this.tableLayoutPanel39.PerformLayout();
            this.tableLayoutPanel40.ResumeLayout(false);
            this.tableLayoutPanel40.PerformLayout();
            this.tableLayoutPanel41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.tableLayoutPanel44.PerformLayout();
            this.tableLayoutPanel45.ResumeLayout(false);
            this.tableLayoutPanel45.PerformLayout();
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tableLayoutPanel46.PerformLayout();
            this.tableLayoutPanel47.ResumeLayout(false);
            this.tableLayoutPanel47.PerformLayout();
            this.tableLayoutPanel48.ResumeLayout(false);
            this.tableLayoutPanel49.ResumeLayout(false);
            this.tableLayoutPanel49.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel29.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.tableLayoutPanel34.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lbl_head;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel pan_lang;
        private System.Windows.Forms.Label lbl_pro_sltlang;
        private System.Windows.Forms.Panel pan_truck;
        private System.Windows.Forms.Label lbl_pro_sltTruck;
        private System.Windows.Forms.Panel pan_del;
        private System.Windows.Forms.Label lbl_pro_deli;
        private System.Windows.Forms.Panel pan_order;
        private System.Windows.Forms.Label lbl_pro_order;
        private System.Windows.Forms.Panel pan_info;
        private System.Windows.Forms.Label lbl_pro_info;
        private System.Windows.Forms.Panel pan_comp;
        private System.Windows.Forms.Label lbl_pro_complete;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_lang;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.PictureBox pb_trktype_euro;
        private System.Windows.Forms.PictureBox pb_trktype_other;
        private System.Windows.Forms.Label lbl_trktype_Inloader;
        private System.Windows.Forms.Label lbl_trktype_euro;
        private System.Windows.Forms.Label lbl_trktype_Container;
        private System.Windows.Forms.Label lbl_trktype_other;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lbl_lang_otrlnag_nor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lbl_lang_rsnsel_nor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lbl_slt_lang_nor;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label lbl_lang_otherlang_max;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label lb_lang_rsnslt_max;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lbl_slt_lang_max;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label lbl_slt_truck;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.PictureBox pb_trkin1;
        private System.Windows.Forms.PictureBox pb_trkin3;
        private System.Windows.Forms.PictureBox pb_trkin4;
        private System.Windows.Forms.PictureBox pb_trkin5;
        private System.Windows.Forms.Panel pan_trkin1;
        private System.Windows.Forms.Panel pan_trkin2;
        private System.Windows.Forms.Panel pan_trkin3;
        private System.Windows.Forms.Panel pan_trkin4;
        private System.Windows.Forms.Panel pan_trkin5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label lbl_slt_truckin;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.PictureBox pb_trkin_next;
        private System.Windows.Forms.PictureBox pb_trkin_back;
        private System.Windows.Forms.Label lb_invsble_sltd_nor;
        private System.Windows.Forms.Label lbl_title_info;
        private System.Windows.Forms.Label lbl_title_returnorder;
        private System.Windows.Forms.Label lbl_title_out;
        private System.Windows.Forms.Label lbl_titlt_truck;
        private System.Windows.Forms.Label lbl_titlt_truckin;
        private System.Windows.Forms.Label lbl_title_deli;
        private System.Windows.Forms.Label lbl_title_lang;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label lbl_pono;
        private System.Windows.Forms.Label lbl_expo;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Label lbl_sg_still;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label lbl_del_stil;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label lbl_nonsg_still;
        private System.Windows.Forms.Label lbl_exstill;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lbl_slt_truckout;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.PictureBox pb_trkout1;
        private System.Windows.Forms.PictureBox pb_trkout2;
        private System.Windows.Forms.PictureBox pb_trkout3;
        private System.Windows.Forms.PictureBox pb_trkout4;
        private System.Windows.Forms.PictureBox pb_trkout5;
        private System.Windows.Forms.Panel pan_trkout1;
        private System.Windows.Forms.Panel pan_trkout2;
        private System.Windows.Forms.Panel pan_trkout3;
        private System.Windows.Forms.Panel pan_trkout4;
        private System.Windows.Forms.Panel pan_trkout5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.PictureBox pb_trkout_next;
        private System.Windows.Forms.PictureBox pb_trkout_back;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Label lbl_trkout1;
        private System.Windows.Forms.Label lbl_trkout2;
        private System.Windows.Forms.Label lbl_trkout3;
        private System.Windows.Forms.Label lbl_trkout4;
        private System.Windows.Forms.Label lbl_trkout5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Label lbl_saleno;
        private System.Windows.Forms.Label lbl_ex_so;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.PictureBox pb_sale_next;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.Label lbl_com_order;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.Label lbl_com_confirm;
        private System.Windows.Forms.Label lbl_comp_thanks;
        private System.Windows.Forms.Label lb_invsble_sltd_max;
        private System.Windows.Forms.Label lbl_invsble_slttruck;
        private System.Windows.Forms.Label lbl_invsble_trkinslt;
        private System.Windows.Forms.Panel pan_slt_inloader;
        private System.Windows.Forms.Panel pan_sltd_container;
        private System.Windows.Forms.Panel pan_sltd_euro;
        private System.Windows.Forms.Panel pan_sltd_other;
        private System.Windows.Forms.Label lbl_other;
        private System.Windows.Forms.Label lbl_cullet;
        private System.Windows.Forms.Label lbl_still;
        private System.Windows.Forms.Label lbl_glass;
        private System.Windows.Forms.Label lbl_empty;
        private System.Windows.Forms.PictureBox pb_trktype_inloader;
        private System.Windows.Forms.PictureBox pb_trktype_Container;
        private System.Windows.Forms.PictureBox pb_trkin2;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label lbl_trkin1;
        private System.Windows.Forms.Label lbl_trkin2;
        private System.Windows.Forms.Label lbl_trkin3;
        private System.Windows.Forms.Label lbl_trkin4;
        private System.Windows.Forms.Label lbl_trkin5;
        private System.Windows.Forms.PictureBox pb_tryktype_back;
        private System.Windows.Forms.PictureBox pb_trktype_next;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Label lbl_trkinfo_truckno;
        private System.Windows.Forms.Label lbl_trkinfo_extruckno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.Label lbl_trkinfo_trailrno;
        private System.Windows.Forms.Label lbl_trkinfo_extrailrno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Label lbl_trkinfo_name;
        private System.Windows.Forms.Label lbl_trkinfo_exname;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.Label lbl_trkinfo_mobno;
        private System.Windows.Forms.Label lbl_trkinfo_exmobno;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.RichTextBox richTextBox11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.Label lbl_trkinfo1_trkno;
        private System.Windows.Forms.Label lbl_trkinfo1_extrkno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.Label lbl_trkinfo1_tralrno;
        private System.Windows.Forms.Label lbl_trkinfo1_extralrno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Label lbl_trkinfo1_name;
        private System.Windows.Forms.Label lbl_trkinfo1_exname;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Label lbl_trkinfo1_mobno;
        private System.Windows.Forms.Label lbl_trkinfo1_exmobno;
        private System.Windows.Forms.RichTextBox richTextBox12;
        private System.Windows.Forms.RichTextBox richTextBox13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.RichTextBox richTextBox14;
        private System.Windows.Forms.RichTextBox richTextBox15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel49;
        private System.Windows.Forms.Label lbl_trkinfo1_conno;
        private System.Windows.Forms.Label lbl_trkinfo1_exconno;
        private System.Windows.Forms.RichTextBox richTextBox16;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}