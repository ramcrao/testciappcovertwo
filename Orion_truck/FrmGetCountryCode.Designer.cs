﻿namespace Orion_truck
{
    partial class FrmGetCountryCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGetCountryCode));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imageList_country = new System.Windows.Forms.ImageList(this.components);
            this.txt_countryfilter = new System.Windows.Forms.TextBox();
            this.lbl_countryfilter = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grid_countrycode = new System.Windows.Forms.DataGridView();
            this.btn_ok = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.flag = new System.Windows.Forms.DataGridViewImageColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_countrycode)).BeginInit();
            this.btn_ok.SuspendLayout();
            this.btn_close.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList_country
            // 
            this.imageList_country.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_country.ImageStream")));
            this.imageList_country.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_country.Images.SetKeyName(0, "ad.png");
            this.imageList_country.Images.SetKeyName(1, "ae.png");
            this.imageList_country.Images.SetKeyName(2, "af.png");
            this.imageList_country.Images.SetKeyName(3, "ag.png");
            this.imageList_country.Images.SetKeyName(4, "al.png");
            this.imageList_country.Images.SetKeyName(5, "am.png");
            this.imageList_country.Images.SetKeyName(6, "ao.png");
            this.imageList_country.Images.SetKeyName(7, "ar.png");
            this.imageList_country.Images.SetKeyName(8, "at.png");
            this.imageList_country.Images.SetKeyName(9, "au.png");
            this.imageList_country.Images.SetKeyName(10, "az.png");
            this.imageList_country.Images.SetKeyName(11, "ba.png");
            this.imageList_country.Images.SetKeyName(12, "bb.png");
            this.imageList_country.Images.SetKeyName(13, "bd.png");
            this.imageList_country.Images.SetKeyName(14, "be.png");
            this.imageList_country.Images.SetKeyName(15, "bf.png");
            this.imageList_country.Images.SetKeyName(16, "bg.png");
            this.imageList_country.Images.SetKeyName(17, "bh.png");
            this.imageList_country.Images.SetKeyName(18, "bi.png");
            this.imageList_country.Images.SetKeyName(19, "bj.png");
            this.imageList_country.Images.SetKeyName(20, "bn.png");
            this.imageList_country.Images.SetKeyName(21, "bo.png");
            this.imageList_country.Images.SetKeyName(22, "br.png");
            this.imageList_country.Images.SetKeyName(23, "bs.png");
            this.imageList_country.Images.SetKeyName(24, "bt.png");
            this.imageList_country.Images.SetKeyName(25, "bw.png");
            this.imageList_country.Images.SetKeyName(26, "by.png");
            this.imageList_country.Images.SetKeyName(27, "bz.png");
            this.imageList_country.Images.SetKeyName(28, "ca.png");
            this.imageList_country.Images.SetKeyName(29, "cd.png");
            this.imageList_country.Images.SetKeyName(30, "cf.png");
            this.imageList_country.Images.SetKeyName(31, "cg.png");
            this.imageList_country.Images.SetKeyName(32, "ch.png");
            this.imageList_country.Images.SetKeyName(33, "ci.png");
            this.imageList_country.Images.SetKeyName(34, "cl.png");
            this.imageList_country.Images.SetKeyName(35, "cm.png");
            this.imageList_country.Images.SetKeyName(36, "cn.png");
            this.imageList_country.Images.SetKeyName(37, "co.png");
            this.imageList_country.Images.SetKeyName(38, "cr.png");
            this.imageList_country.Images.SetKeyName(39, "cu.png");
            this.imageList_country.Images.SetKeyName(40, "cv.png");
            this.imageList_country.Images.SetKeyName(41, "cy.png");
            this.imageList_country.Images.SetKeyName(42, "cz.png");
            this.imageList_country.Images.SetKeyName(43, "de.png");
            this.imageList_country.Images.SetKeyName(44, "dj.png");
            this.imageList_country.Images.SetKeyName(45, "dk.png");
            this.imageList_country.Images.SetKeyName(46, "dm.png");
            this.imageList_country.Images.SetKeyName(47, "do.png");
            this.imageList_country.Images.SetKeyName(48, "dz.png");
            this.imageList_country.Images.SetKeyName(49, "ec.png");
            this.imageList_country.Images.SetKeyName(50, "ee.png");
            this.imageList_country.Images.SetKeyName(51, "eg.png");
            this.imageList_country.Images.SetKeyName(52, "eh.png");
            this.imageList_country.Images.SetKeyName(53, "er.png");
            this.imageList_country.Images.SetKeyName(54, "es.png");
            this.imageList_country.Images.SetKeyName(55, "et.png");
            this.imageList_country.Images.SetKeyName(56, "fi.png");
            this.imageList_country.Images.SetKeyName(57, "fj.png");
            this.imageList_country.Images.SetKeyName(58, "fm.png");
            this.imageList_country.Images.SetKeyName(59, "fr.png");
            this.imageList_country.Images.SetKeyName(60, "ga.png");
            this.imageList_country.Images.SetKeyName(61, "gb.png");
            this.imageList_country.Images.SetKeyName(62, "gd.png");
            this.imageList_country.Images.SetKeyName(63, "ge.png");
            this.imageList_country.Images.SetKeyName(64, "gh.png");
            this.imageList_country.Images.SetKeyName(65, "gm.png");
            this.imageList_country.Images.SetKeyName(66, "gn.png");
            this.imageList_country.Images.SetKeyName(67, "gq.png");
            this.imageList_country.Images.SetKeyName(68, "gr.png");
            this.imageList_country.Images.SetKeyName(69, "gt.png");
            this.imageList_country.Images.SetKeyName(70, "gw.png");
            this.imageList_country.Images.SetKeyName(71, "gy.png");
            this.imageList_country.Images.SetKeyName(72, "hn.png");
            this.imageList_country.Images.SetKeyName(73, "hr.png");
            this.imageList_country.Images.SetKeyName(74, "ht.png");
            this.imageList_country.Images.SetKeyName(75, "hu.png");
            this.imageList_country.Images.SetKeyName(76, "id.png");
            this.imageList_country.Images.SetKeyName(77, "ie.png");
            this.imageList_country.Images.SetKeyName(78, "il.png");
            this.imageList_country.Images.SetKeyName(79, "in.png");
            this.imageList_country.Images.SetKeyName(80, "iq.png");
            this.imageList_country.Images.SetKeyName(81, "ir.png");
            this.imageList_country.Images.SetKeyName(82, "is.png");
            this.imageList_country.Images.SetKeyName(83, "it.png");
            this.imageList_country.Images.SetKeyName(84, "jm.png");
            this.imageList_country.Images.SetKeyName(85, "jo.png");
            this.imageList_country.Images.SetKeyName(86, "jp.png");
            this.imageList_country.Images.SetKeyName(87, "ke.png");
            this.imageList_country.Images.SetKeyName(88, "kg.png");
            this.imageList_country.Images.SetKeyName(89, "kh.png");
            this.imageList_country.Images.SetKeyName(90, "ki.png");
            this.imageList_country.Images.SetKeyName(91, "km.png");
            this.imageList_country.Images.SetKeyName(92, "kn.png");
            this.imageList_country.Images.SetKeyName(93, "kp.png");
            this.imageList_country.Images.SetKeyName(94, "kr.png");
            this.imageList_country.Images.SetKeyName(95, "kw.png");
            this.imageList_country.Images.SetKeyName(96, "kz.png");
            this.imageList_country.Images.SetKeyName(97, "la.png");
            this.imageList_country.Images.SetKeyName(98, "lb.png");
            this.imageList_country.Images.SetKeyName(99, "lc.png");
            this.imageList_country.Images.SetKeyName(100, "li.png");
            this.imageList_country.Images.SetKeyName(101, "lk.png");
            this.imageList_country.Images.SetKeyName(102, "lr.png");
            this.imageList_country.Images.SetKeyName(103, "ls.png");
            this.imageList_country.Images.SetKeyName(104, "lt.png");
            this.imageList_country.Images.SetKeyName(105, "lu.png");
            this.imageList_country.Images.SetKeyName(106, "lv.png");
            this.imageList_country.Images.SetKeyName(107, "ly.png");
            this.imageList_country.Images.SetKeyName(108, "ma.png");
            this.imageList_country.Images.SetKeyName(109, "mc.png");
            this.imageList_country.Images.SetKeyName(110, "md.png");
            this.imageList_country.Images.SetKeyName(111, "me.png");
            this.imageList_country.Images.SetKeyName(112, "mg.png");
            this.imageList_country.Images.SetKeyName(113, "mh.png");
            this.imageList_country.Images.SetKeyName(114, "mk.png");
            this.imageList_country.Images.SetKeyName(115, "ml.png");
            this.imageList_country.Images.SetKeyName(116, "mm.png");
            this.imageList_country.Images.SetKeyName(117, "mn.png");
            this.imageList_country.Images.SetKeyName(118, "mr.png");
            this.imageList_country.Images.SetKeyName(119, "mt.png");
            this.imageList_country.Images.SetKeyName(120, "mu.png");
            this.imageList_country.Images.SetKeyName(121, "mv.png");
            this.imageList_country.Images.SetKeyName(122, "mw.png");
            this.imageList_country.Images.SetKeyName(123, "mx.png");
            this.imageList_country.Images.SetKeyName(124, "my.png");
            this.imageList_country.Images.SetKeyName(125, "mz.png");
            this.imageList_country.Images.SetKeyName(126, "na.png");
            this.imageList_country.Images.SetKeyName(127, "ne.png");
            this.imageList_country.Images.SetKeyName(128, "ng.png");
            this.imageList_country.Images.SetKeyName(129, "ni.png");
            this.imageList_country.Images.SetKeyName(130, "nl.png");
            this.imageList_country.Images.SetKeyName(131, "no.png");
            this.imageList_country.Images.SetKeyName(132, "np.png");
            this.imageList_country.Images.SetKeyName(133, "nr.png");
            this.imageList_country.Images.SetKeyName(134, "nz.png");
            this.imageList_country.Images.SetKeyName(135, "om.png");
            this.imageList_country.Images.SetKeyName(136, "pa.png");
            this.imageList_country.Images.SetKeyName(137, "pe.png");
            this.imageList_country.Images.SetKeyName(138, "pg.png");
            this.imageList_country.Images.SetKeyName(139, "ph.png");
            this.imageList_country.Images.SetKeyName(140, "pk.png");
            this.imageList_country.Images.SetKeyName(141, "pl.png");
            this.imageList_country.Images.SetKeyName(142, "pt.png");
            this.imageList_country.Images.SetKeyName(143, "pw.png");
            this.imageList_country.Images.SetKeyName(144, "py.png");
            this.imageList_country.Images.SetKeyName(145, "qa.png");
            this.imageList_country.Images.SetKeyName(146, "ro.png");
            this.imageList_country.Images.SetKeyName(147, "rs.png");
            this.imageList_country.Images.SetKeyName(148, "ru.png");
            this.imageList_country.Images.SetKeyName(149, "rw.png");
            this.imageList_country.Images.SetKeyName(150, "sa.png");
            this.imageList_country.Images.SetKeyName(151, "sb.png");
            this.imageList_country.Images.SetKeyName(152, "sc.png");
            this.imageList_country.Images.SetKeyName(153, "sd.png");
            this.imageList_country.Images.SetKeyName(154, "se.png");
            this.imageList_country.Images.SetKeyName(155, "sg.png");
            this.imageList_country.Images.SetKeyName(156, "si.png");
            this.imageList_country.Images.SetKeyName(157, "sk.png");
            this.imageList_country.Images.SetKeyName(158, "sl.png");
            this.imageList_country.Images.SetKeyName(159, "sm.png");
            this.imageList_country.Images.SetKeyName(160, "sn.png");
            this.imageList_country.Images.SetKeyName(161, "so.png");
            this.imageList_country.Images.SetKeyName(162, "sr.png");
            this.imageList_country.Images.SetKeyName(163, "st.png");
            this.imageList_country.Images.SetKeyName(164, "sv.png");
            this.imageList_country.Images.SetKeyName(165, "sy.png");
            this.imageList_country.Images.SetKeyName(166, "sz.png");
            this.imageList_country.Images.SetKeyName(167, "td.png");
            this.imageList_country.Images.SetKeyName(168, "tg.png");
            this.imageList_country.Images.SetKeyName(169, "th.png");
            this.imageList_country.Images.SetKeyName(170, "tj.png");
            this.imageList_country.Images.SetKeyName(171, "tl.png");
            this.imageList_country.Images.SetKeyName(172, "tm.png");
            this.imageList_country.Images.SetKeyName(173, "tn.png");
            this.imageList_country.Images.SetKeyName(174, "to.png");
            this.imageList_country.Images.SetKeyName(175, "tr.png");
            this.imageList_country.Images.SetKeyName(176, "tt.png");
            this.imageList_country.Images.SetKeyName(177, "tv.png");
            this.imageList_country.Images.SetKeyName(178, "tw.png");
            this.imageList_country.Images.SetKeyName(179, "tz.png");
            this.imageList_country.Images.SetKeyName(180, "ua.png");
            this.imageList_country.Images.SetKeyName(181, "ug.png");
            this.imageList_country.Images.SetKeyName(182, "us.png");
            this.imageList_country.Images.SetKeyName(183, "uy.png");
            this.imageList_country.Images.SetKeyName(184, "uz.png");
            this.imageList_country.Images.SetKeyName(185, "va.png");
            this.imageList_country.Images.SetKeyName(186, "vc.png");
            this.imageList_country.Images.SetKeyName(187, "ve.png");
            this.imageList_country.Images.SetKeyName(188, "vn.png");
            this.imageList_country.Images.SetKeyName(189, "vu.png");
            this.imageList_country.Images.SetKeyName(190, "ws.png");
            this.imageList_country.Images.SetKeyName(191, "ye.png");
            this.imageList_country.Images.SetKeyName(192, "za.png");
            this.imageList_country.Images.SetKeyName(193, "zm.png");
            this.imageList_country.Images.SetKeyName(194, "zw.png");
            // 
            // txt_countryfilter
            // 
            this.txt_countryfilter.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_countryfilter.Location = new System.Drawing.Point(122, 19);
            this.txt_countryfilter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_countryfilter.Name = "txt_countryfilter";
            this.txt_countryfilter.Size = new System.Drawing.Size(432, 35);
            this.txt_countryfilter.TabIndex = 19;
            this.txt_countryfilter.TextChanged += new System.EventHandler(this.txt_countryfilter_TextChanged);
            // 
            // lbl_countryfilter
            // 
            this.lbl_countryfilter.AutoSize = true;
            this.lbl_countryfilter.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_countryfilter.Location = new System.Drawing.Point(4, 24);
            this.lbl_countryfilter.Name = "lbl_countryfilter";
            this.lbl_countryfilter.Size = new System.Drawing.Size(110, 22);
            this.lbl_countryfilter.TabIndex = 20;
            this.lbl_countryfilter.Text = "Country Name";
            this.lbl_countryfilter.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_countryfilter);
            this.groupBox1.Controls.Add(this.txt_countryfilter);
            this.groupBox1.Location = new System.Drawing.Point(6, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 67);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grid_countrycode);
            this.groupBox2.Location = new System.Drawing.Point(6, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(563, 216);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // grid_countrycode
            // 
            this.grid_countrycode.AllowUserToAddRows = false;
            this.grid_countrycode.AllowUserToResizeColumns = false;
            this.grid_countrycode.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grid_countrycode.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.grid_countrycode.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grid_countrycode.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grid_countrycode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grid_countrycode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.flag,
            this.name,
            this.code,
            this.slno});
            this.grid_countrycode.GridColor = System.Drawing.SystemColors.Control;
            this.grid_countrycode.Location = new System.Drawing.Point(9, 13);
            this.grid_countrycode.MultiSelect = false;
            this.grid_countrycode.Name = "grid_countrycode";
            this.grid_countrycode.ReadOnly = true;
            this.grid_countrycode.RowHeadersVisible = false;
            this.grid_countrycode.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid_countrycode.RowTemplate.DividerHeight = 2;
            this.grid_countrycode.RowTemplate.Height = 30;
            this.grid_countrycode.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid_countrycode.Size = new System.Drawing.Size(545, 190);
            this.grid_countrycode.TabIndex = 1;
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.Green;
            this.btn_ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ok.Controls.Add(this.label3);
            this.btn_ok.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ok.Location = new System.Drawing.Point(351, 308);
            this.btn_ok.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(102, 38);
            this.btn_ok.TabIndex = 47;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(5, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 27);
            this.label3.TabIndex = 15;
            this.label3.Text = "Ok";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Red;
            this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_close.Controls.Add(this.label2);
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(465, 308);
            this.btn_close.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(102, 38);
            this.btn_close.TabIndex = 48;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 27);
            this.label2.TabIndex = 15;
            this.label2.Text = "Close";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // flag
            // 
            this.flag.HeaderText = "Country Flag";
            this.flag.Name = "flag";
            this.flag.ReadOnly = true;
            this.flag.Width = 78;
            // 
            // name
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.DefaultCellStyle = dataGridViewCellStyle8;
            this.name.HeaderText = "Country Name";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 104;
            // 
            // code
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code.DefaultCellStyle = dataGridViewCellStyle9;
            this.code.HeaderText = "Country Code";
            this.code.Name = "code";
            this.code.ReadOnly = true;
            this.code.Width = 102;
            // 
            // slno
            // 
            this.slno.HeaderText = "slno";
            this.slno.Name = "slno";
            this.slno.ReadOnly = true;
            this.slno.Visible = false;
            this.slno.Width = 52;
            // 
            // FrmGetCountryCode
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(576, 350);
            this.ControlBox = false;
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximumSize = new System.Drawing.Size(592, 389);
            this.MinimumSize = new System.Drawing.Size(592, 389);
            this.Name = "FrmGetCountryCode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Country Code";
            this.Load += new System.EventHandler(this.FrmGetCountryCode_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_countrycode)).EndInit();
            this.btn_ok.ResumeLayout(false);
            this.btn_close.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList_country;
        private System.Windows.Forms.TextBox txt_countryfilter;
        private System.Windows.Forms.Label lbl_countryfilter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel btn_ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel btn_close;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView grid_countrycode;
        private System.Windows.Forms.DataGridViewImageColumn flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn code;
        private System.Windows.Forms.DataGridViewTextBoxColumn slno;
    }
}