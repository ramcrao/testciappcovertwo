﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Orion_truck.Model;
using Orion_truck.Entity;
using System.Deployment.Application;

namespace Orion_truck
{
    public partial class MDIParent : Form
    {
        private int childFormNumber = 0;

        public User_Model _usermodel = new User_Model();
        User_Entity _userentity = new User_Entity();
        MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        MessageValidation_Model _alertmodel = new MessageValidation_Model();


        public MDIParent()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

       

       

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIParent_Load(object sender, EventArgs e)
        {
            try
            {

                DataSet dsMaster = new DataSet();

                dsMaster = _usermodel.getMDIMasterData(Globals.GlobalUsername, Globals.GlobalLanguage);

                if (dsMaster.Tables[2].Rows.Count > 0)
                {
                    lblusername.Text = "Welcome " + Globals.GlobalUsername.ToString().Trim() + " - " + dsMaster.Tables[2].Rows[0].ItemArray[0].ToString().Trim();
                    toolsitedesc.Text = dsMaster.Tables[2].Rows[0].ItemArray[1].ToString().Trim() + " " + dsMaster.Tables[3].Rows[0].ItemArray[0].ToString().Trim() + " - " + dsMaster.Tables[3].Rows[0].ItemArray[1].ToString().Trim();
                    
                    tooldate.Text = dsMaster.Tables[3].Rows[0].ItemArray[2].ToString();
                }

                #region clickonece source
              //  ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
               // toolversion.Text = "Version " + ad.CurrentVersion.ToString();
                #endregion

               toolversion.Text = "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                //Form frmsggi = (Form)SampleAssembly.CreateInstance(ts[i].ToString());
                Truckinterface_v1 frmsggi = new Truckinterface_v1();
                frmsggi.MdiParent = this;
                frmsggi.WindowState = FormWindowState.Maximized;
                frmsggi.MaximizeBox = false;
                frmsggi.MinimizeBox = false;
                frmsggi.ControlBox = false;
                frmsggi.BringToFront();
                DisposeAllButThis(frmsggi);
                frmsggi.WindowState = FormWindowState.Maximized;
                frmsggi.MaximizeBox = true;
                frmsggi.MinimizeBox = true;
                frmsggi.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void DisposeAllButThis(Form form)
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (frm != form)
                {
                    frm.Close();
                }
            }
            return;
        }

        private void imglogout_Click(object sender, EventArgs e)
        {
            try
            {

                _alertentity = _alertmodel.Alert_Information("LG", "A1681", Globals.GlobalLanguage);
                DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (result == DialogResult.Yes)
                {
                  _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);

                    Application.Exit();
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MDIParent_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                
                _userentity.flag = _usermodel.getDeleteLoginUsers(Globals.GlobalUsername, Globals.GlobalSGID);
               
                Application.Exit();
            }
            catch
            {
            }
        }
    }
}
