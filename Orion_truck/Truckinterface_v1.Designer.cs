﻿namespace Orion_truck
{
    partial class Truckinterface_v1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Truckinterface_v1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.lbl_head = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pan_truck = new System.Windows.Forms.Panel();
            this.lbl_pro_sltTruck = new System.Windows.Forms.Label();
            this.pan_comp = new System.Windows.Forms.Panel();
            this.lbl_pro_complete = new System.Windows.Forms.Label();
            this.pan_info = new System.Windows.Forms.Panel();
            this.lbl_pro_info = new System.Windows.Forms.Label();
            this.pan_order = new System.Windows.Forms.Panel();
            this.lbl_pro_order = new System.Windows.Forms.Label();
            this.pan_del = new System.Windows.Forms.Panel();
            this.lbl_pro_deli = new System.Windows.Forms.Label();
            this.pan_lang = new System.Windows.Forms.Panel();
            this.lbl_pro_sltlang1 = new System.Windows.Forms.Label();
            this.lbl_pro_sltlang = new System.Windows.Forms.Label();
            this.tabcontrol1 = new System.Windows.Forms.TabControl();
            this.tab_lang = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lbl_title_lang = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lb_invsble_sltd_nor = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_lang_rsnsel_nor = new System.Windows.Forms.Label();
            this.lbl_lang_otrlnag_nor = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lbl_slt_lang_nor = new System.Windows.Forms.Label();
            this.tab_trucktype = new System.Windows.Forms.TabPage();
            this.pb_home = new System.Windows.Forms.PictureBox();
            this.lbl_titlt_truck = new System.Windows.Forms.Label();
            this.lbl_invsble_slttruck = new System.Windows.Forms.Label();
            this.lbl_trktype_Container = new System.Windows.Forms.Label();
            this.lbl_trktype_euro = new System.Windows.Forms.Label();
            this.lbl_trktype_other = new System.Windows.Forms.Label();
            this.lbl_trktype_Inloader = new System.Windows.Forms.Label();
            this.lbl_slt_truck = new System.Windows.Forms.Label();
            this.pan_slt_inloader = new System.Windows.Forms.Panel();
            this.pan_sltd_container = new System.Windows.Forms.Panel();
            this.pan_sltd_euro = new System.Windows.Forms.Panel();
            this.pan_sltd_other = new System.Windows.Forms.Panel();
            this.pb_trktype_Container = new System.Windows.Forms.PictureBox();
            this.pb_trktype_inloader = new System.Windows.Forms.PictureBox();
            this.pb_trktype_other = new System.Windows.Forms.PictureBox();
            this.pb_trktype_euro = new System.Windows.Forms.PictureBox();
            this.next_truck = new System.Windows.Forms.Panel();
            this.lbl_trk_next = new System.Windows.Forms.Label();
            this.back_trk = new System.Windows.Forms.Panel();
            this.lbl_trk_back = new System.Windows.Forms.Label();
            this.tab_truckin = new System.Windows.Forms.TabPage();
            this.lbl_trukin2_title = new System.Windows.Forms.Label();
            this.lbl_truckin1_title = new System.Windows.Forms.Label();
            this.lbl_titlt_truckineuro_empty = new System.Windows.Forms.Label();
            this.lbl_titlt_truckineuro_goods = new System.Windows.Forms.Label();
            this.lbl_stillcullt = new System.Windows.Forms.Label();
            this.lbl_packing = new System.Windows.Forms.Label();
            this.lbl_wood = new System.Windows.Forms.Label();
            this.lbl_trkin_sltgoods = new System.Windows.Forms.Label();
            this.lbl_titlt_truckin_empty = new System.Windows.Forms.Label();
            this.lbl_titlt_truckin_or = new System.Windows.Forms.Label();
            this.lbl_titlt_truckin_goods = new System.Windows.Forms.Label();
            this.lbl_slt_truckin = new System.Windows.Forms.Label();
            this.lbl_other = new System.Windows.Forms.Label();
            this.lbl_cullet = new System.Windows.Forms.Label();
            this.lbl_still = new System.Windows.Forms.Label();
            this.lbl_glass = new System.Windows.Forms.Label();
            this.pan_trkin1 = new System.Windows.Forms.Panel();
            this.lbl_empty = new System.Windows.Forms.Label();
            this.pan_trkin2 = new System.Windows.Forms.Panel();
            this.lbl_invsble_trkinslt = new System.Windows.Forms.Label();
            this.lbl_trkin1 = new System.Windows.Forms.Label();
            this.pan_trkin3 = new System.Windows.Forms.Panel();
            this.lbl_trkin2 = new System.Windows.Forms.Label();
            this.pan_trkin4 = new System.Windows.Forms.Panel();
            this.pan_trkin5 = new System.Windows.Forms.Panel();
            this.lbl_trkin3 = new System.Windows.Forms.Label();
            this.lbl_trkin4 = new System.Windows.Forms.Label();
            this.lbl_trkin5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.next_truckin = new System.Windows.Forms.Panel();
            this.lbl_n_trkin = new System.Windows.Forms.Label();
            this.back_truckin = new System.Windows.Forms.Panel();
            this.lbl_b_trkin = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pb_in = new System.Windows.Forms.PictureBox();
            this.pb_trkin2 = new System.Windows.Forms.PictureBox();
            this.pb_trkin1 = new System.Windows.Forms.PictureBox();
            this.pb_trkin3 = new System.Windows.Forms.PictureBox();
            this.pb_trkin5 = new System.Windows.Forms.PictureBox();
            this.pb_trkin4 = new System.Windows.Forms.PictureBox();
            this.tab_po = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lbl_title_deli = new System.Windows.Forms.Label();
            this.pb_clear = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.next_deli = new System.Windows.Forms.Panel();
            this.lbl_n_po = new System.Windows.Forms.Label();
            this.back_deli = new System.Windows.Forms.Panel();
            this.lbl_b_po = new System.Windows.Forms.Label();
            this.pan_truckin_still = new System.Windows.Forms.Panel();
            this.lbl_sg_still = new System.Windows.Forms.Label();
            this.txt_still = new System.Windows.Forms.TextBox();
            this.lbl_del_stil = new System.Windows.Forms.Label();
            this.pb_chk_nonsgstill = new System.Windows.Forms.PictureBox();
            this.lbl_exstill = new System.Windows.Forms.Label();
            this.pb_chk_sgstill = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lbl_nonsg_still = new System.Windows.Forms.Label();
            this.pan_truckin_po = new System.Windows.Forms.Panel();
            this.lbl_CustReturn = new System.Windows.Forms.Label();
            this.pb_chkbox_cusReturn = new System.Windows.Forms.PictureBox();
            this.txt_po = new System.Windows.Forms.TextBox();
            this.lbl_expo = new System.Windows.Forms.Label();
            this.lblCustR_SO = new System.Windows.Forms.Label();
            this.lbl_pono = new System.Windows.Forms.Label();
            this.lblCustR_SOEX = new System.Windows.Forms.Label();
            this.txtCustR_SO = new System.Windows.Forms.TextBox();
            this.pan_truckin_cullet = new System.Windows.Forms.Panel();
            this.txtSupp = new System.Windows.Forms.TextBox();
            this.lblSupp = new System.Windows.Forms.Label();
            this.lblSupEX = new System.Windows.Forms.Label();
            this.txtNoOfBags = new System.Windows.Forms.TextBox();
            this.lblNoOfBags = new System.Windows.Forms.Label();
            this.lblNoOfBagsEx = new System.Windows.Forms.Label();
            this.txt_cul = new System.Windows.Forms.TextBox();
            this.lbl_cul = new System.Windows.Forms.Label();
            this.lbl_excul = new System.Windows.Forms.Label();
            this.tab_truckout = new System.Windows.Forms.TabPage();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lbl_trkout_sltgoods = new System.Windows.Forms.Label();
            this.lbl_out5 = new System.Windows.Forms.Label();
            this.lbl_title_out = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pan_out1 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.pan_out2 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.lbl_out1 = new System.Windows.Forms.Label();
            this.pan_out3 = new System.Windows.Forms.Panel();
            this.lbl_out2 = new System.Windows.Forms.Label();
            this.pan_out4 = new System.Windows.Forms.Panel();
            this.pan_out5 = new System.Windows.Forms.Panel();
            this.lbl_out3 = new System.Windows.Forms.Label();
            this.lbl_out4 = new System.Windows.Forms.Label();
            this.next_trkout = new System.Windows.Forms.Panel();
            this.lbl_n_trkout = new System.Windows.Forms.Label();
            this.back_truckout = new System.Windows.Forms.Panel();
            this.lbl_b_trkout = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.pb_out = new System.Windows.Forms.PictureBox();
            this.pb_out_2 = new System.Windows.Forms.PictureBox();
            this.pb_out_1 = new System.Windows.Forms.PictureBox();
            this.pb_out_3 = new System.Windows.Forms.PictureBox();
            this.pb_out_5 = new System.Windows.Forms.PictureBox();
            this.pb_out_4 = new System.Windows.Forms.PictureBox();
            this.tab_so = new System.Windows.Forms.TabPage();
            this.panel_still = new System.Windows.Forms.Panel();
            this.txt_so_Still = new System.Windows.Forms.TextBox();
            this.lbl_SOSgStilltxt = new System.Windows.Forms.Label();
            this.pb_NonsoSGStill = new System.Windows.Forms.PictureBox();
            this.lbl_SOSgStillex = new System.Windows.Forms.Label();
            this.pb_soSGStill = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.lbl_SOnonSgStill = new System.Windows.Forms.Label();
            this.lbl_SOSgStill = new System.Windows.Forms.Label();
            this.lbl_title_returnorder = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.Panel();
            this.txt_so = new System.Windows.Forms.TextBox();
            this.lbl_exso = new System.Windows.Forms.Label();
            this.lbl_so = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.next_so = new System.Windows.Forms.Panel();
            this.lbl_n_so = new System.Windows.Forms.Label();
            this.back_so = new System.Windows.Forms.Panel();
            this.lbl_b_so = new System.Windows.Forms.Label();
            this.tab_trkinfo = new System.Windows.Forms.TabPage();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.Panel();
            this.txt_Taraweight = new System.Windows.Forms.TextBox();
            this.txt_contain = new System.Windows.Forms.TextBox();
            this.txt_trilerno = new System.Windows.Forms.TextBox();
            this.txt_truckno = new System.Windows.Forms.TextBox();
            this.lbl_TaraweightEX = new System.Windows.Forms.Label();
            this.lbl_excontain = new System.Windows.Forms.Label();
            this.lbl_trkinfo_extruckno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_extrailrno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_truckno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_trailrno = new System.Windows.Forms.Label();
            this.lbl_contain = new System.Windows.Forms.Label();
            this.lbl_Taraweight = new System.Windows.Forms.Label();
            this.lbl_title_info = new System.Windows.Forms.Label();
            this.back_trkinfo1 = new System.Windows.Forms.Panel();
            this.lbl_b_trkinfo1 = new System.Windows.Forms.Label();
            this.next_trkinfo1 = new System.Windows.Forms.Panel();
            this.lbl_n_trkinfo1 = new System.Windows.Forms.Label();
            this.tab_trukinfo2 = new System.Windows.Forms.TabPage();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.Panel();
            this.txt_countrycodeDis = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.pb_getCountryCode = new System.Windows.Forms.PictureBox();
            this.lblCountryCodeTit = new System.Windows.Forms.Label();
            this.lblCountryCodeTitEx = new System.Windows.Forms.Label();
            this.cmb_country = new System.Windows.Forms.ComboBox();
            this.lbl_top_no = new System.Windows.Forms.Label();
            this.lbl_top_country = new System.Windows.Forms.Label();
            this.txt_mobno = new System.Windows.Forms.TextBox();
            this.pbflg1 = new System.Windows.Forms.PictureBox();
            this.lbl_trkinfo_exname = new System.Windows.Forms.Label();
            this.lbl_trkinfo_exmobno = new System.Windows.Forms.Label();
            this.lbl_trkinfo_name = new System.Windows.Forms.Label();
            this.lbl_trkinfo_mobno = new System.Windows.Forms.Label();
            this.lbl_tit_info_name = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.back_trkinfo2 = new System.Windows.Forms.Panel();
            this.lbl_b_trkinfor2 = new System.Windows.Forms.Label();
            this.next_trkinfo2 = new System.Windows.Forms.Panel();
            this.lbl_trkinfo2 = new System.Windows.Forms.Label();
            this.tab_comp = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lbl_invsble_com2 = new System.Windows.Forms.Label();
            this.lbl_invsble_com1 = new System.Windows.Forms.Label();
            this.lbl_com_order = new System.Windows.Forms.Label();
            this.lbl_comp_thanks = new System.Windows.Forms.Label();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.lbl_com_confirm = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbl_finish = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.imageList_country = new System.Windows.Forms.ImageList(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel2.SuspendLayout();
            this.pan_truck.SuspendLayout();
            this.pan_comp.SuspendLayout();
            this.pan_info.SuspendLayout();
            this.pan_order.SuspendLayout();
            this.pan_del.SuspendLayout();
            this.pan_lang.SuspendLayout();
            this.tabcontrol1.SuspendLayout();
            this.tab_lang.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tab_trucktype.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_Container)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_inloader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_other)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_euro)).BeginInit();
            this.next_truck.SuspendLayout();
            this.back_trk.SuspendLayout();
            this.tab_truckin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.next_truckin.SuspendLayout();
            this.back_truckin.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_in)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin4)).BeginInit();
            this.tab_po.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_clear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.next_deli.SuspendLayout();
            this.back_deli.SuspendLayout();
            this.pan_truckin_still.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chk_nonsgstill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chk_sgstill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pan_truckin_po.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chkbox_cusReturn)).BeginInit();
            this.pan_truckin_cullet.SuspendLayout();
            this.tab_truckout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.next_trkout.SuspendLayout();
            this.back_truckout.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_4)).BeginInit();
            this.tab_so.SuspendLayout();
            this.panel_still.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_NonsoSGStill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_soSGStill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.next_so.SuspendLayout();
            this.back_so.SuspendLayout();
            this.tab_trkinfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.back_trkinfo1.SuspendLayout();
            this.next_trkinfo1.SuspendLayout();
            this.tab_trukinfo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_getCountryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbflg1)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.back_trkinfo2.SuspendLayout();
            this.next_trkinfo2.SuspendLayout();
            this.tab_comp.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabcontrol1, 0, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 654);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImage = global::Orion_truck.Properties.Resources.white_back;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox10);
            this.panel1.Controls.Add(this.lbl_head);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1002, 72);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Orion_truck.Properties.Resources.top_image;
            this.pictureBox1.Location = new System.Drawing.Point(677, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(331, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox10.Image = global::Orion_truck.Properties.Resources.SGGILogo_new_1;
            this.pictureBox10.Location = new System.Drawing.Point(353, 2);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(204, 68);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 1;
            this.pictureBox10.TabStop = false;
            // 
            // lbl_head
            // 
            this.lbl_head.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_head.AutoSize = true;
            this.lbl_head.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_head.Location = new System.Drawing.Point(10, 21);
            this.lbl_head.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_head.Name = "lbl_head";
            this.lbl_head.Size = new System.Drawing.Size(162, 27);
            this.lbl_head.TabIndex = 0;
            this.lbl_head.Text = "Truck Interface";
            this.lbl_head.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.panel2.Controls.Add(this.pan_truck);
            this.panel2.Controls.Add(this.pan_comp);
            this.panel2.Controls.Add(this.pan_info);
            this.panel2.Controls.Add(this.pan_order);
            this.panel2.Controls.Add(this.pan_del);
            this.panel2.Controls.Add(this.pan_lang);
            this.panel2.Location = new System.Drawing.Point(4, 82);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1000, 57);
            this.panel2.TabIndex = 3;
            // 
            // pan_truck
            // 
            this.pan_truck.BackgroundImage = global::Orion_truck.Properties.Resources.process_dis_1;
            this.pan_truck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_truck.Controls.Add(this.lbl_pro_sltTruck);
            this.pan_truck.Location = new System.Drawing.Point(165, 4);
            this.pan_truck.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_truck.Name = "pan_truck";
            this.pan_truck.Size = new System.Drawing.Size(163, 59);
            this.pan_truck.TabIndex = 1;
            // 
            // lbl_pro_sltTruck
            // 
            this.lbl_pro_sltTruck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_sltTruck.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_sltTruck.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_sltTruck.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_sltTruck.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_sltTruck.Name = "lbl_pro_sltTruck";
            this.lbl_pro_sltTruck.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_sltTruck.TabIndex = 0;
            this.lbl_pro_sltTruck.Text = " Select your  Truck Type";
            this.lbl_pro_sltTruck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_comp
            // 
            this.pan_comp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_comp.BackgroundImage")));
            this.pan_comp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_comp.Controls.Add(this.lbl_pro_complete);
            this.pan_comp.Location = new System.Drawing.Point(810, 4);
            this.pan_comp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_comp.Name = "pan_comp";
            this.pan_comp.Size = new System.Drawing.Size(163, 59);
            this.pan_comp.TabIndex = 5;
            // 
            // lbl_pro_complete
            // 
            this.lbl_pro_complete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_complete.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_complete.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_complete.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_complete.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_complete.Name = "lbl_pro_complete";
            this.lbl_pro_complete.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_complete.TabIndex = 0;
            this.lbl_pro_complete.Text = "Registration Completed";
            this.lbl_pro_complete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_info
            // 
            this.pan_info.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_info.BackgroundImage")));
            this.pan_info.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_info.Controls.Add(this.lbl_pro_info);
            this.pan_info.Location = new System.Drawing.Point(650, 4);
            this.pan_info.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_info.Name = "pan_info";
            this.pan_info.Size = new System.Drawing.Size(163, 59);
            this.pan_info.TabIndex = 4;
            // 
            // lbl_pro_info
            // 
            this.lbl_pro_info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_info.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_info.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_info.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_info.Location = new System.Drawing.Point(29, 7);
            this.lbl_pro_info.Name = "lbl_pro_info";
            this.lbl_pro_info.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_info.TabIndex = 0;
            this.lbl_pro_info.Text = "Truck  Information";
            this.lbl_pro_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_order
            // 
            this.pan_order.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_order.BackgroundImage")));
            this.pan_order.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_order.Controls.Add(this.lbl_pro_order);
            this.pan_order.Location = new System.Drawing.Point(490, 4);
            this.pan_order.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_order.Name = "pan_order";
            this.pan_order.Size = new System.Drawing.Size(163, 59);
            this.pan_order.TabIndex = 3;
            // 
            // lbl_pro_order
            // 
            this.lbl_pro_order.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_order.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_order.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_order.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_order.Location = new System.Drawing.Point(28, 7);
            this.lbl_pro_order.Name = "lbl_pro_order";
            this.lbl_pro_order.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_order.TabIndex = 0;
            this.lbl_pro_order.Text = "Your Order";
            this.lbl_pro_order.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_del
            // 
            this.pan_del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pan_del.BackgroundImage")));
            this.pan_del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pan_del.Controls.Add(this.lbl_pro_deli);
            this.pan_del.Location = new System.Drawing.Point(325, 4);
            this.pan_del.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_del.Name = "pan_del";
            this.pan_del.Size = new System.Drawing.Size(163, 59);
            this.pan_del.TabIndex = 2;
            // 
            // lbl_pro_deli
            // 
            this.lbl_pro_deli.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_deli.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_deli.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_deli.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_deli.Location = new System.Drawing.Point(22, 5);
            this.lbl_pro_deli.Name = "lbl_pro_deli";
            this.lbl_pro_deli.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_deli.TabIndex = 0;
            this.lbl_pro_deli.Text = "Your Delivery";
            this.lbl_pro_deli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_lang
            // 
            this.pan_lang.BackgroundImage = global::Orion_truck.Properties.Resources.process_enable_1;
            this.pan_lang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pan_lang.Controls.Add(this.lbl_pro_sltlang1);
            this.pan_lang.Controls.Add(this.lbl_pro_sltlang);
            this.pan_lang.Location = new System.Drawing.Point(5, 4);
            this.pan_lang.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_lang.Name = "pan_lang";
            this.pan_lang.Size = new System.Drawing.Size(163, 59);
            this.pan_lang.TabIndex = 0;
            // 
            // lbl_pro_sltlang1
            // 
            this.lbl_pro_sltlang1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_sltlang1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_sltlang1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_sltlang1.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_sltlang1.Location = new System.Drawing.Point(21, 7);
            this.lbl_pro_sltlang1.Name = "lbl_pro_sltlang1";
            this.lbl_pro_sltlang1.Size = new System.Drawing.Size(121, 44);
            this.lbl_pro_sltlang1.TabIndex = 1;
            this.lbl_pro_sltlang1.Text = "Choose your Language";
            this.lbl_pro_sltlang1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_pro_sltlang
            // 
            this.lbl_pro_sltlang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_pro_sltlang.BackColor = System.Drawing.Color.Transparent;
            this.lbl_pro_sltlang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pro_sltlang.ForeColor = System.Drawing.Color.White;
            this.lbl_pro_sltlang.Location = new System.Drawing.Point(24, 2);
            this.lbl_pro_sltlang.Name = "lbl_pro_sltlang";
            this.lbl_pro_sltlang.Size = new System.Drawing.Size(121, 54);
            this.lbl_pro_sltlang.TabIndex = 0;
            this.lbl_pro_sltlang.Text = "Choose your Language";
            this.lbl_pro_sltlang.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabcontrol1
            // 
            this.tabcontrol1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabcontrol1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabcontrol1.Controls.Add(this.tab_lang);
            this.tabcontrol1.Controls.Add(this.tab_trucktype);
            this.tabcontrol1.Controls.Add(this.tab_truckin);
            this.tabcontrol1.Controls.Add(this.tab_po);
            this.tabcontrol1.Controls.Add(this.tab_truckout);
            this.tabcontrol1.Controls.Add(this.tab_so);
            this.tabcontrol1.Controls.Add(this.tab_trkinfo);
            this.tabcontrol1.Controls.Add(this.tab_trukinfo2);
            this.tabcontrol1.Controls.Add(this.tab_comp);
            this.tabcontrol1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabcontrol1.Location = new System.Drawing.Point(3, 143);
            this.tabcontrol1.Margin = new System.Windows.Forms.Padding(0);
            this.tabcontrol1.MaximumSize = new System.Drawing.Size(1001, 511);
            this.tabcontrol1.Multiline = true;
            this.tabcontrol1.Name = "tabcontrol1";
            this.tabcontrol1.Padding = new System.Drawing.Point(0, 0);
            this.tabcontrol1.SelectedIndex = 0;
            this.tabcontrol1.Size = new System.Drawing.Size(1001, 511);
            this.tabcontrol1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabcontrol1.TabIndex = 0;
            this.tabcontrol1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabcontrol1_KeyDown);
            // 
            // tab_lang
            // 
            this.tab_lang.BackColor = System.Drawing.Color.White;
            this.tab_lang.Controls.Add(this.panel6);
            this.tab_lang.Controls.Add(this.lb_invsble_sltd_nor);
            this.tab_lang.Controls.Add(this.label4);
            this.tab_lang.Controls.Add(this.lbl_lang_rsnsel_nor);
            this.tab_lang.Controls.Add(this.lbl_lang_otrlnag_nor);
            this.tab_lang.Controls.Add(this.panel13);
            this.tab_lang.Controls.Add(this.panel10);
            this.tab_lang.Controls.Add(this.lbl_slt_lang_nor);
            this.tab_lang.Location = new System.Drawing.Point(4, 4);
            this.tab_lang.Margin = new System.Windows.Forms.Padding(0);
            this.tab_lang.Name = "tab_lang";
            this.tab_lang.Size = new System.Drawing.Size(993, 502);
            this.tab_lang.TabIndex = 0;
            this.tab_lang.Text = "Language";
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel6.Controls.Add(this.lbl_title_lang);
            this.panel6.Controls.Add(this.lbl_title);
            this.panel6.Location = new System.Drawing.Point(4, 12);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(984, 31);
            this.panel6.TabIndex = 40;
            // 
            // lbl_title_lang
            // 
            this.lbl_title_lang.AutoSize = true;
            this.lbl_title_lang.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_lang.Location = new System.Drawing.Point(4, 2);
            this.lbl_title_lang.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title_lang.Name = "lbl_title_lang";
            this.lbl_title_lang.Size = new System.Drawing.Size(220, 27);
            this.lbl_title_lang.TabIndex = 1;
            this.lbl_title_lang.Text = "Select Your Language";
            this.lbl_title_lang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(338, 10);
            this.lbl_title.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(31, 15);
            this.lbl_title.TabIndex = 5;
            this.lbl_title.Text = "title";
            this.lbl_title.Visible = false;
            // 
            // lb_invsble_sltd_nor
            // 
            this.lb_invsble_sltd_nor.AutoSize = true;
            this.lb_invsble_sltd_nor.Location = new System.Drawing.Point(126, 41);
            this.lb_invsble_sltd_nor.Name = "lb_invsble_sltd_nor";
            this.lb_invsble_sltd_nor.Size = new System.Drawing.Size(101, 16);
            this.lb_invsble_sltd_nor.TabIndex = 6;
            this.lb_invsble_sltd_nor.Text = "Selected Language";
            this.lb_invsble_sltd_nor.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Select Language";
            // 
            // lbl_lang_rsnsel_nor
            // 
            this.lbl_lang_rsnsel_nor.AutoSize = true;
            this.lbl_lang_rsnsel_nor.Location = new System.Drawing.Point(7, 42);
            this.lbl_lang_rsnsel_nor.Name = "lbl_lang_rsnsel_nor";
            this.lbl_lang_rsnsel_nor.Size = new System.Drawing.Size(91, 16);
            this.lbl_lang_rsnsel_nor.TabIndex = 5;
            this.lbl_lang_rsnsel_nor.Text = "Recent Selection";
            // 
            // lbl_lang_otrlnag_nor
            // 
            this.lbl_lang_otrlnag_nor.AutoSize = true;
            this.lbl_lang_otrlnag_nor.Location = new System.Drawing.Point(3, 197);
            this.lbl_lang_otrlnag_nor.Name = "lbl_lang_otrlnag_nor";
            this.lbl_lang_otrlnag_nor.Size = new System.Drawing.Size(89, 16);
            this.lbl_lang_otrlnag_nor.TabIndex = 4;
            this.lbl_lang_otrlnag_nor.Text = "Other Language";
            // 
            // panel13
            // 
            this.panel13.AutoScroll = true;
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.Controls.Add(this.label10);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Location = new System.Drawing.Point(2, 217);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(981, 257);
            this.panel13.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 16);
            this.label10.TabIndex = 2;
            this.label10.Tag = "";
            this.label10.Text = "label10";
            this.label10.Visible = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Silver;
            this.panel14.Location = new System.Drawing.Point(3, 14);
            this.panel14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 98);
            this.panel14.TabIndex = 0;
            this.panel14.Visible = false;
            // 
            // panel10
            // 
            this.panel10.AutoScroll = true;
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(0, 58);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(981, 138);
            this.panel10.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 1;
            this.label9.Tag = "";
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(3, 28);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(11, 104);
            this.panel11.TabIndex = 0;
            // 
            // lbl_slt_lang_nor
            // 
            this.lbl_slt_lang_nor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_slt_lang_nor.AutoSize = true;
            this.lbl_slt_lang_nor.BackColor = System.Drawing.Color.White;
            this.lbl_slt_lang_nor.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_lang_nor.Location = new System.Drawing.Point(327, 39);
            this.lbl_slt_lang_nor.Name = "lbl_slt_lang_nor";
            this.lbl_slt_lang_nor.Size = new System.Drawing.Size(224, 22);
            this.lbl_slt_lang_nor.TabIndex = 3;
            this.lbl_slt_lang_nor.Text = "Selected Language  Français";
            this.lbl_slt_lang_nor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tab_trucktype
            // 
            this.tab_trucktype.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_trucktype.Controls.Add(this.pb_home);
            this.tab_trucktype.Controls.Add(this.lbl_titlt_truck);
            this.tab_trucktype.Controls.Add(this.lbl_invsble_slttruck);
            this.tab_trucktype.Controls.Add(this.lbl_trktype_Container);
            this.tab_trucktype.Controls.Add(this.lbl_trktype_euro);
            this.tab_trucktype.Controls.Add(this.lbl_trktype_other);
            this.tab_trucktype.Controls.Add(this.lbl_trktype_Inloader);
            this.tab_trucktype.Controls.Add(this.lbl_slt_truck);
            this.tab_trucktype.Controls.Add(this.pan_slt_inloader);
            this.tab_trucktype.Controls.Add(this.pan_sltd_container);
            this.tab_trucktype.Controls.Add(this.pan_sltd_euro);
            this.tab_trucktype.Controls.Add(this.pan_sltd_other);
            this.tab_trucktype.Controls.Add(this.pb_trktype_Container);
            this.tab_trucktype.Controls.Add(this.pb_trktype_inloader);
            this.tab_trucktype.Controls.Add(this.pb_trktype_other);
            this.tab_trucktype.Controls.Add(this.pb_trktype_euro);
            this.tab_trucktype.Controls.Add(this.next_truck);
            this.tab_trucktype.Controls.Add(this.back_trk);
            this.tab_trucktype.Location = new System.Drawing.Point(4, 4);
            this.tab_trucktype.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trucktype.Name = "tab_trucktype";
            this.tab_trucktype.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trucktype.Size = new System.Drawing.Size(993, 478);
            this.tab_trucktype.TabIndex = 2;
            this.tab_trucktype.Text = "truck";
            // 
            // pb_home
            // 
            this.pb_home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_home.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pb_home.Location = new System.Drawing.Point(30, 410);
            this.pb_home.Name = "pb_home";
            this.pb_home.Size = new System.Drawing.Size(89, 82);
            this.pb_home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_home.TabIndex = 28;
            this.pb_home.TabStop = false;
            this.pb_home.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // lbl_titlt_truck
            // 
            this.lbl_titlt_truck.AutoSize = true;
            this.lbl_titlt_truck.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truck.Location = new System.Drawing.Point(6, 2);
            this.lbl_titlt_truck.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truck.Name = "lbl_titlt_truck";
            this.lbl_titlt_truck.Size = new System.Drawing.Size(210, 27);
            this.lbl_titlt_truck.TabIndex = 17;
            this.lbl_titlt_truck.Text = "What kind of truck ?";
            this.lbl_titlt_truck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_invsble_slttruck
            // 
            this.lbl_invsble_slttruck.AutoSize = true;
            this.lbl_invsble_slttruck.Location = new System.Drawing.Point(232, 560);
            this.lbl_invsble_slttruck.Name = "lbl_invsble_slttruck";
            this.lbl_invsble_slttruck.Size = new System.Drawing.Size(81, 16);
            this.lbl_invsble_slttruck.TabIndex = 11;
            this.lbl_invsble_slttruck.Text = "Selected truck";
            this.lbl_invsble_slttruck.Visible = false;
            // 
            // lbl_trktype_Container
            // 
            this.lbl_trktype_Container.AutoSize = true;
            this.lbl_trktype_Container.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_Container.Location = new System.Drawing.Point(647, 195);
            this.lbl_trktype_Container.Name = "lbl_trktype_Container";
            this.lbl_trktype_Container.Size = new System.Drawing.Size(98, 24);
            this.lbl_trktype_Container.TabIndex = 6;
            this.lbl_trktype_Container.Text = "Container";
            this.lbl_trktype_Container.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_euro
            // 
            this.lbl_trktype_euro.AutoSize = true;
            this.lbl_trktype_euro.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_euro.Location = new System.Drawing.Point(157, 388);
            this.lbl_trktype_euro.Name = "lbl_trktype_euro";
            this.lbl_trktype_euro.Size = new System.Drawing.Size(93, 24);
            this.lbl_trktype_euro.TabIndex = 5;
            this.lbl_trktype_euro.Text = "Euroliner";
            this.lbl_trktype_euro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_other
            // 
            this.lbl_trktype_other.AutoSize = true;
            this.lbl_trktype_other.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_other.Location = new System.Drawing.Point(662, 388);
            this.lbl_trktype_other.Name = "lbl_trktype_other";
            this.lbl_trktype_other.Size = new System.Drawing.Size(69, 24);
            this.lbl_trktype_other.TabIndex = 7;
            this.lbl_trktype_other.Text = "Others";
            this.lbl_trktype_other.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_trktype_Inloader
            // 
            this.lbl_trktype_Inloader.AutoSize = true;
            this.lbl_trktype_Inloader.BackColor = System.Drawing.Color.Transparent;
            this.lbl_trktype_Inloader.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trktype_Inloader.Location = new System.Drawing.Point(157, 195);
            this.lbl_trktype_Inloader.Name = "lbl_trktype_Inloader";
            this.lbl_trktype_Inloader.Size = new System.Drawing.Size(83, 24);
            this.lbl_trktype_Inloader.TabIndex = 4;
            this.lbl_trktype_Inloader.Text = "Inloader";
            this.lbl_trktype_Inloader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_slt_truck
            // 
            this.lbl_slt_truck.BackColor = System.Drawing.Color.Transparent;
            this.lbl_slt_truck.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_truck.Location = new System.Drawing.Point(330, 4);
            this.lbl_slt_truck.Name = "lbl_slt_truck";
            this.lbl_slt_truck.Size = new System.Drawing.Size(373, 26);
            this.lbl_slt_truck.TabIndex = 16;
            this.lbl_slt_truck.Text = "Selected truk  Inloader";
            this.lbl_slt_truck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_slt_inloader
            // 
            this.pan_slt_inloader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_slt_inloader.Location = new System.Drawing.Point(30, 30);
            this.pan_slt_inloader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_slt_inloader.Name = "pan_slt_inloader";
            this.pan_slt_inloader.Size = new System.Drawing.Size(10, 163);
            this.pan_slt_inloader.TabIndex = 12;
            // 
            // pan_sltd_container
            // 
            this.pan_sltd_container.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_container.Location = new System.Drawing.Point(514, 30);
            this.pan_sltd_container.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_sltd_container.Name = "pan_sltd_container";
            this.pan_sltd_container.Size = new System.Drawing.Size(10, 163);
            this.pan_sltd_container.TabIndex = 13;
            // 
            // pan_sltd_euro
            // 
            this.pan_sltd_euro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_euro.Location = new System.Drawing.Point(30, 221);
            this.pan_sltd_euro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_sltd_euro.Name = "pan_sltd_euro";
            this.pan_sltd_euro.Size = new System.Drawing.Size(10, 163);
            this.pan_sltd_euro.TabIndex = 14;
            // 
            // pan_sltd_other
            // 
            this.pan_sltd_other.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pan_sltd_other.Location = new System.Drawing.Point(514, 221);
            this.pan_sltd_other.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_sltd_other.Name = "pan_sltd_other";
            this.pan_sltd_other.Size = new System.Drawing.Size(10, 163);
            this.pan_sltd_other.TabIndex = 15;
            // 
            // pb_trktype_Container
            // 
            this.pb_trktype_Container.BackColor = System.Drawing.Color.White;
            this.pb_trktype_Container.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_Container.Image = global::Orion_truck.Properties.Resources._1;
            this.pb_trktype_Container.Location = new System.Drawing.Point(527, 30);
            this.pb_trktype_Container.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trktype_Container.Name = "pb_trktype_Container";
            this.pb_trktype_Container.Size = new System.Drawing.Size(371, 163);
            this.pb_trktype_Container.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_Container.TabIndex = 1;
            this.pb_trktype_Container.TabStop = false;
            this.pb_trktype_Container.Click += new System.EventHandler(this.pb_trktype_Container_Click);
            // 
            // pb_trktype_inloader
            // 
            this.pb_trktype_inloader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_inloader.Image = global::Orion_truck.Properties.Resources.inloader_new_png;
            this.pb_trktype_inloader.Location = new System.Drawing.Point(43, 30);
            this.pb_trktype_inloader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trktype_inloader.Name = "pb_trktype_inloader";
            this.pb_trktype_inloader.Size = new System.Drawing.Size(371, 163);
            this.pb_trktype_inloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_inloader.TabIndex = 0;
            this.pb_trktype_inloader.TabStop = false;
            this.pb_trktype_inloader.Click += new System.EventHandler(this.pb_trktype_inloader_Click);
            // 
            // pb_trktype_other
            // 
            this.pb_trktype_other.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_other.Image = global::Orion_truck.Properties.Resources._4;
            this.pb_trktype_other.Location = new System.Drawing.Point(527, 221);
            this.pb_trktype_other.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trktype_other.Name = "pb_trktype_other";
            this.pb_trktype_other.Size = new System.Drawing.Size(371, 163);
            this.pb_trktype_other.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_other.TabIndex = 3;
            this.pb_trktype_other.TabStop = false;
            this.pb_trktype_other.Click += new System.EventHandler(this.pb_trktype_other_Click);
            // 
            // pb_trktype_euro
            // 
            this.pb_trktype_euro.BackColor = System.Drawing.Color.White;
            this.pb_trktype_euro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trktype_euro.Image = global::Orion_truck.Properties.Resources.EuroNew;
            this.pb_trktype_euro.Location = new System.Drawing.Point(43, 221);
            this.pb_trktype_euro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trktype_euro.Name = "pb_trktype_euro";
            this.pb_trktype_euro.Size = new System.Drawing.Size(371, 163);
            this.pb_trktype_euro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trktype_euro.TabIndex = 2;
            this.pb_trktype_euro.TabStop = false;
            this.pb_trktype_euro.Click += new System.EventHandler(this.pb_trktype_euro_Click);
            // 
            // next_truck
            // 
            this.next_truck.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_truck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_truck.Controls.Add(this.lbl_trk_next);
            this.next_truck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_truck.Location = new System.Drawing.Point(836, 410);
            this.next_truck.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_truck.Name = "next_truck";
            this.next_truck.Size = new System.Drawing.Size(155, 82);
            this.next_truck.TabIndex = 16;
            this.next_truck.Click += new System.EventHandler(this.next_truck_Click);
            // 
            // lbl_trk_next
            // 
            this.lbl_trk_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_trk_next.BackColor = System.Drawing.Color.Transparent;
            this.lbl_trk_next.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trk_next.ForeColor = System.Drawing.Color.White;
            this.lbl_trk_next.Location = new System.Drawing.Point(15, 26);
            this.lbl_trk_next.Name = "lbl_trk_next";
            this.lbl_trk_next.Size = new System.Drawing.Size(95, 25);
            this.lbl_trk_next.TabIndex = 15;
            this.lbl_trk_next.Text = "Next";
            this.lbl_trk_next.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_trk_next.Click += new System.EventHandler(this.next_truck_Click);
            // 
            // back_trk
            // 
            this.back_trk.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_trk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_trk.Controls.Add(this.lbl_trk_back);
            this.back_trk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_trk.Location = new System.Drawing.Point(647, 410);
            this.back_trk.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_trk.Name = "back_trk";
            this.back_trk.Size = new System.Drawing.Size(155, 82);
            this.back_trk.TabIndex = 14;
            this.back_trk.Click += new System.EventHandler(this.back_trk_Click);
            // 
            // lbl_trk_back
            // 
            this.lbl_trk_back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_trk_back.BackColor = System.Drawing.Color.Transparent;
            this.lbl_trk_back.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trk_back.ForeColor = System.Drawing.Color.White;
            this.lbl_trk_back.Location = new System.Drawing.Point(30, 26);
            this.lbl_trk_back.Name = "lbl_trk_back";
            this.lbl_trk_back.Size = new System.Drawing.Size(106, 25);
            this.lbl_trk_back.TabIndex = 15;
            this.lbl_trk_back.Text = "Previous";
            this.lbl_trk_back.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_trk_back.Click += new System.EventHandler(this.back_trk_Click);
            // 
            // tab_truckin
            // 
            this.tab_truckin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_truckin.Controls.Add(this.lbl_trukin2_title);
            this.tab_truckin.Controls.Add(this.lbl_truckin1_title);
            this.tab_truckin.Controls.Add(this.lbl_titlt_truckineuro_empty);
            this.tab_truckin.Controls.Add(this.lbl_titlt_truckineuro_goods);
            this.tab_truckin.Controls.Add(this.lbl_stillcullt);
            this.tab_truckin.Controls.Add(this.lbl_packing);
            this.tab_truckin.Controls.Add(this.lbl_wood);
            this.tab_truckin.Controls.Add(this.lbl_trkin_sltgoods);
            this.tab_truckin.Controls.Add(this.lbl_titlt_truckin_empty);
            this.tab_truckin.Controls.Add(this.lbl_titlt_truckin_or);
            this.tab_truckin.Controls.Add(this.lbl_titlt_truckin_goods);
            this.tab_truckin.Controls.Add(this.lbl_slt_truckin);
            this.tab_truckin.Controls.Add(this.lbl_other);
            this.tab_truckin.Controls.Add(this.lbl_cullet);
            this.tab_truckin.Controls.Add(this.lbl_still);
            this.tab_truckin.Controls.Add(this.lbl_glass);
            this.tab_truckin.Controls.Add(this.pan_trkin1);
            this.tab_truckin.Controls.Add(this.lbl_empty);
            this.tab_truckin.Controls.Add(this.pan_trkin2);
            this.tab_truckin.Controls.Add(this.lbl_invsble_trkinslt);
            this.tab_truckin.Controls.Add(this.lbl_trkin1);
            this.tab_truckin.Controls.Add(this.pan_trkin3);
            this.tab_truckin.Controls.Add(this.lbl_trkin2);
            this.tab_truckin.Controls.Add(this.pan_trkin4);
            this.tab_truckin.Controls.Add(this.pan_trkin5);
            this.tab_truckin.Controls.Add(this.lbl_trkin3);
            this.tab_truckin.Controls.Add(this.lbl_trkin4);
            this.tab_truckin.Controls.Add(this.lbl_trkin5);
            this.tab_truckin.Controls.Add(this.pictureBox3);
            this.tab_truckin.Controls.Add(this.next_truckin);
            this.tab_truckin.Controls.Add(this.back_truckin);
            this.tab_truckin.Controls.Add(this.panel3);
            this.tab_truckin.Controls.Add(this.pb_trkin2);
            this.tab_truckin.Controls.Add(this.pb_trkin1);
            this.tab_truckin.Controls.Add(this.pb_trkin3);
            this.tab_truckin.Controls.Add(this.pb_trkin5);
            this.tab_truckin.Controls.Add(this.pb_trkin4);
            this.tab_truckin.Location = new System.Drawing.Point(4, 4);
            this.tab_truckin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_truckin.Name = "tab_truckin";
            this.tab_truckin.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_truckin.Size = new System.Drawing.Size(993, 478);
            this.tab_truckin.TabIndex = 3;
            this.tab_truckin.Text = "TruckIn";
            // 
            // lbl_trukin2_title
            // 
            this.lbl_trukin2_title.AutoSize = true;
            this.lbl_trukin2_title.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trukin2_title.Location = new System.Drawing.Point(18, 70);
            this.lbl_trukin2_title.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_trukin2_title.Name = "lbl_trukin2_title";
            this.lbl_trukin2_title.Size = new System.Drawing.Size(241, 27);
            this.lbl_trukin2_title.TabIndex = 33;
            this.lbl_trukin2_title.Text = "With Goods to Deliver? ";
            this.lbl_trukin2_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_truckin1_title
            // 
            this.lbl_truckin1_title.AutoSize = true;
            this.lbl_truckin1_title.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_truckin1_title.Location = new System.Drawing.Point(18, 10);
            this.lbl_truckin1_title.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_truckin1_title.Name = "lbl_truckin1_title";
            this.lbl_truckin1_title.Size = new System.Drawing.Size(212, 27);
            this.lbl_truckin1_title.TabIndex = 32;
            this.lbl_truckin1_title.Text = "Do you Arrive Empty";
            this.lbl_truckin1_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_titlt_truckineuro_empty
            // 
            this.lbl_titlt_truckineuro_empty.AutoSize = true;
            this.lbl_titlt_truckineuro_empty.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckineuro_empty.Location = new System.Drawing.Point(24, 156);
            this.lbl_titlt_truckineuro_empty.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truckineuro_empty.Name = "lbl_titlt_truckineuro_empty";
            this.lbl_titlt_truckineuro_empty.Size = new System.Drawing.Size(251, 27);
            this.lbl_titlt_truckineuro_empty.TabIndex = 31;
            this.lbl_titlt_truckineuro_empty.Text = "Other Goods to Deliver? ";
            this.lbl_titlt_truckineuro_empty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_titlt_truckineuro_empty.Visible = false;
            // 
            // lbl_titlt_truckineuro_goods
            // 
            this.lbl_titlt_truckineuro_goods.AutoSize = true;
            this.lbl_titlt_truckineuro_goods.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckineuro_goods.Location = new System.Drawing.Point(24, 96);
            this.lbl_titlt_truckineuro_goods.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truckineuro_goods.Name = "lbl_titlt_truckineuro_goods";
            this.lbl_titlt_truckineuro_goods.Size = new System.Drawing.Size(260, 27);
            this.lbl_titlt_truckineuro_goods.TabIndex = 30;
            this.lbl_titlt_truckineuro_goods.Text = "Do you Arrive with cullet ";
            this.lbl_titlt_truckineuro_goods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_titlt_truckineuro_goods.Visible = false;
            // 
            // lbl_stillcullt
            // 
            this.lbl_stillcullt.AutoSize = true;
            this.lbl_stillcullt.Location = new System.Drawing.Point(518, 119);
            this.lbl_stillcullt.Name = "lbl_stillcullt";
            this.lbl_stillcullt.Size = new System.Drawing.Size(83, 16);
            this.lbl_stillcullt.TabIndex = 25;
            this.lbl_stillcullt.Text = "Cullet + Stillage";
            this.lbl_stillcullt.Visible = false;
            // 
            // lbl_packing
            // 
            this.lbl_packing.AutoSize = true;
            this.lbl_packing.Location = new System.Drawing.Point(536, 28);
            this.lbl_packing.Name = "lbl_packing";
            this.lbl_packing.Size = new System.Drawing.Size(59, 16);
            this.lbl_packing.TabIndex = 24;
            this.lbl_packing.Text = "packaging";
            this.lbl_packing.Visible = false;
            // 
            // lbl_wood
            // 
            this.lbl_wood.AutoSize = true;
            this.lbl_wood.Location = new System.Drawing.Point(189, 119);
            this.lbl_wood.Name = "lbl_wood";
            this.lbl_wood.Size = new System.Drawing.Size(35, 16);
            this.lbl_wood.TabIndex = 23;
            this.lbl_wood.Text = "Wood";
            this.lbl_wood.Visible = false;
            // 
            // lbl_trkin_sltgoods
            // 
            this.lbl_trkin_sltgoods.AutoSize = true;
            this.lbl_trkin_sltgoods.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin_sltgoods.Location = new System.Drawing.Point(6, 193);
            this.lbl_trkin_sltgoods.Name = "lbl_trkin_sltgoods";
            this.lbl_trkin_sltgoods.Size = new System.Drawing.Size(147, 22);
            this.lbl_trkin_sltgoods.TabIndex = 22;
            this.lbl_trkin_sltgoods.Text = "Select Your Goods";
            // 
            // lbl_titlt_truckin_empty
            // 
            this.lbl_titlt_truckin_empty.AutoSize = true;
            this.lbl_titlt_truckin_empty.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckin_empty.Location = new System.Drawing.Point(18, 70);
            this.lbl_titlt_truckin_empty.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truckin_empty.Name = "lbl_titlt_truckin_empty";
            this.lbl_titlt_truckin_empty.Size = new System.Drawing.Size(241, 27);
            this.lbl_titlt_truckin_empty.TabIndex = 21;
            this.lbl_titlt_truckin_empty.Text = "With Goods to Deliver? ";
            this.lbl_titlt_truckin_empty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_titlt_truckin_empty.Visible = false;
            // 
            // lbl_titlt_truckin_or
            // 
            this.lbl_titlt_truckin_or.AutoSize = true;
            this.lbl_titlt_truckin_or.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckin_or.Location = new System.Drawing.Point(104, 40);
            this.lbl_titlt_truckin_or.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truckin_or.Name = "lbl_titlt_truckin_or";
            this.lbl_titlt_truckin_or.Size = new System.Drawing.Size(36, 27);
            this.lbl_titlt_truckin_or.TabIndex = 20;
            this.lbl_titlt_truckin_or.Text = "Or";
            this.lbl_titlt_truckin_or.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_titlt_truckin_goods
            // 
            this.lbl_titlt_truckin_goods.AutoSize = true;
            this.lbl_titlt_truckin_goods.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titlt_truckin_goods.Location = new System.Drawing.Point(18, 10);
            this.lbl_titlt_truckin_goods.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_titlt_truckin_goods.Name = "lbl_titlt_truckin_goods";
            this.lbl_titlt_truckin_goods.Size = new System.Drawing.Size(212, 27);
            this.lbl_titlt_truckin_goods.TabIndex = 19;
            this.lbl_titlt_truckin_goods.Text = "Do you Arrive Empty";
            this.lbl_titlt_truckin_goods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_titlt_truckin_goods.Visible = false;
            // 
            // lbl_slt_truckin
            // 
            this.lbl_slt_truckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_slt_truckin.Location = new System.Drawing.Point(441, 119);
            this.lbl_slt_truckin.Name = "lbl_slt_truckin";
            this.lbl_slt_truckin.Size = new System.Drawing.Size(25, 30);
            this.lbl_slt_truckin.TabIndex = 0;
            this.lbl_slt_truckin.Text = "You Arrived With Glass Goods";
            this.lbl_slt_truckin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_slt_truckin.Visible = false;
            // 
            // lbl_other
            // 
            this.lbl_other.AutoSize = true;
            this.lbl_other.Location = new System.Drawing.Point(277, 119);
            this.lbl_other.Name = "lbl_other";
            this.lbl_other.Size = new System.Drawing.Size(43, 16);
            this.lbl_other.TabIndex = 7;
            this.lbl_other.Text = "Others";
            this.lbl_other.Visible = false;
            // 
            // lbl_cullet
            // 
            this.lbl_cullet.AutoSize = true;
            this.lbl_cullet.Location = new System.Drawing.Point(230, 119);
            this.lbl_cullet.Name = "lbl_cullet";
            this.lbl_cullet.Size = new System.Drawing.Size(41, 16);
            this.lbl_cullet.TabIndex = 6;
            this.lbl_cullet.Text = "Cutllet";
            this.lbl_cullet.Visible = false;
            // 
            // lbl_still
            // 
            this.lbl_still.AutoSize = true;
            this.lbl_still.Location = new System.Drawing.Point(317, 119);
            this.lbl_still.Name = "lbl_still";
            this.lbl_still.Size = new System.Drawing.Size(43, 16);
            this.lbl_still.TabIndex = 5;
            this.lbl_still.Text = "Stillage";
            this.lbl_still.Visible = false;
            // 
            // lbl_glass
            // 
            this.lbl_glass.AutoSize = true;
            this.lbl_glass.Location = new System.Drawing.Point(364, 119);
            this.lbl_glass.Name = "lbl_glass";
            this.lbl_glass.Size = new System.Drawing.Size(33, 16);
            this.lbl_glass.TabIndex = 4;
            this.lbl_glass.Text = "Glass";
            this.lbl_glass.Visible = false;
            // 
            // pan_trkin1
            // 
            this.pan_trkin1.BackColor = System.Drawing.Color.Green;
            this.pan_trkin1.Location = new System.Drawing.Point(5, 218);
            this.pan_trkin1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_trkin1.Name = "pan_trkin1";
            this.pan_trkin1.Size = new System.Drawing.Size(10, 150);
            this.pan_trkin1.TabIndex = 5;
            this.pan_trkin1.Visible = false;
            // 
            // lbl_empty
            // 
            this.lbl_empty.AutoSize = true;
            this.lbl_empty.Location = new System.Drawing.Point(404, 119);
            this.lbl_empty.Name = "lbl_empty";
            this.lbl_empty.Size = new System.Drawing.Size(39, 16);
            this.lbl_empty.TabIndex = 3;
            this.lbl_empty.Text = "Empty";
            this.lbl_empty.Visible = false;
            // 
            // pan_trkin2
            // 
            this.pan_trkin2.BackColor = System.Drawing.Color.Green;
            this.pan_trkin2.Location = new System.Drawing.Point(206, 218);
            this.pan_trkin2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_trkin2.Name = "pan_trkin2";
            this.pan_trkin2.Size = new System.Drawing.Size(10, 150);
            this.pan_trkin2.TabIndex = 6;
            // 
            // lbl_invsble_trkinslt
            // 
            this.lbl_invsble_trkinslt.AutoSize = true;
            this.lbl_invsble_trkinslt.Location = new System.Drawing.Point(472, 119);
            this.lbl_invsble_trkinslt.Name = "lbl_invsble_trkinslt";
            this.lbl_invsble_trkinslt.Size = new System.Drawing.Size(36, 16);
            this.lbl_invsble_trkinslt.TabIndex = 2;
            this.lbl_invsble_trkinslt.Text = "label2";
            this.lbl_invsble_trkinslt.Visible = false;
            // 
            // lbl_trkin1
            // 
            this.lbl_trkin1.AutoSize = true;
            this.lbl_trkin1.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin1.Location = new System.Drawing.Point(54, 375);
            this.lbl_trkin1.Name = "lbl_trkin1";
            this.lbl_trkin1.Size = new System.Drawing.Size(75, 27);
            this.lbl_trkin1.TabIndex = 0;
            this.lbl_trkin1.Text = "Empty ";
            this.lbl_trkin1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pan_trkin3
            // 
            this.pan_trkin3.BackColor = System.Drawing.Color.Green;
            this.pan_trkin3.Location = new System.Drawing.Point(407, 218);
            this.pan_trkin3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_trkin3.Name = "pan_trkin3";
            this.pan_trkin3.Size = new System.Drawing.Size(10, 150);
            this.pan_trkin3.TabIndex = 7;
            this.pan_trkin3.Visible = false;
            // 
            // lbl_trkin2
            // 
            this.lbl_trkin2.AutoSize = true;
            this.lbl_trkin2.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin2.Location = new System.Drawing.Point(254, 375);
            this.lbl_trkin2.Name = "lbl_trkin2";
            this.lbl_trkin2.Size = new System.Drawing.Size(61, 27);
            this.lbl_trkin2.TabIndex = 1;
            this.lbl_trkin2.Text = "Glass";
            this.lbl_trkin2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pan_trkin4
            // 
            this.pan_trkin4.BackColor = System.Drawing.Color.Green;
            this.pan_trkin4.Location = new System.Drawing.Point(608, 218);
            this.pan_trkin4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_trkin4.Name = "pan_trkin4";
            this.pan_trkin4.Size = new System.Drawing.Size(10, 150);
            this.pan_trkin4.TabIndex = 8;
            this.pan_trkin4.Visible = false;
            // 
            // pan_trkin5
            // 
            this.pan_trkin5.BackColor = System.Drawing.Color.Green;
            this.pan_trkin5.Location = new System.Drawing.Point(806, 218);
            this.pan_trkin5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_trkin5.Name = "pan_trkin5";
            this.pan_trkin5.Size = new System.Drawing.Size(10, 150);
            this.pan_trkin5.TabIndex = 9;
            this.pan_trkin5.Visible = false;
            // 
            // lbl_trkin3
            // 
            this.lbl_trkin3.AutoSize = true;
            this.lbl_trkin3.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin3.Location = new System.Drawing.Point(427, 375);
            this.lbl_trkin3.Name = "lbl_trkin3";
            this.lbl_trkin3.Size = new System.Drawing.Size(81, 27);
            this.lbl_trkin3.TabIndex = 2;
            this.lbl_trkin3.Text = "Stillage";
            this.lbl_trkin3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin4
            // 
            this.lbl_trkin4.AutoSize = true;
            this.lbl_trkin4.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin4.Location = new System.Drawing.Point(642, 375);
            this.lbl_trkin4.Name = "lbl_trkin4";
            this.lbl_trkin4.Size = new System.Drawing.Size(67, 27);
            this.lbl_trkin4.TabIndex = 3;
            this.lbl_trkin4.Text = "Cullet";
            this.lbl_trkin4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_trkin5
            // 
            this.lbl_trkin5.AutoSize = true;
            this.lbl_trkin5.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkin5.Location = new System.Drawing.Point(868, 375);
            this.lbl_trkin5.Name = "lbl_trkin5";
            this.lbl_trkin5.Size = new System.Drawing.Size(74, 27);
            this.lbl_trkin5.TabIndex = 4;
            this.lbl_trkin5.Text = "Others";
            this.lbl_trkin5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox3.Location = new System.Drawing.Point(30, 410);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(89, 82);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // next_truckin
            // 
            this.next_truckin.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_truckin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_truckin.Controls.Add(this.lbl_n_trkin);
            this.next_truckin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_truckin.Location = new System.Drawing.Point(836, 410);
            this.next_truckin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_truckin.Name = "next_truckin";
            this.next_truckin.Size = new System.Drawing.Size(155, 82);
            this.next_truckin.TabIndex = 15;
            this.next_truckin.Click += new System.EventHandler(this.next_truckin_Click);
            // 
            // lbl_n_trkin
            // 
            this.lbl_n_trkin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_n_trkin.BackColor = System.Drawing.Color.Transparent;
            this.lbl_n_trkin.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_n_trkin.ForeColor = System.Drawing.Color.White;
            this.lbl_n_trkin.Location = new System.Drawing.Point(15, 26);
            this.lbl_n_trkin.Name = "lbl_n_trkin";
            this.lbl_n_trkin.Size = new System.Drawing.Size(95, 25);
            this.lbl_n_trkin.TabIndex = 15;
            this.lbl_n_trkin.Text = "Next";
            this.lbl_n_trkin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_n_trkin.Click += new System.EventHandler(this.next_truckin_Click);
            // 
            // back_truckin
            // 
            this.back_truckin.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_truckin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_truckin.Controls.Add(this.lbl_b_trkin);
            this.back_truckin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_truckin.Location = new System.Drawing.Point(647, 410);
            this.back_truckin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_truckin.Name = "back_truckin";
            this.back_truckin.Size = new System.Drawing.Size(155, 82);
            this.back_truckin.TabIndex = 17;
            this.back_truckin.Click += new System.EventHandler(this.back_truckin_Click);
            // 
            // lbl_b_trkin
            // 
            this.lbl_b_trkin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_trkin.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_trkin.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_trkin.ForeColor = System.Drawing.Color.White;
            this.lbl_b_trkin.Location = new System.Drawing.Point(30, 26);
            this.lbl_b_trkin.Name = "lbl_b_trkin";
            this.lbl_b_trkin.Size = new System.Drawing.Size(106, 25);
            this.lbl_b_trkin.TabIndex = 15;
            this.lbl_b_trkin.Text = "Previous";
            this.lbl_b_trkin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_trkin.Click += new System.EventHandler(this.back_truckin_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.BackgroundImage = global::Orion_truck.Properties.Resources.white_1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.pb_in);
            this.panel3.Location = new System.Drawing.Point(658, -2);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(329, 200);
            this.panel3.TabIndex = 8;
            // 
            // pb_in
            // 
            this.pb_in.BackColor = System.Drawing.Color.Transparent;
            this.pb_in.Image = global::Orion_truck.Properties.Resources.inloadIN1;
            this.pb_in.Location = new System.Drawing.Point(6, 4);
            this.pb_in.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_in.Name = "pb_in";
            this.pb_in.Size = new System.Drawing.Size(320, 192);
            this.pb_in.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_in.TabIndex = 1;
            this.pb_in.TabStop = false;
            // 
            // pb_trkin2
            // 
            this.pb_trkin2.BackColor = System.Drawing.Color.White;
            this.pb_trkin2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin2.Image = global::Orion_truck.Properties.Resources.glass_newup;
            this.pb_trkin2.Location = new System.Drawing.Point(217, 218);
            this.pb_trkin2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trkin2.Name = "pb_trkin2";
            this.pb_trkin2.Size = new System.Drawing.Size(174, 150);
            this.pb_trkin2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trkin2.TabIndex = 10;
            this.pb_trkin2.TabStop = false;
            this.pb_trkin2.Click += new System.EventHandler(this.pb_trkin2_Click_1);
            // 
            // pb_trkin1
            // 
            this.pb_trkin1.BackColor = System.Drawing.Color.White;
            this.pb_trkin1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin1.Image = global::Orion_truck.Properties.Resources.empty_truck;
            this.pb_trkin1.Location = new System.Drawing.Point(15, 218);
            this.pb_trkin1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trkin1.Name = "pb_trkin1";
            this.pb_trkin1.Size = new System.Drawing.Size(174, 150);
            this.pb_trkin1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trkin1.TabIndex = 0;
            this.pb_trkin1.TabStop = false;
            this.pb_trkin1.Click += new System.EventHandler(this.pb_trkin1_Click_1);
            // 
            // pb_trkin3
            // 
            this.pb_trkin3.BackColor = System.Drawing.Color.White;
            this.pb_trkin3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin3.Image = global::Orion_truck.Properties.Resources.con_glass_n1;
            this.pb_trkin3.Location = new System.Drawing.Point(419, 218);
            this.pb_trkin3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trkin3.Name = "pb_trkin3";
            this.pb_trkin3.Size = new System.Drawing.Size(174, 150);
            this.pb_trkin3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trkin3.TabIndex = 2;
            this.pb_trkin3.TabStop = false;
            this.pb_trkin3.Click += new System.EventHandler(this.pb_trkin3_Click_1);
            // 
            // pb_trkin5
            // 
            this.pb_trkin5.BackColor = System.Drawing.Color.White;
            this.pb_trkin5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin5.Image = global::Orion_truck.Properties.Resources.EuroStillage;
            this.pb_trkin5.Location = new System.Drawing.Point(817, 218);
            this.pb_trkin5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trkin5.Name = "pb_trkin5";
            this.pb_trkin5.Size = new System.Drawing.Size(174, 150);
            this.pb_trkin5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trkin5.TabIndex = 4;
            this.pb_trkin5.TabStop = false;
            this.pb_trkin5.Click += new System.EventHandler(this.pb_trkin5_Click_1);
            // 
            // pb_trkin4
            // 
            this.pb_trkin4.BackColor = System.Drawing.Color.White;
            this.pb_trkin4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_trkin4.Image = global::Orion_truck.Properties.Resources.culletwithstill_1;
            this.pb_trkin4.Location = new System.Drawing.Point(619, 218);
            this.pb_trkin4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_trkin4.Name = "pb_trkin4";
            this.pb_trkin4.Size = new System.Drawing.Size(174, 150);
            this.pb_trkin4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_trkin4.TabIndex = 3;
            this.pb_trkin4.TabStop = false;
            this.pb_trkin4.Click += new System.EventHandler(this.pb_trkin4_Click_1);
            // 
            // tab_po
            // 
            this.tab_po.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_po.Controls.Add(this.listBox1);
            this.tab_po.Controls.Add(this.lbl_title_deli);
            this.tab_po.Controls.Add(this.pb_clear);
            this.tab_po.Controls.Add(this.pictureBox4);
            this.tab_po.Controls.Add(this.next_deli);
            this.tab_po.Controls.Add(this.back_deli);
            this.tab_po.Controls.Add(this.pan_truckin_still);
            this.tab_po.Controls.Add(this.pan_truckin_po);
            this.tab_po.Controls.Add(this.pan_truckin_cullet);
            this.tab_po.Location = new System.Drawing.Point(4, 4);
            this.tab_po.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_po.Name = "tab_po";
            this.tab_po.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_po.Size = new System.Drawing.Size(993, 478);
            this.tab_po.TabIndex = 4;
            this.tab_po.Text = "Purchase Order";
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 27;
            this.listBox1.Location = new System.Drawing.Point(308, 162);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(366, 193);
            this.listBox1.TabIndex = 32;
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
            this.listBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyDown);
            // 
            // lbl_title_deli
            // 
            this.lbl_title_deli.AutoSize = true;
            this.lbl_title_deli.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_deli.Location = new System.Drawing.Point(5, 3);
            this.lbl_title_deli.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title_deli.Name = "lbl_title_deli";
            this.lbl_title_deli.Size = new System.Drawing.Size(597, 27);
            this.lbl_title_deli.TabIndex = 21;
            this.lbl_title_deli.Text = "Enter your Incoming Delivery Number (Purchase Order no.) ";
            this.lbl_title_deli.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_clear
            // 
            this.pb_clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_clear.Image = global::Orion_truck.Properties.Resources.clear_new1234;
            this.pb_clear.Location = new System.Drawing.Point(147, 410);
            this.pb_clear.Name = "pb_clear";
            this.pb_clear.Size = new System.Drawing.Size(89, 82);
            this.pb_clear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_clear.TabIndex = 31;
            this.pb_clear.TabStop = false;
            this.pb_clear.Click += new System.EventHandler(this.pb_clear_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox4.Location = new System.Drawing.Point(30, 410);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(89, 82);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 29;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // next_deli
            // 
            this.next_deli.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_deli.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_deli.Controls.Add(this.lbl_n_po);
            this.next_deli.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_deli.Location = new System.Drawing.Point(836, 410);
            this.next_deli.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_deli.Name = "next_deli";
            this.next_deli.Size = new System.Drawing.Size(155, 82);
            this.next_deli.TabIndex = 20;
            this.next_deli.Click += new System.EventHandler(this.next_deli_Click);
            // 
            // lbl_n_po
            // 
            this.lbl_n_po.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_n_po.BackColor = System.Drawing.Color.Transparent;
            this.lbl_n_po.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_n_po.ForeColor = System.Drawing.Color.White;
            this.lbl_n_po.Location = new System.Drawing.Point(15, 23);
            this.lbl_n_po.Name = "lbl_n_po";
            this.lbl_n_po.Size = new System.Drawing.Size(95, 31);
            this.lbl_n_po.TabIndex = 15;
            this.lbl_n_po.Text = "Next";
            this.lbl_n_po.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_n_po.Click += new System.EventHandler(this.next_deli_Click);
            // 
            // back_deli
            // 
            this.back_deli.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_deli.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_deli.Controls.Add(this.lbl_b_po);
            this.back_deli.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_deli.Location = new System.Drawing.Point(647, 410);
            this.back_deli.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_deli.Name = "back_deli";
            this.back_deli.Size = new System.Drawing.Size(155, 82);
            this.back_deli.TabIndex = 19;
            this.back_deli.Click += new System.EventHandler(this.back_deli_Click);
            // 
            // lbl_b_po
            // 
            this.lbl_b_po.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_po.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_po.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_po.ForeColor = System.Drawing.Color.White;
            this.lbl_b_po.Location = new System.Drawing.Point(30, 23);
            this.lbl_b_po.Name = "lbl_b_po";
            this.lbl_b_po.Size = new System.Drawing.Size(106, 31);
            this.lbl_b_po.TabIndex = 15;
            this.lbl_b_po.Text = "Previous";
            this.lbl_b_po.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_po.Click += new System.EventHandler(this.back_deli_Click);
            // 
            // pan_truckin_still
            // 
            this.pan_truckin_still.Controls.Add(this.lbl_sg_still);
            this.pan_truckin_still.Controls.Add(this.txt_still);
            this.pan_truckin_still.Controls.Add(this.lbl_del_stil);
            this.pan_truckin_still.Controls.Add(this.pb_chk_nonsgstill);
            this.pan_truckin_still.Controls.Add(this.lbl_exstill);
            this.pan_truckin_still.Controls.Add(this.pb_chk_sgstill);
            this.pan_truckin_still.Controls.Add(this.pictureBox8);
            this.pan_truckin_still.Controls.Add(this.pictureBox9);
            this.pan_truckin_still.Controls.Add(this.lbl_nonsg_still);
            this.pan_truckin_still.Location = new System.Drawing.Point(128, 166);
            this.pan_truckin_still.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_truckin_still.Name = "pan_truckin_still";
            this.pan_truckin_still.Size = new System.Drawing.Size(750, 225);
            this.pan_truckin_still.TabIndex = 30;
            // 
            // lbl_sg_still
            // 
            this.lbl_sg_still.AutoSize = true;
            this.lbl_sg_still.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sg_still.Location = new System.Drawing.Point(35, 14);
            this.lbl_sg_still.Name = "lbl_sg_still";
            this.lbl_sg_still.Size = new System.Drawing.Size(264, 22);
            this.lbl_sg_still.TabIndex = 12;
            this.lbl_sg_still.Text = "My stillage is a Saint-Gobain stillage";
            this.lbl_sg_still.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_still
            // 
            this.txt_still.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_still.Location = new System.Drawing.Point(176, 65);
            this.txt_still.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_still.Name = "txt_still";
            this.txt_still.Size = new System.Drawing.Size(363, 35);
            this.txt_still.TabIndex = 3;
            this.txt_still.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_still_KeyDown);
            // 
            // lbl_del_stil
            // 
            this.lbl_del_stil.AutoSize = true;
            this.lbl_del_stil.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_del_stil.Location = new System.Drawing.Point(35, 65);
            this.lbl_del_stil.Name = "lbl_del_stil";
            this.lbl_del_stil.Size = new System.Drawing.Size(86, 22);
            this.lbl_del_stil.TabIndex = 20;
            this.lbl_del_stil.Text = "Stillage No";
            this.lbl_del_stil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_chk_nonsgstill
            // 
            this.pb_chk_nonsgstill.BackColor = System.Drawing.Color.White;
            this.pb_chk_nonsgstill.Image = global::Orion_truck.Properties.Resources.uncheck_box;
            this.pb_chk_nonsgstill.Location = new System.Drawing.Point(384, 157);
            this.pb_chk_nonsgstill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_chk_nonsgstill.Name = "pb_chk_nonsgstill";
            this.pb_chk_nonsgstill.Size = new System.Drawing.Size(45, 45);
            this.pb_chk_nonsgstill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_chk_nonsgstill.TabIndex = 25;
            this.pb_chk_nonsgstill.TabStop = false;
            this.pb_chk_nonsgstill.Click += new System.EventHandler(this.pb_chk_nonsgstill_Click);
            // 
            // lbl_exstill
            // 
            this.lbl_exstill.AutoSize = true;
            this.lbl_exstill.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_exstill.Location = new System.Drawing.Point(35, 101);
            this.lbl_exstill.Name = "lbl_exstill";
            this.lbl_exstill.Size = new System.Drawing.Size(95, 22);
            this.lbl_exstill.TabIndex = 14;
            this.lbl_exstill.Text = "(SGG1234M)";
            // 
            // pb_chk_sgstill
            // 
            this.pb_chk_sgstill.BackColor = System.Drawing.Color.White;
            this.pb_chk_sgstill.Image = global::Orion_truck.Properties.Resources.check_box;
            this.pb_chk_sgstill.Location = new System.Drawing.Point(379, 5);
            this.pb_chk_sgstill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_chk_sgstill.Name = "pb_chk_sgstill";
            this.pb_chk_sgstill.Size = new System.Drawing.Size(45, 45);
            this.pb_chk_sgstill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_chk_sgstill.TabIndex = 24;
            this.pb_chk_sgstill.TabStop = false;
            this.pb_chk_sgstill.Click += new System.EventHandler(this.pb_chk_sgstill_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.White;
            this.pictureBox8.Image = global::Orion_truck.Properties.Resources.inloader_stillage_1;
            this.pictureBox8.Location = new System.Drawing.Point(576, 3);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(116, 103);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 17;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.White;
            this.pictureBox9.Image = global::Orion_truck.Properties.Resources.Ext_still;
            this.pictureBox9.Location = new System.Drawing.Point(576, 117);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(116, 103);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 19;
            this.pictureBox9.TabStop = false;
            // 
            // lbl_nonsg_still
            // 
            this.lbl_nonsg_still.AutoSize = true;
            this.lbl_nonsg_still.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nonsg_still.Location = new System.Drawing.Point(35, 171);
            this.lbl_nonsg_still.Name = "lbl_nonsg_still";
            this.lbl_nonsg_still.Size = new System.Drawing.Size(286, 22);
            this.lbl_nonsg_still.TabIndex = 16;
            this.lbl_nonsg_still.Text = "My stillage is not a saint gobain stillage";
            this.lbl_nonsg_still.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pan_truckin_po
            // 
            this.pan_truckin_po.Controls.Add(this.lbl_CustReturn);
            this.pan_truckin_po.Controls.Add(this.pb_chkbox_cusReturn);
            this.pan_truckin_po.Controls.Add(this.txt_po);
            this.pan_truckin_po.Controls.Add(this.lbl_expo);
            this.pan_truckin_po.Controls.Add(this.lblCustR_SO);
            this.pan_truckin_po.Controls.Add(this.lbl_pono);
            this.pan_truckin_po.Controls.Add(this.lblCustR_SOEX);
            this.pan_truckin_po.Controls.Add(this.txtCustR_SO);
            this.pan_truckin_po.Location = new System.Drawing.Point(132, 34);
            this.pan_truckin_po.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_truckin_po.Name = "pan_truckin_po";
            this.pan_truckin_po.Size = new System.Drawing.Size(752, 118);
            this.pan_truckin_po.TabIndex = 22;
            // 
            // lbl_CustReturn
            // 
            this.lbl_CustReturn.AutoSize = true;
            this.lbl_CustReturn.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CustReturn.Location = new System.Drawing.Point(615, 11);
            this.lbl_CustReturn.Name = "lbl_CustReturn";
            this.lbl_CustReturn.Size = new System.Drawing.Size(131, 22);
            this.lbl_CustReturn.TabIndex = 27;
            this.lbl_CustReturn.Text = "Customer returns";
            this.lbl_CustReturn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_chkbox_cusReturn
            // 
            this.pb_chkbox_cusReturn.BackColor = System.Drawing.Color.White;
            this.pb_chkbox_cusReturn.Image = global::Orion_truck.Properties.Resources.uncheck_box;
            this.pb_chkbox_cusReturn.Location = new System.Drawing.Point(560, 4);
            this.pb_chkbox_cusReturn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_chkbox_cusReturn.Name = "pb_chkbox_cusReturn";
            this.pb_chkbox_cusReturn.Size = new System.Drawing.Size(45, 36);
            this.pb_chkbox_cusReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_chkbox_cusReturn.TabIndex = 26;
            this.pb_chkbox_cusReturn.TabStop = false;
            this.pb_chkbox_cusReturn.Click += new System.EventHandler(this.pb_chkbox_cusReturn_Click);
            // 
            // txt_po
            // 
            this.txt_po.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_po.Location = new System.Drawing.Point(176, 5);
            this.txt_po.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_po.MaxLength = 10;
            this.txt_po.Name = "txt_po";
            this.txt_po.Size = new System.Drawing.Size(363, 35);
            this.txt_po.TabIndex = 1;
            this.txt_po.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_po_KeyDown);
            this.txt_po.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_po_KeyPress_1);
            // 
            // lbl_expo
            // 
            this.lbl_expo.AutoSize = true;
            this.lbl_expo.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expo.Location = new System.Drawing.Point(35, 35);
            this.lbl_expo.Name = "lbl_expo";
            this.lbl_expo.Size = new System.Drawing.Size(102, 22);
            this.lbl_expo.TabIndex = 13;
            this.lbl_expo.Text = "(6500456483)";
            // 
            // lblCustR_SO
            // 
            this.lblCustR_SO.AutoSize = true;
            this.lblCustR_SO.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustR_SO.Location = new System.Drawing.Point(35, 10);
            this.lblCustR_SO.Name = "lblCustR_SO";
            this.lblCustR_SO.Size = new System.Drawing.Size(85, 22);
            this.lblCustR_SO.TabIndex = 29;
            this.lblCustR_SO.Text = "Sale Order";
            this.lblCustR_SO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_pono
            // 
            this.lbl_pono.AutoSize = true;
            this.lbl_pono.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pono.Location = new System.Drawing.Point(35, 10);
            this.lbl_pono.Name = "lbl_pono";
            this.lbl_pono.Size = new System.Drawing.Size(113, 22);
            this.lbl_pono.TabIndex = 11;
            this.lbl_pono.Text = "Puchase Order";
            this.lbl_pono.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCustR_SOEX
            // 
            this.lblCustR_SOEX.AutoSize = true;
            this.lblCustR_SOEX.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustR_SOEX.Location = new System.Drawing.Point(35, 35);
            this.lblCustR_SOEX.Name = "lblCustR_SOEX";
            this.lblCustR_SOEX.Size = new System.Drawing.Size(78, 22);
            this.lblCustR_SOEX.TabIndex = 30;
            this.lblCustR_SOEX.Text = "(3018507)";
            // 
            // txtCustR_SO
            // 
            this.txtCustR_SO.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustR_SO.Location = new System.Drawing.Point(176, 5);
            this.txtCustR_SO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCustR_SO.MaxLength = 10;
            this.txtCustR_SO.Name = "txtCustR_SO";
            this.txtCustR_SO.Size = new System.Drawing.Size(363, 35);
            this.txtCustR_SO.TabIndex = 28;
            // 
            // pan_truckin_cullet
            // 
            this.pan_truckin_cullet.Controls.Add(this.txtSupp);
            this.pan_truckin_cullet.Controls.Add(this.lblSupp);
            this.pan_truckin_cullet.Controls.Add(this.lblSupEX);
            this.pan_truckin_cullet.Controls.Add(this.txtNoOfBags);
            this.pan_truckin_cullet.Controls.Add(this.lblNoOfBags);
            this.pan_truckin_cullet.Controls.Add(this.lblNoOfBagsEx);
            this.pan_truckin_cullet.Controls.Add(this.txt_cul);
            this.pan_truckin_cullet.Controls.Add(this.lbl_cul);
            this.pan_truckin_cullet.Controls.Add(this.lbl_excul);
            this.pan_truckin_cullet.Location = new System.Drawing.Point(132, 34);
            this.pan_truckin_cullet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_truckin_cullet.Name = "pan_truckin_cullet";
            this.pan_truckin_cullet.Size = new System.Drawing.Size(752, 131);
            this.pan_truckin_cullet.TabIndex = 29;
            // 
            // txtSupp
            // 
            this.txtSupp.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupp.Location = new System.Drawing.Point(176, 91);
            this.txtSupp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSupp.Name = "txtSupp";
            this.txtSupp.Size = new System.Drawing.Size(366, 35);
            this.txtSupp.TabIndex = 26;
            this.txtSupp.TextChanged += new System.EventHandler(this.txtSupp_TextChanged);
            this.txtSupp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupp_KeyDown);
            this.txtSupp.Leave += new System.EventHandler(this.txtSupp_Leave);
            // 
            // lblSupp
            // 
            this.lblSupp.AutoSize = true;
            this.lblSupp.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupp.Location = new System.Drawing.Point(35, 80);
            this.lblSupp.Name = "lblSupp";
            this.lblSupp.Size = new System.Drawing.Size(68, 22);
            this.lblSupp.TabIndex = 28;
            this.lblSupp.Text = "Supplier";
            this.lblSupp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSupEX
            // 
            this.lblSupEX.AutoSize = true;
            this.lblSupEX.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupEX.Location = new System.Drawing.Point(33, 103);
            this.lblSupEX.Name = "lblSupEX";
            this.lblSupEX.Size = new System.Drawing.Size(100, 22);
            this.lblSupEX.TabIndex = 27;
            this.lblSupEX.Text = "(Gomelglass)";
            // 
            // txtNoOfBags
            // 
            this.txtNoOfBags.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoOfBags.Location = new System.Drawing.Point(176, 8);
            this.txtNoOfBags.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNoOfBags.MaxLength = 5;
            this.txtNoOfBags.Name = "txtNoOfBags";
            this.txtNoOfBags.Size = new System.Drawing.Size(88, 35);
            this.txtNoOfBags.TabIndex = 23;
            this.txtNoOfBags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfBags_KeyPress);
            // 
            // lblNoOfBags
            // 
            this.lblNoOfBags.AutoSize = true;
            this.lblNoOfBags.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfBags.Location = new System.Drawing.Point(37, 10);
            this.lblNoOfBags.Name = "lblNoOfBags";
            this.lblNoOfBags.Size = new System.Drawing.Size(88, 22);
            this.lblNoOfBags.TabIndex = 24;
            this.lblNoOfBags.Text = "No.Of Bags";
            this.lblNoOfBags.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNoOfBagsEx
            // 
            this.lblNoOfBagsEx.AutoSize = true;
            this.lblNoOfBagsEx.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfBagsEx.Location = new System.Drawing.Point(37, 34);
            this.lblNoOfBagsEx.Name = "lblNoOfBagsEx";
            this.lblNoOfBagsEx.Size = new System.Drawing.Size(30, 22);
            this.lblNoOfBagsEx.TabIndex = 25;
            this.lblNoOfBagsEx.Text = "(3)";
            // 
            // txt_cul
            // 
            this.txt_cul.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cul.Location = new System.Drawing.Point(451, 8);
            this.txt_cul.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_cul.Name = "txt_cul";
            this.txt_cul.Size = new System.Drawing.Size(88, 35);
            this.txt_cul.TabIndex = 2;
            this.txt_cul.Visible = false;
            this.txt_cul.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_cul_KeyDown);
            this.txt_cul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cul_KeyPress);
            // 
            // lbl_cul
            // 
            this.lbl_cul.AutoSize = true;
            this.lbl_cul.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cul.Location = new System.Drawing.Point(325, 10);
            this.lbl_cul.Name = "lbl_cul";
            this.lbl_cul.Size = new System.Drawing.Size(98, 22);
            this.lbl_cul.TabIndex = 21;
            this.lbl_cul.Text = "Weight (KG)";
            this.lbl_cul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_cul.Visible = false;
            // 
            // lbl_excul
            // 
            this.lbl_excul.AutoSize = true;
            this.lbl_excul.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_excul.Location = new System.Drawing.Point(325, 34);
            this.lbl_excul.Name = "lbl_excul";
            this.lbl_excul.Size = new System.Drawing.Size(54, 22);
            this.lbl_excul.TabIndex = 22;
            this.lbl_excul.Text = "(1000)";
            this.lbl_excul.Visible = false;
            // 
            // tab_truckout
            // 
            this.tab_truckout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_truckout.Controls.Add(this.pictureBox5);
            this.tab_truckout.Controls.Add(this.lbl_trkout_sltgoods);
            this.tab_truckout.Controls.Add(this.lbl_out5);
            this.tab_truckout.Controls.Add(this.lbl_title_out);
            this.tab_truckout.Controls.Add(this.label7);
            this.tab_truckout.Controls.Add(this.label17);
            this.tab_truckout.Controls.Add(this.label20);
            this.tab_truckout.Controls.Add(this.label22);
            this.tab_truckout.Controls.Add(this.label23);
            this.tab_truckout.Controls.Add(this.pan_out1);
            this.tab_truckout.Controls.Add(this.label24);
            this.tab_truckout.Controls.Add(this.pan_out2);
            this.tab_truckout.Controls.Add(this.label25);
            this.tab_truckout.Controls.Add(this.lbl_out1);
            this.tab_truckout.Controls.Add(this.pan_out3);
            this.tab_truckout.Controls.Add(this.lbl_out2);
            this.tab_truckout.Controls.Add(this.pan_out4);
            this.tab_truckout.Controls.Add(this.pan_out5);
            this.tab_truckout.Controls.Add(this.lbl_out3);
            this.tab_truckout.Controls.Add(this.lbl_out4);
            this.tab_truckout.Controls.Add(this.next_trkout);
            this.tab_truckout.Controls.Add(this.back_truckout);
            this.tab_truckout.Controls.Add(this.panel20);
            this.tab_truckout.Controls.Add(this.pb_out_2);
            this.tab_truckout.Controls.Add(this.pb_out_1);
            this.tab_truckout.Controls.Add(this.pb_out_3);
            this.tab_truckout.Controls.Add(this.pb_out_5);
            this.tab_truckout.Controls.Add(this.pb_out_4);
            this.tab_truckout.Location = new System.Drawing.Point(4, 4);
            this.tab_truckout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_truckout.Name = "tab_truckout";
            this.tab_truckout.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_truckout.Size = new System.Drawing.Size(993, 478);
            this.tab_truckout.TabIndex = 5;
            this.tab_truckout.Text = "Truckout";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox5.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox5.Location = new System.Drawing.Point(30, 410);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(89, 82);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 48;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // lbl_trkout_sltgoods
            // 
            this.lbl_trkout_sltgoods.AutoSize = true;
            this.lbl_trkout_sltgoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkout_sltgoods.Location = new System.Drawing.Point(8, 183);
            this.lbl_trkout_sltgoods.Name = "lbl_trkout_sltgoods";
            this.lbl_trkout_sltgoods.Size = new System.Drawing.Size(161, 20);
            this.lbl_trkout_sltgoods.TabIndex = 47;
            this.lbl_trkout_sltgoods.Text = "Select Your Goods";
            // 
            // lbl_out5
            // 
            this.lbl_out5.AutoSize = true;
            this.lbl_out5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out5.Location = new System.Drawing.Point(869, 372);
            this.lbl_out5.Name = "lbl_out5";
            this.lbl_out5.Size = new System.Drawing.Size(78, 25);
            this.lbl_out5.TabIndex = 46;
            this.lbl_out5.Text = "Empty ";
            this.lbl_out5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_out
            // 
            this.lbl_title_out.AutoSize = true;
            this.lbl_title_out.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_out.Location = new System.Drawing.Point(6, 7);
            this.lbl_title_out.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title_out.Name = "lbl_title_out";
            this.lbl_title_out.Size = new System.Drawing.Size(433, 27);
            this.lbl_title_out.TabIndex = 44;
            this.lbl_title_out.Text = "After Unloading will you load a new Order ?";
            this.lbl_title_out.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 30);
            this.label7.TabIndex = 20;
            this.label7.Text = "You Arrived With Glass Goods";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 16);
            this.label17.TabIndex = 37;
            this.label17.Text = "Others";
            this.label17.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(35, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 16);
            this.label20.TabIndex = 34;
            this.label20.Text = "Cutllet";
            this.label20.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(23, 122);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 16);
            this.label22.TabIndex = 33;
            this.label22.Text = "Stillage";
            this.label22.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(41, 113);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(33, 16);
            this.label23.TabIndex = 31;
            this.label23.Text = "Glass";
            this.label23.Visible = false;
            // 
            // pan_out1
            // 
            this.pan_out1.BackColor = System.Drawing.Color.Green;
            this.pan_out1.Location = new System.Drawing.Point(5, 217);
            this.pan_out1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_out1.Name = "pan_out1";
            this.pan_out1.Size = new System.Drawing.Size(10, 150);
            this.pan_out1.TabIndex = 32;
            this.pan_out1.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(35, 122);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 16);
            this.label24.TabIndex = 29;
            this.label24.Text = "Empty";
            this.label24.Visible = false;
            // 
            // pan_out2
            // 
            this.pan_out2.BackColor = System.Drawing.Color.Green;
            this.pan_out2.Location = new System.Drawing.Point(209, 218);
            this.pan_out2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_out2.Name = "pan_out2";
            this.pan_out2.Size = new System.Drawing.Size(10, 150);
            this.pan_out2.TabIndex = 35;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(37, 95);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 16);
            this.label25.TabIndex = 26;
            this.label25.Text = "label2";
            this.label25.Visible = false;
            // 
            // lbl_out1
            // 
            this.lbl_out1.AutoSize = true;
            this.lbl_out1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out1.Location = new System.Drawing.Point(51, 372);
            this.lbl_out1.Name = "lbl_out1";
            this.lbl_out1.Size = new System.Drawing.Size(78, 25);
            this.lbl_out1.TabIndex = 22;
            this.lbl_out1.Text = "Empty ";
            this.lbl_out1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pan_out3
            // 
            this.pan_out3.BackColor = System.Drawing.Color.Green;
            this.pan_out3.Location = new System.Drawing.Point(409, 218);
            this.pan_out3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_out3.Name = "pan_out3";
            this.pan_out3.Size = new System.Drawing.Size(10, 150);
            this.pan_out3.TabIndex = 36;
            this.pan_out3.Visible = false;
            // 
            // lbl_out2
            // 
            this.lbl_out2.AutoSize = true;
            this.lbl_out2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out2.Location = new System.Drawing.Point(251, 372);
            this.lbl_out2.Name = "lbl_out2";
            this.lbl_out2.Size = new System.Drawing.Size(67, 25);
            this.lbl_out2.TabIndex = 23;
            this.lbl_out2.Text = "Glass";
            this.lbl_out2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pan_out4
            // 
            this.pan_out4.BackColor = System.Drawing.Color.Green;
            this.pan_out4.Location = new System.Drawing.Point(609, 218);
            this.pan_out4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_out4.Name = "pan_out4";
            this.pan_out4.Size = new System.Drawing.Size(10, 150);
            this.pan_out4.TabIndex = 38;
            this.pan_out4.Visible = false;
            // 
            // pan_out5
            // 
            this.pan_out5.BackColor = System.Drawing.Color.Green;
            this.pan_out5.Location = new System.Drawing.Point(807, 218);
            this.pan_out5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pan_out5.Name = "pan_out5";
            this.pan_out5.Size = new System.Drawing.Size(10, 150);
            this.pan_out5.TabIndex = 40;
            this.pan_out5.Visible = false;
            // 
            // lbl_out3
            // 
            this.lbl_out3.AutoSize = true;
            this.lbl_out3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out3.Location = new System.Drawing.Point(446, 372);
            this.lbl_out3.Name = "lbl_out3";
            this.lbl_out3.Size = new System.Drawing.Size(83, 25);
            this.lbl_out3.TabIndex = 25;
            this.lbl_out3.Text = "Stillage";
            this.lbl_out3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_out4
            // 
            this.lbl_out4.AutoSize = true;
            this.lbl_out4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out4.Location = new System.Drawing.Point(656, 372);
            this.lbl_out4.Name = "lbl_out4";
            this.lbl_out4.Size = new System.Drawing.Size(67, 25);
            this.lbl_out4.TabIndex = 27;
            this.lbl_out4.Text = "Cullet";
            this.lbl_out4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // next_trkout
            // 
            this.next_trkout.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_trkout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_trkout.Controls.Add(this.lbl_n_trkout);
            this.next_trkout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_trkout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.next_trkout.Location = new System.Drawing.Point(836, 410);
            this.next_trkout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_trkout.Name = "next_trkout";
            this.next_trkout.Size = new System.Drawing.Size(155, 82);
            this.next_trkout.TabIndex = 45;
            this.next_trkout.Click += new System.EventHandler(this.next_truckout_Click);
            // 
            // lbl_n_trkout
            // 
            this.lbl_n_trkout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_n_trkout.BackColor = System.Drawing.Color.Transparent;
            this.lbl_n_trkout.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_n_trkout.ForeColor = System.Drawing.Color.White;
            this.lbl_n_trkout.Location = new System.Drawing.Point(15, 23);
            this.lbl_n_trkout.Name = "lbl_n_trkout";
            this.lbl_n_trkout.Size = new System.Drawing.Size(95, 31);
            this.lbl_n_trkout.TabIndex = 15;
            this.lbl_n_trkout.Text = "Next";
            this.lbl_n_trkout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_n_trkout.Click += new System.EventHandler(this.next_truckout_Click);
            // 
            // back_truckout
            // 
            this.back_truckout.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_truckout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_truckout.Controls.Add(this.lbl_b_trkout);
            this.back_truckout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_truckout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back_truckout.Location = new System.Drawing.Point(647, 410);
            this.back_truckout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_truckout.Name = "back_truckout";
            this.back_truckout.Size = new System.Drawing.Size(155, 82);
            this.back_truckout.TabIndex = 43;
            this.back_truckout.Click += new System.EventHandler(this.back_truckout_Click);
            // 
            // lbl_b_trkout
            // 
            this.lbl_b_trkout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_trkout.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_trkout.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_trkout.ForeColor = System.Drawing.Color.White;
            this.lbl_b_trkout.Location = new System.Drawing.Point(30, 23);
            this.lbl_b_trkout.Name = "lbl_b_trkout";
            this.lbl_b_trkout.Size = new System.Drawing.Size(106, 31);
            this.lbl_b_trkout.TabIndex = 15;
            this.lbl_b_trkout.Text = "Previous";
            this.lbl_b_trkout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_trkout.Click += new System.EventHandler(this.back_truckout_Click);
            // 
            // panel20
            // 
            this.panel20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel20.BackgroundImage = global::Orion_truck.Properties.Resources.white_1;
            this.panel20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel20.Controls.Add(this.pb_out);
            this.panel20.Location = new System.Drawing.Point(661, 0);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(326, 197);
            this.panel20.TabIndex = 39;
            // 
            // pb_out
            // 
            this.pb_out.BackColor = System.Drawing.Color.Transparent;
            this.pb_out.Image = global::Orion_truck.Properties.Resources.inloadOUT1;
            this.pb_out.Location = new System.Drawing.Point(6, 5);
            this.pb_out.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out.Name = "pb_out";
            this.pb_out.Size = new System.Drawing.Size(317, 188);
            this.pb_out.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_out.TabIndex = 1;
            this.pb_out.TabStop = false;
            // 
            // pb_out_2
            // 
            this.pb_out_2.BackColor = System.Drawing.Color.White;
            this.pb_out_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_out_2.Image = global::Orion_truck.Properties.Resources.empty_truck1;
            this.pb_out_2.Location = new System.Drawing.Point(220, 218);
            this.pb_out_2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out_2.Name = "pb_out_2";
            this.pb_out_2.Size = new System.Drawing.Size(174, 150);
            this.pb_out_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_out_2.TabIndex = 42;
            this.pb_out_2.TabStop = false;
            this.pb_out_2.Click += new System.EventHandler(this.pb_out_2_Click);
            // 
            // pb_out_1
            // 
            this.pb_out_1.BackColor = System.Drawing.Color.White;
            this.pb_out_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_out_1.Image = global::Orion_truck.Properties.Resources.empty_euro;
            this.pb_out_1.Location = new System.Drawing.Point(16, 218);
            this.pb_out_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out_1.Name = "pb_out_1";
            this.pb_out_1.Size = new System.Drawing.Size(174, 150);
            this.pb_out_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_out_1.TabIndex = 21;
            this.pb_out_1.TabStop = false;
            this.pb_out_1.Click += new System.EventHandler(this.pb_out_1_Click);
            // 
            // pb_out_3
            // 
            this.pb_out_3.BackColor = System.Drawing.Color.White;
            this.pb_out_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_out_3.Image = global::Orion_truck.Properties.Resources.sasa;
            this.pb_out_3.Location = new System.Drawing.Point(420, 218);
            this.pb_out_3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out_3.Name = "pb_out_3";
            this.pb_out_3.Size = new System.Drawing.Size(174, 150);
            this.pb_out_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_out_3.TabIndex = 24;
            this.pb_out_3.TabStop = false;
            this.pb_out_3.Click += new System.EventHandler(this.pb_out_3_Click);
            // 
            // pb_out_5
            // 
            this.pb_out_5.BackColor = System.Drawing.Color.White;
            this.pb_out_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_out_5.Image = global::Orion_truck.Properties.Resources.con_glass_n1;
            this.pb_out_5.Location = new System.Drawing.Point(818, 218);
            this.pb_out_5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out_5.Name = "pb_out_5";
            this.pb_out_5.Size = new System.Drawing.Size(174, 150);
            this.pb_out_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_out_5.TabIndex = 30;
            this.pb_out_5.TabStop = false;
            this.pb_out_5.Click += new System.EventHandler(this.pb_out_5_Click);
            // 
            // pb_out_4
            // 
            this.pb_out_4.BackColor = System.Drawing.Color.White;
            this.pb_out_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_out_4.Image = global::Orion_truck.Properties.Resources.Cullet;
            this.pb_out_4.Location = new System.Drawing.Point(620, 218);
            this.pb_out_4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_out_4.Name = "pb_out_4";
            this.pb_out_4.Size = new System.Drawing.Size(174, 150);
            this.pb_out_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_out_4.TabIndex = 28;
            this.pb_out_4.TabStop = false;
            this.pb_out_4.Click += new System.EventHandler(this.pb_out_4_Click);
            // 
            // tab_so
            // 
            this.tab_so.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_so.Controls.Add(this.panel_still);
            this.tab_so.Controls.Add(this.lbl_title_returnorder);
            this.tab_so.Controls.Add(this.groupBox2);
            this.tab_so.Controls.Add(this.pictureBox6);
            this.tab_so.Controls.Add(this.pictureBox2);
            this.tab_so.Controls.Add(this.next_so);
            this.tab_so.Controls.Add(this.back_so);
            this.tab_so.Location = new System.Drawing.Point(4, 4);
            this.tab_so.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_so.Name = "tab_so";
            this.tab_so.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_so.Size = new System.Drawing.Size(993, 478);
            this.tab_so.TabIndex = 6;
            this.tab_so.Text = "Saleordr";
            // 
            // panel_still
            // 
            this.panel_still.Controls.Add(this.txt_so_Still);
            this.panel_still.Controls.Add(this.lbl_SOSgStilltxt);
            this.panel_still.Controls.Add(this.pb_NonsoSGStill);
            this.panel_still.Controls.Add(this.lbl_SOSgStillex);
            this.panel_still.Controls.Add(this.pb_soSGStill);
            this.panel_still.Controls.Add(this.pictureBox16);
            this.panel_still.Controls.Add(this.pictureBox17);
            this.panel_still.Controls.Add(this.lbl_SOnonSgStill);
            this.panel_still.Controls.Add(this.lbl_SOSgStill);
            this.panel_still.Location = new System.Drawing.Point(189, 149);
            this.panel_still.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_still.Name = "panel_still";
            this.panel_still.Size = new System.Drawing.Size(713, 240);
            this.panel_still.TabIndex = 42;
            // 
            // txt_so_Still
            // 
            this.txt_so_Still.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_so_Still.Location = new System.Drawing.Point(176, 65);
            this.txt_so_Still.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_so_Still.Name = "txt_so_Still";
            this.txt_so_Still.Size = new System.Drawing.Size(363, 35);
            this.txt_so_Still.TabIndex = 3;
            // 
            // lbl_SOSgStilltxt
            // 
            this.lbl_SOSgStilltxt.AutoSize = true;
            this.lbl_SOSgStilltxt.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SOSgStilltxt.Location = new System.Drawing.Point(35, 14);
            this.lbl_SOSgStilltxt.Name = "lbl_SOSgStilltxt";
            this.lbl_SOSgStilltxt.Size = new System.Drawing.Size(264, 22);
            this.lbl_SOSgStilltxt.TabIndex = 12;
            this.lbl_SOSgStilltxt.Text = "My stillage is a Saint-Gobain stillage";
            this.lbl_SOSgStilltxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_NonsoSGStill
            // 
            this.pb_NonsoSGStill.BackColor = System.Drawing.Color.White;
            this.pb_NonsoSGStill.Image = global::Orion_truck.Properties.Resources.uncheck_box;
            this.pb_NonsoSGStill.Location = new System.Drawing.Point(327, 172);
            this.pb_NonsoSGStill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_NonsoSGStill.Name = "pb_NonsoSGStill";
            this.pb_NonsoSGStill.Size = new System.Drawing.Size(45, 45);
            this.pb_NonsoSGStill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_NonsoSGStill.TabIndex = 25;
            this.pb_NonsoSGStill.TabStop = false;
            this.pb_NonsoSGStill.Click += new System.EventHandler(this.pb_NonsoSGStill_Click);
            // 
            // lbl_SOSgStillex
            // 
            this.lbl_SOSgStillex.AutoSize = true;
            this.lbl_SOSgStillex.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SOSgStillex.Location = new System.Drawing.Point(35, 101);
            this.lbl_SOSgStillex.Name = "lbl_SOSgStillex";
            this.lbl_SOSgStillex.Size = new System.Drawing.Size(95, 22);
            this.lbl_SOSgStillex.TabIndex = 14;
            this.lbl_SOSgStillex.Text = "(SGG1234M)";
            // 
            // pb_soSGStill
            // 
            this.pb_soSGStill.BackColor = System.Drawing.Color.White;
            this.pb_soSGStill.Image = global::Orion_truck.Properties.Resources.check_box;
            this.pb_soSGStill.Location = new System.Drawing.Point(322, 5);
            this.pb_soSGStill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_soSGStill.Name = "pb_soSGStill";
            this.pb_soSGStill.Size = new System.Drawing.Size(45, 45);
            this.pb_soSGStill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_soSGStill.TabIndex = 24;
            this.pb_soSGStill.TabStop = false;
            this.pb_soSGStill.Click += new System.EventHandler(this.pb_soSGStill_Click);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.White;
            this.pictureBox16.Image = global::Orion_truck.Properties.Resources.inloader_stillage_1;
            this.pictureBox16.Location = new System.Drawing.Point(576, 3);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(116, 103);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 17;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.White;
            this.pictureBox17.Image = global::Orion_truck.Properties.Resources.Ext_still;
            this.pictureBox17.Location = new System.Drawing.Point(576, 132);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(116, 103);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 19;
            this.pictureBox17.TabStop = false;
            // 
            // lbl_SOnonSgStill
            // 
            this.lbl_SOnonSgStill.AutoSize = true;
            this.lbl_SOnonSgStill.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SOnonSgStill.Location = new System.Drawing.Point(35, 186);
            this.lbl_SOnonSgStill.Name = "lbl_SOnonSgStill";
            this.lbl_SOnonSgStill.Size = new System.Drawing.Size(286, 22);
            this.lbl_SOnonSgStill.TabIndex = 16;
            this.lbl_SOnonSgStill.Text = "My stillage is not a saint gobain stillage";
            this.lbl_SOnonSgStill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_SOSgStill
            // 
            this.lbl_SOSgStill.AutoSize = true;
            this.lbl_SOSgStill.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SOSgStill.Location = new System.Drawing.Point(35, 65);
            this.lbl_SOSgStill.Name = "lbl_SOSgStill";
            this.lbl_SOSgStill.Size = new System.Drawing.Size(86, 22);
            this.lbl_SOSgStill.TabIndex = 20;
            this.lbl_SOSgStill.Text = "Stillage No";
            this.lbl_SOSgStill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_title_returnorder
            // 
            this.lbl_title_returnorder.AutoSize = true;
            this.lbl_title_returnorder.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_returnorder.Location = new System.Drawing.Point(5, 9);
            this.lbl_title_returnorder.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title_returnorder.Name = "lbl_title_returnorder";
            this.lbl_title_returnorder.Size = new System.Drawing.Size(210, 27);
            this.lbl_title_returnorder.TabIndex = 39;
            this.lbl_title_returnorder.Text = "Enter the Order no. ";
            this.lbl_title_returnorder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_so);
            this.groupBox2.Controls.Add(this.lbl_exso);
            this.groupBox2.Controls.Add(this.lbl_so);
            this.groupBox2.Location = new System.Drawing.Point(189, 43);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(713, 98);
            this.groupBox2.TabIndex = 40;
            // 
            // txt_so
            // 
            this.txt_so.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_so.Location = new System.Drawing.Point(158, 18);
            this.txt_so.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_so.Name = "txt_so";
            this.txt_so.Size = new System.Drawing.Size(363, 35);
            this.txt_so.TabIndex = 4;
            this.txt_so.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_so_KeyDown);
            this.txt_so.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_so_KeyPress);
            // 
            // lbl_exso
            // 
            this.lbl_exso.AutoSize = true;
            this.lbl_exso.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_exso.Location = new System.Drawing.Point(35, 50);
            this.lbl_exso.Name = "lbl_exso";
            this.lbl_exso.Size = new System.Drawing.Size(94, 23);
            this.lbl_exso.TabIndex = 1;
            this.lbl_exso.Text = "(60123456)";
            // 
            // lbl_so
            // 
            this.lbl_so.AutoSize = true;
            this.lbl_so.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_so.Location = new System.Drawing.Point(35, 24);
            this.lbl_so.Name = "lbl_so";
            this.lbl_so.Size = new System.Drawing.Size(89, 23);
            this.lbl_so.TabIndex = 0;
            this.lbl_so.Text = "Sale Order";
            this.lbl_so.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox6.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox6.Location = new System.Drawing.Point(30, 410);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(89, 82);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 41;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::Orion_truck.Properties.Resources.clear_new1234;
            this.pictureBox2.Location = new System.Drawing.Point(147, 410);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(89, 82);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pb_clear_Click);
            // 
            // next_so
            // 
            this.next_so.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_so.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_so.Controls.Add(this.lbl_n_so);
            this.next_so.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_so.Location = new System.Drawing.Point(836, 410);
            this.next_so.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_so.Name = "next_so";
            this.next_so.Size = new System.Drawing.Size(155, 82);
            this.next_so.TabIndex = 38;
            this.next_so.Click += new System.EventHandler(this.next_so_Click);
            // 
            // lbl_n_so
            // 
            this.lbl_n_so.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_n_so.BackColor = System.Drawing.Color.Transparent;
            this.lbl_n_so.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_n_so.ForeColor = System.Drawing.Color.White;
            this.lbl_n_so.Location = new System.Drawing.Point(15, 23);
            this.lbl_n_so.Name = "lbl_n_so";
            this.lbl_n_so.Size = new System.Drawing.Size(95, 31);
            this.lbl_n_so.TabIndex = 15;
            this.lbl_n_so.Text = "Next";
            this.lbl_n_so.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_n_so.Click += new System.EventHandler(this.next_so_Click);
            // 
            // back_so
            // 
            this.back_so.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_so.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_so.Controls.Add(this.lbl_b_so);
            this.back_so.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_so.Location = new System.Drawing.Point(647, 410);
            this.back_so.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_so.Name = "back_so";
            this.back_so.Size = new System.Drawing.Size(155, 82);
            this.back_so.TabIndex = 37;
            this.back_so.Click += new System.EventHandler(this.back_so_Click);
            // 
            // lbl_b_so
            // 
            this.lbl_b_so.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_so.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_so.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_so.ForeColor = System.Drawing.Color.White;
            this.lbl_b_so.Location = new System.Drawing.Point(30, 23);
            this.lbl_b_so.Name = "lbl_b_so";
            this.lbl_b_so.Size = new System.Drawing.Size(106, 31);
            this.lbl_b_so.TabIndex = 15;
            this.lbl_b_so.Text = "Previous";
            this.lbl_b_so.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_so.Click += new System.EventHandler(this.back_so_Click);
            // 
            // tab_trkinfo
            // 
            this.tab_trkinfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_trkinfo.Controls.Add(this.pictureBox13);
            this.tab_trkinfo.Controls.Add(this.pictureBox7);
            this.tab_trkinfo.Controls.Add(this.groupBox5);
            this.tab_trkinfo.Controls.Add(this.lbl_title_info);
            this.tab_trkinfo.Controls.Add(this.back_trkinfo1);
            this.tab_trkinfo.Controls.Add(this.next_trkinfo1);
            this.tab_trkinfo.Location = new System.Drawing.Point(4, 4);
            this.tab_trkinfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trkinfo.Name = "tab_trkinfo";
            this.tab_trkinfo.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trkinfo.Size = new System.Drawing.Size(993, 478);
            this.tab_trkinfo.TabIndex = 8;
            this.tab_trkinfo.Text = "truck_info";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox13.Image = global::Orion_truck.Properties.Resources.clear_new1234;
            this.pictureBox13.Location = new System.Drawing.Point(147, 410);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(89, 82);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 43;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Click += new System.EventHandler(this.pb_clear_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox7.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox7.Location = new System.Drawing.Point(30, 410);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(89, 82);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 29;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txt_Taraweight);
            this.groupBox5.Controls.Add(this.txt_contain);
            this.groupBox5.Controls.Add(this.txt_trilerno);
            this.groupBox5.Controls.Add(this.txt_truckno);
            this.groupBox5.Controls.Add(this.lbl_TaraweightEX);
            this.groupBox5.Controls.Add(this.lbl_excontain);
            this.groupBox5.Controls.Add(this.lbl_trkinfo_extruckno);
            this.groupBox5.Controls.Add(this.lbl_trkinfo_extrailrno);
            this.groupBox5.Controls.Add(this.lbl_trkinfo_truckno);
            this.groupBox5.Controls.Add(this.lbl_trkinfo_trailrno);
            this.groupBox5.Controls.Add(this.lbl_contain);
            this.groupBox5.Controls.Add(this.lbl_Taraweight);
            this.groupBox5.Location = new System.Drawing.Point(162, 48);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(692, 343);
            this.groupBox5.TabIndex = 42;
            // 
            // txt_Taraweight
            // 
            this.txt_Taraweight.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Taraweight.Location = new System.Drawing.Point(170, 277);
            this.txt_Taraweight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Taraweight.Name = "txt_Taraweight";
            this.txt_Taraweight.Size = new System.Drawing.Size(363, 35);
            this.txt_Taraweight.TabIndex = 19;
            this.txt_Taraweight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Taraweight_KeyPress);
            // 
            // txt_contain
            // 
            this.txt_contain.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contain.Location = new System.Drawing.Point(170, 194);
            this.txt_contain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_contain.Name = "txt_contain";
            this.txt_contain.Size = new System.Drawing.Size(363, 35);
            this.txt_contain.TabIndex = 7;
            this.txt_contain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_contain_KeyDown);
            // 
            // txt_trilerno
            // 
            this.txt_trilerno.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_trilerno.Location = new System.Drawing.Point(170, 114);
            this.txt_trilerno.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_trilerno.Name = "txt_trilerno";
            this.txt_trilerno.Size = new System.Drawing.Size(363, 35);
            this.txt_trilerno.TabIndex = 6;
            this.txt_trilerno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_trilerno_KeyDown);
            // 
            // txt_truckno
            // 
            this.txt_truckno.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_truckno.Location = new System.Drawing.Point(170, 22);
            this.txt_truckno.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_truckno.Name = "txt_truckno";
            this.txt_truckno.Size = new System.Drawing.Size(363, 35);
            this.txt_truckno.TabIndex = 5;
            this.txt_truckno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_truckno_KeyDown);
            // 
            // lbl_TaraweightEX
            // 
            this.lbl_TaraweightEX.AutoSize = true;
            this.lbl_TaraweightEX.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TaraweightEX.Location = new System.Drawing.Point(31, 316);
            this.lbl_TaraweightEX.Name = "lbl_TaraweightEX";
            this.lbl_TaraweightEX.Size = new System.Drawing.Size(54, 22);
            this.lbl_TaraweightEX.TabIndex = 21;
            this.lbl_TaraweightEX.Text = "(1000)";
            // 
            // lbl_excontain
            // 
            this.lbl_excontain.AutoSize = true;
            this.lbl_excontain.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_excontain.Location = new System.Drawing.Point(31, 238);
            this.lbl_excontain.Name = "lbl_excontain";
            this.lbl_excontain.Size = new System.Drawing.Size(87, 22);
            this.lbl_excontain.TabIndex = 18;
            this.lbl_excontain.Text = "(LJ5582ES)";
            // 
            // lbl_trkinfo_extruckno
            // 
            this.lbl_trkinfo_extruckno.AutoSize = true;
            this.lbl_trkinfo_extruckno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_extruckno.Location = new System.Drawing.Point(31, 58);
            this.lbl_trkinfo_extruckno.Name = "lbl_trkinfo_extruckno";
            this.lbl_trkinfo_extruckno.Size = new System.Drawing.Size(98, 22);
            this.lbl_trkinfo_extruckno.TabIndex = 14;
            this.lbl_trkinfo_extruckno.Text = "(TN01X1456)";
            // 
            // lbl_trkinfo_extrailrno
            // 
            this.lbl_trkinfo_extrailrno.AutoSize = true;
            this.lbl_trkinfo_extrailrno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_extrailrno.Location = new System.Drawing.Point(31, 148);
            this.lbl_trkinfo_extrailrno.Name = "lbl_trkinfo_extrailrno";
            this.lbl_trkinfo_extrailrno.Size = new System.Drawing.Size(87, 22);
            this.lbl_trkinfo_extrailrno.TabIndex = 15;
            this.lbl_trkinfo_extrailrno.Text = "(LJ5582ES)";
            // 
            // lbl_trkinfo_truckno
            // 
            this.lbl_trkinfo_truckno.AutoSize = true;
            this.lbl_trkinfo_truckno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_truckno.Location = new System.Drawing.Point(31, 22);
            this.lbl_trkinfo_truckno.Name = "lbl_trkinfo_truckno";
            this.lbl_trkinfo_truckno.Size = new System.Drawing.Size(77, 22);
            this.lbl_trkinfo_truckno.TabIndex = 11;
            this.lbl_trkinfo_truckno.Text = "Truck No.";
            this.lbl_trkinfo_truckno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_trailrno
            // 
            this.lbl_trkinfo_trailrno.AutoSize = true;
            this.lbl_trkinfo_trailrno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_trailrno.Location = new System.Drawing.Point(31, 114);
            this.lbl_trkinfo_trailrno.Name = "lbl_trkinfo_trailrno";
            this.lbl_trkinfo_trailrno.Size = new System.Drawing.Size(85, 22);
            this.lbl_trkinfo_trailrno.TabIndex = 12;
            this.lbl_trkinfo_trailrno.Text = "Trailer No.";
            this.lbl_trkinfo_trailrno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_contain
            // 
            this.lbl_contain.AutoSize = true;
            this.lbl_contain.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contain.Location = new System.Drawing.Point(31, 206);
            this.lbl_contain.Name = "lbl_contain";
            this.lbl_contain.Size = new System.Drawing.Size(109, 22);
            this.lbl_contain.TabIndex = 17;
            this.lbl_contain.Text = "Container No.";
            this.lbl_contain.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_Taraweight
            // 
            this.lbl_Taraweight.AutoSize = true;
            this.lbl_Taraweight.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Taraweight.Location = new System.Drawing.Point(31, 284);
            this.lbl_Taraweight.Name = "lbl_Taraweight";
            this.lbl_Taraweight.Size = new System.Drawing.Size(95, 22);
            this.lbl_Taraweight.TabIndex = 20;
            this.lbl_Taraweight.Text = "Tara Weight";
            this.lbl_Taraweight.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_title_info
            // 
            this.lbl_title_info.AutoSize = true;
            this.lbl_title_info.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title_info.Location = new System.Drawing.Point(10, 12);
            this.lbl_title_info.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_title_info.Name = "lbl_title_info";
            this.lbl_title_info.Size = new System.Drawing.Size(304, 27);
            this.lbl_title_info.TabIndex = 11;
            this.lbl_title_info.Text = "Enter your Truck Information.";
            this.lbl_title_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // back_trkinfo1
            // 
            this.back_trkinfo1.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_trkinfo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_trkinfo1.Controls.Add(this.lbl_b_trkinfo1);
            this.back_trkinfo1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_trkinfo1.Location = new System.Drawing.Point(647, 410);
            this.back_trkinfo1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_trkinfo1.Name = "back_trkinfo1";
            this.back_trkinfo1.Size = new System.Drawing.Size(155, 82);
            this.back_trkinfo1.TabIndex = 41;
            this.back_trkinfo1.Click += new System.EventHandler(this.back_trkinfo1_Click);
            // 
            // lbl_b_trkinfo1
            // 
            this.lbl_b_trkinfo1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_trkinfo1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_trkinfo1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_trkinfo1.ForeColor = System.Drawing.Color.White;
            this.lbl_b_trkinfo1.Location = new System.Drawing.Point(30, 23);
            this.lbl_b_trkinfo1.Name = "lbl_b_trkinfo1";
            this.lbl_b_trkinfo1.Size = new System.Drawing.Size(106, 31);
            this.lbl_b_trkinfo1.TabIndex = 15;
            this.lbl_b_trkinfo1.Text = "Previous";
            this.lbl_b_trkinfo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_trkinfo1.Click += new System.EventHandler(this.back_trkinfo1_Click);
            // 
            // next_trkinfo1
            // 
            this.next_trkinfo1.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_trkinfo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_trkinfo1.Controls.Add(this.lbl_n_trkinfo1);
            this.next_trkinfo1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_trkinfo1.Location = new System.Drawing.Point(836, 410);
            this.next_trkinfo1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_trkinfo1.Name = "next_trkinfo1";
            this.next_trkinfo1.Size = new System.Drawing.Size(155, 82);
            this.next_trkinfo1.TabIndex = 39;
            this.next_trkinfo1.Click += new System.EventHandler(this.next_trkinfo1_Click);
            // 
            // lbl_n_trkinfo1
            // 
            this.lbl_n_trkinfo1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_n_trkinfo1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_n_trkinfo1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_n_trkinfo1.ForeColor = System.Drawing.Color.White;
            this.lbl_n_trkinfo1.Location = new System.Drawing.Point(15, 23);
            this.lbl_n_trkinfo1.Name = "lbl_n_trkinfo1";
            this.lbl_n_trkinfo1.Size = new System.Drawing.Size(95, 31);
            this.lbl_n_trkinfo1.TabIndex = 15;
            this.lbl_n_trkinfo1.Text = "Next";
            this.lbl_n_trkinfo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_n_trkinfo1.Click += new System.EventHandler(this.next_trkinfo1_Click);
            // 
            // tab_trukinfo2
            // 
            this.tab_trukinfo2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_trukinfo2.Controls.Add(this.pictureBox14);
            this.tab_trukinfo2.Controls.Add(this.pictureBox11);
            this.tab_trukinfo2.Controls.Add(this.groupBox3);
            this.tab_trukinfo2.Controls.Add(this.lbl_tit_info_name);
            this.tab_trukinfo2.Controls.Add(this.panel7);
            this.tab_trukinfo2.Controls.Add(this.back_trkinfo2);
            this.tab_trukinfo2.Controls.Add(this.next_trkinfo2);
            this.tab_trukinfo2.Location = new System.Drawing.Point(4, 4);
            this.tab_trukinfo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trukinfo2.Name = "tab_trukinfo2";
            this.tab_trukinfo2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_trukinfo2.Size = new System.Drawing.Size(993, 478);
            this.tab_trukinfo2.TabIndex = 9;
            this.tab_trukinfo2.Text = "truck_info1";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Image = global::Orion_truck.Properties.Resources.clear_new1234;
            this.pictureBox14.Location = new System.Drawing.Point(147, 410);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(89, 82);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 46;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Click += new System.EventHandler(this.pb_clear_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Image = global::Orion_truck.Properties.Resources.Home_new1234;
            this.pictureBox11.Location = new System.Drawing.Point(30, 410);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(89, 82);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 29;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Click += new System.EventHandler(this.pb_home_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_countrycodeDis);
            this.groupBox3.Controls.Add(this.txt_name);
            this.groupBox3.Controls.Add(this.pb_getCountryCode);
            this.groupBox3.Controls.Add(this.lblCountryCodeTit);
            this.groupBox3.Controls.Add(this.lblCountryCodeTitEx);
            this.groupBox3.Controls.Add(this.cmb_country);
            this.groupBox3.Controls.Add(this.lbl_top_no);
            this.groupBox3.Controls.Add(this.lbl_top_country);
            this.groupBox3.Controls.Add(this.txt_mobno);
            this.groupBox3.Controls.Add(this.pbflg1);
            this.groupBox3.Controls.Add(this.lbl_trkinfo_exname);
            this.groupBox3.Controls.Add(this.lbl_trkinfo_exmobno);
            this.groupBox3.Controls.Add(this.lbl_trkinfo_name);
            this.groupBox3.Controls.Add(this.lbl_trkinfo_mobno);
            this.groupBox3.Location = new System.Drawing.Point(119, 87);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(735, 255);
            this.groupBox3.TabIndex = 45;
            // 
            // txt_countrycodeDis
            // 
            this.txt_countrycodeDis.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_countrycodeDis.Location = new System.Drawing.Point(214, 102);
            this.txt_countrycodeDis.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_countrycodeDis.Name = "txt_countrycodeDis";
            this.txt_countrycodeDis.ReadOnly = true;
            this.txt_countrycodeDis.Size = new System.Drawing.Size(97, 35);
            this.txt_countrycodeDis.TabIndex = 28;
            // 
            // txt_name
            // 
            this.txt_name.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.Location = new System.Drawing.Point(214, 25);
            this.txt_name.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(469, 35);
            this.txt_name.TabIndex = 8;
            this.txt_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_name_KeyDown);
            // 
            // pb_getCountryCode
            // 
            this.pb_getCountryCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_getCountryCode.Image = global::Orion_truck.Properties.Resources.telephone_icon;
            this.pb_getCountryCode.Location = new System.Drawing.Point(321, 97);
            this.pb_getCountryCode.Name = "pb_getCountryCode";
            this.pb_getCountryCode.Size = new System.Drawing.Size(49, 40);
            this.pb_getCountryCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_getCountryCode.TabIndex = 31;
            this.pb_getCountryCode.TabStop = false;
            this.pb_getCountryCode.Click += new System.EventHandler(this.pb_getCountryCode_Click);
            // 
            // lblCountryCodeTit
            // 
            this.lblCountryCodeTit.AutoSize = true;
            this.lblCountryCodeTit.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryCodeTit.Location = new System.Drawing.Point(424, 187);
            this.lblCountryCodeTit.Name = "lblCountryCodeTit";
            this.lblCountryCodeTit.Size = new System.Drawing.Size(107, 22);
            this.lblCountryCodeTit.TabIndex = 29;
            this.lblCountryCodeTit.Text = "Country Code";
            this.lblCountryCodeTit.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblCountryCodeTit.Visible = false;
            // 
            // lblCountryCodeTitEx
            // 
            this.lblCountryCodeTitEx.AutoSize = true;
            this.lblCountryCodeTitEx.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryCodeTitEx.Location = new System.Drawing.Point(424, 217);
            this.lblCountryCodeTitEx.Name = "lblCountryCodeTitEx";
            this.lblCountryCodeTitEx.Size = new System.Drawing.Size(54, 22);
            this.lblCountryCodeTitEx.TabIndex = 30;
            this.lblCountryCodeTitEx.Text = "(0091)";
            this.lblCountryCodeTitEx.Visible = false;
            // 
            // cmb_country
            // 
            this.cmb_country.DropDownWidth = 250;
            this.cmb_country.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_country.FormattingEnabled = true;
            this.cmb_country.IntegralHeight = false;
            this.cmb_country.Location = new System.Drawing.Point(255, 214);
            this.cmb_country.Name = "cmb_country";
            this.cmb_country.Size = new System.Drawing.Size(132, 37);
            this.cmb_country.TabIndex = 27;
            this.cmb_country.Visible = false;
            this.cmb_country.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_country_KeyDown);
            this.cmb_country.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmb_country_KeyPress);
            this.cmb_country.Leave += new System.EventHandler(this.cmb_country_Leave);
            // 
            // lbl_top_no
            // 
            this.lbl_top_no.AutoSize = true;
            this.lbl_top_no.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbl_top_no.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_top_no.Location = new System.Drawing.Point(397, 76);
            this.lbl_top_no.Name = "lbl_top_no";
            this.lbl_top_no.Size = new System.Drawing.Size(95, 18);
            this.lbl_top_no.TabIndex = 26;
            this.lbl_top_no.Text = "Mobile Number";
            this.lbl_top_no.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_top_country
            // 
            this.lbl_top_country.AutoSize = true;
            this.lbl_top_country.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_top_country.Location = new System.Drawing.Point(211, 76);
            this.lbl_top_country.Name = "lbl_top_country";
            this.lbl_top_country.Size = new System.Drawing.Size(86, 18);
            this.lbl_top_country.TabIndex = 25;
            this.lbl_top_country.Text = "Country Code";
            this.lbl_top_country.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txt_mobno
            // 
            this.txt_mobno.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mobno.Location = new System.Drawing.Point(389, 102);
            this.txt_mobno.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_mobno.Name = "txt_mobno";
            this.txt_mobno.Size = new System.Drawing.Size(294, 35);
            this.txt_mobno.TabIndex = 10;
            this.txt_mobno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_mobno_KeyDown);
            this.txt_mobno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mobno_KeyPress);
            // 
            // pbflg1
            // 
            this.pbflg1.Location = new System.Drawing.Point(214, 151);
            this.pbflg1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbflg1.Name = "pbflg1";
            this.pbflg1.Size = new System.Drawing.Size(75, 36);
            this.pbflg1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbflg1.TabIndex = 24;
            this.pbflg1.TabStop = false;
            // 
            // lbl_trkinfo_exname
            // 
            this.lbl_trkinfo_exname.AutoSize = true;
            this.lbl_trkinfo_exname.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_exname.Location = new System.Drawing.Point(46, 50);
            this.lbl_trkinfo_exname.Name = "lbl_trkinfo_exname";
            this.lbl_trkinfo_exname.Size = new System.Drawing.Size(107, 22);
            this.lbl_trkinfo_exname.TabIndex = 20;
            this.lbl_trkinfo_exname.Text = "(John Smith )";
            // 
            // lbl_trkinfo_exmobno
            // 
            this.lbl_trkinfo_exmobno.AutoSize = true;
            this.lbl_trkinfo_exmobno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_exmobno.Location = new System.Drawing.Point(46, 127);
            this.lbl_trkinfo_exmobno.Name = "lbl_trkinfo_exmobno";
            this.lbl_trkinfo_exmobno.Size = new System.Drawing.Size(140, 22);
            this.lbl_trkinfo_exmobno.TabIndex = 21;
            this.lbl_trkinfo_exmobno.Text = "(0033-9638527412)";
            // 
            // lbl_trkinfo_name
            // 
            this.lbl_trkinfo_name.AutoSize = true;
            this.lbl_trkinfo_name.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_name.Location = new System.Drawing.Point(46, 25);
            this.lbl_trkinfo_name.Name = "lbl_trkinfo_name";
            this.lbl_trkinfo_name.Size = new System.Drawing.Size(100, 22);
            this.lbl_trkinfo_name.TabIndex = 18;
            this.lbl_trkinfo_name.Text = "Driver Name";
            this.lbl_trkinfo_name.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_trkinfo_mobno
            // 
            this.lbl_trkinfo_mobno.AutoSize = true;
            this.lbl_trkinfo_mobno.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo_mobno.Location = new System.Drawing.Point(46, 97);
            this.lbl_trkinfo_mobno.Name = "lbl_trkinfo_mobno";
            this.lbl_trkinfo_mobno.Size = new System.Drawing.Size(87, 22);
            this.lbl_trkinfo_mobno.TabIndex = 17;
            this.lbl_trkinfo_mobno.Text = "Mobile No.";
            this.lbl_trkinfo_mobno.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_tit_info_name
            // 
            this.lbl_tit_info_name.AutoSize = true;
            this.lbl_tit_info_name.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tit_info_name.Location = new System.Drawing.Point(12, 15);
            this.lbl_tit_info_name.Margin = new System.Windows.Forms.Padding(10, 0, 3, 12);
            this.lbl_tit_info_name.Name = "lbl_tit_info_name";
            this.lbl_tit_info_name.Size = new System.Drawing.Size(304, 27);
            this.lbl_tit_info_name.TabIndex = 43;
            this.lbl_tit_info_name.Text = "Enter your Truck Information.";
            this.lbl_tit_info_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.comboBox1);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.textBox1);
            this.panel7.Controls.Add(this.textBox2);
            this.panel7.Controls.Add(this.pictureBox12);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Location = new System.Drawing.Point(62, 112);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(735, 255);
            this.panel7.TabIndex = 46;
            this.panel7.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownWidth = 250;
            this.comboBox1.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.IntegralHeight = false;
            this.comboBox1.Location = new System.Drawing.Point(214, 100);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(179, 37);
            this.comboBox1.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(407, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 18);
            this.label3.TabIndex = 26;
            this.label3.Text = "Mobile Number";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(211, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 18);
            this.label5.TabIndex = 25;
            this.label5.Text = "Country Code";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(399, 102);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(284, 35);
            this.textBox1.TabIndex = 10;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(214, 25);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(469, 35);
            this.textBox2.TabIndex = 8;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new System.Drawing.Point(214, 148);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(75, 36);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 24;
            this.pictureBox12.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 22);
            this.label6.TabIndex = 20;
            this.label6.Text = "(John Smith )";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(46, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 22);
            this.label8.TabIndex = 17;
            this.label8.Text = "Mobile No.";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(46, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 22);
            this.label11.TabIndex = 18;
            this.label11.Text = "Driver Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(46, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(140, 22);
            this.label12.TabIndex = 21;
            this.label12.Text = "(0091-9638527412)";
            // 
            // back_trkinfo2
            // 
            this.back_trkinfo2.BackgroundImage = global::Orion_truck.Properties.Resources.back_t1;
            this.back_trkinfo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.back_trkinfo2.Controls.Add(this.lbl_b_trkinfor2);
            this.back_trkinfo2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_trkinfo2.Location = new System.Drawing.Point(647, 410);
            this.back_trkinfo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.back_trkinfo2.Name = "back_trkinfo2";
            this.back_trkinfo2.Size = new System.Drawing.Size(155, 82);
            this.back_trkinfo2.TabIndex = 44;
            this.back_trkinfo2.Click += new System.EventHandler(this.back_trkinfo2_Click);
            // 
            // lbl_b_trkinfor2
            // 
            this.lbl_b_trkinfor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_b_trkinfor2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_b_trkinfor2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_b_trkinfor2.ForeColor = System.Drawing.Color.White;
            this.lbl_b_trkinfor2.Location = new System.Drawing.Point(30, 23);
            this.lbl_b_trkinfor2.Name = "lbl_b_trkinfor2";
            this.lbl_b_trkinfor2.Size = new System.Drawing.Size(106, 31);
            this.lbl_b_trkinfor2.TabIndex = 15;
            this.lbl_b_trkinfor2.Text = "Previous";
            this.lbl_b_trkinfor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_b_trkinfor2.Click += new System.EventHandler(this.back_trkinfo2_Click);
            // 
            // next_trkinfo2
            // 
            this.next_trkinfo2.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.next_trkinfo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.next_trkinfo2.Controls.Add(this.lbl_trkinfo2);
            this.next_trkinfo2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next_trkinfo2.Location = new System.Drawing.Point(836, 410);
            this.next_trkinfo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.next_trkinfo2.Name = "next_trkinfo2";
            this.next_trkinfo2.Size = new System.Drawing.Size(155, 82);
            this.next_trkinfo2.TabIndex = 42;
            this.next_trkinfo2.Click += new System.EventHandler(this.next_trkinfo2_Click);
            // 
            // lbl_trkinfo2
            // 
            this.lbl_trkinfo2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_trkinfo2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_trkinfo2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_trkinfo2.ForeColor = System.Drawing.Color.White;
            this.lbl_trkinfo2.Location = new System.Drawing.Point(15, 23);
            this.lbl_trkinfo2.Name = "lbl_trkinfo2";
            this.lbl_trkinfo2.Size = new System.Drawing.Size(95, 31);
            this.lbl_trkinfo2.TabIndex = 15;
            this.lbl_trkinfo2.Text = "Next";
            this.lbl_trkinfo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_trkinfo2.Click += new System.EventHandler(this.next_trkinfo2_Click);
            // 
            // tab_comp
            // 
            this.tab_comp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.tab_comp.Controls.Add(this.label13);
            this.tab_comp.Controls.Add(this.label1);
            this.tab_comp.Controls.Add(this.groupBox6);
            this.tab_comp.Controls.Add(this.panel5);
            this.tab_comp.Controls.Add(this.label2);
            this.tab_comp.Location = new System.Drawing.Point(4, 4);
            this.tab_comp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_comp.Name = "tab_comp";
            this.tab_comp.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tab_comp.Size = new System.Drawing.Size(993, 478);
            this.tab_comp.TabIndex = 7;
            this.tab_comp.Text = "complete";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(207, 423);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 27);
            this.label13.TabIndex = 46;
            this.label13.Text = "Sec";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(264, 427);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 22);
            this.label1.TabIndex = 44;
            this.label1.Text = "Automatically Finish the Process After ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.groupBox6.Controls.Add(this.lbl_invsble_com2);
            this.groupBox6.Controls.Add(this.lbl_invsble_com1);
            this.groupBox6.Controls.Add(this.lbl_com_order);
            this.groupBox6.Controls.Add(this.lbl_comp_thanks);
            this.groupBox6.Controls.Add(this.pictureBox24);
            this.groupBox6.Controls.Add(this.lbl_com_confirm);
            this.groupBox6.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(113, 4);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(766, 388);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            // 
            // lbl_invsble_com2
            // 
            this.lbl_invsble_com2.AutoSize = true;
            this.lbl_invsble_com2.Location = new System.Drawing.Point(80, 174);
            this.lbl_invsble_com2.Name = "lbl_invsble_com2";
            this.lbl_invsble_com2.Size = new System.Drawing.Size(183, 16);
            this.lbl_invsble_com2.TabIndex = 3;
            this.lbl_invsble_com2.Text = "You are now registerd for Order";
            this.lbl_invsble_com2.Visible = false;
            // 
            // lbl_invsble_com1
            // 
            this.lbl_invsble_com1.AutoSize = true;
            this.lbl_invsble_com1.Location = new System.Drawing.Point(16, 174);
            this.lbl_invsble_com1.Name = "lbl_invsble_com1";
            this.lbl_invsble_com1.Size = new System.Drawing.Size(62, 16);
            this.lbl_invsble_com1.TabIndex = 2;
            this.lbl_invsble_com1.Text = "Welcome ";
            this.lbl_invsble_com1.Visible = false;
            // 
            // lbl_com_order
            // 
            this.lbl_com_order.Font = new System.Drawing.Font("Trebuchet MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_com_order.Location = new System.Drawing.Point(18, 15);
            this.lbl_com_order.Name = "lbl_com_order";
            this.lbl_com_order.Size = new System.Drawing.Size(721, 96);
            this.lbl_com_order.TabIndex = 0;
            this.lbl_com_order.Text = "Welcome Raja. Now your registration Completed";
            this.lbl_com_order.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_comp_thanks
            // 
            this.lbl_comp_thanks.AutoSize = true;
            this.lbl_comp_thanks.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_comp_thanks.Location = new System.Drawing.Point(240, 326);
            this.lbl_comp_thanks.Name = "lbl_comp_thanks";
            this.lbl_comp_thanks.Size = new System.Drawing.Size(294, 29);
            this.lbl_comp_thanks.TabIndex = 1;
            this.lbl_comp_thanks.Text = "Thank you for registration";
            this.lbl_comp_thanks.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.White;
            this.pictureBox24.Image = global::Orion_truck.Properties.Resources.complete;
            this.pictureBox24.Location = new System.Drawing.Point(302, 115);
            this.pictureBox24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(148, 110);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox24.TabIndex = 0;
            this.pictureBox24.TabStop = false;
            // 
            // lbl_com_confirm
            // 
            this.lbl_com_confirm.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_com_confirm.Location = new System.Drawing.Point(14, 239);
            this.lbl_com_confirm.Name = "lbl_com_confirm";
            this.lbl_com_confirm.Size = new System.Drawing.Size(725, 70);
            this.lbl_com_confirm.TabIndex = 0;
            this.lbl_com_confirm.Text = "You will soon receive a confirmation SMS. You will then receive a second SMS once" +
    " your order is ready with the  gate number.";
            this.lbl_com_confirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::Orion_truck.Properties.Resources.next_t1;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.lbl_finish);
            this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel5.Location = new System.Drawing.Point(836, 410);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(155, 82);
            this.panel5.TabIndex = 43;
            this.panel5.Click += new System.EventHandler(this.panel5_Click);
            // 
            // lbl_finish
            // 
            this.lbl_finish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_finish.BackColor = System.Drawing.Color.Transparent;
            this.lbl_finish.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_finish.ForeColor = System.Drawing.Color.White;
            this.lbl_finish.Location = new System.Drawing.Point(15, 23);
            this.lbl_finish.Name = "lbl_finish";
            this.lbl_finish.Size = new System.Drawing.Size(95, 31);
            this.lbl_finish.TabIndex = 15;
            this.lbl_finish.Text = "Finish";
            this.lbl_finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_finish.Click += new System.EventHandler(this.panel5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(566, 423);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 27);
            this.label2.TabIndex = 45;
            this.label2.Text = "10 Sec";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "fr.png");
            this.imageList1.Images.SetKeyName(1, "UK.png");
            this.imageList1.Images.SetKeyName(2, "ger.png");
            this.imageList1.Images.SetKeyName(3, "spa.png");
            this.imageList1.Images.SetKeyName(4, "por.png");
            this.imageList1.Images.SetKeyName(5, "italy.png");
            this.imageList1.Images.SetKeyName(6, "pol.png");
            this.imageList1.Images.SetKeyName(7, "Rom.png");
            this.imageList1.Images.SetKeyName(8, "Canada.png");
            this.imageList1.Images.SetKeyName(9, "Korea.png");
            this.imageList1.Images.SetKeyName(10, "Malays.png");
            this.imageList1.Images.SetKeyName(11, "greece.png");
            this.imageList1.Images.SetKeyName(12, "Russia.png");
            this.imageList1.Images.SetKeyName(13, "Aus.png");
            this.imageList1.Images.SetKeyName(14, "Czech.png");
            this.imageList1.Images.SetKeyName(15, "Estonia.png");
            this.imageList1.Images.SetKeyName(16, "Egypt.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "UK.png");
            this.imageList2.Images.SetKeyName(1, "fr.png");
            this.imageList2.Images.SetKeyName(2, "ger.png");
            this.imageList2.Images.SetKeyName(3, "ita.png");
            this.imageList2.Images.SetKeyName(4, "pol.png");
            this.imageList2.Images.SetKeyName(5, "por.png");
            this.imageList2.Images.SetKeyName(6, "Rom.png");
            this.imageList2.Images.SetKeyName(7, "spa.png");
            this.imageList2.Images.SetKeyName(8, "aus_140x90.png");
            this.imageList2.Images.SetKeyName(9, "canada_140x90.png");
            this.imageList2.Images.SetKeyName(10, "czech_140x90.png");
            this.imageList2.Images.SetKeyName(11, "Egypt_140x90.png");
            this.imageList2.Images.SetKeyName(12, "Estonia_140x90.png");
            this.imageList2.Images.SetKeyName(13, "greece_140x90.png");
            this.imageList2.Images.SetKeyName(14, "koria_140x90.png");
            this.imageList2.Images.SetKeyName(15, "malasia_140x90.png");
            this.imageList2.Images.SetKeyName(16, "russia_140x90.png");
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.panel4.Location = new System.Drawing.Point(0, 825);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1008, 0);
            this.panel4.TabIndex = 7;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // imageList_country
            // 
            this.imageList_country.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_country.ImageStream")));
            this.imageList_country.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_country.Images.SetKeyName(0, "ad.png");
            this.imageList_country.Images.SetKeyName(1, "ae.png");
            this.imageList_country.Images.SetKeyName(2, "af.png");
            this.imageList_country.Images.SetKeyName(3, "ag.png");
            this.imageList_country.Images.SetKeyName(4, "al.png");
            this.imageList_country.Images.SetKeyName(5, "am.png");
            this.imageList_country.Images.SetKeyName(6, "ao.png");
            this.imageList_country.Images.SetKeyName(7, "ar.png");
            this.imageList_country.Images.SetKeyName(8, "at.png");
            this.imageList_country.Images.SetKeyName(9, "au.png");
            this.imageList_country.Images.SetKeyName(10, "az.png");
            this.imageList_country.Images.SetKeyName(11, "ba.png");
            this.imageList_country.Images.SetKeyName(12, "bb.png");
            this.imageList_country.Images.SetKeyName(13, "bd.png");
            this.imageList_country.Images.SetKeyName(14, "be.png");
            this.imageList_country.Images.SetKeyName(15, "bf.png");
            this.imageList_country.Images.SetKeyName(16, "bg.png");
            this.imageList_country.Images.SetKeyName(17, "bh.png");
            this.imageList_country.Images.SetKeyName(18, "bi.png");
            this.imageList_country.Images.SetKeyName(19, "bj.png");
            this.imageList_country.Images.SetKeyName(20, "bn.png");
            this.imageList_country.Images.SetKeyName(21, "bo.png");
            this.imageList_country.Images.SetKeyName(22, "br.png");
            this.imageList_country.Images.SetKeyName(23, "bs.png");
            this.imageList_country.Images.SetKeyName(24, "bt.png");
            this.imageList_country.Images.SetKeyName(25, "bw.png");
            this.imageList_country.Images.SetKeyName(26, "by.png");
            this.imageList_country.Images.SetKeyName(27, "bz.png");
            this.imageList_country.Images.SetKeyName(28, "ca.png");
            this.imageList_country.Images.SetKeyName(29, "cd.png");
            this.imageList_country.Images.SetKeyName(30, "cf.png");
            this.imageList_country.Images.SetKeyName(31, "cg.png");
            this.imageList_country.Images.SetKeyName(32, "ch.png");
            this.imageList_country.Images.SetKeyName(33, "ci.png");
            this.imageList_country.Images.SetKeyName(34, "cl.png");
            this.imageList_country.Images.SetKeyName(35, "cm.png");
            this.imageList_country.Images.SetKeyName(36, "cn.png");
            this.imageList_country.Images.SetKeyName(37, "co.png");
            this.imageList_country.Images.SetKeyName(38, "cr.png");
            this.imageList_country.Images.SetKeyName(39, "cu.png");
            this.imageList_country.Images.SetKeyName(40, "cv.png");
            this.imageList_country.Images.SetKeyName(41, "cy.png");
            this.imageList_country.Images.SetKeyName(42, "cz.png");
            this.imageList_country.Images.SetKeyName(43, "de.png");
            this.imageList_country.Images.SetKeyName(44, "dj.png");
            this.imageList_country.Images.SetKeyName(45, "dk.png");
            this.imageList_country.Images.SetKeyName(46, "dm.png");
            this.imageList_country.Images.SetKeyName(47, "do.png");
            this.imageList_country.Images.SetKeyName(48, "dz.png");
            this.imageList_country.Images.SetKeyName(49, "ec.png");
            this.imageList_country.Images.SetKeyName(50, "ee.png");
            this.imageList_country.Images.SetKeyName(51, "eg.png");
            this.imageList_country.Images.SetKeyName(52, "eh.png");
            this.imageList_country.Images.SetKeyName(53, "er.png");
            this.imageList_country.Images.SetKeyName(54, "es.png");
            this.imageList_country.Images.SetKeyName(55, "et.png");
            this.imageList_country.Images.SetKeyName(56, "fi.png");
            this.imageList_country.Images.SetKeyName(57, "fj.png");
            this.imageList_country.Images.SetKeyName(58, "fm.png");
            this.imageList_country.Images.SetKeyName(59, "fr.png");
            this.imageList_country.Images.SetKeyName(60, "ga.png");
            this.imageList_country.Images.SetKeyName(61, "gb.png");
            this.imageList_country.Images.SetKeyName(62, "gd.png");
            this.imageList_country.Images.SetKeyName(63, "ge.png");
            this.imageList_country.Images.SetKeyName(64, "gh.png");
            this.imageList_country.Images.SetKeyName(65, "gm.png");
            this.imageList_country.Images.SetKeyName(66, "gn.png");
            this.imageList_country.Images.SetKeyName(67, "gq.png");
            this.imageList_country.Images.SetKeyName(68, "gr.png");
            this.imageList_country.Images.SetKeyName(69, "gt.png");
            this.imageList_country.Images.SetKeyName(70, "gw.png");
            this.imageList_country.Images.SetKeyName(71, "gy.png");
            this.imageList_country.Images.SetKeyName(72, "hn.png");
            this.imageList_country.Images.SetKeyName(73, "hr.png");
            this.imageList_country.Images.SetKeyName(74, "ht.png");
            this.imageList_country.Images.SetKeyName(75, "hu.png");
            this.imageList_country.Images.SetKeyName(76, "id.png");
            this.imageList_country.Images.SetKeyName(77, "ie.png");
            this.imageList_country.Images.SetKeyName(78, "il.png");
            this.imageList_country.Images.SetKeyName(79, "in.png");
            this.imageList_country.Images.SetKeyName(80, "iq.png");
            this.imageList_country.Images.SetKeyName(81, "ir.png");
            this.imageList_country.Images.SetKeyName(82, "is.png");
            this.imageList_country.Images.SetKeyName(83, "it.png");
            this.imageList_country.Images.SetKeyName(84, "jm.png");
            this.imageList_country.Images.SetKeyName(85, "jo.png");
            this.imageList_country.Images.SetKeyName(86, "jp.png");
            this.imageList_country.Images.SetKeyName(87, "ke.png");
            this.imageList_country.Images.SetKeyName(88, "kg.png");
            this.imageList_country.Images.SetKeyName(89, "kh.png");
            this.imageList_country.Images.SetKeyName(90, "ki.png");
            this.imageList_country.Images.SetKeyName(91, "km.png");
            this.imageList_country.Images.SetKeyName(92, "kn.png");
            this.imageList_country.Images.SetKeyName(93, "kp.png");
            this.imageList_country.Images.SetKeyName(94, "kr.png");
            this.imageList_country.Images.SetKeyName(95, "kw.png");
            this.imageList_country.Images.SetKeyName(96, "kz.png");
            this.imageList_country.Images.SetKeyName(97, "la.png");
            this.imageList_country.Images.SetKeyName(98, "lb.png");
            this.imageList_country.Images.SetKeyName(99, "lc.png");
            this.imageList_country.Images.SetKeyName(100, "li.png");
            this.imageList_country.Images.SetKeyName(101, "lk.png");
            this.imageList_country.Images.SetKeyName(102, "lr.png");
            this.imageList_country.Images.SetKeyName(103, "ls.png");
            this.imageList_country.Images.SetKeyName(104, "lt.png");
            this.imageList_country.Images.SetKeyName(105, "lu.png");
            this.imageList_country.Images.SetKeyName(106, "lv.png");
            this.imageList_country.Images.SetKeyName(107, "ly.png");
            this.imageList_country.Images.SetKeyName(108, "ma.png");
            this.imageList_country.Images.SetKeyName(109, "mc.png");
            this.imageList_country.Images.SetKeyName(110, "md.png");
            this.imageList_country.Images.SetKeyName(111, "me.png");
            this.imageList_country.Images.SetKeyName(112, "mg.png");
            this.imageList_country.Images.SetKeyName(113, "mh.png");
            this.imageList_country.Images.SetKeyName(114, "mk.png");
            this.imageList_country.Images.SetKeyName(115, "ml.png");
            this.imageList_country.Images.SetKeyName(116, "mm.png");
            this.imageList_country.Images.SetKeyName(117, "mn.png");
            this.imageList_country.Images.SetKeyName(118, "mr.png");
            this.imageList_country.Images.SetKeyName(119, "mt.png");
            this.imageList_country.Images.SetKeyName(120, "mu.png");
            this.imageList_country.Images.SetKeyName(121, "mv.png");
            this.imageList_country.Images.SetKeyName(122, "mw.png");
            this.imageList_country.Images.SetKeyName(123, "mx.png");
            this.imageList_country.Images.SetKeyName(124, "my.png");
            this.imageList_country.Images.SetKeyName(125, "mz.png");
            this.imageList_country.Images.SetKeyName(126, "na.png");
            this.imageList_country.Images.SetKeyName(127, "ne.png");
            this.imageList_country.Images.SetKeyName(128, "ng.png");
            this.imageList_country.Images.SetKeyName(129, "ni.png");
            this.imageList_country.Images.SetKeyName(130, "nl.png");
            this.imageList_country.Images.SetKeyName(131, "no.png");
            this.imageList_country.Images.SetKeyName(132, "np.png");
            this.imageList_country.Images.SetKeyName(133, "nr.png");
            this.imageList_country.Images.SetKeyName(134, "nz.png");
            this.imageList_country.Images.SetKeyName(135, "om.png");
            this.imageList_country.Images.SetKeyName(136, "pa.png");
            this.imageList_country.Images.SetKeyName(137, "pe.png");
            this.imageList_country.Images.SetKeyName(138, "pg.png");
            this.imageList_country.Images.SetKeyName(139, "ph.png");
            this.imageList_country.Images.SetKeyName(140, "pk.png");
            this.imageList_country.Images.SetKeyName(141, "pl.png");
            this.imageList_country.Images.SetKeyName(142, "pt.png");
            this.imageList_country.Images.SetKeyName(143, "pw.png");
            this.imageList_country.Images.SetKeyName(144, "py.png");
            this.imageList_country.Images.SetKeyName(145, "qa.png");
            this.imageList_country.Images.SetKeyName(146, "ro.png");
            this.imageList_country.Images.SetKeyName(147, "rs.png");
            this.imageList_country.Images.SetKeyName(148, "ru.png");
            this.imageList_country.Images.SetKeyName(149, "rw.png");
            this.imageList_country.Images.SetKeyName(150, "sa.png");
            this.imageList_country.Images.SetKeyName(151, "sb.png");
            this.imageList_country.Images.SetKeyName(152, "sc.png");
            this.imageList_country.Images.SetKeyName(153, "sd.png");
            this.imageList_country.Images.SetKeyName(154, "se.png");
            this.imageList_country.Images.SetKeyName(155, "sg.png");
            this.imageList_country.Images.SetKeyName(156, "si.png");
            this.imageList_country.Images.SetKeyName(157, "sk.png");
            this.imageList_country.Images.SetKeyName(158, "sl.png");
            this.imageList_country.Images.SetKeyName(159, "sm.png");
            this.imageList_country.Images.SetKeyName(160, "sn.png");
            this.imageList_country.Images.SetKeyName(161, "so.png");
            this.imageList_country.Images.SetKeyName(162, "sr.png");
            this.imageList_country.Images.SetKeyName(163, "st.png");
            this.imageList_country.Images.SetKeyName(164, "sv.png");
            this.imageList_country.Images.SetKeyName(165, "sy.png");
            this.imageList_country.Images.SetKeyName(166, "sz.png");
            this.imageList_country.Images.SetKeyName(167, "td.png");
            this.imageList_country.Images.SetKeyName(168, "tg.png");
            this.imageList_country.Images.SetKeyName(169, "th.png");
            this.imageList_country.Images.SetKeyName(170, "tj.png");
            this.imageList_country.Images.SetKeyName(171, "tl.png");
            this.imageList_country.Images.SetKeyName(172, "tm.png");
            this.imageList_country.Images.SetKeyName(173, "tn.png");
            this.imageList_country.Images.SetKeyName(174, "to.png");
            this.imageList_country.Images.SetKeyName(175, "tr.png");
            this.imageList_country.Images.SetKeyName(176, "tt.png");
            this.imageList_country.Images.SetKeyName(177, "tv.png");
            this.imageList_country.Images.SetKeyName(178, "tw.png");
            this.imageList_country.Images.SetKeyName(179, "tz.png");
            this.imageList_country.Images.SetKeyName(180, "ua.png");
            this.imageList_country.Images.SetKeyName(181, "ug.png");
            this.imageList_country.Images.SetKeyName(182, "us.png");
            this.imageList_country.Images.SetKeyName(183, "uy.png");
            this.imageList_country.Images.SetKeyName(184, "uz.png");
            this.imageList_country.Images.SetKeyName(185, "va.png");
            this.imageList_country.Images.SetKeyName(186, "vc.png");
            this.imageList_country.Images.SetKeyName(187, "ve.png");
            this.imageList_country.Images.SetKeyName(188, "vn.png");
            this.imageList_country.Images.SetKeyName(189, "vu.png");
            this.imageList_country.Images.SetKeyName(190, "ws.png");
            this.imageList_country.Images.SetKeyName(191, "ye.png");
            this.imageList_country.Images.SetKeyName(192, "za.png");
            this.imageList_country.Images.SetKeyName(193, "zm.png");
            this.imageList_country.Images.SetKeyName(194, "zw.png");
            // 
            // Truckinterface_v1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(245)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(1008, 653);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Truckinterface_v1";
            this.Text = "Truckinterface_v1";
            this.Load += new System.EventHandler(this.FrmTruckinterface_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pan_truck.ResumeLayout(false);
            this.pan_comp.ResumeLayout(false);
            this.pan_info.ResumeLayout(false);
            this.pan_order.ResumeLayout(false);
            this.pan_del.ResumeLayout(false);
            this.pan_lang.ResumeLayout(false);
            this.tabcontrol1.ResumeLayout(false);
            this.tab_lang.ResumeLayout(false);
            this.tab_lang.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tab_trucktype.ResumeLayout(false);
            this.tab_trucktype.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_Container)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_inloader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_other)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trktype_euro)).EndInit();
            this.next_truck.ResumeLayout(false);
            this.back_trk.ResumeLayout(false);
            this.tab_truckin.ResumeLayout(false);
            this.tab_truckin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.next_truckin.ResumeLayout(false);
            this.back_truckin.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_in)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_trkin4)).EndInit();
            this.tab_po.ResumeLayout(false);
            this.tab_po.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_clear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.next_deli.ResumeLayout(false);
            this.back_deli.ResumeLayout(false);
            this.pan_truckin_still.ResumeLayout(false);
            this.pan_truckin_still.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chk_nonsgstill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chk_sgstill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pan_truckin_po.ResumeLayout(false);
            this.pan_truckin_po.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_chkbox_cusReturn)).EndInit();
            this.pan_truckin_cullet.ResumeLayout(false);
            this.pan_truckin_cullet.PerformLayout();
            this.tab_truckout.ResumeLayout(false);
            this.tab_truckout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.next_trkout.ResumeLayout(false);
            this.back_truckout.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_out)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_out_4)).EndInit();
            this.tab_so.ResumeLayout(false);
            this.tab_so.PerformLayout();
            this.panel_still.ResumeLayout(false);
            this.panel_still.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_NonsoSGStill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_soSGStill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.next_so.ResumeLayout(false);
            this.back_so.ResumeLayout(false);
            this.tab_trkinfo.ResumeLayout(false);
            this.tab_trkinfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.back_trkinfo1.ResumeLayout(false);
            this.next_trkinfo1.ResumeLayout(false);
            this.tab_trukinfo2.ResumeLayout(false);
            this.tab_trukinfo2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_getCountryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbflg1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.back_trkinfo2.ResumeLayout(false);
            this.next_trkinfo2.ResumeLayout(false);
            this.tab_comp.ResumeLayout(false);
            this.tab_comp.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabcontrol1;
        private System.Windows.Forms.TabPage tab_lang;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lbl_lang_otrlnag_nor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lb_invsble_sltd_nor;
        private System.Windows.Forms.Label lbl_lang_rsnsel_nor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lbl_slt_lang_nor;
        private System.Windows.Forms.TabPage tab_trucktype;
        private System.Windows.Forms.PictureBox pb_trktype_inloader;
        private System.Windows.Forms.PictureBox pb_trktype_Container;
        private System.Windows.Forms.PictureBox pb_trktype_euro;
        private System.Windows.Forms.PictureBox pb_trktype_other;
        private System.Windows.Forms.Label lbl_trktype_Inloader;
        private System.Windows.Forms.Label lbl_trktype_euro;
        private System.Windows.Forms.Label lbl_trktype_Container;
        private System.Windows.Forms.Label lbl_trktype_other;
        private System.Windows.Forms.Label lbl_invsble_slttruck;
        private System.Windows.Forms.Panel pan_slt_inloader;
        private System.Windows.Forms.Panel pan_sltd_container;
        private System.Windows.Forms.Panel pan_sltd_euro;
        private System.Windows.Forms.Panel pan_sltd_other;
        private System.Windows.Forms.TabPage tab_truckin;
        private System.Windows.Forms.PictureBox pb_trkin2;
        private System.Windows.Forms.PictureBox pb_trkin1;
        private System.Windows.Forms.PictureBox pb_trkin3;
        private System.Windows.Forms.PictureBox pb_trkin4;
        private System.Windows.Forms.PictureBox pb_trkin5;
        private System.Windows.Forms.Panel pan_trkin1;
        private System.Windows.Forms.Panel pan_trkin2;
        private System.Windows.Forms.Panel pan_trkin3;
        private System.Windows.Forms.Panel pan_trkin4;
        private System.Windows.Forms.Panel pan_trkin5;
        private System.Windows.Forms.Label lbl_trkin1;
        private System.Windows.Forms.Label lbl_trkin2;
        private System.Windows.Forms.Label lbl_trkin3;
        private System.Windows.Forms.Label lbl_trkin4;
        private System.Windows.Forms.Label lbl_trkin5;
        private System.Windows.Forms.Label lbl_other;
        private System.Windows.Forms.Label lbl_cullet;
        private System.Windows.Forms.Label lbl_still;
        private System.Windows.Forms.Label lbl_glass;
        private System.Windows.Forms.Label lbl_empty;
        private System.Windows.Forms.Label lbl_invsble_trkinslt;
        private System.Windows.Forms.TabPage tab_po;
        private System.Windows.Forms.TabPage tab_truckout;
        private System.Windows.Forms.TabPage tab_so;
        private System.Windows.Forms.TabPage tab_trkinfo;
        private System.Windows.Forms.TabPage tab_trukinfo2;
        private System.Windows.Forms.TabPage tab_comp;
        private System.Windows.Forms.Label lbl_comp_thanks;
        private System.Windows.Forms.Label lbl_com_order;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Label lbl_com_confirm;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Panel pan_lang;
        private System.Windows.Forms.Label lbl_pro_sltlang;
        private System.Windows.Forms.Panel pan_truck;
        private System.Windows.Forms.Label lbl_pro_sltTruck;
        private System.Windows.Forms.Panel pan_del;
        private System.Windows.Forms.Label lbl_pro_deli;
        private System.Windows.Forms.Panel pan_order;
        private System.Windows.Forms.Label lbl_pro_order;
        private System.Windows.Forms.Panel pan_info;
        private System.Windows.Forms.Label lbl_pro_info;
        private System.Windows.Forms.Panel pan_comp;
        private System.Windows.Forms.Label lbl_pro_complete;
        private System.Windows.Forms.Label lbl_pro_sltlang1;
        private System.Windows.Forms.Panel next_truck;
        private System.Windows.Forms.Label lbl_trk_next;
        private System.Windows.Forms.Panel back_trk;
        private System.Windows.Forms.Label lbl_trk_back;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_head;
        private System.Windows.Forms.Label lbl_slt_truck;
        private System.Windows.Forms.Panel back_truckin;
        private System.Windows.Forms.Label lbl_b_trkin;
        private System.Windows.Forms.Label lbl_slt_truckin;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pb_in;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel next_truckin;
        private System.Windows.Forms.Label lbl_n_trkin;
        private System.Windows.Forms.Panel next_deli;
        private System.Windows.Forms.Label lbl_n_po;
        private System.Windows.Forms.Panel back_deli;
        private System.Windows.Forms.Label lbl_b_po;
        private System.Windows.Forms.Panel next_so;
        private System.Windows.Forms.Label lbl_n_so;
        private System.Windows.Forms.Panel back_so;
        private System.Windows.Forms.Label lbl_b_so;
        private System.Windows.Forms.Panel next_trkinfo1;
        private System.Windows.Forms.Label lbl_n_trkinfo1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lbl_title_lang;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label lbl_titlt_truck;
        private System.Windows.Forms.Label lbl_titlt_truckin_goods;
        private System.Windows.Forms.Label lbl_so;
        private System.Windows.Forms.Label lbl_exso;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel next_trkinfo2;
        private System.Windows.Forms.Label lbl_trkinfo2;
        private System.Windows.Forms.Label lbl_title_deli;
        private System.Windows.Forms.Label lbl_title_out;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel pan_out1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel pan_out2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbl_out1;
        private System.Windows.Forms.Panel pan_out3;
        private System.Windows.Forms.Label lbl_out2;
        private System.Windows.Forms.Panel pan_out4;
        private System.Windows.Forms.Panel pan_out5;
        private System.Windows.Forms.Label lbl_out3;
        private System.Windows.Forms.Label lbl_out4;
        private System.Windows.Forms.Panel back_truckout;
        private System.Windows.Forms.Label lbl_b_trkout;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.PictureBox pb_out;
        private System.Windows.Forms.PictureBox pb_out_2;
        private System.Windows.Forms.PictureBox pb_out_1;
        private System.Windows.Forms.PictureBox pb_out_3;
        private System.Windows.Forms.PictureBox pb_out_5;
        private System.Windows.Forms.PictureBox pb_out_4;
        private System.Windows.Forms.Label lbl_title_returnorder;
        private System.Windows.Forms.Label lbl_tit_info_name;
        private System.Windows.Forms.Label lbl_title_info;
        private System.Windows.Forms.Panel next_trkout;
        private System.Windows.Forms.Label lbl_n_trkout;
        private System.Windows.Forms.Panel back_trkinfo1;
        private System.Windows.Forms.Label lbl_b_trkinfo1;
        private System.Windows.Forms.Panel back_trkinfo2;
        private System.Windows.Forms.Label lbl_b_trkinfor2;
        private System.Windows.Forms.Label lbl_out5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbl_invsble_com2;
        private System.Windows.Forms.Label lbl_invsble_com1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_finish;
        private System.Windows.Forms.Label lbl_titlt_truckin_empty;
        private System.Windows.Forms.Label lbl_titlt_truckin_or;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel groupBox2;
        private System.Windows.Forms.Panel pan_truckin_po;
        private System.Windows.Forms.Label lbl_cul;
        private System.Windows.Forms.Label lbl_excul;
        private System.Windows.Forms.Label lbl_pono;
        private System.Windows.Forms.Label lbl_expo;
        private System.Windows.Forms.Panel groupBox5;
        private System.Windows.Forms.Label lbl_contain;
        private System.Windows.Forms.Label lbl_excontain;
        private System.Windows.Forms.Label lbl_trkinfo_truckno;
        private System.Windows.Forms.Label lbl_trkinfo_extruckno;
        private System.Windows.Forms.Label lbl_trkinfo_trailrno;
        private System.Windows.Forms.Label lbl_trkinfo_extrailrno;
        private System.Windows.Forms.Panel groupBox3;
        private System.Windows.Forms.Label lbl_trkinfo_exname;
        private System.Windows.Forms.Label lbl_trkinfo_mobno;
        private System.Windows.Forms.Label lbl_trkinfo_name;
        private System.Windows.Forms.Label lbl_trkinfo_exmobno;
        private System.Windows.Forms.Label lbl_trkin_sltgoods;
        private System.Windows.Forms.Label lbl_trkout_sltgoods;
        private System.Windows.Forms.PictureBox pbflg1;
        private System.Windows.Forms.ImageList imageList_country;
        private System.Windows.Forms.TextBox txt_po;
        private System.Windows.Forms.TextBox txt_cul;
        private System.Windows.Forms.TextBox txt_so;
        private System.Windows.Forms.TextBox txt_contain;
        private System.Windows.Forms.TextBox txt_trilerno;
        private System.Windows.Forms.TextBox txt_truckno;
        private System.Windows.Forms.TextBox txt_mobno;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Panel pan_truckin_still;
        private System.Windows.Forms.Label lbl_sg_still;
        private System.Windows.Forms.TextBox txt_still;
        private System.Windows.Forms.Label lbl_del_stil;
        private System.Windows.Forms.PictureBox pb_chk_nonsgstill;
        private System.Windows.Forms.Label lbl_exstill;
        private System.Windows.Forms.PictureBox pb_chk_sgstill;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label lbl_nonsg_still;
        private System.Windows.Forms.Panel pan_truckin_cullet;
        private System.Windows.Forms.Label lbl_wood;
        private System.Windows.Forms.Label lbl_packing;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_stillcullt;
        private System.Windows.Forms.Label lbl_top_no;
        private System.Windows.Forms.Label lbl_top_country;
        private System.Windows.Forms.PictureBox pb_home;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pb_clear;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cmb_country;
        private System.Windows.Forms.Panel panel_still;
        private System.Windows.Forms.Label lbl_SOSgStilltxt;
        private System.Windows.Forms.TextBox txt_so_Still;
        private System.Windows.Forms.Label lbl_SOSgStill;
        private System.Windows.Forms.PictureBox pb_NonsoSGStill;
        private System.Windows.Forms.Label lbl_SOSgStillex;
        private System.Windows.Forms.PictureBox pb_soSGStill;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Label lbl_SOnonSgStill;
        private System.Windows.Forms.Label lbl_titlt_truckineuro_empty;
        private System.Windows.Forms.Label lbl_titlt_truckineuro_goods;
        private System.Windows.Forms.Label lbl_trukin2_title;
        private System.Windows.Forms.Label lbl_truckin1_title;
        private System.Windows.Forms.TextBox txtSupp;
        private System.Windows.Forms.Label lblSupp;
        private System.Windows.Forms.Label lblSupEX;
        private System.Windows.Forms.TextBox txtNoOfBags;
        private System.Windows.Forms.Label lblNoOfBags;
        private System.Windows.Forms.Label lblNoOfBagsEx;
        private System.Windows.Forms.Label lbl_CustReturn;
        private System.Windows.Forms.PictureBox pb_chkbox_cusReturn;
        private System.Windows.Forms.TextBox txtCustR_SO;
        private System.Windows.Forms.Label lblCustR_SO;
        private System.Windows.Forms.Label lblCustR_SOEX;
        private System.Windows.Forms.Label lblCountryCodeTit;
        private System.Windows.Forms.Label lblCountryCodeTitEx;
        private System.Windows.Forms.TextBox txt_countrycodeDis;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pb_getCountryCode;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label13;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txt_Taraweight;
        private System.Windows.Forms.Label lbl_Taraweight;
        private System.Windows.Forms.Label lbl_TaraweightEX;
    }
}