﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Orion_truck.Entity;
using Orion_truck.Control;
using System.Windows.Forms;

namespace Orion_truck.Model
{
   public class MessageValidation_Model
    {
       private MessageValidation_Control _usercontrol;

       public MessageValidation_Model()
        {
            _usercontrol = new MessageValidation_Control();
        }

       public MessageValidation_Entity Alert_Information(string _app, string _err, string _lang)
       {

           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.Alert_Info(_app, _err, _lang);

           foreach (DataRow dr in dataTable.Rows)
           { 
               userVO.alertmessage = dr["L_ERR"].ToString();
               userVO.alerttitle = dr["C_MSGTYPE"].ToString() + " - " + dr["C_ERR"].ToString();
           }
           return userVO;
       }

       

       public DataTable F_PRODUCT_EXIST_PROD(string PC_CPROD)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_PRODUCT_EXIST_PROD(PC_CPROD); 
           return dataTable;
       }

       public DataTable F_PRODUCT_EXIST_PRODL(string PC_CPROD)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_PRODUCT_EXIST_PRODL(PC_CPROD);
           return dataTable;
       }

       public DataTable F_CDC_EXIST_CDCQ(string PC_CDC)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_CDC_EXIST_CDCQ(PC_CDC);
           return dataTable;
       }

       public DataTable F_CDC_EXIST_CDCQL(string PC_CDC)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_CDC_EXIST_CDCQL(PC_CDC);
           return dataTable;
       }

       public DataTable F_CODE_TABLG_VALIDIFIER(string code, string table)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_CODE_TABLG_VALIDIFIER(code, table);
           return dataTable;
       }

       public DataTable F_CODE_TABLGL_VALIDIFIER(string code, string table)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_CODE_TABLGL_VALIDIFIER(code, table);
           return dataTable;
       }

       public DataTable P_CTRL_EMPL(string bay, string rack)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.P_CTRL_EMPL(bay, rack);
           return dataTable;
       }

       public DataTable P_CTRL_MAGA(string bay)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.P_CTRL_MAGA(bay);
           return dataTable;
       }

       public DataTable P_ARTICLE_SAP_CONTROLER(string cprod, string cdcr, string cpile, string cembl, string ctmes, int vlon, int vlar, string cintv, string cintp, string vtrait, string vmarq, string cstri)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.P_ARTICLE_SAP_CONTROLER(cprod, cdcr, cpile, cembl, ctmes,vlon,vlar, cintv, cintp, vtrait, vmarq, cstri);
           return dataTable;
       }

       public DataTable F_Motif_Existe_Indc(string _cmotif)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_Motif_Existe_Indc(_cmotif);
           return dataTable;
       }

       public DataTable F_Motif_Existe_Indcl(string _cmotif)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_Motif_Existe_Indcl(_cmotif);
           return dataTable;
       }

       public DataTable F_STT_FICHE_STOCKER(string _fich)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_STT_FICHE_STOCKER(_fich);
           return dataTable;
       }

       public DataTable F_CUTTING_ORDER_SETTLEMENT(string _fich)
       {
           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.F_CUTTING_ORDER_SETTLEMENT(_fich);
           return dataTable;
       }

       public MessageValidation_Entity Message_Info(string _actcode, string _ord, string _seq, string _lang)
       {

           MessageValidation_Entity userVO = new MessageValidation_Entity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.Message_Info(_actcode, _ord,_seq,_lang);

           foreach (DataRow dr in dataTable.Rows)
           {
               userVO.alertmessage = dr["P_ACTIDESC"].ToString();
           }
           return userVO;
       }

    }
}
