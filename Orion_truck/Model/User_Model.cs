﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Orion_truck.Control;
using Orion_truck.Entity;

namespace Orion_truck.Model
{
    public class User_Model
    {
        private User_Control _userControl;
        public bool status;
        public int langordid;

        /// <constructor>
        /// Constructor UserBUS
        /// </constructor>
        public User_Model()
        {
            _userControl = new User_Control();
        }

        /// <method>
        /// Get User Email By Firstname or Lastname and return VO
        /// </method>
        //public DataTable getUserEmailByName(string name)
        //{
        //    User_Entity userEntity = new User_Entity();
        //    DataTable dataTable = new DataTable();

        //    dataTable = _userControl.searchByName(name);

        //    foreach (DataRow dr in dataTable.Rows)
        //    {
        //        userEntity._username = dr["NOM_UTI"].ToString();
        //        userEntity._group = dr["C_UTI"].ToString();
        //        userEntity._term = dr["C_TERM"].ToString();
        //        userEntity._application = dr["C_APP"].ToString();
        //    }
        //    return dataTable;
        //}

        //Validate the login users
        public User_Entity getValidateLoginUser(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUser(username, password);
            userEntity.loginstatus = 2;
            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
                userEntity.cparam = dr["C_PARM"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
                userEntity.site = dr["C_LIEU"].ToString();
                userEntity.form = dr["TY_OUTIL"].ToString();
                if (dr["LOGIN_STATUS"] != null || dr["LOGIN_STATUS"].ToString() != "")
                {
                    userEntity.loginstatus = Convert.ToInt32(dr["LOGIN_STATUS"].ToString());
                }
                else
                {
                    userEntity.loginstatus = 1;
                }
            }
            return userEntity;
        }

        public string getCurrLnag(string lang)
        {

            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();
            dataTable = _userControl.getCurrLnag(lang);

            foreach (DataRow dr in dataTable.Rows)
            {
                //userEntity.currlang = dr["C_LNGCODE"].ToString();
                lang = dr["C_LNGCODE"].ToString();
            }
            return lang;
        }

        public int getCurrLnagOrdID(string lang)
        {

            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();
            dataTable = _userControl.getCurrLnagOrdID(lang);

            foreach (DataRow dr in dataTable.Rows)
            {
                //userEntity.currlang = dr["C_LNGCODE"].ToString();
                langordid = Convert.ToInt32(dr["C_IMGORD"]);
            }
            return langordid;
        }

        //Bind the language combobox here
        public DataTable getBindLanguage()
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getBindLanguage();

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["Name"].ToString();
                userEntity.languagevalue = dr["C_LANG"].ToString();
            }
            return dataTable;
        }
        //Bind the language combobox here
        public DataTable getBindInFormLanguage(string lang)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getBindInFormLanguage(lang);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["Name"].ToString();
                userEntity.languagevalue = dr["C_LNGCODE"].ToString();
            }
            return dataTable;
        }


        //Bind the location combobox here
        public DataTable getBindLocation()
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getBindLocation();

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["CMPDESC"].ToString();
                userEntity.languagevalue = dr["CMPCODE"].ToString();
            }
            return dataTable;
        }

        ////Bind the process type combobox here
        //public DataTable getBindProcessType(string location)
        //{
        //    User_Entity userEntity = new User_Entity();
        //    DataTable dataTable = new DataTable();

        //    dataTable = _userControl.getBindProcessType(location);

        //    foreach (DataRow dr in dataTable.Rows)
        //    {
        //        userEntity.language = dr["C_SITEL"].ToString();
        //        userEntity.languagevalue = dr["C_SITE"].ToString();
        //    }
        //    return dataTable;
        //}

        //Bind the process type combobox here
        public DataTable getBindProcessType(string location)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getBindProcessType(location);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["C_SITEL"].ToString();
                userEntity.languagevalue = dr["C_SITE"].ToString();
            }
            return dataTable;
        }

        //Validate the login users
        public User_Entity getValidateLoginUserOnly(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserOnly(username);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
            }
            return userEntity;
        }

        //show current menu clicked form
        public User_Entity getCurrentMenuForm(string username,string strLang)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getCurrentMenuForm(username, strLang);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.form = dr["FRMNAME"].ToString();
                userEntity.transname = dr["TRANSNAME"].ToString();
            }
            return userEntity;
        }
     
        //Validate the login users for password failed attempt
        public User_Entity getValidateLoginUserOnly(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserOnly(username, password);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
            }
            return userEntity;
        }

        //Validate the login users for password failed attempt
        public User_Entity getValidateLoginUserOnlyNew(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserOnlyNew(username);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.domainname = dr["C_DOMAINAME"].ToString();
            }
            return userEntity;
        }

        //Validate the language
        public User_Entity getValidateLanguage(string language)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLanguage(language);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["LANGL"].ToString();
            }
            return userEntity;
        }

        //Validate the language
        public User_Entity getValidateLocation(string location)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLocation(location);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["CMPDESC"].ToString();
            }
            return userEntity;
        }

        //Validate the language
        public User_Entity getValidateProcessType(string location, string csitel)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateProcessType(location, csitel);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["C_SITEL"].ToString();
            }
            return userEntity;
        }

        //GET THE CURRENT CONNECTION
        public User_Entity getCurrentConnection(string location, string csitel)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getCurrentConnection(location, csitel);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.connection = dr["DBSOURCE"].ToString();
            }
            return userEntity;
        }

        //Update the wrong password count to table
        public bool getUpdateWrongPwd(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getUpdateWrongPwd(username, password);

            //foreach (DataRow dr in dataTable.Rows)
            //{
            //    userEntity.username = dr["NOM_UTI"].ToString();
            //    userEntity.group = dr["C_UTI"].ToString();
            //    userEntity.term = dr["C_TERM"].ToString();
            //    userEntity.application = dr["C_APP"].ToString();
            //}
            return userEntity.flag;
        }

        //Update the new password
        public bool getUpdateNewPwd(string username, string newpwd,string _creaby)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getUpdateNewPwd(username, newpwd, _creaby);

            return userEntity.flag;
        }

        //Update the wrong password count to table
        public bool getAccountLock(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getAccountLock(username);

            //foreach (DataRow dr in dataTable.Rows)
            //{
            //    userEntity.username = dr["NOM_UTI"].ToString();
            //    userEntity.group = dr["C_UTI"].ToString();
            //    userEntity.term = dr["C_TERM"].ToString();
            //    userEntity.application = dr["C_APP"].ToString();
            //}
            return userEntity.flag;
        }

        //Validate the login users using sgid
        public User_Entity getValidateLoginUserSGID(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserSGID(username, password);
            userEntity.loginstatus = 2;
            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
                userEntity.cparam = dr["C_PARM"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
                userEntity.site = dr["C_LIEU"].ToString();
                userEntity.form = dr["TY_OUTIL"].ToString();
                if (dr["LOGIN_STATUS"] != null || dr["LOGIN_STATUS"].ToString() != "")
                {
                    userEntity.loginstatus = Convert.ToInt32(dr["LOGIN_STATUS"].ToString());
                }
                else
                {
                    userEntity.loginstatus = 1;
                }

                //userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.group = dr["C_UTI"].ToString();
                //userEntity.term = dr["C_TERM"].ToString();
                //userEntity.application = dr["C_APP"].ToString();
            }
            return userEntity;
        }

        //Validate the login users using sgid
        public User_Entity getValidateLoginUserSGIDNEW(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserSGIDNEW(username);
            userEntity.loginstatus = 2;
            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
                userEntity.cparam = dr["C_PARM"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
                userEntity.site = dr["C_LIEU"].ToString();
                userEntity.form = dr["TY_OUTIL"].ToString();
                if (dr["LOGIN_STATUS"] != null || dr["LOGIN_STATUS"].ToString() != "")
                {
                    userEntity.loginstatus = Convert.ToInt32(dr["LOGIN_STATUS"].ToString());
                }
                else
                {
                    userEntity.loginstatus = 1;
                }

                //userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.group = dr["C_UTI"].ToString();
                //userEntity.term = dr["C_TERM"].ToString();
                //userEntity.application = dr["C_APP"].ToString();
            }
            return userEntity;
        }

        //Validate the login users using sgid
        public User_Entity getValidateEmpMaster(string sgid)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateEmpMaster(sgid);
            userEntity.loginstatus = 2;
            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["EMP_EMPNO"].ToString();
                //userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.group = dr["C_UTI"].ToString();
                //userEntity.term = dr["C_TERM"].ToString();
                //userEntity.application = dr["C_APP"].ToString();
                //userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                //userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
                //userEntity.cparam = dr["C_PARM"].ToString();
                //userEntity.sgid = dr["NOM_SGID"].ToString();
                //userEntity.site = dr["C_LIEU"].ToString();
                //userEntity.form = dr["TY_OUTIL"].ToString();
                //if (dr["LOGIN_STATUS"] != null || dr["LOGIN_STATUS"].ToString() != "")
                //{
                //    userEntity.loginstatus = Convert.ToInt32(dr["LOGIN_STATUS"].ToString());
                //}
                //else
                //{
                userEntity.loginstatus = 1;
                //}

                //userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.group = dr["C_UTI"].ToString();
                //userEntity.term = dr["C_TERM"].ToString();
                //userEntity.application = dr["C_APP"].ToString();
            }
            return userEntity;
        }

        //Validate the multi user login
        public User_Entity getValidateMultiUserLogin(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateMultiUserLogin(username);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.noofusers = Convert.ToInt32(dr["no_of_usercount"]);
                userEntity.processid = dr["SP_ID"].ToString();
                userEntity.machinename = dr["MACHINE_NAME"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
            }
            return userEntity;
        }

        //Insert login users to table
        public bool getInsertLoginUsers(string username, string sgid, string machinename, string appversion)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            status = _userControl.getInsertLoginUsers(username, sgid, machinename, appversion);

            return status;
        }

        //Delete login users to table
        public bool getDeleteLoginUsers(string username, string sgid)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            status = _userControl.getDeleteLoginUsers(username, sgid);

            return status;
        }

        //Delete login users to table
        public bool getDeleteAvlUsers(string username, string machinename, string processid)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            status = _userControl.getDeleteAvlUsers(username, machinename, processid);

            return status;
        }

        //Bind controls based on language
        public DataSet getBindControlsBaseLanguage(string language, string formname)
        {
            User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getBindControlsBaseLanguage(language, formname);

            foreach (DataRow dr in dataset.Tables[0].Rows)
            {
                userEntity.formname = dr["frmname"].ToString();
                userEntity.formcontrol = dr["frmctrl"].ToString();
                userEntity.formcontrolid = dr["frmctrlid"].ToString();
                userEntity.formcontrolname = dr["frmctlname"].ToString();
                userEntity.language = dr["langname"].ToString();
            }
            return dataset;
        }

        //Bind controls based on language
        public DataSet getValidateAvlUser(string userid, string sgid)
        {
            User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getValidateAvlUser(userid, sgid);

            foreach (DataRow dr in dataset.Tables[0].Rows)
            {
                userEntity.sgid = dr["NOM_SGID"].ToString();
            }
            return dataset;
        }
        //Bind controls based on language
        public DataSet getCurrentForm(string formname)
        {
            User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getCurrentForm(formname);

            foreach (DataRow dr in dataset.Tables[0].Rows)
            {
                userEntity.sgid = dr["T_LINK"].ToString();
            }
            return dataset;
        }


        //Bind controls based on language
        public DataSet getLoadMainMenus(string language, string application, string usergroup, string menugroup, string userid)
        {
            //User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getLoadMainMenus(language, application, usergroup, menugroup, userid);

            //foreach (DataRow dr in dataset.Tables[0].Rows)
            //{
            //    userEntity.formname = dr["frmname"].ToString();
            //    userEntity.formcontrol = dr["frmctrl"].ToString();
            //    userEntity.formcontrolid = dr["frmctrlid"].ToString();
            //    userEntity.formcontrolname = dr["frmctlname"].ToString();
            //    userEntity.language = dr["langname"].ToString();
            //}
            return dataset;
        }

        //Bind controls based on language
        public DataSet getLoadMenusLines()
        {
            //User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getLoadMenusLines();

            //foreach (DataRow dr in dataset.Tables[0].Rows)
            //{
            //    userEntity.formname = dr["frmname"].ToString();
            //    userEntity.formcontrol = dr["frmctrl"].ToString();
            //    userEntity.formcontrolid = dr["frmctrlid"].ToString();
            //    userEntity.formcontrolname = dr["frmctlname"].ToString();
            //    userEntity.language = dr["langname"].ToString();
            //}
            return dataset;
        }

        //Bind controls based on language
        public DataSet getEnableScreenMaximize()
        {
            //User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();
            dataset = _userControl.getEnableScreenMaximize();
            return dataset;
        }

        //Bind controls based on language
        public DataSet getUsernameSite(string username)
        {
            //User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();
            dataset = _userControl.getUsernameSite(username);
            return dataset;
        }

        //GET CURRENT FICHE 
        public User_Entity getSuperCodeValidation(string site, string tcode, string status, string lockcode)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dt = new DataTable();

            dt = _userControl.getSuperCodeValidation(site, tcode, status, lockcode);

            foreach (DataRow dr in dt.Rows)
            {
                userEntity.statusstr = dr["STATUS"].ToString();
            }

            return userEntity;
        }

        //GET CURRENT FICHE 
        public User_Entity getSkipValidation(string site, string tcode, string status, string lockcode)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dt = new DataTable();

            dt = _userControl.getSkipValidation(site, tcode, status, lockcode);

            foreach (DataRow dr in dt.Rows)
            {
                userEntity.statusstr = dr["STATUS"].ToString();
            }

            return userEntity;
        }

        //GET CURRENT FICHE 
        public User_Entity getSupervisorLengthValidation(string site, string tcode, string paramid)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dt = new DataTable();

            dt = _userControl.getSupervisorLengthValidation(site, tcode, paramid);

            foreach (DataRow dr in dt.Rows)
            {
                userEntity.statusstr = dr["NEWVALUE"].ToString();
            }

            return userEntity;
        }

        //Validate the default password
        public DataTable getMasterData(string _site, string _flag)
        {
            try
            {
                return _userControl.getMasterData(_site, _flag);
            }
            catch
            {
                throw;
            }
        }

        //Validate the default password time limit
        public DataTable getPasswordChangeTime(string username, string password)
        {
            try
            {
                return _userControl.getPasswordChangeTime(username, password);
            }
            catch
            {
                throw;
            }
        }

        public DataTable getPasswordHistory(string username, string password)
        {
            try
            {
                return _userControl.getPasswordHistory(username, password);
            }
            catch
            {
                throw;
            }
        }

        public DataTable getHomeImage(string userid)
        {
            DataTable dataTable = new DataTable();
            return dataTable = _userControl.getHomeImages(userid);
        }

        public bool LoginUpdate(string userid)
        {   
            return _userControl.LoginUpdate(userid);
        }

        public DataTable getMultiUserLoginLock()
        {
            DataTable dataTable = new DataTable();
            return dataTable = _userControl.getMultiUserLoginLock();
        }

        public DataSet getMDIMasterData(string userid, string lang)
        {
            return _userControl.getMDIMasterData(userid, lang);
        }

        internal DataTable getBindInFormLanguage()
        {
            throw new NotImplementedException();
        }
        #region sathishkumar 26-08-2016
        public DataSet ML_GETMDITRANSCAPTRDT(string strStatus, string strValue1, string strValue2, string strValue3, string strValue4)
        {
            return _userControl.CL_GETMDITRANSCAPTRDT(strStatus,strValue1, strValue2, strValue3, strValue4);
        }
        public void ML_MANAGECAPTURE(string strTransCode, string strTransName, string strTransForm,
                                       string strUserId, int nStatus)
        {
            _userControl.CL_MANAGECAPTURE(strTransCode, strTransName, strTransForm,
                                        strUserId, nStatus);
        }
        #endregion

        public DataTable getSessionLimit()
        {
            return _userControl.getSessionLimit();
        }

        public DataSet GET_REL_HINTS(string userid, string lang)
        {
            return _userControl.GET_REL_HINTS(userid, lang);
        }

        public bool SAVE_REL_HINTS(string userid, string lang)
        {
            return _userControl.SAVE_REL_HINTS(userid, lang);
        }

        public DataTable val_UsersAccess(string Uid)
        {
            return _userControl.val_UsersAccess(Uid);
        }

        public DataTable getuserid(string _hostid, string _type, string _username)
        {
            return _userControl.getuserid(_hostid, _type, _username);
        }
    }
}
