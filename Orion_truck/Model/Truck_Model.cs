﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orion_truck.Controls;
using Orion_truck.Entity;
using System.Data;

namespace Orion_truck.Model
{
  

    class Truck_Model
    {
        Truck_Control _control;

        public Truck_Model()
        {
            _control = new Truck_Control();
        }

        public DataSet getlang(string _username)
        {
            return _control.getlang(_username);
        }

        public bool savesltlang(string _user, string _lang, string _langl)
        {
            return _control.savesltlang(_user, _lang, _langl);
        }
        public DataSet val_product(string flag, string sono, string plant, string lang, string userid, string stil)
        {
            return _control.val_product(flag, sono, plant, lang,userid,stil);

        }
        public DataSet save_product(string _so_no, string _cust, string _still, string _truckno, string _container, string _mob, string _dvrname, string _crtby, string _orddt, string _plant, string _wight, string _flag,
          string _img, string _trans, string _code, string _sltlang, string _slttrk, string _triler,int rsno,int lineitem, int noofbags,string supp,string taraweight)
        {
            return _control.save_product(_so_no, _cust, _still, _truckno, _container, _mob, _dvrname, _crtby, _orddt, _plant, _wight, _flag, _img, _trans, _code, _sltlang, _slttrk, _triler,rsno,lineitem,noofbags,supp, taraweight);
        }


   }
}
