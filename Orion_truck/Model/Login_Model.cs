﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orion_truck.Controls;
using Orion_truck.Entity;
using System.Data;

namespace Orion_truck.Model
{

    public class Login_Model
    {
        Login_Control _control;

        public Login_Model()
        {
            _control = new Login_Control();
        }

        public DataTable getBindLanguage()
        {
            return _control.getBindLanguage();
        }

        public DataTable getBindProcessType(string location)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _control.getBindProcessType(location);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["C_SITEL"].ToString();
                userEntity.languagevalue = dr["C_SITE"].ToString();
            }
            return dataTable;
        }

        public User_Entity getValidateLanguage(string language)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _control.getValidateLanguage(language);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.language = dr["LANGL"].ToString();
            }
            return userEntity;
        }

        public User_Entity getCurrentConnection(string location, string csitel)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _control.getCurrentConnection(location, csitel);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.connection = dr["DBSOURCE"].ToString();
            }
            return userEntity;
        }

        public DataSet getload_det(string _plant,string _host)
        {
            return _control.getload_det(_plant,_host);
        }

      



    }
}
