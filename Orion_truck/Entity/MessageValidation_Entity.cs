﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orion_truck.Entity
{
   public class MessageValidation_Entity
    {

       private string _message;
       private string _alertcode = string.Empty;
       private string _alerttitle;

        
        /// <constructor>
        /// Constructor UserVO
        /// </constructor>
       public MessageValidation_Entity()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       public string alertmessage
       {
           get
           {
               return _message;
           }

           set
           {
               _message = value;
           }
       } 

       public string alerttitle
       {
           get
           {
               return _alerttitle;
           }

           set
           {
               _alerttitle = value;
           }
       }



    }
}
