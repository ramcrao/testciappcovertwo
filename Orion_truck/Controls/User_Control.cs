﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Orion_truck.Control
{
    public class User_Control
    {
        private DBConnection conn;

        /// <constructor>
        /// Constructor UserDAO
        /// </constructor>
        public User_Control()
        {
            conn = new DBConnection();
        }
                

        /// <method>
        //Validate the login users
        /// </method>
        public DataTable getValidateLoginUser(string _username,string _password)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATEUSER", sqlParameters);
        }

        /// <method>
        //Bind the language combobox here
        /// </method>
        public DataTable getBindLanguage()
        {
            //string query = string.Format("select LANGL,C_LANG from [XT_LANG] WHERE C_STA='Y' ORDER BY LANGID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[0];
            //sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            //sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_LOAD_LANGUAGE", sqlParameters);
        }

        /// <method>
        //Bind the language combobox here
        /// </method>
        public DataTable getBindInFormLanguage(string _lang)
        {
            //string query = string.Format("select LANGL,C_LANG from [XT_LANG] WHERE C_STA='Y' ORDER BY LANGID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@LANGUAGE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_lang);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_LOAD_LANGUAGE", sqlParameters);
        }

        /// <method>
        //Bind the location combobox here
        /// </method>
        public DataTable getBindLocation()
        {
            //string query = string.Format("select CMPDESC,CMPCODE from [XT_PLANT] WHERE C_STA='Y' ORDER BY PLANTID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[0];
            //sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            //sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_LOAD_LOCATION", sqlParameters);
        }

        /// <method>
        //Bind the PROCESS TYPE combobox here
        /// </method>
        public DataTable getBindProcessType(string _location)
        {
            //string query = string.Format("select C_SITEL,C_SITE from [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_STA='Y' ORDER BY LOCATID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_LOAD_PROCTYPE", sqlParameters);
        }

        ///// <method>
        ////Validate the login users
        ///// </method>
        //public DataTable getValidateLoginUserOnly(string _username)
        //{
        //    string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
        //    SqlParameter[] sqlParameters = new SqlParameter[1];
        //    sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
        //    sqlParameters[0].Value = Convert.ToString(_username);
        //    //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
        //    //sqlParameters[1].Value = Convert.ToString(_password);
        //    return conn.executeSelectQuery(query, sqlParameters);
        //}

        /// <method>
        //Validate the login users
        /// </method>
        public DataTable getValidateLoginUserOnly(string _username)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQueryWithSPNew("SP_VALIDLOGINUSERONLY", sqlParameters);
        }

        ///<Method>
        /// Null Text Validation
        /// </Method>
        public DataTable getCurrLnag(string _lang)
        {
            string query = string.Format("SELECT top 1 C_LNGCODE FROM [XT_LANG] WHERE C_STA='Y' AND C_LANG=@lang");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@lang", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_lang);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        ///<Method>
        /// Null Text Validation
        /// </Method>
        public DataTable getCurrLnagOrdID(string _lang)
        {
            string query = string.Format("SELECT top 1 C_IMGORD FROM [XT_LANG] WHERE C_STA='Y' AND C_LANG=@lang");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@lang", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_lang);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        public DataSet getMDIMasterData(string userid, string lang)
        {
            return conn.DBDataSetExecuteParamSP("SP_GETMDIMSTRDATA", userid, lang);
        }

        /// <method>
        //show current menu clicked form
        /// </method>
        public DataTable getCurrentMenuForm(string _menuname,string strLang)
        {
            //string query = string.Format("SELECT DISTINCT C_PRG AS FRMNAME FROM XT_XCON WHERE C_CON IN (SELECT DISTINCT C_CON FROM [XT_XCONL] WHERE L_CON=@MENUNAME)");
            //string query = string.Format("SELECT DISTINCT C_PRG AS FRMNAME, FROM XT_XCON WHERE C_CON =@MENUNAME");
            //SqlParameter[] sqlParameters = new SqlParameter[1];
            //sqlParameters[0] = new SqlParameter("@MENUNAME", SqlDbType.VarChar);
            //sqlParameters[0].Value = Convert.ToString(_menuname);
            ////sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
//            return conn.DBExecuteSqlQuery(@"SELECT DISTINCT A.C_PRG AS FRMNAME,B.L_CON AS TRANSNAME FROM XT_XCON AS A
//                                            INNER JOIN XT_XCONL B 
//                                            ON A.C_CON=B.C_CON WHERE B.C_CON ='" + _menuname + "' AND B.C_LANG='" + strLang + "'");
            return conn.DBDataTableExecuteParamSP("SP_GETCURRTRANSFORMNAME", _menuname, strLang);
        }
        public void CL_MANAGECAPTURE(string strTransCode,string strTransName,string strTransForm,
                                        string strUserId,int nStatus)
        {
            conn.DBInsertExecuteParamSP("SP_MANAGEMDITRANSCAPTR", strTransCode, strTransName, strTransForm,
                                        strUserId,nStatus);
        }
        public DataSet CL_GETMDITRANSCAPTRDT(string strStatus, string strValue1, string strValue2, string strValue3, string strValue4)
        {
            return conn.DBDataSetExecuteParamSP("SP_GETMDITRANSCAPTRDT", strStatus, strValue1, strValue2, strValue3, strValue4);
        }
        /// <method>
        //Validate the login users for failed password attempt
        /// </method>
        //public DataTable getValidateLoginUserOnly(string _username, string _password)
        //{
        //    string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS<>@password");
        //    SqlParameter[] sqlParameters = new SqlParameter[2];
        //    sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
        //    sqlParameters[0].Value = Convert.ToString(_username);
        //    sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
        //    sqlParameters[1].Value = Convert.ToString(_password);
        //    return conn.executeSelectQuery(query, sqlParameters);
        //}

        /// <method>
        //Validate the login users for failed password attempt
        /// </method>
        public DataTable getValidateLoginUserOnly(string _username, string _password)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS<>@password");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQueryWithSPNew("SP_VALIDLOGINUSER", sqlParameters);
        }

        /// <method>
        //Validate the login users for failed password attempt
        /// </method>
        public DataTable getValidateLoginUserOnlyNew(string _username)
        {
            string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //Validate the language
        /// </method>
        public DataTable getValidateLanguage(string _language)
        {
            //string query = string.Format("select LANGL from [XT_LANG] where LANGL=@language AND C_STA='Y' ORDER BY LANGID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@language", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_language);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATE_LANGUAGE", sqlParameters);
        }

        /// <method>
        //Validate the location
        /// </method>
        public DataTable getValidateLocation(string _location)
        {
            //string query = string.Format("select CMPDESC from [XT_PLANT] where CMPCODE=@location AND C_STA='Y' ORDER BY PLANTID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@location", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATE_LOCATION", sqlParameters);
        }

        /// <method>
        //Validate the process type
        /// </method>
        public DataTable getValidateProcessType(string _location,string _csitel)
        {
            //string query = string.Format("SELECT C_SITEL FROM [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_SITEL=@CSITEL AND C_STA='Y' ORDER BY LOCATID ASC");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);
            sqlParameters[1] = new SqlParameter("@CSITEL", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_csitel);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATE_PROCTYPE", sqlParameters);
        }

        ///// <method>
        ////GET THE CURRENT CONNECTION
        ///// </method>
        //public DataTable getCurrentConnection(string _location, string _csitel)
        //{
        //    string query = string.Format("SELECT DBSOURCE FROM [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_SITEL=@CSITEL AND C_STA='Y'");
        //    SqlParameter[] sqlParameters = new SqlParameter[2];
        //    sqlParameters[0] = new SqlParameter("@location", SqlDbType.VarChar);
        //    sqlParameters[0].Value = Convert.ToString(_location);
        //    sqlParameters[1] = new SqlParameter("@CSITEL", SqlDbType.VarChar);
        //    sqlParameters[1].Value = Convert.ToString(_csitel);
        //    return conn.executeSelectQuery(query, sqlParameters);
        //}
        /// <method>
        //GET THE CURRENT CONNECTION
        /// </method>
        ////public DataTable getCurrentConnection(string _location, string _csitel)
        ////{
        ////    //string query = string.Format("SELECT DBSOURCE FROM [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_SITEL=@CSITEL AND C_STA='Y'");
        ////    SqlParameter[] sqlParameters = new SqlParameter[2];
        ////    sqlParameters[0] = new SqlParameter("@location", SqlDbType.VarChar);
        ////    sqlParameters[0].Value = Convert.ToString(_location);
        ////    sqlParameters[1] = new SqlParameter("@CSITEL", SqlDbType.VarChar);
        ////    sqlParameters[1].Value = Convert.ToString(_csitel);
        ////    return conn.executeSelectQuerySPDataTable("LG_LOAD_CURRCONN", sqlParameters);
        ////}

        public DataTable getCurrentConnection(string _location, string _csitel)
        {
            //string query = string.Format("SELECT DBSOURCE FROM [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_SITEL=@CSITEL AND C_STA='Y'");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@location", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);
            sqlParameters[1] = new SqlParameter("@CSITEL", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_csitel);
            //sqlParameters[2] = new SqlParameter("@CSITE", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString("G500");
            return conn.executeSelectQuerySPDataTable("LG_LOAD_CURRCONN", sqlParameters);

        }
        /// <method>
        //Update the wrong password count to table
        /// </method>
        public bool getUpdateWrongPwd(string _username, string _password)
        {
            //string query = string.Format("update [xt_xuti] set NO_OF_ATTEMPT= where NOM_UTI=@username and MOT_PAS<>@password");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@MOT_PAS", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeInsertQueryWithSP("LG_UPDATEWRONGPWD_COUNT_PROC", sqlParameters);
        }

        /// <method>
        //Update the new password
        /// </method>
        public bool getUpdateNewPwd(string _username, string _newpwd,string _creaby)
        {
            //string query = string.Format("update [xt_xuti] set MOT_PAS=@password where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_sgid);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_newpwd);
            sqlParameters[2] = new SqlParameter("@CREATEDUSER", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_creaby);
            return conn.executeInsertQueryWithSP("LG_CHANGEPASSWORD", sqlParameters);
        }

        /// <method>
        //Update the wrong password count to table
        /// </method>
        public bool getAccountLock(string _username)
        {
            string query = string.Format("update [xt_xuti] set LOGIN_STATUS=0 where NOM_UTI=@NOM_UTI");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@MOT_PAS", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeInsertQuery(query, sqlParameters);
        }

        /// <method>
        //Validate the login users with sgid
        /// </method>
        public DataTable getValidateLoginUserSGID(string _username, string _password)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password and NOM_SGID=@sgid");
            string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_password);
            //sqlParameters[2] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(_sgid);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //Validate the login users with sgid
        /// </method>
        public DataTable getValidateLoginUserSGIDNEW(string _username)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password and NOM_SGID=@sgid");
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            //sqlParameters[2] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(_sgid);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATESGIDUSER", sqlParameters);
        }

        /// <method>
        //Validate the login users with sgid
        /// </method>
        public DataTable getValidateEmpMaster(string _sgid)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password and NOM_SGID=@sgid");
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@sgid", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_sgid);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            //sqlParameters[2] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(_sgid);
            return conn.executeSelectQuerySPDataTable("LG_VALIDATEEMPMSTR", sqlParameters);
        }

        /// <method>
        //Validate the multi user login
        /// </method>
        public DataTable getValidateMultiUserLogin(string _username)
        {
            //string query = string.Format("select * from [XT_AVLUSR] where nom_uti=@username and nom_sgid=@sgid and login_status=1");
            string query = string.Format("select * from [XT_AVLUSR] where nom_uti=@username and login_status=1");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_sgid);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //Insert login user to table
        /// </method>
        public bool getInsertLoginUsers(string _username, string _sgid, string _machinename, string _appversion)
        {
            //string query = string.Format("insert into XT_AVLUSR(nom_uti,nom_sgid,current_ip,login_date) values()");
            SqlParameter[] sqlParameters = new SqlParameter[5];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@NOM_SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_sgid);
            //sqlParameters[2] = new SqlParameter("@SP_ID", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(_sgid);
            //sqlParameters[2] = new SqlParameter("@NO_OF_USERCOUNT", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(1);
            sqlParameters[2] = new SqlParameter("@LOGIN_STATUS", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(1);
            sqlParameters[3] = new SqlParameter("@MACHINENAME", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_machinename);
            sqlParameters[4] = new SqlParameter("@APPVERSION", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(_appversion);
            return conn.executeInsertQueryWithSP("LG_INSERTLOGINUSER_PROC", sqlParameters);
        }

        /// <method>
        //Insert login user to table
        /// </method>
        public bool getDeleteLoginUsers(string _username, string _sgid)
        {
            //string query = string.Format("insert into XT_AVLUSR(nom_uti,nom_sgid,current_ip,login_date) values()");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@NOM_SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_sgid);
            //sqlParameters[2] = new SqlParameter("@FLAG", SqlDbType.Int);
            //sqlParameters[2].Value = Convert.ToString(2);
            return conn.executeInsertQueryWithSP("LG_DELETELOGINUSER_PROC", sqlParameters);
        }

        /// <method>
        //Insert login user to table
        /// </method>
        public bool getDeleteAvlUsers(string _username, string _machinename,string _processid)
        {
            //string query = string.Format("insert into XT_AVLUSR(nom_uti,nom_sgid,current_ip,login_date) values()");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@MACHINENAME", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_machinename);
            sqlParameters[2] = new SqlParameter("@PROCESSID", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_processid);
            return conn.executeInsertQueryWithSP("LG_VALIDATEMUTILOGINUSER", sqlParameters);
        }

        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getBindControlsBaseLanguage(string _language, string _formname)
        {
            DBConnection con = new DBConnection();
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@langname", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_language);
            sqlParameters[1] = new SqlParameter("@formname", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_formname);
            return con.executeSelectQueryWithSP("LG_LOADCTRLBASELAGN", sqlParameters);
        }

        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getValidateAvlUser(string _userid, string _sgid)
        {
            string query = string.Format("select top 1 * from XT_AVLUSR where NOM_UTI=@USERID AND NOM_SGID=@SGID AND LOGIN_STATUS='1'");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_userid);
            sqlParameters[1] = new SqlParameter("@SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_sgid);
            //sqlParameters[1] = new SqlParameter("@formname", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_formname);
            return conn.executeSelectQueryNew(query, sqlParameters);
        }

        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getCurrentForm(string _formname)
        {
            string query = string.Format("select top 1 T_LINK from XT_HELPTRAN where c_prg=@FORMNAME");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@FORMNAME", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_formname);
            //sqlParameters[1] = new SqlParameter("@formname", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_formname);
            return conn.executeSelectQueryNew(query, sqlParameters);
        }

        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getLoadMainMenus(string _language, string _application, string _usergroup, string _menugroup, string _userid)
        {
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[5];
            sqlParameters[0] = new SqlParameter("@LANGUAGE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_language);
            sqlParameters[1] = new SqlParameter("@APPLICATION", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_application);
            sqlParameters[2] = new SqlParameter("@USERGROUP", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_usergroup);
            sqlParameters[3] = new SqlParameter("@MENUGROUP", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_menugroup);
            sqlParameters[4] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(_userid);
            ////return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENU", sqlParameters);
            //return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENUNEW_FR", sqlParameters);
            //return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENUNEW", sqlParameters);
            return conn.executeSelectQueryWithSP("SP_LOAD_MENUSUBMENUNEW", sqlParameters);
        }

        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getLoadMenusLines()
        {
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[0];
            return conn.executeSelectQueryWithSP("LG_LOAD_MENULINES", sqlParameters);
        }

        /// <method>
        //Get login username, site
        /// </method>
        public DataSet getUsernameSite(string _username)
        {
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            return conn.executeSelectQueryWithSP("LG_LOAD_USERNAMESITE", sqlParameters);
        }

        /// <method>
        //Get login username, site
        /// </method>
        public DataSet getEnableScreenMaximize()
        {
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[0];
            //sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
            //sqlParameters[0].Value = Convert.ToString(_username);
            return conn.executeSelectQueryWithSP("SP_ENABLESCRNMAXIMIZE", sqlParameters);
        }

        /// <method>
        //get current fiche
        /// </method>
        public DataTable getSuperCodeValidation(string _site, string _tcode, string _status, string _lockcode)
        {
            string query = string.Format("SELECT STATUS FROM [XT_LOCK] WHERE C_SITE=@SITE AND TCODE=@TCODE AND STATUS=@STATUS AND LOCK_CODE=@LOCKCODE");
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@SITE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_site);
            sqlParameters[1] = new SqlParameter("@TCODE", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_tcode);
            sqlParameters[2] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_status);
            sqlParameters[3] = new SqlParameter("@LOCKCODE", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_lockcode);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //get current fiche
        /// </method>
        public DataTable getSkipValidation(string _site, string _tcode, string _status, string _lockcode)
        {
            //string query = string.Format("SELECT STATUS FROM [XT_LOCK] WHERE C_SITE=@SITE AND TCODE=@TCODE AND STATUS=@STATUS AND LOCK_CODE=@LOCKCODE");
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@SITE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_site);
            sqlParameters[1] = new SqlParameter("@TCODE", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_tcode);
            sqlParameters[2] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_status);
            sqlParameters[3] = new SqlParameter("@LOCKCODE", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_lockcode);
            return conn.executeSelectQueryWithSPNew("LG_VALIDATESKIP", sqlParameters);
        }

        /// <method>
        //get current fiche
        /// </method>
        public DataTable getSupervisorLengthValidation(string _site, string _tcode, string _paramid)
        {
            string query = string.Format("SELECT NEWVALUE FROM [XT_PARM] WHERE C_SITE=@SITE AND TCODE=@TCODE AND PARAM_ID=@PARAMID");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@SITE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_site);
            sqlParameters[1] = new SqlParameter("@TCODE", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_tcode);
            sqlParameters[2] = new SqlParameter("@PARAMID", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_paramid);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //Get Data
        /// </method>
        public DataTable getMasterData(string _site, string _flag)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@SITE", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(_site);
                sqlParameters[1] = new SqlParameter("@FLAG", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(_flag);
                return conn.executeSelectQuerySPDataTable("SP_GETDCPDATA", sqlParameters);
            }
            catch
            {
                throw;
            }
        }


        public DataTable getPasswordChangeTime(string username, string password)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(username);
                sqlParameters[1] = new SqlParameter("@PASSWORD", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(password);
                return conn.executeSelectQuerySPDataTable("SP_GETPASSWORDCT", sqlParameters);
            }
            catch
            {
                throw;
            }
        }

        public DataTable getPasswordHistory(string username, string password)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(username);
                sqlParameters[1] = new SqlParameter("@PASSWORD", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(password);
                return conn.executeSelectQuerySPDataTable("SP_GETPWDH", sqlParameters);
            }
            catch
            {
                throw;
            }
        }

        public DataTable getHomeImages(string userid)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(userid);
                return conn.executeSelectQuerySPDataTable("SP_HOMEIMAGE", sqlParameters);
            }
            catch
            {
                throw;
            }
        }

        public bool LoginUpdate(string userid)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(userid);
                return conn.executeInsertQueryWithSP("SP_LOGINMODUPT", sqlParameters);
            }
            catch
            {
                throw;
            }
        }

        public DataTable getMultiUserLoginLock()
        { 
            SqlParameter[] sqlParameters = new SqlParameter[0];
            return conn.executeSelectQuery(@"SP_GETMLOGINLOCK", sqlParameters);
        }

        internal DataTable getBindInFormLanguage()
        {
            throw new NotImplementedException();
        }

        public DataTable getSessionLimit()
        {
            SqlParameter[] sqlParameters = new SqlParameter[0];
            return conn.executeSelectQuery(@"SP_GETSESSIONLIMIT", sqlParameters);
        }

        public DataSet GET_REL_HINTS(string userid, string lang)
        {
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@USER", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(userid);
            sqlParameters[1] = new SqlParameter("@LANG", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(lang);
            return conn.executeSelectQueryWithSP("SP_GETRELEASE_HINTS", sqlParameters);
        }

        public bool SAVE_REL_HINTS(string userid, string lang)
        {
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@USER", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(userid);
            sqlParameters[1] = new SqlParameter("@LANG", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(lang);
            return conn.executeInsertQueryWithSP("SP_SAVE_REL_HINTS", sqlParameters);
        }
        public DataTable val_UsersAccess(string Uid)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(Uid);
            return conn.executeSelectQuerySPDataTable("SP_TRK_ACCESS", sqlParameters);
        }

        public DataTable getuserid(string _hostid, string _type, string _username)
        {

            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@HOSTID", SqlDbType.NVarChar);
            sqlParameters[0].Value = Convert.ToString(_hostid);
            sqlParameters[1] = new SqlParameter("@TYPE", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_type);
            sqlParameters[2] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_username);
            return conn.executeSelectQuerySPDataTable("SP_TRKGETUSERID", sqlParameters);

        }

    }
}
