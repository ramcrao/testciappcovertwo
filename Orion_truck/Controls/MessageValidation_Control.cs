﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Orion_truck.Control
{
   public class MessageValidation_Control
    {
       private DBConnection conn;

        /// <constructor>
        /// Constructor UserDAO
        /// </constructor>
       public MessageValidation_Control()
        {
           conn = new DBConnection();
        }
        
       ///<Method>
       /// Null Text Validation
       /// </Method>
       public DataTable Alert_Info(string _app, string _err, string _lang)
       {
            DBConnection con = new DBConnection();
           string query = string.Format("SELECT C_ERR,L_ERR,C_MSGTYPE FROM XT_XERRL WHERE  C_APP=@app AND C_ERR=@err AND C_LANG=@lang");
           SqlParameter[] sqlParameters = new SqlParameter[3];
           sqlParameters[0] = new SqlParameter("@app", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_app);
           sqlParameters[1] = new SqlParameter("@err", SqlDbType.VarChar);
           sqlParameters[1].Value = Convert.ToString(_err);
           sqlParameters[2] = new SqlParameter("@lang", SqlDbType.VarChar);
           sqlParameters[2].Value = Convert.ToString(_lang);
           return con.executeSelectQuery(query, sqlParameters);
       }

       

       public DataTable F_PRODUCT_EXIST_PROD(string PC_CPROD)
       {
           string query = string.Format("SELECT C_PROD FROM [XT_PROD] WHERE C_PROD=@CPROD");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@CPROD", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(PC_CPROD); 
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable F_PRODUCT_EXIST_PRODL(string PC_CPROD)
       {
           string query = string.Format("SELECT L_LIBL FROM [XT_PRODL] WHERE C_PROD=@CPROD");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@CPROD", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(PC_CPROD);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable F_CDC_EXIST_CDCQL(string PC_CDC)
       {
           string query = string.Format("SELECT L_CDC FROM [XT_CDCQL] WHERE CDC=@CDCQ AND C_LANG ='"+ Globals.GlobalLanguage+ "'");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@CDCQ", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(PC_CDC);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable F_CDC_EXIST_CDCQ(string PC_CDC)
       {
           string query = string.Format("SELECT LIBL FROM [XT_CDCQ] WHERE CDC=@CDCQ");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@CDCQ", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(PC_CDC);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable F_CODE_TABLG_VALIDIFIER(string code,string table)
       {
           string query = string.Format("SELECT C_TAB FROM [XT_TABLG] WHERE C_TAB=@CODE and NO_TAB=@TABLE");
           SqlParameter[] sqlParameters = new SqlParameter[2];
           sqlParameters[0] = new SqlParameter("@CODE", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(code);
           sqlParameters[1] = new SqlParameter("@TABLE", SqlDbType.VarChar);
           sqlParameters[1].Value = Convert.ToString(table);
           return conn.executeSelectQuery(query, sqlParameters);
       }    

       public DataTable F_CODE_TABLGL_VALIDIFIER(string code, string table)
       {
           string query = string.Format("SELECT L_LIBL FROM [XT_TABLGL] WHERE C_TAB=@CODE and NO_TAB=@TABLE and C_LANG='" + Globals.GlobalLanguage + "'");
           SqlParameter[] sqlParameters = new SqlParameter[2];
           sqlParameters[0] = new SqlParameter("@CODE", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(code);
           sqlParameters[1] = new SqlParameter("@TABLE", SqlDbType.VarChar);
           sqlParameters[1].Value = Convert.ToString(table);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable P_CTRL_EMPL(string bay, string rack)
       {
           string query = string.Format("SELECT C_EMPL FROM [XT_EMPL] WHERE C_SITE='" + Globals.GlobalSite + "' and C_MAGA=@BAY and C_EMPL=@RACK");
           SqlParameter[] sqlParameters = new SqlParameter[2];
           sqlParameters[0] = new SqlParameter("@BAY", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(bay);
           sqlParameters[1] = new SqlParameter("@RACK", SqlDbType.VarChar);
           sqlParameters[1].Value = Convert.ToString(rack);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable P_CTRL_MAGA(string bay)
       {
           string query = string.Format("SELECT C_MAGA FROM [XT_MAGA] WHERE C_SITE='" + Globals.GlobalSite + "' and C_MAGA=@BAY and  C_TYP in (select C_TYP from XT_PROF where c_prof='"+Globals.GlobalCParam+"')");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@BAY", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(bay); 
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable P_ARTICLE_SAP_CONTROLER(string cprod, string cdcr, string cpile, string cembl, string ctmes, int vlon, int vlar, string cintv, string cintp, string vtrait, string vmarq, string cstri)
       {
           string query = string.Format("SELECT COUNT(*)CNT FROM  [XT_ARTS] WHERE C_PROD='" + cprod + "' AND CDC='" + cdcr + "' AND C_PILE='" + cpile + "' AND C_EMBL='" + cembl + "' AND C_TMES='" + ctmes + "' AND LON='" + vlon + "' AND LAR='" + vlar + "' AND C_INTV='" + cintv + "' AND C_INTP= '" + cintp + "' AND TRAIT='" + vtrait + "' AND MARQ='" + vmarq + "' AND C_STRI='" + cstri + "'");
           SqlParameter[] sqlParameters= new SqlParameter[0];
           //sqlParameters[0] = new SqlParameter("@BAY", SqlDbType.VarChar);
           //sqlParameters[0].Value = Convert.ToString(bay);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       ///<Method> 
       /// Used for Transactions QU3C,QU03C for Quality Reason.
       /// To Check that the Code is Available
       ///</Method>
       public DataTable F_Motif_Existe_Indc(string _cmotif)
       {
           string query = string.Format("SELECT C_IND FROM [XT_INDC] WHERE C_IND =@motif and C_IND > 499");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@motif", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_cmotif);
           return conn.executeSelectQuery(query, sqlParameters);
       }


       ///<Method> 
       /// Used for Transactions QU3C,QU03C for Quality Reason.
       /// To Check that the Description is Available
       ///</Method>
       public DataTable F_Motif_Existe_Indcl(string _cmotif)
       {
           string query = string.Format("SELECT L_IND FROM [XT_INDCL] WHERE C_IND =@motif and C_IND > 499 and c_lang ='"+Globals.GlobalLanguage+"'");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@motif", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_cmotif);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       ///<Method> 
       /// Used for Transactions QU3C,QU03C for Quality Reason.
       /// To Check that the Description is Available
       ///</Method>
       public DataTable F_STT_FICHE_STOCKER(string _fich)
       {
           string query = string.Format("Select m.s_trait From xt_maga m,xt_fich f Where m.c_maga = f.c_maga and m.s_trait is not null and f.c_fich =@fich");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@fich", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_fich);
           return conn.executeSelectQuery(query, sqlParameters);
       }
       
       public DataTable F_CUTTING_ORDER_SETTLEMENT(string _fich)
       {
           string query = string.Format("SELECT COUNT(*)CHK_OPEN_ORDER FROM UNSETTLED_CUTTING_ORDER_DTLS WHERE MOTHER_FICHE=@fich OR CHILD_FICHE=@fich");
           SqlParameter[] sqlParameters = new SqlParameter[1];
           sqlParameters[0] = new SqlParameter("@fich", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_fich);
           return conn.executeSelectQuery(query, sqlParameters);
       }

       public DataTable Message_Info(string _actcode, string _ord, string _seq, string _lang)
       {
           string query = string.Format("SELECT P_ACTIDESC FROM XT_PIACTYLST WHERE P_ACTICODE=@ACTCODE AND P_ORD=@ORD AND P_LANG=@LANG AND C_STA='Y' AND P_SORD=@SEQ");
           SqlParameter[] sqlParameters = new SqlParameter[4];
           sqlParameters[0] = new SqlParameter("@ACTCODE", SqlDbType.VarChar);
           sqlParameters[0].Value = Convert.ToString(_actcode);
           sqlParameters[1] = new SqlParameter("@ORD", SqlDbType.Int);
           sqlParameters[1].Value = Convert.ToInt32(_ord);
           sqlParameters[2] = new SqlParameter("@SEQ", SqlDbType.VarChar);
           sqlParameters[2].Value = Convert.ToString(_seq);
           sqlParameters[3] = new SqlParameter("@LANG", SqlDbType.VarChar);
           sqlParameters[3].Value = Convert.ToString(_lang);
           return conn.executeSelectQuery(query, sqlParameters);
       }
    }
}
