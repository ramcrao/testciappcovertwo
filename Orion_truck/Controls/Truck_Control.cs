﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Orion_truck.Controls
{


    class Truck_Control
    {
        private DBConnection conn;

        public Truck_Control()
        {
            conn = new DBConnection();
        }      
        public DataSet getlang(string _username)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            return conn.executeSelectQueryWithSP("SP_DRI_GETFLAG", sqlParameters);
        }

        public bool savesltlang(string _user, string _lang, string _langl)
        {
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_user);
            sqlParameters[1] = new SqlParameter("@C_LANG", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_lang);
            sqlParameters[2] = new SqlParameter("@LANGL", SqlDbType.NVarChar);
            sqlParameters[2].Value = Convert.ToString(_langl);           
            
            return conn.executeInsertQueryWithSP("SP_DRI_SLTLANG", sqlParameters);
        }
        public DataSet val_product(string flag, string sono, string plant, string lang,string userid , string stil)
        {
            SqlParameter[] sqlParameters = new SqlParameter[6];
            sqlParameters[0] = new SqlParameter("@FLAG", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(flag);
            sqlParameters[1] = new SqlParameter("@SONO", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(sono);
            sqlParameters[2] = new SqlParameter("@PLANT", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(plant);
            sqlParameters[3] = new SqlParameter("@LANG", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(lang);

            sqlParameters[4] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(userid);
            sqlParameters[5] = new SqlParameter("@STILLAGE", SqlDbType.VarChar);
            sqlParameters[5].Value = Convert.ToString(stil);
            return conn.executeSelectQueryWithSP("SP_TRK_DET", sqlParameters);
        }

        public DataSet save_product(string _so_no, string _cust, string _still, string _truckno,string _container, string _mob, string _dvrname, string _crtby, string _orddt, string _plant,string _wight, string _flag,
            string _img, string _trans, string _code, string _sltlang,string _slttrk,string _triler,int rsno,int lineitem,int noofbags,string supp,string taraweight )
        {
            SqlParameter[] sqlParameters = new SqlParameter[23];
            sqlParameters[0] = new SqlParameter("@SO_NO", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_so_no);
            sqlParameters[1] = new SqlParameter("@CUST", SqlDbType.NVarChar);
            sqlParameters[1].Value = Convert.ToString(_cust);
            sqlParameters[2] = new SqlParameter("@STILLAGE", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_still);
            sqlParameters[3] = new SqlParameter("@TRUCKNO", SqlDbType.NVarChar);
            sqlParameters[3].Value = Convert.ToString(_truckno);
            sqlParameters[4] = new SqlParameter("@CONTAINER", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(_container);
            sqlParameters[5] = new SqlParameter("@MOBILENO", SqlDbType.VarChar);
            sqlParameters[5].Value = Convert.ToString(_mob);
            sqlParameters[6] = new SqlParameter("@DRIVNAME", SqlDbType.NVarChar);
            sqlParameters[6].Value = Convert.ToString(_dvrname);
            sqlParameters[7] = new SqlParameter("@CRBY", SqlDbType.VarChar);
            sqlParameters[7].Value = Convert.ToString(_crtby);
            sqlParameters[8] = new SqlParameter("@ORD_DATE", SqlDbType.VarChar);
            sqlParameters[8].Value = Convert.ToString(_orddt);
            sqlParameters[9] = new SqlParameter("@PLANT", SqlDbType.VarChar);
            sqlParameters[9].Value = Convert.ToString(_plant);
            sqlParameters[10] = new SqlParameter("@WEIGHT", SqlDbType.VarChar);
            sqlParameters[10].Value = Convert.ToString(_wight);
            sqlParameters[11] = new SqlParameter("@FLAG", SqlDbType.VarChar);
            sqlParameters[11].Value = Convert.ToString(_flag);
            sqlParameters[12] = new SqlParameter("@IMAGE", SqlDbType.VarChar);
            sqlParameters[12].Value = Convert.ToString(_img);
            sqlParameters[13] = new SqlParameter("@TRANS", SqlDbType.VarChar);
            sqlParameters[13].Value = Convert.ToString(_trans);
            sqlParameters[14] = new SqlParameter("@CODE", SqlDbType.VarChar);
            sqlParameters[14].Value = Convert.ToString(_code);
            sqlParameters[15] = new SqlParameter("@SLTLANG", SqlDbType.VarChar);
            sqlParameters[15].Value = Convert.ToString(_sltlang);
            sqlParameters[16] = new SqlParameter("@SLTTRK", SqlDbType.VarChar);
            sqlParameters[16].Value = Convert.ToString(_slttrk);
            sqlParameters[17] = new SqlParameter("@TRAILER", SqlDbType.NVarChar);
            sqlParameters[17].Value = Convert.ToString(_triler);
            sqlParameters[18] = new SqlParameter("@RSNO", SqlDbType.Int);
            sqlParameters[18].Value = rsno;
            sqlParameters[19] = new SqlParameter("@LINEITEM", SqlDbType.Int);
            sqlParameters[19].Value = lineitem;
            sqlParameters[20] = new SqlParameter("@NOOFBAGS", SqlDbType.Int);
            sqlParameters[20].Value = noofbags;
            sqlParameters[21] = new SqlParameter("@SUPP_DESC", SqlDbType.NVarChar);
            sqlParameters[21].Value = supp;
            sqlParameters[22] = new SqlParameter("@TARAWEIGHT", SqlDbType.VarChar);
            sqlParameters[22].Value = taraweight;
            return conn.executeSelectQueryWithSP("SP_TRKDRIV_SAVE", sqlParameters);
        }


    }


}
