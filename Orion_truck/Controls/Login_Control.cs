﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Orion_truck.Controls
{
    class Login_Control
    {
        private DBConnection conn;

          public Login_Control()
        {
            conn = new DBConnection();
        }
        public DataTable getBindLanguage()
        {
            SqlParameter[] sqlParameters = new SqlParameter[0];            
            return conn.executeSelectQuerySPDataTable("LG_LOAD_LANGUAGE", sqlParameters);
        }

        public DataTable getBindProcessType(string _location)
        {
            
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);            
            return conn.executeSelectQuerySPDataTable("LG_LOAD_PROCTYPE", sqlParameters);
        }

        public DataTable getValidateLanguage(string _language)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@language", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_language);            
            return conn.executeSelectQuerySPDataTable("LG_VALIDATE_LANGUAGE", sqlParameters);
        }

        public DataTable getCurrentConnection(string _location, string _csitel)
        {
            //string query = string.Format("SELECT DBSOURCE FROM [XT_LOCAT] WHERE CMPCODE=@LOCATION AND C_SITEL=@CSITEL AND C_STA='Y'");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@location", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_location);
            sqlParameters[1] = new SqlParameter("@CSITEL", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_csitel);
            //sqlParameters[2] = new SqlParameter("@CSITE", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString("G500");
            return conn.executeSelectQuerySPDataTable("LG_LOAD_CURRCONN", sqlParameters);

        }

        public DataSet getload_det(string _plant,string hostid)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_plant);
            //sqlParameters[1] = new SqlParameter("@HOSTID", SqlDbType.NVarChar);
            //sqlParameters[1].Value = Convert.ToString(hostid);
            return conn.executeSelectQueryWithSP("LG_LOAD_DRIDET", sqlParameters);
        }

       


    }
}
