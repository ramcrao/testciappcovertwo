﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Orion_truck.Model;
using System.Globalization;
using System.Resources;
using System.Collections;
using Orion_truck.Entity;

namespace Orion_truck
{
    public partial class Truckinterface_v1 : Form
    {
        Truck_Model _model;
        MessageValidation_Entity _alertentity = new MessageValidation_Entity();
        MessageValidation_Model _alertmodel = new MessageValidation_Model();
        AutoCompleteStringCollection matlcode = new AutoCompleteStringCollection();
        AutoCompleteStringCollection suppcode = new AutoCompleteStringCollection();
        AutoCompleteStringCollection suppcode1 = new AutoCompleteStringCollection();
        AutoCompleteStringCollection suppcode2 = new AutoCompleteStringCollection();


        public Truckinterface_v1()
        {
            InitializeComponent();
            _model = new Truck_Model();
        }       
        ArrayList list_trkin = new ArrayList();
        ArrayList list_trkout = new ArrayList();
        DataSet ds_code = new DataSet();
        static int pan_oth_x, pan_oth_y, pan_rsn_x, pan_rsn_y;
        string sltd_truck;
        string still_type = string.Empty;
        string sltd_lang = string.Empty;
        
        string product1 = string.Empty;
        int rsno, lineitem;
        int time_cnt;
        string lang_code = string.Empty;
        bool CustReturn = false;
        int refresh_time;

        bool Show_Cullet_Inload = true;
        bool Show_Cullet_euro = false;

        public static DataTable dt_mstrcountrycode = new DataTable();
        private void FrmTruckinterface_Load(object sender, EventArgs e)
        {
            try
            {
                
                listBox1.Visible = false;
                ChangeLanguage(Globals.GlobalTransLanguage);
                pan_oth_x = panel13.Location.X;
                pan_oth_y = panel13.Location.Y;
                pan_rsn_x = panel10.Location.X;
                pan_rsn_y = panel10.Location.Y;
                this.WindowState = FormWindowState.Maximized;
                Rectangle resolution = Screen.PrimaryScreen.Bounds;

                clearform();
                langload();
                tooltip();
                ds_code = _model.val_product("LOADCODE", "", "", "", "", "");

                if (ds_code.Tables[0].Rows.Count > 0)
                {
                    dt_mstrcountrycode = ds_code.Tables[0];
                    //foreach (DataRow dr in ds_code.Tables[0].Rows)
                    //{
                    //    matlcode.Add(dr["DESC"].ToString());
                    //}

                    ////cmb_country.SelectedIndexChanged -= new EventHandler(cmb_country_SelectedIndexChanged);
                    //cmb_country.DataSource = ds_code.Tables[0];

                    //cmb_country.DisplayMember = "DESC";
                    //cmb_country.ValueMember = "DESC";


                    //cmb_country.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //cmb_country.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    //cmb_country.AutoCompleteCustomSource = matlcode;
                    //cmb_country.SelectedIndex = -1;
                    ////cmb_country.SelectedIndexChanged += new EventHandler(cmb_country_SelectedIndexChanged);

                }

                if (ds_code.Tables.Count > 1)
                {
                    if (ds_code.Tables[1].Rows.Count > 0)
                    {
                        var source = new List<string>();
                        foreach (DataRow dr in ds_code.Tables[1].Rows)
                        {
                            //suppcode.Add(dr["SUPPDESC"].ToString());
                            //suppcode1.Add(dr["SUPPCODE"].ToString());
                            suppcode2.Add(dr["DESC"].ToString());
                            source.Add(dr["DESC"].ToString());

                        }
                        //txtSupp.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                       // txtSupp.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        listBox1.DataSource = source;
                        //txtSupp.AutoCompleteCustomSource = suppcode;
                        //txtSupp.AutoCompleteCustomSource = suppcode1;
                       // txtSupp.AutoCompleteCustomSource = suppcode2;
                       

                    }
                }

                if (ds_code.Tables.Count > 2)
                {
                    if(ds_code.Tables[2].Rows.Count > 0)
                    {
                        Show_Cullet_Inload = true;
                    }
                    else
                    {
                        Show_Cullet_Inload = false;

                    }
                    if (ds_code.Tables[3].Rows.Count > 0)
                    {
                        Show_Cullet_euro = true;
                    }
                    else
                    {
                        Show_Cullet_euro = false;
                    }
                }



                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          

        
        }
        public void ChangeLanguage(string lang)
        {
            Globals.SetGlobalLangString(lang);
            CultureInfo cul = new CultureInfo(lang);
            ResourceManager rm = new ResourceManager("Orion_truck.App_Global.FrmTruckInterface", this.GetType().Assembly);
            this.Text = rm.GetString("Truckinterface_v1", cul);
            foreach (System.Windows.Forms.Control c in this.Controls)
            {
                c.Text = rm.GetString(c.Name, cul);
                foreach (System.Windows.Forms.Control c1 in c.Controls)
                {
                    c1.Text = rm.GetString(c1.Name, cul);

                    if (c1.Name == "panel1")
                    {
                        foreach (System.Windows.Forms.Control c2 in c1.Controls)
                        {
                            c2.Text = rm.GetString(c2.Name, cul);                           
                        }
                    }
                    else if (c1.Name == "panel2")
                    {
                        foreach (System.Windows.Forms.Control c2 in c1.Controls)
                        {
                            c2.Text = rm.GetString(c2.Name, cul);
                            foreach (System.Windows.Forms.Control c3 in c2.Controls)
                            {
                                    c3.Text = rm.GetString(c3.Name, cul);
                            }
                        }

                    }                  
                    else if (c1.Name == "tabcontrol1")
                    {
                        foreach (System.Windows.Forms.Control c2 in c1.Controls)
                        {
                            c2.Text = rm.GetString(c2.Name, cul);

                            if (c2.Name == "tab_lang")
                            {
                                foreach (System.Windows.Forms.Control c3 in c2.Controls)
                                {
                                    if (c3.Name != "lbl_slt_lang_nor" && c3.Name != "panel10" && c3.Name != "panel13")
                                    {
                                        c3.Text = rm.GetString(c3.Name, cul);
                                        foreach (System.Windows.Forms.Control c4 in c3.Controls)
                                        {
                                            c4.Text = rm.GetString(c4.Name, cul);
                                        }
                                    }

                                }
                            }
                            else if (c2.Name == "tab_trucktype")
                            {
                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    if (c4.GetType() != typeof(PictureBox) && c4.Name != "lbl_slt_truck")
                                    {
                                        c4.Text = rm.GetString(c4.Name, cul);
                                        if (c4.Name == "back_trk" || c4.Name == "next_truck")
                                        {
                                            foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                            {
                                                c5.Text = rm.GetString(c5.Name, cul);
                                            }
                                        }
                                    }
                                }
                            }

                            else if (c2.Name == "tab_truckin")
                            {
                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    if (c4.GetType() != typeof(PictureBox))
                                    {
                                        c4.Text = rm.GetString(c4.Name, cul);
                                        if (c4.Name == "back_truckin" || c4.Name == "next_truckin")
                                        {
                                            foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                            {
                                                c5.Text = rm.GetString(c5.Name, cul);
                                            }
                                        }
                                    }
                                }

                            }
                            else if (c2.Name == "tab_po")
                            {
                                c2.Text = rm.GetString(c2.Name, cul);

                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.Name == "pan_truckin_po" || c4.Name == "pan_truckin_cullet" || c4.Name == "pan_truckin_still" || c4.Name == "back_deli" || c4.Name == "next_deli")
                                    {
                                        if (c4.GetType() != typeof(PictureBox) && c4.GetType() != typeof(TextBox) && c4.GetType() != typeof(CheckBox))
                                        {
                                            c4.Text = rm.GetString(c4.Name, cul);
                                            //if (c4.Name == "back_deli" || c4.Name == "next_deli")
                                            //{
                                                foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                                {
                                                    c5.Text = rm.GetString(c5.Name, cul);
                                                }
                                          //  }
                                        }
                                    }
                                    }
                                
                            }
                      
                            else if (c2.Name == "tab_truckout")
                            {
                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.GetType() != typeof(PictureBox))
                                    {
                                        c4.Text = rm.GetString(c4.Name, cul);
                                        
                                            foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                            {
                                                c5.Text = rm.GetString(c5.Name, cul);
                                            }
                                       
                                    }
                                }

                            }
                          

                            else if (c2.Name == "tab_so")
                            {
                                c2.Text = rm.GetString(c2.Name, cul);

                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.Name == "groupBox2" || c4.Name == "back_so" || c4.Name == "next_so" )
                                    {
                                        if (c4.GetType() != typeof(PictureBox) && c4.GetType() != typeof(TextBox) && c4.GetType() != typeof(CheckBox))
                                        {
                                            c4.Text = rm.GetString(c4.Name, cul);

                                            
                                                foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                                {
                                                    c5.Text = rm.GetString(c5.Name, cul);
                                                }
                                            

                                        }
                                    }
                                }

                            }
                            
                            else if (c2.Name == "tab_trkinfo")
                            {
                                c2.Text = rm.GetString(c2.Name, cul);

                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.Name == "groupBox5" || c4.Name == "back_trkinfo1" || c4.Name == "next_trkinfo1")
                                    {
                                        if (c4.GetType() != typeof(PictureBox) && c4.GetType() != typeof(TextBox) && c4.GetType() != typeof(CheckBox))
                                        {
                                            c4.Text = rm.GetString(c4.Name, cul);
                                           
                                                foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                                {
                                                    c5.Text = rm.GetString(c5.Name, cul);
                                                }
                                            

                                        }
                                    }
                                }

                            }
                         
                            else if (c2.Name == "tab_trukinfo2")
                            {
                                c2.Text = rm.GetString(c2.Name, cul);

                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.Name == "groupBox3" || c4.Name == "back_trkinfo2" || c4.Name == "next_trkinfo2")
                                    {
                                        if (c4.GetType() != typeof(PictureBox) && c4.GetType() != typeof(TextBox) && c4.GetType() != typeof(CheckBox))
                                        {
                                            c4.Text = rm.GetString(c4.Name, cul);
                                            
                                                foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                                {
                                                    c5.Text = rm.GetString(c5.Name, cul);
                                                }
                                            

                                        }
                                    }
                                }

                            }

                            else if (c2.Name == "tab_comp")
                            {
                                foreach (System.Windows.Forms.Control c4 in c2.Controls)
                                {
                                    c4.Text = rm.GetString(c4.Name, cul);
                                    if (c4.Name == "groupBox6" || c4.Name == "panel5")
                                    {
                                        foreach (System.Windows.Forms.Control c5 in c4.Controls)
                                        {
                                            if (c5.GetType() != typeof(PictureBox))
                                            {
                                                c5.Text = rm.GetString(c5.Name, cul);

                                            }
                                        }
                                    }
                                }

                                
                            }
                        }

                    }
                }
            }
           
        }
        private void clearform()
        {
            rsno = 0;
            lineitem = 0;
            lbl_title.Text = string.Empty;
            still_type = string.Empty;

            list_trkin.Clear();
            list_trkout.Clear();
            sltd_lang = string.Empty;
            pan_slt_inloader.Visible = false;
            pan_sltd_container.Visible = false;
            pan_sltd_euro.Visible = false;
            pan_sltd_other.Visible = false;

            lbl_slt_lang_nor.Text = string.Empty;
       
            lbl_slt_truck.Text = string.Empty;
            lbl_slt_truckin.Text = string.Empty;
            //lbl_slt_truckout.Text = string.Empty;

            pan_lang.BackgroundImage = Properties.Resources.process_enable_1;
            pan_truck.BackgroundImage = Properties.Resources.process_dis_1;
            pan_del.BackgroundImage = Properties.Resources.process_dis_1;
            pan_order.BackgroundImage = Properties.Resources.process_dis_1;
            pan_info.BackgroundImage = Properties.Resources.process_dis_1;
            pan_comp.BackgroundImage = Properties.Resources.process_dis_1;

            sltd_truck = string.Empty;

            txt_po.Text = string.Empty;
            txt_cul.Text = string.Empty;
            txt_still.Text = string.Empty;
            txt_still.Enabled = false;
            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;


            txt_so.Text = string.Empty;
            txt_so_Still.Text = string.Empty;
            
            txt_truckno.Text = string.Empty;
            txt_trilerno.Text = string.Empty;
            txt_contain.Text = string.Empty;
            txt_name.Text = string.Empty;
          //  cmb_country.SelectedIndex = -1;
            txt_countrycodeDis.Text = string.Empty;
            txt_mobno.Text = string.Empty;
            cmb_country.Text = string.Empty;

            time_cnt = 0;

            product1 = string.Empty;

            refresh_time = 0;

            pb_in.Image = null;
            pb_out.Image = null;

            pbflg1.Image = null;

            CustReturn = false;

            lbl_pono.Visible = true;
            lbl_expo.Visible = true;
            txt_po.Visible = true;
            txt_po.Text = string.Empty;

            lblCustR_SO.Visible = false;
            lblCustR_SOEX.Visible = false;
            txtCustR_SO.Visible = false;
            txtCustR_SO.Text = string.Empty;
            pb_chkbox_cusReturn.Image = Properties.Resources.uncheck_box;

            lang_code = string.Empty;
            txtSupp.Text = string.Empty;
            txtNoOfBags.Text = string.Empty;
        }
        private void langload()
        {
            try
            {
                lbl_title.Text = lbl_title_lang.Text;
                pan_lang.BackgroundImage = Properties.Resources.process_enable_1;
                DataSet ds_lang = new DataSet();
                ds_lang = _model.getlang(Globals.GlobalUsername);
                imgload(ds_lang);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

          
          

        }
        private void LangFlag_Click(object sender, EventArgs e)
        {
            try
            {
                PictureBox flg_img = sender as PictureBox;
            string[] s = null;
            s = flg_img.Tag.ToString().Split('-');
                lang_code = flg_img.Tag.ToString();
            bool  dt = _model.savesltlang(Globals.GlobalUsername, s[1].ToString(), flg_img.Name.ToString());
                sltd_lang = s[1].ToString();
            if (dt == true)
                {
                    removecontrol();

                    ChangeLanguage(flg_img.Tag.ToString());
                    lbl_slt_lang_nor.Text = string.Empty;
                    lbl_slt_lang_nor.Text = lb_invsble_sltd_nor.Text + "  " + flg_img.Name;
                    truckslt();
                }
          
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void removecontrol()
        {
            int cnt_rsn = panel10.Controls.Count;

            if (cnt_rsn > 2)
            {
                for (int y = cnt_rsn - 1; y >= 0; y--)
                {
                    if (panel10.Controls[y].Name != "panel11" && panel10.Controls[y].Name != "label9" && panel10.Controls[y].Name != "lbl_lang_rsnsel_nor" && panel10.Controls[y].Name != "lb_invsble_sltd_nor")
                    {
                        panel10.Controls.RemoveAt(y);

                    }
                }
            }

            int cnt_oth = panel13.Controls.Count;

            if (cnt_oth > 2)
            {
                for (int y = cnt_oth - 1; y >= 0; y--)
                {
                    if (panel13.Controls[y].Name != "panel14" && panel13.Controls[y].Name != "label10")
                    {
                        panel13.Controls.RemoveAt(y);

                    }
                }
            }
            langload();
        }
        private void truckslt()
        {
            lbl_title.Text = lbl_titlt_truck.Text;
            pan_truck.BackgroundImage = Properties.Resources.process_enable_1;
            tabcontrol1.SelectedIndex = 1;

        }
        private void pb_trktype_inloader_Click(object sender, EventArgs e)
        {
            try
            {
                sltd_truck = "INLOADER";
                pan_slt_inloader.Visible = true;
                pan_sltd_container.Visible = false;
                pan_sltd_euro.Visible = false;
                pan_sltd_other.Visible = false;

                next_truck_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         

        }
        private void pb_trktype_Container_Click(object sender, EventArgs e)
        {
            try
            {
                sltd_truck = "CONTAINER";
                pan_slt_inloader.Visible = false;
                pan_sltd_container.Visible = true;
                pan_sltd_euro.Visible = false;
                pan_sltd_other.Visible = false;

                next_truck_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           

        }
        private void pb_trktype_euro_Click(object sender, EventArgs e)
        {
            try
            {
                sltd_truck = "EURO";
                pan_slt_inloader.Visible = false;
                pan_sltd_container.Visible = false;
                pan_sltd_euro.Visible = true;
                pan_sltd_other.Visible = false;

                next_truck_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        private void pb_trktype_other_Click(object sender, EventArgs e)
        {
            try
            {
                sltd_truck = "OTHER";

                pan_slt_inloader.Visible = false;
                pan_sltd_container.Visible = false;
                pan_sltd_euro.Visible = false;
                pan_sltd_other.Visible = true;
                tabcontrol1.SelectedIndex = 6;
                // next_truck_Click(sender, e);
                pan_del.BackgroundImage = Properties.Resources.process_enable_1;
                pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                pan_info.BackgroundImage = Properties.Resources.process_enable_1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           

        }
        private void next_truck_Click(object sender, EventArgs e)
        {
            try
            {
                if (sltd_truck == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2243", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // MessageBox.Show("Please Select any Goods");
                    return;
                }
                if (sltd_truck == "INLOADER")
                {
                    lbl_truckin1_title.Text = lbl_titlt_truckin_goods.Text;
                    lbl_trukin2_title.Text = lbl_titlt_truckin_empty.Text;
                    lbl_slt_truck.Text = string.Empty;
                    lbl_slt_truck.Text = lbl_invsble_slttruck.Text + "  " + lbl_trktype_Inloader.Text;
                    pb_in.Image = Properties.Resources.inloadIN1;
                    pb_out.Image = Properties.Resources.inloadOUT1;

                }
                else if (sltd_truck == "CONTAINER")
                {
                    lbl_truckin1_title.Text = lbl_titlt_truckin_goods.Text;
                    lbl_trukin2_title.Text = lbl_titlt_truckin_empty.Text;
                    lbl_slt_truck.Text = string.Empty;
                    lbl_slt_truck.Text = lbl_invsble_slttruck.Text + "  " + lbl_trktype_Container.Text;
                    pb_in.Image = Properties.Resources.conIN1;
                    pb_out.Image = Properties.Resources.conOUT1;

                }
                else if (sltd_truck == "EURO")
                {
                    lbl_truckin1_title.Text = lbl_titlt_truckineuro_goods.Text;
                    lbl_trukin2_title.Text = lbl_titlt_truckineuro_empty.Text;
                    lbl_slt_truck.Text = string.Empty;
                    lbl_slt_truck.Text = lbl_invsble_slttruck.Text + "  " + lbl_trktype_euro.Text;
                    pb_in.Image = Properties.Resources.euroIN1;
                    pb_out.Image = Properties.Resources.euroOUT1;


                }
                else if (sltd_truck == "OTHER")
                {
                    lbl_slt_truck.Text = string.Empty;
                    lbl_slt_truck.Text = lbl_invsble_slttruck.Text + "  " + lbl_trktype_other.Text;
                    // pb_in.Image = Properties.Resources.other_in12;
                    pan_del.BackgroundImage = Properties.Resources.process_enable_1;
                    pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                    pan_info.BackgroundImage = Properties.Resources.process_enable_1;
                    tabcontrol1.SelectedIndex = 6;

                }

                if (sltd_truck != "OTHER")
                {
                    list_trkin.Clear();
                    tabcontrol1.SelectedIndex = 2;
                    pan_del.BackgroundImage = Properties.Resources.process_enable_1;
                    pan_order.BackgroundImage = Properties.Resources.process_dis_1;
                    pan_info.BackgroundImage = Properties.Resources.process_dis_1;
                    pan_comp.BackgroundImage = Properties.Resources.process_dis_1;
                    truckin();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void truckin()
        {
            list_trkin.Clear();
            pan_trkin1.Visible = false;
            pan_trkin2.Visible = false;
            pan_trkin3.Visible = false;
            pan_trkin4.Visible = false;
            pan_trkin5.Visible = false;

            pb_trkin1.Image = null;
            pb_trkin2.Image = null;
            pb_trkin3.Image = null;
            pb_trkin4.Image = null;
            pb_trkin5.Image = null;

            pb_trkin1.BackColor = Color.Transparent;
            pb_trkin2.BackColor = Color.Transparent;
            pb_trkin3.BackColor = Color.Transparent;
            pb_trkin4.BackColor = Color.Transparent;
            pb_trkin5.BackColor = Color.Transparent;


            lbl_trkin1.Text = string.Empty;
            lbl_trkin2.Text = string.Empty;
            lbl_trkin3.Text = string.Empty;
            lbl_trkin4.Text = string.Empty;
            lbl_trkin5.Text = string.Empty;
            if (sltd_truck == "INLOADER")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.inloader_stillage_1;
                pb_trkin3.Image = Properties.Resources.glass_newup;

                pb_trkin2.BackColor = Color.White;
                pb_trkin3.BackColor = Color.White;

                pb_trkin2.Tag = "STILLAGE";
                pb_trkin3.Tag = "GLASS";

                lbl_trkin2.Text = lbl_still.Text;
                lbl_trkin3.Text = lbl_glass.Text;
                

                if(Show_Cullet_Inload == true)
                {
                    pb_trkin4.Tag = string.Empty;
                    pb_trkin4.Image = Properties.Resources.culletwithstill_1;
                    pb_trkin4.BackColor = Color.White;
                    pb_trkin4.Tag = "CULLET";
                    lbl_trkin4.Text = lbl_stillcullt.Text;
                }
            }
            if (sltd_truck == "CONTAINER")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
              //  pb_trkin4.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty_truck;
                //pb_trkin3.Image = Properties.Resources.glass_2132;
                pb_trkin3.Image = Properties.Resources.con_glass_n1;

                // pb_trkin4.Image = Properties.Resources.Cullet;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "GLASS";
               // pb_trkin4.Tag = "CULLET";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_glass.Text;
                //lbl_trkin4.Text = lbl_cullet.Text;


            }
            if (sltd_truck == "EURO")
            {
                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty_euro;
                pb_trkin3.Image = Properties.Resources.EuroStillage;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "GLASS";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_glass.Text;

                if(Show_Cullet_euro == true)
                {
                    pb_trkin4.Tag = string.Empty;
                    pb_trkin4.Image = Properties.Resources.Euro_Cullet;
                    pb_trkin4.Tag = "CULLET";
                    lbl_trkin4.Text = lbl_cullet.Text;
                }
            }

            if (sltd_truck == "OTHER")
            {

                pb_trkin2.Tag = string.Empty;
                pb_trkin3.Tag = string.Empty;
                pb_trkin4.Tag = string.Empty;

                pb_trkin2.Image = Properties.Resources.empty;
                pb_trkin3.Image = Properties.Resources.aframe;
                pb_trkin4.Image = Properties.Resources.wood;

                pb_trkin2.Tag = "EMPTY";
                pb_trkin3.Tag = "AFRAME";
                pb_trkin4.Tag = "WOOD";

                lbl_trkin2.Text = lbl_empty.Text;
                lbl_trkin3.Text = lbl_packing.Text;
                lbl_trkin4.Text = lbl_wood.Text;
            }
        }
        private void imgload(DataSet _ds)
        {
            int x = 0;
            int xRef1 = 50;
            int yRef1 = 0;
            int y2ref = 0;
            int cnt = 0;
            int _distanceval = 170;

            if (_ds.Tables[0].Rows.Count > 0)
            {
                panel13.Location = new Point(pan_oth_x, pan_oth_y);
                panel13.Size = new Size(981, 257);
                lbl_lang_otrlnag_nor.Visible = true;
                label4.Visible = false;

                foreach (DataRow dr1 in _ds.Tables[0].Rows)
                {


                    PictureBox pb = new PictureBox();
                    int XPos = panel11.Location.X;
                    int YPos = panel11.Location.Y;
                    pb.Name = dr1["C_ETEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[2].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                    pb.BorderStyle = BorderStyle.FixedSingle;
                    pb.Tag = dr1["C_LNGCODE"].ToString();
                    int lXPos = label9.Location.X;
                    int lyPos = label9.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["C_ETEXT"].ToString();
                    lbl.Name = "lb" + dr1["C_ETEXT"].ToString();

                    lbl.Font = label9.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1 + 30, lyPos + yRef1);


                    pb.Click += new EventHandler(LangFlag_Click);


                    panel10.Controls.Add(pb);
                    panel10.Controls.Add(lbl);

                    xRef1 += _distanceval;

                }
            }

            else
            {
                panel13.Location = new Point(pan_rsn_x,pan_rsn_y);
               
                lbl_lang_otrlnag_nor.Visible = false;
                panel13.Size = new Size(981, 395);
                label4.Visible = true;
                lbl_lang_rsnsel_nor.Visible = false;
            }

            xRef1 = 50;
            yRef1 = 0;
            y2ref = 0;
            cnt = 0;

            if (_ds.Tables[1].Rows.Count > 0)
            {
            foreach (DataRow dr1 in _ds.Tables[1].Rows)
            {

                if (cnt < 5)
                {
                    PictureBox pb = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb.Name = dr1["C_ETEXT"].ToString();
                    pb.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[2].ToString());
                    pb.Image = imageList1.Images[x];
                    pb.AutoSize = true;
                    pb.Cursor = Cursors.Hand;
                    pb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    //  pb.SizeMode = PictureBoxSizeMode.Normal;
                    pb.Location = new Point(XPos + xRef1, YPos + yRef1);
                        pb.Tag = dr1["C_LNGCODE"].ToString() ;
                        pb.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl = new Label();
                    lbl.Text = dr1["C_ETEXT"].ToString();
                    lbl.Name = "lb" + dr1["C_ETEXT"].ToString();
                    //  lbl.Font = new Font("Open Sans", 10.0f, FontStyle.SemiBold);
                    lbl.Font = label10.Font;
                    lbl.Size = new Size(22, 4);
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lXPos + xRef1 + 30, lyPos + yRef1);
                    pb.Click += new EventHandler(LangFlag_Click);



                    panel13.Controls.Add(pb);
                    panel13.Controls.Add(lbl);
                    xRef1 += _distanceval;
                }
                else if (cnt < 10)
                {

                    PictureBox pb1 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb1.Name = dr1["C_ETEXT"].ToString();
                    pb1.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[2].ToString());
                    pb1.Image = imageList1.Images[x];
                    pb1.AutoSize = true;
                    pb1.Cursor = Cursors.Hand;
                    pb1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    //pb1.SizeMode = PictureBoxSizeMode.Normal;
                    pb1.Location = new Point(XPos + xRef1, YPos + yRef1);
                        pb1.Tag = dr1["C_LNGCODE"].ToString() ;
                        pb1.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_1 = new Label();
                    lbl_1.Text = dr1["C_ETEXT"].ToString();
                    lbl_1.Name = "lb" + dr1["C_ETEXT"].ToString();
                    //lbl_1.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_1.Font = label10.Font;
                    lbl_1.Size = new Size(22, 4);
                    lbl_1.AutoSize = true;
                    lbl_1.Location = new Point(lXPos + xRef1 + 30, lyPos + yRef1);
                    pb1.Click += new EventHandler(LangFlag_Click);



                    panel13.Controls.Add(pb1);
                    panel13.Controls.Add(lbl_1);
                    xRef1 += _distanceval;
                }
                else if (cnt < 15)
                {

                    PictureBox pb2 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb2.Name = dr1["C_ETEXT"].ToString();
                    pb2.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[2].ToString());
                    pb2.Image = imageList1.Images[x];
                    pb2.AutoSize = true;
                    pb2.Cursor = Cursors.Hand;
                    pb2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    //pb2.SizeMode = PictureBoxSizeMode.Normal;
                    pb2.Location = new Point(XPos + xRef1, YPos + y2ref);
                        pb2.Tag = dr1["C_LNGCODE"].ToString() ;
                        pb2.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_2 = new Label();
                    lbl_2.Text = dr1["C_ETEXT"].ToString();
                    lbl_2.Name = "lb" + dr1["C_ETEXT"].ToString();
                    //lbl_2.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_2.Font = label10.Font;
                    lbl_2.Size = new Size(22, 4);
                    lbl_2.AutoSize = true;
                    lbl_2.Location = new Point(lXPos + xRef1 + 30, lyPos + y2ref);
                    pb2.Click += new EventHandler(LangFlag_Click);



                    panel13.Controls.Add(pb2);
                    panel13.Controls.Add(lbl_2);
                    xRef1 += _distanceval;
                }
                else if (cnt < 20)
                {
                    PictureBox pb3 = new PictureBox();
                    int XPos = panel14.Location.X;
                    int YPos = panel14.Location.Y;
                    pb3.Name = dr1["C_ETEXT"].ToString();
                    pb3.Size = new Size(120, 70);
                    x = Convert.ToInt32(dr1[2].ToString());
                    pb3.Image = imageList1.Images[x];
                    pb3.AutoSize = true;
                    pb3.Cursor = Cursors.Hand;
                    pb3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                    pb3.SizeMode = PictureBoxSizeMode.Normal;
                    pb3.Location = new Point(XPos + xRef1, YPos + yRef1);
                        pb3.Tag = dr1["C_LNGCODE"].ToString() ;
                        pb3.BorderStyle = BorderStyle.FixedSingle;
                    int lXPos = label10.Location.X;
                    int lyPos = label10.Location.Y;
                    Label lbl_3 = new Label();
                    lbl_3.Text = dr1["C_ETEXT"].ToString();
                    lbl_3.Name = "lb" + dr1["C_ETEXT"].ToString();
                    // lbl_3.Font = new Font("Verdana", 8.0f, FontStyle.Regular);
                    lbl_3.Font = label10.Font;
                    lbl_3.Size = new Size(22, 4);
                    lbl_3.AutoSize = true;
                    lbl_3.Location = new Point(lXPos + xRef1 + 30, lyPos + yRef1);
                    pb3.Click += new EventHandler(LangFlag_Click);


                    panel13.Controls.Add(pb3);
                    panel13.Controls.Add(lbl_3);
                    xRef1 += _distanceval;
                }
                cnt = cnt + 1;
                if (cnt == 5)
                {

                    yRef1 = 0 + 120;
                    xRef1 = 50;
                }

                if (cnt == 10)
                {

                    y2ref = 0 + 240;
                    xRef1 = 50;
                }
                if (cnt == 15)
                {

                    yRef1 = 0 + 360;
                    xRef1 = 50;
                } 
            }
        }
        }        
        private void back_trk_Click(object sender, EventArgs e)
        {
            try
            {
                tabcontrol1.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
            
        }
        
        private void back_truckin_Click(object sender, EventArgs e)
        {
            try
            {
                tabcontrol1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            
        }
        private void next_truckin_Click(object sender, EventArgs e)
        {
            try
            {

                if (list_trkin.Count == 0)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2142", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                    // MessageBox.Show("Please Select any Goods");
                }               
                    #region Inloader
                    if (sltd_truck == "INLOADER")
                    {
                        for (int k = 0; k < list_trkin.Count; k++)
                        {
                            if (list_trkin[k].ToString() == "GLASS")
                            {
                            pan_truckin_po.Visible = true;
                            pan_truckin_still.Visible = true;
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            txt_po.Focus();
                            pan_truckin_cullet.Visible = false;

                            CustReturn = false;

                            lbl_pono.Visible = true;
                            lbl_expo.Visible = true;
                            txt_po.Visible = true;
                            txt_po.Text = string.Empty;

                            lblCustR_SO.Visible = false;
                            lblCustR_SOEX.Visible = false;
                            txtCustR_SO.Visible = false;
                            txtCustR_SO.Text = string.Empty;
                            pb_chkbox_cusReturn.Image = Properties.Resources.uncheck_box;
                            if (list_trkin.Contains("STILLAGE") || list_trkin.Contains("CULLET"))
                                {
                                    list_trkin.Remove("STILLAGE");
                                    list_trkin.Remove("CULLET");
                                    pan_trkin3.Visible = false;
                                    pan_trkin4.Visible = false;
                                }
                                txt_po.Focus();
                            tabcontrol1.SelectedIndex = 3;
                        }
                            //else if (list_trkin[k].ToString() == "EMPTY")
                            //{
                            //truckout();
                            //tabcontrol1.SelectedIndex = 4;
                            //}
                        else if (list_trkin[k].ToString() == "STILLAGE")
                        {
                            pan_truckin_po.Visible = false;
                            pan_truckin_still.Visible = true;
                            pan_truckin_cullet.Visible = false;
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            //txt_cul.Focus();
                            //pan_truckin_still.Visible = true;
                            tabcontrol1.SelectedIndex = 3;
                        }
                        else if (list_trkin[k].ToString() == "CULLET")
                            {  
                                 pan_truckin_po.Visible = false;
                                 pan_truckin_still.Visible = true;
                                 txt_still.Text = string.Empty;
                                 txt_still.Enabled = false;
                                 pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                                 pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                                 txt_cul.Focus();
                                 pan_truckin_cullet.Visible = true;
                                 tabcontrol1.SelectedIndex = 3;
                          }
                        }
                 
                }
                      
                    #endregion
                    #region Container
                    if(sltd_truck == "CONTAINER")
                    {
                    for (int k = 0; k < list_trkin.Count; k++)
                    {

                        if (list_trkin[k].ToString() == "EMPTY")
                        {
                            truckout();
                            pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                            tabcontrol1.SelectedIndex = 4;
                        }
                        else if (list_trkin[k].ToString() == "GLASS")
                        {
                            pan_truckin_po.Visible = true;
                            pan_truckin_still.Visible = false;
                            pan_truckin_cullet.Visible = false;
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            CustReturn = false;

                            lbl_pono.Visible = true;
                            lbl_expo.Visible = true;
                            txt_po.Visible = true;
                            txt_po.Text = string.Empty;

                            lblCustR_SO.Visible = false;
                            lblCustR_SOEX.Visible = false;
                            txtCustR_SO.Visible = false;
                            txtCustR_SO.Text = string.Empty;
                            pb_chkbox_cusReturn.Image = Properties.Resources.uncheck_box;
                            txt_po.Focus();
                            tabcontrol1.SelectedIndex = 3;

                        }
                        else if (list_trkin[k].ToString() == "CULLET")
                        {
                            pan_truckin_po.Visible = false;
                            pan_truckin_still.Visible = false;
                            pan_truckin_cullet.Visible = true;
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            txt_cul.Focus();
                            tabcontrol1.SelectedIndex = 3;
                        }
                    }
                    }

                    #endregion
                    #region Euro
                    if (sltd_truck == "EURO")
                    {
                    for (int k = 0; k < list_trkin.Count; k++)
                    {


                        if (list_trkin[k].ToString() == "EMPTY")
                        {
                            truckout();
                            pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                            tabcontrol1.SelectedIndex = 4;
                        }
                        else if (list_trkin[k].ToString() == "GLASS")
                        {
                            pan_truckin_po.Visible = true;
                            pan_truckin_still.Visible = false;
                            pan_truckin_cullet.Visible = false;
                            
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            CustReturn = false;

                            lbl_pono.Visible = true;
                            lbl_expo.Visible = true;
                            txt_po.Visible = true;
                            txt_po.Text = string.Empty;

                            lblCustR_SO.Visible = false;
                            lblCustR_SOEX.Visible = false;
                            txtCustR_SO.Visible = false;
                            txtCustR_SO.Text = string.Empty;
                            pb_chkbox_cusReturn.Image = Properties.Resources.uncheck_box;
                            txt_po.Focus();
                            tabcontrol1.SelectedIndex = 3;

                        }
                        else if (list_trkin[k].ToString() == "CULLET")
                        {
                            pan_truckin_po.Visible = false;
                            pan_truckin_still.Visible = false;
                            pan_truckin_cullet.Visible = true;
                            txt_still.Text = string.Empty;
                            txt_still.Enabled = false;
                            pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                            pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                            txt_cul.Focus();
                            tabcontrol1.SelectedIndex = 3;
                        }
                    }
                }
                    #endregion
                    #region OTHER
                    if (sltd_truck == "OTHER")
                    {
                    pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                    pan_info.BackgroundImage = Properties.Resources.process_enable_1;

                    txt_contain.Enabled = false;
                    txt_Taraweight.Enabled = false;
                    tabcontrol1.SelectedIndex = 6;
                }
                    
                    #endregion

               

                //still_type = "SG";
                //pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                //tabcontrol1.SelectedIndex = 3;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }         
            
        }
        private void back_deli_Click(object sender, EventArgs e)
        {
            try
            {
                tabcontrol1.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
           
        }
        private void next_deli_Click(object sender, EventArgs e)
        {
            try
            {
                check_trckindata();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

           
        }
        private void truckout()
        {
            list_trkout.Clear();
            pan_out1.Visible = false;
            pan_out2.Visible = false;
            pan_out3.Visible = false;
            pan_out4.Visible = false;
            pan_out5.Visible = false;

           pb_out_1.Image = null;
           pb_out_2.Image = null;
           pb_out_3.Image = null;
           pb_out_4.Image = null;
           pb_out_5.Image = null;
            pb_out_1.BackColor = Color.Transparent;
            pb_out_2.BackColor = Color.Transparent;
            pb_out_3.BackColor = Color.Transparent;
            pb_out_4.BackColor = Color.Transparent;
            pb_out_5.BackColor = Color.Transparent;


            lbl_out1.Text = string.Empty;
            lbl_out2.Text = string.Empty;
            lbl_out3.Text = string.Empty;
            lbl_out4.Text = string.Empty;
            lbl_out5.Text = string.Empty;

            if (sltd_truck == "INLOADER")
            {
                pb_out_2.Tag = string.Empty;
                pb_out_3.Tag = string.Empty;

                pb_out_2.Image = Properties.Resources.glass_newup;
               pb_out_3.Image = Properties.Resources.inloader_stillage_1;

                pb_out_2.BackColor = Color.White;
               pb_out_3.BackColor = Color.White;

                pb_out_2.Tag = "GLASS";
                pb_out_3.Tag = "EMPTY";

                lbl_out2.Text = lbl_glass.Text;
                lbl_out3.Text = lbl_empty.Text;
            
            }
            if (sltd_truck == "CONTAINER")
            {
                if (list_trkin.Contains("EMPTY"))
                {
                    pb_out_2.Tag = string.Empty;
                    //pb_out_3.Tag = string.Empty;

                    pb_out_2.Image = Properties.Resources.con_glass_n1;
                    //  pb_out_3.Image = Properties.Resources.glass_newup;

                    pb_out_2.BackColor = Color.White;
                    //  pb_out_3.BackColor = Color.White;

                    pb_out_2.Tag = "GLASS";
                    // pb_out_3.Tag = "GLASS";

                    lbl_out2.Text = lbl_glass.Text;
                    // lbl_out3.Text = lbl_glass.Text;
                }
                else
                {
                    pb_out_2.Tag = string.Empty;
                    pb_out_3.Tag = string.Empty;

                    pb_out_2.Image = Properties.Resources.con_glass_n1;
                    pb_out_3.Image = Properties.Resources.empty_truck;

                    pb_out_2.BackColor = Color.White;
                    pb_out_3.BackColor = Color.White;

                    pb_out_2.Tag = "GLASS";
                    pb_out_3.Tag = "EMPTY";

                    lbl_out2.Text = lbl_glass.Text;
                    lbl_out3.Text = lbl_empty.Text;
                }
                


            }
            if (sltd_truck == "EURO")
            {
                pb_out_2.Tag = string.Empty;
                pb_out_3.Tag = string.Empty;

                pb_out_2.Image = Properties.Resources.Glass_EuroNew;
                pb_out_3.Image = Properties.Resources.empty_euro;

                pb_out_2.BackColor = Color.White;
                pb_out_3.BackColor = Color.White;

                pb_out_2.Tag = "GLASS";
                pb_out_3.Tag = "EMPTY";

                lbl_out2.Text = lbl_glass.Text;
                lbl_out3.Text = lbl_empty.Text;
            }

            if (sltd_truck == "OTHER")
            {
                pb_out_2.Tag = string.Empty;
                pb_out_3.Tag = string.Empty;

                pb_out_2.Image = Properties.Resources.glass_2132;
                pb_out_3.Image = Properties.Resources.sasa;

                pb_out_2.BackColor = Color.White;
                pb_out_3.BackColor = Color.White;

                pb_out_2.Tag = "GLASS";
                pb_out_3.Tag = "STILLAGE";

                lbl_out2.Text = lbl_glass.Text;
                lbl_out3.Text = lbl_still.Text;
            }
        }
        private void back_truckout_Click(object sender, EventArgs e)
        {
            try
            {
               if ( list_trkin.Contains("EMPTY") == true)
                {
                    tabcontrol1.SelectedIndex = 2;
                }
               else
                {
                    tabcontrol1.SelectedIndex = 3;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            
        }
        private void next_truckout_Click(object sender, EventArgs e)
        {
            try
            {
                if (list_trkout.Count == 0)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2142", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MessageBox.Show("Please Select any Goods");
                    return;
                }

                if (list_trkout.Count > 0)
                {
                    for (int k = 0; k < list_trkout.Count; k++)
                    {
                        if (list_trkout[k].ToString() == "EMPTY" &&  sltd_truck != "INLOADER")
                        {
                            pan_info.BackgroundImage = Properties.Resources.process_enable_1;
                            tabcontrol1.SelectedIndex = 6;
                            txt_truckno.Focus();

                            if (sltd_truck == "CONTAINER")
                            {  
                                                            
                                txt_contain.Enabled = true;
                                txt_Taraweight.Enabled = true;

                            }
                            else
                            {
                                
                                txt_contain.Enabled = false;
                                txt_Taraweight.Enabled = false;

                            }

                        }
                        else if (list_trkout[k].ToString() == "GLASS")
                        {
                            panel_still.Visible = false;
                            groupBox2.Visible = true;
                            pan_order.BackgroundImage = Properties.Resources.process_enable_1;
                            txt_so.Focus();
                            tabcontrol1.SelectedIndex = 5;
                        }

                        else if (list_trkout[k].ToString() == "EMPTY" && sltd_truck == "INLOADER")
                        {
                            txt_contain.Enabled = false;
                            txt_Taraweight.Enabled = false;

                            pan_info.BackgroundImage = Properties.Resources.process_enable_1;
                            tabcontrol1.SelectedIndex = 6;
                            txt_truckno.Focus();
                            // panel_still.Visible = true;
                            // groupBox2.Visible = false;
                            // txt_so_Still.Text = string.Empty;
                            // txt_so_Still.Enabled = false;
                            // pb_soSGStill.Image = Properties.Resources.uncheck_box;
                            // pb_NonsoSGStill.Image = Properties.Resources.uncheck_box;
                            // pan_order.BackgroundImage = Properties.Resources.process_enable_1;

                            // tabcontrol1.SelectedIndex = 5;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
              
           
        }        
        private void pb_trkin1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (pb_trkin1.Image != null)
                {
                    chktruckinprod(pb_trkin1, pan_trkin1);
                    if(product1 == "NEW")
                    {
                        next_truckin_Click(sender, e);
                        //string s1 = pb_trkin1.Tag.ToString();
                        //if (s1 == "GLASS")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                    }                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void chktruckinprod(PictureBox pb,Panel pan)
        {
            if (pb.Image != null)
            {
                product1 = string.Empty;

                string s1 = pb.Tag.ToString();
                if (list_trkin.Contains(s1) == false)
                {
                    product1 = "NEW";
                    #region Inloader
                    if (sltd_truck == "INLOADER")
                    {
                        list_trkin.Clear();
                        list_trkin.Add(s1);
                        pan_trkin2.Visible = false;
                        pan_trkin3.Visible = false;
                        pan_trkin4.Visible = false;
                        pan.Visible = true;
                        //list_trkin.Add(s1);
                        //pan.Visible = true;

                        //if (s1 != "GLASS")
                        //{
                        //    if (list_trkin.Contains("GLASS") == true)
                        //    {
                        //        list_trkin.Remove("GLASS");
                        //        pan_trkin2.Visible = false;
                        //    }
                        //}
                    }
                    #endregion
                    #region CONTAINER
                    if (sltd_truck == "CONTAINER")
                    {
                        list_trkin.Clear();
                        list_trkin.Add(s1);
                        pan_trkin2.Visible = false;
                        pan_trkin3.Visible = false;
                        pan_trkin4.Visible = false;
                        pan.Visible = true;
                                                
                    }
                    #endregion
                    #region EURO
                    if (sltd_truck == "EURO")
                    {
                        list_trkin.Clear();
                        list_trkin.Add(s1);
                        pan_trkin2.Visible = false;
                        pan_trkin3.Visible = false;
                        pan_trkin4.Visible = false;
                        pan.Visible = true;
                    }
                    #endregion
                    #region OTHER
                    if (sltd_truck == "OTHER")
                    {
                        list_trkin.Clear();
                        list_trkin.Add(s1);
                        pan_trkin2.Visible = false;
                        pan_trkin3.Visible = false;
                        pan_trkin4.Visible = false;
                        pan.Visible = true;
                    }
                    #endregion


                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                    // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (result == DialogResult.Yes)
                    {
                        product1 = "CHG";
                        list_trkin.Remove(s1);
                        pan.Visible = false;
                        return;
                    }
                }
            }
        }
        private void pb_trkin2_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (pb_trkin2.Image != null)
                {
                    chktruckinprod(pb_trkin2, pan_trkin2);
                    if (product1 == "NEW")
                    {
                        next_truckin_Click(sender, e);
                        //string s1 = pb_trkin2.Tag.ToString();
                        //if (s1 == "GLASS")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }
        private void pb_trkin5_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (pb_trkin5.Image != null)
                {
                    chktruckinprod(pb_trkin5, pan_trkin5);
                    if (product1 == "NEW")
                    {
                        next_truckin_Click(sender, e);
                        //string s1 = pb_trkin5.Tag.ToString();
                        //if (s1 == "GLASS")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }
        private void pb_trkin3_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (pb_trkin3.Image != null)
                {
                    chktruckinprod(pb_trkin3, pan_trkin3);
                    if (product1 == "NEW")
                    {
                        next_truckin_Click(sender, e);
                        //string s1 = pb_trkin3.Tag.ToString();
                        //if (s1 == "GLASS")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
      
        }
        private void pb_trkin4_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (pb_trkin4.Image != null)
                {
                    chktruckinprod(pb_trkin4, pan_trkin4);
                    if (product1 == "NEW")
                    {
                        next_truckin_Click(sender, e);
                        //string s1 = pb_trkin4.Tag.ToString();
                        //if (s1 == "GLASS")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckin_Click(sender, e);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void back_po_Click(object sender, EventArgs e)
        {

        }
        private void back_so_Click(object sender, EventArgs e)
        {
            try
            {
                tabcontrol1.SelectedIndex = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void next_so_Click(object sender, EventArgs e)
        {
            try
            {
                check_trckoutdata();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void check_trckoutdata()
        {
            if (list_trkout.Count == 0)
            {
                _alertentity = _alertmodel.Alert_Information("LG", "A2142", Globals.GlobalLanguage);
                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
               // MessageBox.Show("Please Select any Goods");
                return ;
            }

            for (int k = 0; k < list_trkout.Count; k++)
            {
                if (list_trkout[k].ToString() == "GLASS")
                {
                    if (string.IsNullOrEmpty(txt_so.Text))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2143", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show("Please enter Sale Order Number");
                        txt_so.Text = string.Empty;
                        txt_so.Focus();
                        
                        return ;
                        
                    }
                    DataSet dslk = new DataSet();
                    
                    dslk = _model.val_product("SO_VAL", txt_so.Text.ToString(), Globals.GlobalSite, Globals.GlobalLanguage,Globals.GlobalUsername, txt_still.Text);
                    if (dslk.Tables.Count > 0)
                    {
                        if (dslk.Tables[0].Rows.Count > 0)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2144", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // MessageBox.Show("Alredy exists this Sale Order number");
                            txt_so.Text = string.Empty;
                            txt_so.Focus();
                            return ;
                        }

                        if (dslk.Tables[1].Rows.Count == 0)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2145", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //MessageBox.Show("Invalid Sale Order number");
                            txt_so.Text = string.Empty;
                            txt_so.Focus();
                            return ;
                        }

                        if (dslk.Tables[2].Rows.Count > 0)
                        {
                            if (dslk.Tables[3].Rows.Count > 0)
                            {
                                int paramhours = Convert.ToInt32(dslk.Tables[3].Rows[0][0].ToString());
                                if (dslk.Tables[3].Rows.Count > 0 && dslk.Tables[4].Rows.Count > 0)
                                {
                                    int orderhours = Convert.ToInt32(dslk.Tables[4].Rows[0][0].ToString());
                                    if (orderhours >= paramhours)
                                    {
                                        _alertentity = _alertmodel.Alert_Information("LG", "A2146", Globals.GlobalLanguage);
                                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //MessageBox.Show("Within 4hrs");
                                        return ;
                                    }
                                }
                            }
                        }                   

                    }
                }
            }


            if (sltd_truck == "CONTAINER")
            {
               // lbl_contain.Visible = true;
              //  lbl_excontain.Visible = true;
                txt_contain.Enabled = true;
                txt_Taraweight.Enabled = true;





            }
            else
            {
                //lbl_contain.Visible = false;
               // lbl_excontain.Visible = false;
                            txt_Taraweight.Enabled = false;
                txt_contain.Enabled = false;
            }

            tabcontrol1.SelectedIndex = 6;
            txt_truckno.Focus();
            pan_info.BackgroundImage = Properties.Resources.process_enable_1;
        }
        private void pb_out_1_Click(object sender, EventArgs e)
        {
            try
            {
                if (pb_out_1.Image != null)
                {

                    string s1 = pb_out_1.Tag.ToString();
                    if (list_trkout.Contains(s1) == false)
                    {
                        list_trkout.Add(s1);
                        pan_out1.Visible = true;
                        next_truckout_Click(sender, e);

                        //if (s1 == "GLASS")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                        //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            list_trkout.Remove(s1);
                            pan_out1.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        private void pb_out_2_Click(object sender, EventArgs e)
        {
            try
            {
                if (pb_out_2.Image != null)
                {
                    string s1 = pb_out_2.Tag.ToString();
                    if (list_trkout.Contains(s1) == false)
                    {
                        list_trkout.Add(s1);
                        pan_out2.Visible = true;

                        if (s1 == "GLASS")
                        {
                            if (list_trkout.Contains("EMPTY"))
                            {
                                list_trkout.Remove("EMPTY");
                                pan_out3.Visible = false;
                            } 
                            //next_truckout_Click(sender, e);

                        }
                        else if (s1 == "EMPTY")
                        {
                            if (list_trkout.Contains("GLASS"))
                            {
                                list_trkout.Remove("GLASS");
                                pan_out2.Visible = false;
                            }
                            //next_truckout_Click(sender, e);
                        }
                        next_truckout_Click(sender, e);
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                       // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            list_trkout.Remove(s1);
                            pan_out2.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void pb_out_3_Click(object sender, EventArgs e)
        {
            try
            {
                if (pb_out_3.Image != null)
                {
                    string s1 = pb_out_3.Tag.ToString();
                    if (list_trkout.Contains(s1) == false)
                    {
                        list_trkout.Add(s1);
                        pan_out3.Visible = true;

                        if (s1 == "GLASS")
                        {
                            if (list_trkout.Contains("EMPTY"))
                            {
                                list_trkout.Remove("EMPTY");
                                pan_out3.Visible = false;
                            }
                            //next_truckout_Click(sender, e);

                        }
                        else if (s1 == "EMPTY")
                        {
                            if (list_trkout.Contains("GLASS"))
                            {
                                list_trkout.Remove("GLASS");
                                pan_out2.Visible = false;
                            }
                            //next_truckout_Click(sender, e);

                        }
                        next_truckout_Click(sender, e);

                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                       // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            list_trkout.Remove(s1);
                            pan_out3.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          

        }
        private void pb_out_4_Click(object sender, EventArgs e)
        {
            try
            {
                if (pb_out_4.Image != null)
                {
                    string s1 = pb_out_4.Tag.ToString();
                    if (list_trkout.Contains(s1) == false)
                    {
                        list_trkout.Add(s1);
                        pan_out4.Visible = true;

                        next_truckout_Click(sender, e);

                        //if (s1 == "GLASS")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                       // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            list_trkout.Remove(s1);
                            pan_out4.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        private void pb_out_5_Click(object sender, EventArgs e)
        {
            try
            {
                if (pb_out_5.Image != null)
                {
                    string s1 = pb_out_5.Tag.ToString();
                    if (list_trkout.Contains(s1) == false)
                    {
                        list_trkout.Add(s1);
                        pan_out5.Visible = true;

                        next_truckout_Click(sender, e);

                        //if (s1 == "GLASS")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                        //else if (s1 == "EMPTY")
                        //{
                        //    next_truckout_Click(sender, e);

                        //}
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2160", Globals.GlobalLanguage);
                       // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult result = MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                        if (result == DialogResult.Yes)
                        {
                            list_trkout.Remove(s1);
                            pan_out5.Visible = false;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           

        }
        private void back_trkinfo1_Click(object sender, EventArgs e)
        {
            try
            {
                if (sltd_truck == "OTHER")
                {
                    tabcontrol1.SelectedIndex = 1;
                    return;
                }
                if(string.IsNullOrEmpty(txt_so.Text))
                {
                    tabcontrol1.SelectedIndex = 4;
                }
                else
                {
                    txt_so.Focus();
                    tabcontrol1.SelectedIndex = 5;
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void next_trkinfo1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txt_truckno.Text))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2147", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MessageBox.Show("Please enter Truck  Number");
                    txt_truckno.Focus();
                    return;
                }

                if (sltd_truck == "CONTAINER")
                {
                    if (string.IsNullOrEmpty(txt_contain.Text))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2148", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // MessageBox.Show("Please enter Container  Number");
                        txt_contain.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txt_Taraweight.Text))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2395", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // MessageBox.Show("Please enter Container  Number");
                        txt_Taraweight.Focus();
                        return;
                    }
                }

                DataSet ds_con = _model.val_product("GETCOUNTRY", lang_code, "", "", "", "");
                if (ds_con.Tables.Count > 0)
                {
                    if (ds_con.Tables[0].Rows.Count > 0)
                    {
                        cmb_country.Text = ds_con.Tables[0].Rows[0]["CODE"].ToString();
                        countrycode();
                    }
                }
                tabcontrol1.SelectedIndex = 7;
                txt_name.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }

        private void txt_po_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    check_trckindata();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void check_trckindata()
        {
            int x;

            x = list_trkin.Count;
            for (int k = 0; k < list_trkin.Count; k++)
            {
                if (list_trkin[k].ToString() == "GLASS")
                {
                    if (CustReturn == false)
                    {
                        if (string.IsNullOrEmpty(txt_po.Text))
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2149", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // MessageBox.Show("Please enter Purchase Order Number");
                            txt_po.Text = string.Empty;
                            txt_po.Focus();
                            return;
                        }
                        //  public DataSet val_product(string flag, string sono, string plant, string lang)

                        DataSet ds_t = new DataSet();
                        ds_t = _model.val_product("PO_VAL", txt_po.Text.ToString(), "", "", "", "");

                        if (ds_t.Tables.Count > 0)

                        {
                            if (ds_t.Tables[0].Rows.Count > 0)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A2150", Globals.GlobalLanguage);
                                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txt_po.Text = string.Empty;
                                txt_po.Focus();
                                //  MessageBox.Show("Alredy exists this purchase Order number");
                                return;
                            }

                            if (ds_t.Tables[1].Rows.Count == 0)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A2151", Globals.GlobalLanguage);
                                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txt_po.Text = string.Empty;
                                txt_po.Focus();
                                //   MessageBox.Show("Invalid  purchase Order number");
                                return;
                            }
                        }

                        if (still_type == "SG")
                        {
                            DataSet dtparam = new DataSet();

                            dtparam = _model.val_product("PARMPKG", txt_po.Text.Trim(), Globals.GlobalSite, Globals.GlobalLanguage, "", "");
                            if (dtparam.Tables.Count > 0)
                            {
                                if (dtparam.Tables[0].Rows.Count > 0)
                                {
                                    if (dtparam.Tables[0].Rows[0][0].ToString() == "Y")
                                    {
                                        if (dtparam.Tables[1].Rows.Count > 0)
                                        {
                                            if (dtparam.Tables[1].Rows[0][0].ToString() == "002")
                                            {
                                                if (string.IsNullOrEmpty(txt_still.Text.Trim()))
                                                {
                                                    _alertentity = _alertmodel.Alert_Information("LG", "A2152", Globals.GlobalLanguage);
                                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                    // MessageBox.Show("Enter Stillage");
                                                    txt_still.Focus();
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtCustR_SO.Text))
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2292", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // MessageBox.Show("Please enter sale Order Number");
                            txtCustR_SO.Text = string.Empty;
                            txtCustR_SO.Focus();
                            return;
                        }
                        else
                        {

                            DataSet ds_t = new DataSet();
                            ds_t = _model.val_product("CUSTSO", txtCustR_SO.Text.Trim().ToString(), "", "", "", "");

                            if (ds_t.Tables.Count > 0)

                            {
                                if (ds_t.Tables[0].Rows.Count > 0)
                                {
                                    _alertentity = _alertmodel.Alert_Information("LG", "A2293", Globals.GlobalLanguage);
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtCustR_SO.Text = string.Empty;
                                    txtCustR_SO.Focus();
                                    //  MessageBox.Show("Alredy exists Sale Order number");
                                    return;
                                }

                                if (ds_t.Tables[1].Rows.Count == 0)
                                {
                                    _alertentity = _alertmodel.Alert_Information("LG", "A2294", Globals.GlobalLanguage);
                                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtCustR_SO.Text = string.Empty;
                                    txtCustR_SO.Focus();
                                    //   MessageBox.Show("Invalid  purchase Order number");
                                    return;
                                }
                                else
                                {
                                    if (ds_t.Tables[1].Rows[0][1].ToString() != "CR")
                                    {
                                        _alertentity = _alertmodel.Alert_Information("LG", "A2295", Globals.GlobalLanguage);
                                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtCustR_SO.Text = string.Empty;
                                        txtCustR_SO.Focus();
                                        //   MessageBox.Show("Order number not CR status");
                                        return;
                                    }
                                }


                            }

                        }
                        //  public DataSet val_product(string flag, string sono, string plant, string lang)

                    }
                    
                }
                else if (list_trkin[k].ToString() == "STILLAGE")
                {
                    if(still_type == "")
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2443", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);                        
                        return;
                    }
                    if (still_type == "SG")
                    {
                        if (string.IsNullOrEmpty(txt_still.Text.Trim()))
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2152", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // MessageBox.Show("Enter Stillage");

                            txt_still.Focus();
                            return;
                        }
                        DataSet dt1 = new DataSet();
                        dt1 = _model.val_product("STILL", txt_still.Text.Trim(), Globals.GlobalSite, Globals.GlobalLanguage, "", "");
                        if (dt1.Tables[0].Rows.Count == 0)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A2153", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    MessageBox.Show("Stillage is Invalid");
                            txt_still.Text = string.Empty;
                            txt_still.Focus();
                            return;
                        }
                    }
                }
                else if (list_trkin[k].ToString() == "CULLET")
                {
                    if (string.IsNullOrEmpty(txtNoOfBags.Text.Trim()))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2296", Globals.GlobalLanguage);
                         MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK,MessageBoxIcon.Information);
                        // MessageBox.Show("Enter Cullet  Weight");
                        txtNoOfBags.Focus();
                        return;
                    }
                    if ((Convert.ToInt32(txtNoOfBags.Text)) <= 0)
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2297", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // MessageBox.Show("Enter Cullet  Weight");
                        txtNoOfBags.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtSupp.Text.Trim()))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A2298", Globals.GlobalLanguage);
                        MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // MessageBox.Show("Enter Cullet  Weight");
                        txtSupp.Focus();
                        return;
                    }
                    else
                    {
                        DataRow[] dr_data = ds_code.Tables[1].Select("DESC2 = '" + txtSupp.Text.Replace("*", "[*]") + "'");
                        if (dr_data.Length == 0)
                        {
                            txtSupp.Text = string.Empty;
                            _alertentity = _alertmodel.Alert_Information("LG", "A2298", Globals.GlobalLanguage);
                            MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // MessageBox.Show("Enter Cullet  Weight");
                            txtSupp.Focus();
                            return;
                        }
                        if (!string.IsNullOrEmpty(txt_still.Text.Trim()))
                        {
                            DataSet dt1 = new DataSet();
                            dt1 = _model.val_product("STILL", txt_still.Text.Trim(), Globals.GlobalSite, Globals.GlobalLanguage, "", "");
                            if (dt1.Tables[0].Rows.Count == 0)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A2153", Globals.GlobalLanguage);
                                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                // MessageBox.Show("Stillage is Invalid");
                                txt_still.Text = string.Empty;
                                txt_still.Focus();
                                return;
                            }
                        }
                    }
                }
            }

            if (still_type == "SG")
            {
                if (string.IsNullOrEmpty(txt_still.Text.Trim()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2152", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // MessageBox.Show("Enter Stillage");

                    txt_still.Focus();
                    return;
                }
                DataSet dt1 = new DataSet();
                dt1 = _model.val_product("STILL", txt_still.Text.Trim(), Globals.GlobalSite, Globals.GlobalLanguage, "", "");
                if (dt1.Tables[0].Rows.Count == 0)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2153", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    MessageBox.Show("Stillage is Invalid");
                    txt_still.Text = string.Empty;
                    txt_still.Focus();
                    return;
                }
            }
            tabcontrol1.SelectedIndex = 4;
            pan_order.BackgroundImage = Properties.Resources.process_enable_1;
            truckout();



        }
       
        private void pb_chk_sgstill_Click(object sender, EventArgs e)
        {
            try
            {
                still_type = "SG";
                txt_still.Text = string.Empty;
                pb_chk_sgstill.Image = Properties.Resources.check_box;
                pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                txt_still.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
               
        }

        private void pb_chk_nonsgstill_Click(object sender, EventArgs e)
        {
            try
            {
                still_type = "NONSG";
                pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                pb_chk_nonsgstill.Image = Properties.Resources.check_box;
                txt_still.Text = string.Empty;
                txt_still.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           

        }

        private void pb_soSGStill_Click(object sender, EventArgs e)
        {
            try
            {
                still_type = "SG";
                pb_soSGStill.Image = Properties.Resources.check_box;
                pb_NonsoSGStill.Image = Properties.Resources.uncheck_box;
                txt_so_Still.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pb_NonsoSGStill_Click(object sender, EventArgs e)
        {
            try
            {
                still_type = "NONSG";
                pb_soSGStill.Image = Properties.Resources.uncheck_box;
                pb_NonsoSGStill.Image = Properties.Resources.check_box;
                txt_so_Still.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void back_trkinfo2_Click(object sender, EventArgs e)
        {
            try
            {
                tabcontrol1.SelectedIndex = 6;
                txt_truckno.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

      

        private void txt_mobno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txt_country_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar != 22)
            //{
            //    if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            //    {
            //        e.Handled = true;
            //    }
            //}
        }

        private void txt_so_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
        }

        private void txt_po_KeyPress_1(object sender, KeyPressEventArgs e)
        {
        //    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        //(e.KeyChar == '.'))
        //    {
        //        e.Handled = true;
        //    }

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
            // txt_po.SelectionFont = new Font("Open Sans", 16, FontStyle.Regular);

        }

     

        private void txt_so_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                next_so_Click(sender, e);
            }
        }

        private void txt_truckno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                txt_trilerno.Focus();
            }
                
        }

        private void txt_trilerno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (sltd_truck != "CONTAINER")
                {
                    next_trkinfo1_Click(sender, e);
                }
                else
                {
                    txt_contain.Focus();
                }

            }
          
        }

        private void txt_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                cmb_country.Focus();
            }
              
        }

        private void txt_mobno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                next_trkinfo2_Click(sender, e);
            }
              
        }

        private void next_trkinfo2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txt_name.Text))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2155", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MessageBox.Show("Please Youe Name");
                    txt_name.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txt_mobno.Text))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2156", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //  MessageBox.Show("Enter Mobile Number");
                    txt_mobno.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txt_countrycodeDis.Text))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A2157", Globals.GlobalLanguage);
                    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // MessageBox.Show("Enter Country Code");
                    txt_countrycodeDis.Focus();
                    return;
                }
               txt_mobno.Text =  txt_mobno.Text.TrimStart('0');

                DataSet ds_code = new DataSet();

                //ds_code = _model.val_product("GETCODE", txt_country.Text.Trim(), "", "", "", "");

                //if (ds_code.Tables[0].Rows.Count == 0)
                //{
                //    _alertentity = _alertmodel.Alert_Information("LG", "A2159", Globals.GlobalLanguage);
                //    MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    // MessageBox.Show("Invalid Country Code");
                //    txt_country.Focus();
                //    return;
                //}

                save();

                lbl_com_order.Text = string.Empty;

                lbl_com_order.Text = lbl_invsble_com1.Text +"  "+ txt_name.Text.ToUpper() + ". " + lbl_invsble_com2.Text;
                tabcontrol1.SelectedIndex = 8;
                pan_comp.BackgroundImage = Properties.Resources.process_enable_1;
                if(refresh_time>0)
                {
                    complete();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            
        }

        private void txt_po_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                //next_deli_Click(sender, e);
            }
               
        }

        private void txt_cul_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                next_deli_Click(sender, e);
            }
        }

        private void txt_cul_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txt_still_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                next_deli_Click(sender, e);
            }
        }

        private void txt_contain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                next_trkinfo1_Click(sender, e);
            }
                
        }

        private void pb_home_Click(object sender, EventArgs e)
        {
            try
            {
                pan_truck.BackgroundImage = Properties.Resources.process_dis_1;
                pan_del.BackgroundImage = Properties.Resources.process_dis_1;
                pan_order.BackgroundImage = Properties.Resources.process_dis_1;
                pan_info.BackgroundImage = Properties.Resources.process_dis_1;
                pan_comp.BackgroundImage = Properties.Resources.process_dis_1;
                clearform();
                tabcontrol1.SelectedIndex = 0;
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pb_clear_Click(object sender, EventArgs e)
        {
            try
            {
                if(tabcontrol1.SelectedIndex == 3)//po
                {
                    txt_po.Text = string.Empty;
                    txt_cul.Text = string.Empty;
                    txt_still.Text = string.Empty;
                    pb_chk_nonsgstill.Image = Properties.Resources.uncheck_box;
                    pb_chk_sgstill.Image = Properties.Resources.uncheck_box;
                    still_type = string.Empty;
                    txtCustR_SO.Text = string.Empty;
                    txtNoOfBags.Text = string.Empty;
                    txtSupp.Text = string.Empty;
                }
                else if (tabcontrol1.SelectedIndex == 5)//so
                {
                    txt_so.Text = string.Empty;
                }
                else if (tabcontrol1.SelectedIndex == 6)//info1
                {
                    txt_truckno.Text = string.Empty;
                    txt_trilerno.Text = string.Empty;
                    txt_contain.Text = string.Empty;
                }
                else if (tabcontrol1.SelectedIndex == 7)//info2
                {
                    txt_name.Text = string.Empty;
                     cmb_country.SelectedIndex = -1; ;
                    txt_countrycodeDis.Text = string.Empty;
                    txt_mobno.Text = string.Empty;
                    pbflg1.Image = null;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void save()
        {
            try
            {
                rsno = 0;
                lineitem = 0;
                DataSet ds_save = new DataSet();
                ds_save.Tables.Clear();

                if(list_trkin.Contains("EMPTY") && list_trkout.Contains("EMPTY"))
                {
                    ds_save.Tables.Clear();

                    refresh_time = 10;
                    //ds_save = _model.save_product("EMPTY", "", "", txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                    //    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OL", "", "", txt_country.Text.Trim(), sltd_lang,sltd_truck,txt_trilerno.Text.ToUpper());
                }
                #region Inloder
                if (sltd_truck == "INLOADER")
                {
                    for (int k = 0; k < list_trkin.Count; k++)
                    {
                        if (list_trkin[k].ToString() == "GLASS")
                        {
                            if (CustReturn == false)
                            {
                                lineitem = lineitem + 1;
                                ds_save.Tables.Clear();
                                ds_save = _model.save_product(txt_po.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                            else
                            {
                                lineitem = lineitem + 1;
                                ds_save.Tables.Clear();
                                ds_save = _model.save_product(txtCustR_SO.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                            

                        }
                        else if (list_trkin[k].ToString() == "CULLET")
                        {
                            ds_save.Tables.Clear();
                            lineitem = lineitem + 1;
                            ds_save = _model.save_product("Cullet", "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno, lineitem,Convert.ToInt32(txtNoOfBags.Text),txtSupp.Text,"");
                            if (ds_save.Tables.Count > 0)
                            {
                                if (ds_save.Tables[2].Rows.Count > 0)
                                {
                                    rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                }
                            }
                        }
                        else if (list_trkin[k].ToString() == "STILLAGE")
                        {
                            if (list_trkout.Contains("GLASS"))
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txt_so.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }

                            }
                            else
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product("STILLAGE", "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, "", "OL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }

                            }
                        }
                    }

                    for (int k = 0; k < list_trkout.Count; k++)
                    {
                        if (list_trkout[k].ToString() == "GLASS")
                        {
                            if (!list_trkin.Contains("STILLAGE"))
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txt_so.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }


                            }
                           

                            //if (list_trkin.Contains("STILLAGE") == true && list_trkin.Contains("CULLET") == false)
                            //{
                            //    ds_save = _model.save_product(txt_so.Text.Trim(), txt_still.Text.Trim().ToUpper(), txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                            //        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_country.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper());
                            //} 
                            //else
                            //{

                            //    ds_save = _model.save_product(txt_so.Text.Trim(), "", "", txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                            //        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_country.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper());
                            //}

                        }
                        else if (list_trkout[k].ToString() == "EMPTY")
                        {
                            //ds_save.Tables.Clear();

                           // ds_save = _model.save_product("STILLAGE", "", txt_so_Still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                 //   , Globals.GlobalUsername, "", Globals.GlobalSite, "", "OL", "", "", cmb_country.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper());

                        }

                    }

                    //if (list_trkin.Contains("STILLAGE") == true && list_trkin.Contains("CULLET") == false && list_trkin.Contains("GLASS") == false && list_trkout.Contains("EMPTY") == false)
                    //{

                    //    ds_save.Tables.Clear();

                    //    ds_save = _model.save_product("STILLAGE", "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                    //        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GUL", "", "", txt_country.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper());
                    //}
                }
                #endregion
                #region CONTAINER
               else if (sltd_truck == "CONTAINER")
                {
                    for (int k = 0; k < list_trkin.Count; k++)
                    {
                        if (list_trkin[k].ToString() == "GLASS")
                        {
                            if (CustReturn == false)
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txt_po.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"",Convert.ToString(txt_Taraweight.Text));
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                            else
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txtCustR_SO.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"", Convert.ToString(txt_Taraweight.Text));
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                            
                        }



                        else if (list_trkin[k].ToString() == "CULLET")
                        {
                            ds_save.Tables.Clear();
                            lineitem = lineitem + 1;
                            ds_save = _model.save_product("Cullet", "", "", txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,Convert.ToInt32(txtNoOfBags.Text),txtSupp.Text, Convert.ToString(txt_Taraweight.Text));
                            if (ds_save.Tables.Count > 0)
                            {
                                if (ds_save.Tables[2].Rows.Count > 0)
                                {
                                    rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                }
                            }
                        }
                    }

                    for (int k = 0; k < list_trkout.Count; k++)
                    {
                        if (list_trkout[k].ToString() == "GLASS")
                        {
                            ds_save.Tables.Clear();
                            lineitem = lineitem + 1;
                            ds_save = _model.save_product(txt_so.Text.Trim(), txt_still.Text.Trim().ToUpper(), txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"", Convert.ToString(txt_Taraweight.Text));
                            if (ds_save.Tables.Count > 0)
                            {
                                if (ds_save.Tables[2].Rows.Count > 0)
                                {
                                    rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                }
                            }
                        }

                    }
                    
                }
                #endregion
                #region EURO
               else if (sltd_truck == "EURO")
                {
                    for (int k = 0; k < list_trkin.Count; k++)
                    {
                        if (list_trkin[k].ToString() == "GLASS")
                        {
                            if(CustReturn == false)
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txt_po.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                            else
                            {
                                ds_save.Tables.Clear();
                                lineitem = lineitem + 1;
                                ds_save = _model.save_product(txtCustR_SO.Text.Trim(), "", txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                    , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OUL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(), rsno, lineitem,0,"","");
                                if (ds_save.Tables.Count > 0)
                                {
                                    if (ds_save.Tables[2].Rows.Count > 0)
                                    {
                                        rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                    }
                                }
                            }
                           
                        }



                        else if (list_trkin[k].ToString() == "CULLET")
                        {
                            ds_save.Tables.Clear();
                            lineitem = lineitem + 1;
                            ds_save = _model.save_product("Cullet", "", "", txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem, Convert.ToInt32(txtNoOfBags.Text),txtSupp.Text,"");
                            if (ds_save.Tables.Count > 0)
                            {
                                if (ds_save.Tables[2].Rows.Count > 0)
                                {
                                    rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                }
                            }
                        }
                    }

                    for (int k = 0; k < list_trkout.Count; k++)
                    {
                        if (list_trkout[k].ToString() == "GLASS")
                        {
                            ds_save.Tables.Clear();
                            lineitem = lineitem + 1;
                            ds_save = _model.save_product(txt_so.Text.Trim(), txt_still.Text.Trim().ToUpper(), txt_still.Text.Trim().ToUpper(), txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                                , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "GL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"","");
                            if (ds_save.Tables.Count > 0)
                            {
                                if (ds_save.Tables[2].Rows.Count > 0)
                                {
                                    rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                                }
                            }
                        }

                    }

                }
                #endregion
                #region Other
              else  if (sltd_truck == "OTHER")
                {
                    ds_save.Tables.Clear();
                    lineitem = lineitem + 1;
                    ds_save = _model.save_product("Other", "", "", txt_truckno.Text.ToUpper(), txt_contain.Text.ToUpper(), txt_mobno.Text.Trim(), txt_name.Text.ToUpper()
                        , Globals.GlobalUsername, "", Globals.GlobalSite, txt_cul.Text, "OL", "", "", txt_countrycodeDis.Text.Trim(), sltd_lang, sltd_truck, txt_trilerno.Text.ToUpper(),rsno,lineitem,0,"","");
                    if (ds_save.Tables.Count > 0)
                    {
                        if (ds_save.Tables[2].Rows.Count > 0)
                        {
                            rsno = Convert.ToInt32(ds_save.Tables[2].Rows[0][0].ToString());
                        }
                    }
                }
                #endregion

                if (ds_save.Tables.Count > 0)
                {
                    if (ds_save.Tables[0].Rows.Count > 0)
                    {
                        lbl_com_confirm.Visible = true;
                    }
                    else
                    {
                        lbl_com_confirm.Visible = false;
                    }

                    if (ds_save.Tables[1].Rows.Count > 0)
                    {
                        refresh_time = Convert.ToInt32( ds_save.Tables[1].Rows[0][0].ToString());
                    }

                }

                else
                {

                   _alertentity = _alertmodel.Alert_Information("LG", "A2224", Globals.GlobalLanguage);
                   MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // MessageBox.Show("Registration Not Completed");
                    
                    return;
                }

            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void txt_country_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                cmb_country.DroppedDown = false;
                if (e.KeyData == Keys.Enter)
                {
                    cmb_country.Leave -= new EventHandler(cmb_country_Leave);
                    countrycode();
                    cmb_country.Leave += new EventHandler(cmb_country_Leave);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void cmb_country_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb_country.SelectedIndex != -1)
                {
                   
                    countrycode();
                    return;
                    
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void countrycode() 
        {
            txt_countrycodeDis.Text = string.Empty;
            string coder;
            string code = cmb_country.Text;

            if (code.Contains("("))
            {
                string[] splitvn = code.Split(new string[] { "(" }, StringSplitOptions.None);
                coder =  splitvn[1].ToString();
                coder = coder.Substring(0, coder.Length - 1);
            }
            else
            {
                coder = code.Trim();
            }
          
           
           
            DataSet dt2q1 = new DataSet();
            DataTable dt1 = new DataTable();
            dt2q1 = _model.val_product("GETCODE", coder, "", "", "", "");
            dt1 = dt2q1.Tables[0];
            if (dt1.Rows.Count > 0)
            {
                Graphics g = Graphics.FromHwnd(this.Handle);
                cmb_country.Text = dt1.Rows[0]["DESC"].ToString();
                txt_countrycodeDis.Text = dt1.Rows[0]["CODE"].ToString();
                int x = Convert.ToInt32(dt1.Rows[0]["SLNO"].ToString());
                pbflg1.Image = imageList_country.Images[x];
                // pbflg1.Size = new Size(Convert.ToInt32(dt.Rows[0]["WIDTH"].ToString()), Convert.ToInt32(dt.Rows[0]["HEIGHT"].ToString()));
                txt_mobno.Focus();
            }
            else
            {
                pbflg1.Image = null;
                _alertentity = _alertmodel.Alert_Information("LG", "A2244", Globals.GlobalLanguage);
                MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           // cmb_country.SelectedIndexChanged += new EventHandler(cmb_country_SelectedIndexChanged);
        }
        private void panel5_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            clearform();
            tabcontrol1.SelectedIndex = 0;
        }
        private void cmb_country_Leave(object sender, EventArgs e)
        {
            if (cmb_country.Text != "")
            {
                countrycode();
            }
        }

       
        private void pb_chkbox_cusReturn_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustReturn == false)
                {
                    pb_chkbox_cusReturn.Image = Properties.Resources.check_box;
                    CustReturn = true;
                    lbl_pono.Visible = false;
                    lbl_expo.Visible = false;
                    txt_po.Visible = false;
                    txt_po.Text = string.Empty;

                    lblCustR_SO.Visible = true;
                    lblCustR_SOEX.Visible = true;
                    txtCustR_SO.Visible = true;
                    txtCustR_SO.Text = string.Empty;
                    
                    txtCustR_SO.Focus();

                }
                else
                {
                    pb_chkbox_cusReturn.Image = Properties.Resources.uncheck_box;
                    CustReturn = false;

                    lbl_pono.Visible = true;
                    lbl_expo.Visible = true;
                    txt_po.Visible = true;
                    txt_po.Text = string.Empty;

                    lblCustR_SO.Visible = false;
                    lblCustR_SOEX.Visible = false;
                    txtCustR_SO.Visible = false;
                    txtCustR_SO.Text = string.Empty;

                    txt_po.Focus();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtNoOfBags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void pb_getCountryCode_Click(object sender, EventArgs e)
        {
            FrmGetCountryCode frm = new FrmGetCountryCode(dt_mstrcountrycode);
          DialogResult drslt =  frm.ShowDialog();

            if(drslt == DialogResult.OK)
            {
                Graphics g = Graphics.FromHwnd(this.Handle);

                txt_countrycodeDis.Text = frm.countrycode;
                int x = frm.imgcode;
                pbflg1.Image = imageList_country.Images[x];
                // pbflg1.Size = new Size(Convert.ToInt32(dt.Rows[0]["WIDTH"].ToString()), Convert.ToInt32(dt.Rows[0]["HEIGHT"].ToString()));
                txt_mobno.Focus();
            }
            
        }

        private void txtSupp_TextChanged(object sender, EventArgs e)
        {
            var source = new List<string>();
            listBox1.DataSource = null;
            if (!string.IsNullOrEmpty(txtSupp.Text))
            {
                DataRow[] dr_data = ds_code.Tables[1].Select("DESC2 like '%" +  txtSupp.Text.Replace("*","[*]" )+ "%'");
               
                foreach (DataRow dr in dr_data)
                {
                    source.Add(dr["DESC"].ToString());
                }
                listBox1.DataSource = source;
            }
            if (source.Count > 0)
            {
                listBox1.Visible = true;
            }
            else
            {
                listBox1.Visible = false;
            }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                txtSupp.TextChanged -= txtSupp_TextChanged;
                txtSupp.Text = listBox1.SelectedValue.ToString();  
                txtSupp.TextChanged += txtSupp_TextChanged;
                listBox1.Visible = false;
            }
        }

        private void txtSupp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                MoveSelectionInListBox(listBox1.SelectedIndex - 1);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                MoveSelectionInListBox(listBox1.SelectedIndex + 1);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.PageUp)
            {
                MoveSelectionInListBox(listBox1.SelectedIndex - 5);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                MoveSelectionInListBox(listBox1.SelectedIndex + 5);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (listBox1.SelectedIndex >= 0)
                {
                    listBox1.Visible = false;
                    txtSupp.Text = listBox1.SelectedValue.ToString();
                    listBox1.Visible = false;
                }
                e.Handled = true;
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        private void MoveSelectionInListBox(int Index)
        {
            // beginning of list
            if (Index <= -1)
            { listBox1.SelectedIndex = 0; }
            else
                // end of liste
                if (Index > (listBox1.Items.Count - 1))
            {
                listBox1.SelectedIndex = (listBox1.Items.Count - 1);
            }
            else
            // somewhere in the middle
            { listBox1.SelectedIndex = Index; }
        }

        private void txtSupp_Leave(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtSupp.Text))
            {
                listBox1.Visible = false;
            }
            
           
            
        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                txtSupp.TextChanged -= txtSupp_TextChanged;
                txtSupp.Text = listBox1.SelectedValue.ToString();
                txtSupp.TextChanged += txtSupp_TextChanged;
                listBox1.Visible = false;
            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                txtSupp.TextChanged -= txtSupp_TextChanged;
                txtSupp.Text = listBox1.SelectedValue.ToString();
                txtSupp.TextChanged += txtSupp_TextChanged;
                listBox1.Visible = false;
            }
        }

        private void tabcontrol1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
            {
                e.Handled = true;
            }
        }

        private void txt_Taraweight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void cmb_country_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '(')
            {
                e.Handled = true;
            }
        }

      

        private void complete()
        {
            time_cnt = 0;
            time_cnt = refresh_time;
            timer1.Interval = 1000;
            timer1.Start();
            
            
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                time_cnt--;
                label2.Text = " "+time_cnt.ToString() + " "+ label13.Text;
                if (time_cnt == 0)
                {
                    timer1.Stop();
                    clearform();

                    tabcontrol1.SelectedIndex = 0;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }

        private void tooltip()
        {
            ToolTip a = new ToolTip();
            a.SetToolTip(pb_home, "Home");
            a.SetToolTip(pictureBox3, "Home");
            a.SetToolTip(pictureBox4, "Home");
            a.SetToolTip(pb_clear, "Clear All");
            a.SetToolTip(pictureBox5, "Home");
            a.SetToolTip(pictureBox6, "Home");
            a.SetToolTip(pictureBox2, "Clear All");
            a.SetToolTip(pictureBox7, "Home");
            a.SetToolTip(pictureBox13, "Clear All");
            a.SetToolTip(pictureBox11, "Home");
            a.SetToolTip(pictureBox14, "Clear All");

        }


    }
}

